<?php


return [
    'WEEKDAYS' => [
        'SUNDAY' => 7,
        'MONDAY' => 1,
        'TUESDAY' => 2,
        'WEDNESDAY' => 3,
        'THURSDAY' => 4,
        'FRIDAY' => 5,
        'SATURDAY' => 6
    ],
    'USERROLE' => [
        'ADMIN' => 1,
    ],
    'STORAGE' => [
       1 => 'profile-images' ,
       2 => 'documents' ,
    ],

    'PAGINATION_LIMIT' =>10,

    'IMAGE_FOLDER' => [
        1 => 'profile_image' ,
        2 => 'attactment' ,
    ],
    'SALES_EMAIL_ADDRESS'=> 'sales@homejab.com',
	'DEFAULT_TO_ADDRESS_EMAIL' => 'social.oequal@gmail.com',
   /* 'JOB_CREATE_RECEIVER_EMAILID' => 'shrikant.r@flatworldsolutions.com',
    'DEFAULT_FROM_EMAIL_ADDRESS' => 'noreply@flatworldsolutions.com',
    'DEFAULT_JOB_CONFIRM_RECIVER_EMAILID' => 'shrikant.r@flatworldsolutions.com'*/
    'DEFAULT_FROM_EMAIL_ADDRESS'   => 'developersatblackhole@gmail.com',
    'JOB_CREATE_RECEIVER_EMAILID' => 'social.oequal@gmail.com',
    'JOB_CONFIRM_RECIVER_EMAILID' => 'social.oequal@gmail.com',
    'CRON_EMAILID' => 'social.oequal@gmail.com',
    'SENDER_KEY' => 'AIzaSyCAtCJDCDNzy8qR7r5Z6JF5oXzVrRCQT1g',
    'SENDER_ID' => '738312034058',
    'ProfilePicUrl'=>'http://buildapi.homejab.com/HomwjabScheduling.Web/ProfilePicture/',
    'HOME_JOB_ADMIN_EMAIL_ADDRESS' => 'social.oequal@gmail.com',

    'StatusReport'  => 'StatusReport',
    'PayPerUser'    => 'PayPerUser',
    'JobForNextDay' => 'JobForNextDay',
    'JobsForMonth'  => 'JobsForMonth'
];