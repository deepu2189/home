<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Homejab'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', true),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'api_url' => env('APP_STATIC_URL'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => env('APP_TIMEZONE', 'UTC'),

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    /*
      |--------------------------------------------------------------------------
      | Logging Configuration
      |--------------------------------------------------------------------------
      |
      | Here you may configure the log settings for your application. Out of
      | the box, Laravel uses the Monolog PHP logging library. This gives
      | you a variety of powerful log handlers / formatters to utilize.
      |
      | Available Settings: "single", "daily", "syslog", "errorlog"
      |
     */
    'log' => 'daily',


    /**
     * Loan zipcode default country
     */
    'loan_zipcode_country'=>env('LOAN_ZIPCODE_COUNTRY'),

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
        * Package Service Providers...
        */         
        Barryvdh\Cors\ServiceProvider::class,
        App\VimeoIntegration\VimeoServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

        /*
         * Rutorika Provider for sortable many to many - https://github.com/boxfrommars/rutorika-sortable
         */
        Rutorika\Sortable\SortableServiceProvider::class,

        Grimzy\LaravelMysqlSpatial\SpatialServiceProvider::class,
        Laravel\Passport\PassportServiceProvider::class,
        Maatwebsite\Excel\ExcelServiceProvider::class,

        /**
         *  Image Intervention
         */
        Intervention\Image\ImageServiceProvider::class,
        /**
         * HTML and Form Helpers
         */
       // Illuminate\Html\HtmlServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,
        /**
         * Laravel Cashier - Stripe integration
         */
        Laravel\Cashier\CashierServiceProvider::class,
        /**
         * Amazon web services integration
         */
        Aws\Laravel\AwsServiceProvider::class,
        /**
         * eWarp service integration
         */
        App\eWarpIntegration\eWarpServiceProvider::class,
        /**
         * LERN provider for logging exception errors and notify
         * DOC: https://github.com/tylercd100/lern
         */
        Tylercd100\LERN\LERNServiceProvider::class,
        /**
         * infusionsoft provider
         */
        Infusionsoft\FrameworkSupport\Laravel\InfusionsoftServiceProvider::class,
        /**
         * siteMap
         */
        Roumen\Sitemap\SitemapServiceProvider::class,
        /**
         * OpenGraph
         */
        ChrisKonnertz\OpenGraph\OpenGraphServiceProvider::class,

        /**
         * Metadata Composer for the view
         */
        App\Http\ViewComposers\MetadataComposerServiceProvider::class,

        /**
         * Zipcode
         */
        PragmaRX\ZipCode\Vendor\Laravel\ServiceProvider::class,
         /**
         * Facebook SDK
         */
        SammyK\LaravelFacebookSdk\LaravelFacebookSdkServiceProvider::class,


    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        
        'Vimeo' => App\VimeoIntegration\Facades\Vimeo::class,

        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        'Input' => Illuminate\Support\Facades\Input::class,
        'Inspiring' => Illuminate\Foundation\Inspiring::class,
        /*
         * Image intervention
         */
        'Image' => Intervention\Image\Facades\Image::class,
        /**
         * HTML and Form Helpers
         */
       // 'Form' => Illuminate\Html\FormFacade::class,
        //'HTML' => Illuminate\Html\HtmlFacade::class,
        'Form' => Collective\Html\FormFacade::class,
         'HTML' => Collective\Html\HtmlFacade::class,
        /**
         * Amazon Web Services alias
         */
        'AWS' => Aws\Laravel\AwsFacade::class,
        /**
         * eWarp service integration
         */
        'ewarp' => App\eWarpIntegration\eWarpFacade::class,
        'ewarpTesting' => App\eWarpIntegration\eWarpTestingFacade::class,
        /**
         * LERN Aliases for the loging errors
         */
        "LERN" => Tylercd100\LERN\Facades\LERN::class,
        /**
         * infusionsoft
         */
        'Infusionsoft' => Infusionsoft\FrameworkSupport\Laravel\InfusionsoftFacade::class,

        /**
         * Sitemap
         */
        'siteMap'   => Roumen\Sitemap\SitemapServiceProvider::class,

        /**
         * Facebook SDK
         */
        'Facebook' => SammyK\LaravelFacebookSdk\FacebookFacade::class,

        /**
         * OpenGraph libary
         */
        'OpenGraph' => 'ChrisKonnertz\OpenGraph\OpenGraph',

        /**
         * Zipcode
         */
        'ZipCode' => 'PragmaRX\ZipCode\Vendor\Laravel\Facade',
    ],

    /**
     * Date time formats to be used on the app
     */
    'datetime_format' => env('APP_DATETIME_FORMAT', 'Y-m-d H:i:s'),
    'date_format' => env('APP_DATE_FORMAT', 'Y-m-d'),
    /**
     * Static application url. To be used for linking at emails or messages
     */
    'static_url' => env('APP_STATIC_URL', 'http://localhost:3000/#/'),
    /**
     * Pagination limit
     */
    'pagination_limit' => env('APP_PAGINATION_LIMIT', 15),
    /**
     * MSL page url
     */
    'mls_url' => env('MLS_URL', 'http://mls.homejab.com'),
    /**
     * MLS old domain SEO
     */
    'old_domain_seo' => env('OLD_DOMAIN_SEO', 'http://videos.homejab.com/property/'),
    'old_domain_mls_seo' => env('OLD_DOMAIN_MLS_SEO', 'http://old.mls.homejab.com/'),
    /*
     * MLS new domain SEO
     */
    'new_domain_seo' => env('NEW_DOMAIN_SEO', 'http://videos.homejab.com/property/'),
    'new_domain_mls_seo' => env('NEW_DOMAIN_MLS_SEO', 'http://mls.videos.homejab.com/property/'),
    /**
     * MLS old domain ID
     */
    'mls_old_domain_id' => env('MLS_OLD_DOMAIN_ID', 'http://mls.homejab.com/'),
    /*
     * MLS new domain ID
     */
    'mls_new_domain_id' => env('MLS_NEW_DOMAIN_ID', 'http://mls.videos.homejab.com/'),
    /**
     * Jw Player key Access
     */
    'jw_key' => env('JW_KEY', false),
    /**
     * Jw Player Secret Key Access
     */
    'jw_secret' => env('JW_SECRET', false),
    /**
     * Unique Key for the template player
     */
    'jw_template' => env('JW_TEMPLATE', 'default'),
    /**
     * Contact email to send the order info when is placed
     */
    'email_contact_on_place_order' => env('EMAIL_CONTACT_ON_PLACE_ORDER'),
    /**
     * Full name for the place order email notifications
     */
    'full_name_on_place_order' => env('FULL_NAME_ON_PLACE_ORDER'),
    /**
     * stripe dashboard url
     */
    'stripe_url' => env('STRIPE_URL'),
    /**
     * Email address to send email to laons
     */
    'email_to_loans' => env('EMAIL_TO_LOANS', 'loans@homejab.com'),
    /**
     * Email address to send email to sales
     */
    'email_to_sales' => env('EMAIL_TO_SALES'),
    /**
     * Old site property uploaded content domain
     */
    'old_site_upload_content_domain' => env('OLD_SITE_UPLOAD_CONTENT_DOMAIN'),
    /**
     * Email configuration for copy the email sended to rea when property mediafiles are ready
     */
    'email_to_jobs' => env('EMAIL_TO_JOBS'),
    /**
     *  Email configuration for support
     */
    'support_email' => env('SUPPORT_EMAIL'),
  /**
     * number of collums for the home photos grid
     */
    'home_photo_grid_columns'=>env('APP_HOME_PHOTO_GRID_COLUMNS'),

    /**
     * Google Application name
     */
    'application_name'=>env('APPLICATION_NAME'),

    /**
     * Google service account email
     */
    'account_service_email'=>env('ACCOUNT_SERVICE_EMAIL'),

    /**
     * Path to Google API config file
     */
    'json_config_file'=>env('JSON_CONFIG_FILE'),

    /**
     * Callback url when order is placed
     */
    'callback_url'=>env('CALLBACK_URL', 'http://buildapi.homejab.com/SchedulingAPI/api/GetNewJobDetails/NewJobDetails'),

    /**
     * Callback url when an order rescheduled is requested
     */
    'callback_url_reschedule'=>env('CALLBACK_URL_RESCHEDULE'),

     /**
     * Callback url when an order is placed on hold
     */
    'callback_url_on_hold'=>env('CALLBACK_URL_ON_HOLD'),

    /**
    * Double Callback flag
    */
    'double_callback'=>env('DOUBLE_CALLBACK'),

    /**
     * Loan zipcode default country
     */
    'loan_zipcode_country'=>env('LOAN_ZIPCODE_COUNTRY', 'US'),

    /**
     *  Route to Home Main Image
     */
    'main_image_file'=>env('MAIN_IMAGE_FILE'),

    /**
     * Maximum amount of months that are saved extra files
     */
    'extra_files_max_months'=>env('EXTRA_FILES_MAX_MONTHS', 6),

    /*
     * Marker icon image to show in google maps
     */
    'marker_icon' => "assets/images/home/marker_icon.png",


    /**
    * Assets id for video, photos and aerials
    */

    '3d_asset_virtual_dusk'       => env('ASSET_VIRTUAL_DUSK'),
    '3d_asset_hdr_photos'         => env('ASSET_HDR_PHOTOS'),
    '3d_asset_standard_photos'    => env('ASSET_STANDARD_PIC'),
    '3d_asset_video_standard'     => env('ASSET_VIDEO_STANDARD'),
    '3d_asset_video_intermediate' => env('ASSET_VIDEO_INTERMEDIATE'),

    '3d_asset_1500_less_id'  => env('ASSET_SQFT_1500_LESS_ID'),
    '3d_asset_1500_3000_id'  => env('ASSET_SQFT_1500_3000_ID'),
    '3d_asset_3000_5000_id'  => env('ASSET_SQFT_3000_5000_ID'),
    '3d_asset_5000_7000_id'  => env('ASSET_SQFT_5000_7000_ID'),
    '3d_asset_7000_10000_id' => env('ASSET_SQFT_7000_10000_ID'),

    'vimeo_redirect_uri' => env('VIMEO_URI_REDIRECT'),

    /**
     * Webhooks url when an user updates it info
     */
    'callback_url_user_updated' => env('CALLBACK_URL_USER_UPDATED', 'http://jabjobs.net/SchedulingAPI/api/UpdateAssistantEmails'),

    /**
     * Esoft customer service email.
     */
    'esoft_customer_service' => env('ESOFT_CUSTOMER_SERVICE', 'customerservice.vn@esoftflow.com'),


    /**
     * Homejab email service to ewarp.
     */
    'homejab_ewarp_email_service' => env('HOMEJAB_EWARP_EMAIL', 'post@homejab.com'),

    /**
     * Homejab homepage cache variables
     */
    'cache_counters'        => env('CACHE_COUNTER', 'counters'),
    'cache_packages'        => env('CACHE_PACKAGE', 'packages'),
    'cache_ourwork'         => env('CACHE_OURWORK', 'ourworks'),
    'cache_zip'             => env('CACHE_ZIP', 'zips'),
    'cache_recomendations'  => env('CACHE_RECOMENDATIONS', 'recomendations'),
    'cache_metadata'        => env('CACHE_METADATA', 'url_metadata'),
    'cache_property_list'   => env('CACHE_PROPERTY_LIST', 'property_home_list'),
    'cache_video'           => env('CACHE_HOME_VIDEO', 'home_video'),

];
