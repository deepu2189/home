<?php

return [
  'gcm' => [
      'priority' => 'high',
      'dry_run' => false,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AIzaSyCAtCJDCDNzy8qR7r5Z6JF5oXzVrRCQT1g',
        'senderid'=> '738312034058'
  ],
  'apn' => [
      'certificate' => __DIR__ . '/iosCertificates/HomeJabCert.pem',
      //'passPhrase' => 'password', //Optional
      //'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
      'dry_run' => false
  ]
];