<?php




/*
 * This file is part of Laravel Vimeo.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// return VimeoAccounts::getAllConnections();


return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Vimeo Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [
        
//        'main' => [
//          //  'client_id' => '2eb46ae2e276a7429c5b2176a3c5ddc69ec40103',
//           // 'client_secret' => 'b1E0s/n5SO8Ef7hqYb3TN4FL4JAsysA241djPBXvXTx/YuZeFKSNdkKM3lNnuhDPh7c4DRM/3D9okPcC8ZgvAiM27esrQQ1yWTjA+fayXHsBqhQdGNqfr1yRsD19C7zP',
//           // 'access_token' => 'a5311a640115aeca125afce9ca226fee',
//            'client_id' => '3b015fd0e6d0bdf1ef8a51a6485ae66d3a29d226',
//            'client_secret' => 'fjPyrRWtd+FrwiKzBrv0WxQficaAxmmGf4pf7qJc3t5Yaz2kLedFmXHGoG9dnN50bM5IiYRYE8KnYpIVSC7RK2VqlCf+q7DKt9/QwWzUkzvDc7H9pS4On0RCyWqLx8xp',
//            'access_token' => '849460208604223c775c4b3fb080031d',
//        ],

        // 'main' => [
        //     'client_id' => '3b015fd0e6d0bdf1ef8a51a6485ae66d3a29d226',
        //     'client_secret' => 'fjPyrRWtd+FrwiKzBrv0WxQficaAxmmGf4pf7qJc3t5Yaz2kLedFmXHGoG9dnN50bM5IiYRYE8KnYpIVSC7RK2VqlCf+q7DKt9/QwWzUkzvDc7H9pS4On0RCyWqLx8xp',
        //     'access_token' => '333fa9af4294751819c6633a442a3ff4',
        // ],

         'main' => [
            'client_id' => '2eb46ae2e276a7429c5b2176a3c5ddc69ec40103',
            'client_secret' => 'b1E0s/n5SO8Ef7hqYb3TN4FL4JAsysA241djPBXvXTx/YuZeFKSNdkKM3lNnuhDPh7c4DRM/3D9okPcC8ZgvAiM27esrQQ1yWTjA+fayXHsBqhQdGNqfr1yRsD19C7zP',
            'access_token' => 'a5311a640115aeca125afce9ca226fee',
        ],
        'alternative' => [
            'client_id' => 'your-client-id',
            'client_secret' => 'your-client-secret',
            'access_token' => null,
        ],
    ],

];
