<?php


return [
    'entities' => [
        'propertyHasMediafiles' => [
            'entity' =>'App\PropertyHasMediaFile'
        ],
        'packages' => [
            'entity' =>'App\Package'
        ]
    ]
];
