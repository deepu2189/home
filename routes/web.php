<?php

use Illuminate\Http\Request;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::post(
    'Api/WebAPI/Delete_Unavailablity',
    [
        'uses' => 'WebAPIController@UpdateAssistantEmails',
        'as'   => 'WebAPI.updateassistantemails'
    ]
);


Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '5',
        'redirect_uri' => 'http://18.234.11.52/callback',
        'response_type' => 'code',  
        'scope' => '',
    ]);

    return redirect('http://homejabapi.matrixmarketers.com/oauth/authorize?'.$query);
})->name('redirect');


// Route::get('/callback', function (Request $request) {
    
//     $http = new GuzzleHttp\Client;
//     $response = $http->post('http://homejabapi.matrixmarketers.com/oauth/token', [
//         'form_params' => [
//             'grant_type' => 'authorization_code',
//             'client_id' => '5',
//             'client_secret' => 'gkzRW1VdQSDB9HNT36M11fUcaCLnZa04o7UE13SQ',
//             'redirect_uri' => 'http://18.234.11.52/callback',
//             'code' => $request->code,
//         ],
//     ]);     

//     return json_decode((string) $response->getBody(), true);
// });



Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
