<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'            => 'These credentials do not match our records.',
    'throttle'          => 'Too many login attempts. Please try again in :seconds seconds.',
    'no_user'           => 'There is no user authenticated with provided credentials',
    'user_get'          => 'User authenticated found',
    'password_recovery_fails'   => 'User password recovery has failed',
    'password_recovery_invalid' => 'The provided email is not associated with a valid account',
    'password_recovery_success'=>'Check your email!',
    'valid_password_token'=>'Valid Password Recovery Token',
    'invalid_password_token'=>'Invalid Password Recovery Code'

];
