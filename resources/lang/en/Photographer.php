<?php

return [
    'photographer_created'      => 'Photographer added successfully.',
	'photographer_updated'      => 'Photographer updated successfully.',
    'photographer_not_exists'   => 'Photographer does not exist.',
    'creation_failed'           => 'Photographer could not be created.',
    'creation_validation_failed'=> 'Photographer could not be created.Please check all your inputs.',
    'email_exist'               => 'Email has been already exist.',
    'update_fail'               => 'Photographer could not be updated.',
    'activated'                 => 'Photographer has been set to active successfully.',
    'deactivated'               => 'Photographer has been set to inactive successfully.',
    'error_msg'                 => 'Sorry, unable to process request.',
    'update_rank_success_msg'   => 'Photographer rank updated successfully',

    'assign_userpaymentinfo_failed' 		=> 'UserPayment Could not be assigned.Please check all your inputs.',
    'assign_userpaymentinfo_success_msg' 	=> 'UserPayment has been assigned successfully.',
    'assign_userpaymentinfo_error_msg'   	=> 'Something went wrong.Please try again.'
];
?>
