<?php

return [
    'source_address_not_found'      => 'Source address not found for perticular this job.',
	'jobid_not_exist'               =>  'jobid Doesn\'t exists in JabJob.',
	'order_not_exists'   			=> 'OrderDetail does not exist.',
    'orderdetail_update' 			=> 'OrderDetails updated Successfully.',
    'update_fail'        			=> 'OrderDetails could not be updated.Please Try Again.',
    'open_success_msg'       		=> 'Message has been set to open successfully',
    'close_success_msg'       		=> 'Message has been set to close successfully',
    'error_msg'                 	=> 'Sorry, unable to process request.',
	'reschedule_error_msg'			=> 'Something Went Wrong.Please Try Again.',
	'jobs_onhold_success_msg'		=>	'Job has been set to on hold status successfully.',
	'job_deleted_success_msg'		=>	'Job has been  deleted successfully.',
	'job_reschedule_success_msg'	=>	'Job has been  rescheduled successfully.',
	'jobs_added_success_msg'		=>  'Job has been added successfully.'
];
?>

