<?php

return [
	'block_number_success_msg'      => 'Do-Not-Text list has been updated successfully',
	'error_msg'        => 'Something went wrong.Pleas try again',
	'send_notification_status_success_msg' => 'Status has been change successfully',
	'template_does_not_exist' => 'Template does not exist',
	'template_created'             => 'Template added successfully.',
	'template_updated'             => 'Template updated successfully.',
	'creation_validation_failed'=> 'Template could not be created.Please check all your inputs.',
	'creation_failed'           => 'Template could not be created.',
	'update_fail'               => 'Template detail could not be updated.',
	'template_deleted'          => 'Template deleted successfully.',



];
?>