<?php
return [
    'assign_package_failed'      => 'Please select package and user.',
    'assign_package_success_msg' => 'Packages has been assigned to users successfully.',
    'assign_package_error_msg'   => 'Something went wrong.Please try again.',
    'chat_not_exist'             =>  'Chat does not exist.',
    'send_message_success'       => 'Message Send Successfully.',
    'inactive_user'              => 'Your status is Inactive',
    'no_user'                    => 'No user exist',
    'no_permission'              => 'You dont have permission to access the application',
    'invalid_user'               => 'Invalid Email Id or Password'

];
?>