<?php

return [
    'update_fail'               => 'Metadata detail could not be updated.',
    'metadata_updated'          => 'Metadata detail updated successfully.',
];
