<?php

return [
    'group_created'             => 'group added successfully.',
    'creation_validation_failed'=> 'Group could not be created.Please check all your inputs.',
    'group_exist'               => 'Group Name already exist.',
    'creation_failed'           => 'Group could not be created.',
    'update_fail'               => 'Group detail could not be updated.',
    'group_updated'             => 'Group detail updated successfully.',
    'activated'                 => 'Group has been set to active successfully.',
    'deactivated'               => 'Group has been set to inactive successfully.',
    'error_msg'                 => 'Sorry, unable to process request.',
    'group_not_exists'          => 'Group does not exist.',

    'assign_group_failed'      => 'Please select groups.',
    'assign_group_success_msg' => 'Group has been assigned to users successfully.',
    'assign_group_error_msg'   => 'Something went wrong.Please try again.'
];
