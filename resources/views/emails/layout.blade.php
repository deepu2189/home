<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>HomeJab Real Estate Videos, Aerial Drones & HDR Photography</title>
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,500,700);
        @import url(https://fonts.googleapis.com/css?family=Lato:400,700);
        body {margin: 0px 0 10px; padding: 0 0px; background: #FFF; font-size: 14px;line-height:1.5;}
        table {border-collapse: collapse;}
        td {font-family: "Roboto",sans-serif; color: #333333;}
        table td p {font-size:14px; color: rgb(0,0,0)}
        table td ul, table td ul li {font-size:14px;color:rgb(0,0,0);list-style: none;font-family: "Roboto",sans-serif;}
        table td ul {
            margin-left: auto;
            margin-right: auto;
            max-width: 500px;
            padding-left:0px;
        }
        table td ul li {
            margin-bottom:15px;
        }
        table td ul li b {
            display:block;
            margin-bottom:3px;
        }
        table table table td{
            font-family: 'Lato', sans-serif;
        }
        table table a {
            text-decoration:none;
            color:#1dc3a6;
            font-size:14px;
            font-weight:700;
        }

        table table tr:nth-child(2) span{
            color: #000000 !important;
        }

        table table a:hover {
            text-decoration:underline;
        }
        table table span {
            color:#fff;
        }
        table table span img {
            margin-right:3px;
        }
        table td a:hover {
            color:rgb(52, 213, 184);
            background:transparent;

        }
        table table div > a {
            background-color: #1dd0b0;
            border: 2px solid #11816d;
            border-radius: 10px;
            color: #fff;
            font-weight: bold;
            padding: 8px 28px;
            text-decoration: none;
            display:inline-block;
            font-size:14.5px;
        }
        table td div > a:hover {
            color:#1dd0b0;
            background:transparent;
            text-decoration: none;
        }
        table td h1 {margin-top: 15px;margin-bottom: 3%;color:#1dc3a6;font-size:25.9px;}
        @media only screen and (max-width: 480px) {
            body,table,td,p,a,li,blockquote {
                -webkit-text-size-adjust:none !important;
            }
            table {width: 100% !important;}
            div {
                width:auto !important;
            }
            .responsive-image img {
                height: auto !important;
                max-width: 100% !important;
            }
        }
    </style>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="0" align="center" width="800" bgcolor="#FFFFFF">
                <tr>
                    <td bgcolor="#121e2e" style="font-size: 0; line-height: 0; padding: 20px 10px 20px;" align="center" class="responsive-image">
                        <img src="https://homejab.com/assets/images/email/logo.png" alt="HomeJab " />
                    </td>
                </tr>
                <tr>
                    <td style="">
                        <div style="margin-left:auto;margin-right:auto;padding-left: 15px;padding-right: 15px;padding-bottom: 5%;padding-top: 5%;text-align:center;position:relative;background-repeat: no-repeat;background-size: cover;background-position: center center;background-color: #efefef;">
                            @yield('content')
                        </div>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#121e2e" >
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="text-align:center;padding-top:20px;padding-bottom:21px;">
                                    <img src="https://homejab.com/assets/images/email/logo.png" alt=""/>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center;padding-bottom:19px;">
                                    <a href="https://twitter.com/HomeJab" style="margin-right:7px;"><img alt="twitter" src="https://homejab.com/assets/images/email/twitter.jpg"/></a>
                                    <a href="https://www.facebook.com/homejab"  style="margin-right:7px;"><img alt="facebook" src="https://homejab.com/assets/images/email/facebook.jpg" /></a>
                                    <a href="https://www.instagram.com/homejab_"  style="margin-right:7px;"><img alt="instagram" src="https://homejab.com/assets/images/email/instagram.jpg" /></a>
                                    <a href="https://www.pinterest.com/homejab"  style="margin-right:7px;"><img alt="pinterest" src="https://homejab.com/assets/images/email/printerest.jpg" /></a>
                                    <a href="https://www.youtube.com/channel/UC8GOaTipjEzi4W2kVjO1-ig" style="margin-right:7px;"><img alt="youtube" src="https://homejab.com/assets/images/email/youtube.jpg"/></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center;font-size:14px;padding-bottom:15px;vertical-align: middle;">
                                    <span style="margin-right:15px;display: inline-block;vertical-align: middle"><img alt="phone" style="display: inline-block;vertical-align: middle" src="https://homejab.com/assets/images/email/telephone.jpg" /> <span style="display: inline-block;vertical-align: middle;color: white;">855.226.8305</span></span>
                                    <span style="display: inline-block;vertical-align: middle"><img alt="contact" src="https://homejab.com/assets/images/email/mail.jpg" style="display: inline-block;vertical-align: middle;margin-right:6px;"/><a href="mailto:sales@homejab.com" style="color:#fff;display: inline-block;vertical-align: middle">CONTACT US</a></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center;">
                                    <p style="color: #fff;background-color:#101b2b;font-size:14px;margin-top:0px;margin-bottom:0px;padding-top:16px;padding-bottom:8px;">&#169; 2019 HOMEJAB. ALL RIGHTS RESERVED. UNITED STATES </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>