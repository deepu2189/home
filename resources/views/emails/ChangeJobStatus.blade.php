@extends('emails.layout')

@section('content')
<table>
    <tbody>
        <tr>
            <td bgcolor="#ffffff" style="text-align: left;margin-left:auto;margin-right:auto;padding-left: 3%;padding-right: 3%;padding-bottom: 3%;padding-top: 4%;position:relative;background-repeat: no-repeat;background-size: cover;background-position: center center;background-color: white;">
                    Hi,<br />
                    <div>
                        The following job(s) has been moved to urgent as none of the users accepted the same:
                    </div>
                    @if($arrJobList != null)
                    <div style="background-color: #fff; margin: 20px 0px;">
                        <table border = '0' width='100%' cellpadding='6' cellspacing='0' border='0'>
                            <tr><th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.cron_job_change_status.JOBID')</th>
                                <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.cron_job_change_status.HJJobID')</th>
                                <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.cron_job_change_status.HJPackageName')</th>
                                <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.cron_job_change_status.PropertyAddress')</th>
                                <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.cron_job_change_status.ScheduleDate')</th>
                                <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.cron_job_change_status.ScheduleSlot')</th>
                            </tr>
                            @foreach ($arrJobList as $key=>$val)
                            <tr>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->JOBID}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->HJJobID}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->HJPackageName}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->PropertyAddress}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->ScheduleDate}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->ScheduleSlot}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    @endif

                    <div style="text-align: left;">
                        You will find this job under urgent status. Kindly reschedule the same.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
@endsection
