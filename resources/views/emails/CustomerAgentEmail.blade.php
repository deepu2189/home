@extends('emails.layout')

@section('content')
    <table>
        <tbody>
            <tr>
                <td bgcolor="#ffffff" style="font-size:15px;text-align:center;margin-left:auto;margin-right:auto;padding-left: 3%;padding-right: 3%;padding-bottom: 3%;padding-top: 4%;position:relative;background-repeat: no-repeat;background-size: cover;background-position: center center;background-color: white;">
                    <h1 align="center">@lang('emails.customer_accepted_job_email.title')</h1></span>
                    <p style="font-size:15px">@lang('emails.customer_accepted_job_email.subtitle') {{$arrJobs['JobUser']}}!</p>
                    <p style="font-size:15px"><b>@lang('emails.customer_accepted_job_email.datetime') </b>{{$arrJobs['ScheduleDate']}} {{$arrJobs['ScheduleStartTime']}}</p>
                    <p style="font-size:15px"><b>@lang('emails.customer_accepted_job_email.property_address') </b>{{$arrJobs['PropertyAddress']}}</p>
                    <div>-- <b>@lang('emails.customer_accepted_job_email.reminder_title')</b> -- </div>
                    <div>@lang('emails.customer_accepted_job_email.reminder_content')</div>
                    <div>@lang('emails.customer_accepted_job_email.reminder_content1')</div>
                    <div> </div>
                    <div>-- <b>@lang('emails.customer_accepted_job_email.cancelation_title')</b> -- </div>
                    <div>@lang('emails.customer_accepted_job_email.cancelation_content') </div>
                    <p style="font-size:15px">@lang('emails.customer_accepted_job_email.content_1')</p>

                    <p style="font-size:15px">@lang('emails.customer_accepted_job_email.thankyou'),</p>
                    <div>-@lang('emails.customer_accepted_job_email.homejab_team') </div>
                    <a href="mailto:sales@homejab.com">sales@homejab.com</a>
                    <div> </div>
                </td>
            </tr>
        </tbody>
    </table>
@endsection

