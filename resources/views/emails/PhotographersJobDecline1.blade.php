@extends('emails.layout')

@section('content')
    <td bgcolor="#ffffff" style="margin-left:auto;margin-right:auto;padding-left: 3%;padding-right: 3%;padding-bottom: 3%;padding-top: 4%;position:relative;background-repeat: no-repeat;background-size: cover;background-position: center center;background-color: white;">
        Hi,<br />

            <div style="background-color: #fff; margin: 20px 0px;">
                <table border = '0' width='100%' cellpadding='6' cellspacing='0' border='0'>
                    <tr>
                        <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.photographers_accepted_job_email.homejabid')</th>
                        <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.photographers_accepted_job_email.package_name')</th>
                        <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.photographers_accepted_job_email.property_address')</th>
                        <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.photographers_accepted_job_email.scheduling_date')</th>
                        <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.photographers_accepted_job_email.scheduling_slot')</th>
                    </tr>
                    <tr>
                        <td style='border-bottom: solid 1px #f4f4f4'>{{$objJobs->HJJobID}}</td>
                        <td style='border-bottom: solid 1px #f4f4f4'>{{$objJobs->HJPackageName}}</td>
                        <td style='border-bottom: solid 1px #f4f4f4'>{{$objJobs->PropertyAddress}}</td>
                        <td style='border-bottom: solid 1px #f4f4f4'>{{$objJobs->ScheduleDate}}</td>
                        <td style='border-bottom: solid 1px #f4f4f4'>{{$objJobs->ScheduleSlot}}</td>
                    </tr>
                </table><br/>
            </div>
            @if($arrJobDeclineList != null)
                <span style="text-align: left;color: #000;">The Above Job Has been Decline By Following user(s),</span><br />
                <div style="background-color: #fff; margin: 20px 0px;">
                    <table border = '0' width='100%' cellpadding='6' cellspacing='0' border='0'>
                        <tr>
                            <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.job_assign_user_detail.serial_no')</th>
                            <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.job_assign_user_detail.user_name')</th>
                            <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.job_assign_user_detail.address')</th>
                            <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.job_assign_user_detail.email')</th>
                            <th style='text-align: left; border-bottom: solid 2px #dddddd'>@lang('emails.job_assign_user_detail.phone_no')</th>
                        </tr>
                        @foreach ($arrJobDeclineList as $key=>$val)
                            <tr>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->SerialNo}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->Name}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->Address}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->Email}}</td>
                                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->PhoneNo}}</td>
                            </tr>
                        @endforeach
                    </table><br/>
                </div>
            @endif
    </td>
@endsection
