@extends('emails.layout')

@section('content')
<tr>
    <td bgcolor="#ffffff" style="text-align:left;margin-left:auto;margin-right:auto;padding-left: 3%;padding-right: 3%;padding-bottom: 3%;padding-top: 4%;position:relative;background-repeat: no-repeat;background-size: cover;background-position: center center;background-color: white;">


        <p>Hi,</P>
        <p> The following jobs have been completed by you this week:</p>
    </td>
</tr>
<tr>
    <td style="background:white;width:100%">

        <table border=1 style="width:100%">
            <tr>
                <th style='text-align: left; border-bottom: solid 2px #dddddd'>
                    @lang('emails.HomeJab_Send_Weekly_Report.JobId')
                </th>
                <th style='text-align: left; border-bottom: solid 2px #dddddd'>
                    @lang('emails.HomeJab_Send_Weekly_Report.HJPackageName')
                </th>
                <th style='text-align: left; border-bottom: solid 2px #dddddd'>
                    @lang('emails.HomeJab_Send_Weekly_Report.UpdatedOn')
                </th>
                <th style='text-align: left; border-bottom: solid 2px #dddddd'>
                    @lang('emails.HomeJab_Send_Weekly_Report.PackagePay')
                </th>
            </tr>
            @php($total = 0)
            @foreach ($arrReport as $key=>$val)
            <tr>
                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->JobId}}</td>
                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->HJPackageName}}</td>
                <td style='border-bottom: solid 1px #f4f4f4'>{{$val->UpdatedOn}}</td>
                <td style='border-bottom: solid 1px #f4f4f4'>${{$val->PackagePay}}</td>
                @php($total += $val->PackagePay)
            </tr>
            @endforeach

            <tr>
                <td style='border-bottom: solid 1px #f4f4f4'></td>
                <td style='border-bottom: solid 1px #f4f4f4'></td>
                <td style='border-bottom: solid 1px #f4f4f4'> Total</td>
                <td style='border-bottom: solid 1px #f4f4f4'>${{ $total }}</td>
            </tr>
        </table>

</td>
</tr>
@endsection
