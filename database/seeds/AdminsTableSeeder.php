<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'id'                => 1,
            'user_id'           => 3
        ]);

        Admin::create([
            'id'                => 2,
            'user_id'           => 4
        ]);
    }
}
