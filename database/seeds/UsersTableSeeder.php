<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'                => 1,
            'profile_image_id'  => null,
            'firstname'         => 'Harry R. ',
            'lastname'          => 'Lindberg',
            'slug'              => '',
            'email'             => env('email').'+rea@gmail.com',
            'password'          => env('password'),
            'contact_phone'     => '(456) 456-4564',
            'contact_text'      => 'Please, send me an email first',
            'bio'               => 'Born and living',
            'account_verified'  => 1,
            'account_type'      => 'rea',
        ]);

        User::create([
            'id'                => 2,
            'profile_image_id'  => null,
            'firstname'         => 'Gertrude W. ',
            'lastname'          => 'Chambers',
            'slug'              => '',
            'email'             => env('email').'+hjph@gmail.com',
            'password'          => env('password'),
            'contact_phone'     => '(456) 456-4564',
            'contact_text'      => null,
            'bio'               => 'I am Jebus',
            'account_verified'  => 1,
            'account_type'      => 'hjph',
        ]);

        User::create([
            'id'                => 3,
            'profile_image_id'  => null,
            'firstname'         => 'Toni L. ',
            'lastname'          => 'Kindred',
            'slug'              => '',
            'email'             => env('email').'+su@gmail.com',
            'password'          => env('password'),
            'contact_phone'     => '(456) 456-4564',
            'contact_text'      => null,
            'bio'               => 'I am ROOT',
            'account_verified'  => 1,
            'account_type'      => 'su',
        ]);

        User::create([
            'id'                => 4,
            'profile_image_id'  => null,
            'firstname'         => 'Devon B.',
            'lastname'          => 'Guerra',
            'slug'              => '',
            'email'             => env('email').'+su2@gmail.com',
            'password'          => env('password'),
            'contact_phone'     => '(456) 456-4564',
            'contact_text'      => null,
            'bio'               => 'I am ROOT 2',
            'account_verified'  => 1,
            'account_type'      => 'su',
        ]);
                
        User::create([
            'id'                => 5,
            'profile_image_id'  => null,
            'firstname'         => 'Jeanie J.',
            'lastname'          => 'Fernandez',
            'slug'              => '',
            'email'             => env('email').'+seller@gmail.com',
            'password'          => env('password'),
            'contact_phone'     => '(456) 456-4564',
            'contact_text'      => null,
            'bio'               => 'I am SELLER',
            'account_verified'  => 1,
            'account_type'      => 'seller',
        ]);
    }
}
