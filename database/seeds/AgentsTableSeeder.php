<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Agent;

class AgentsTableSeeder extends Seeder


{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Agent::create([
            'id'                => 1,
            'user_id'           => 1
        ]);
    }
}
