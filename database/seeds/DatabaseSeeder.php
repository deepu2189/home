<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();

         DB::statement('SET FOREIGN_KEY_CHECKS = 0');

       // $this->call('OAuthClientsTableSeeder');
       // $this->call('UsersTableSeeder');
        //$this->call('AgentsTableSeeder');
       // $this->call('PhotographersTableSeeder');
        //$this->call('AdminsTableSeeder');
        //$this->call('PackagesTableSeeder');
       //  $this->call('AssetTypeTableSeeder');
       // $this->call('PackageHasAssetTypesTableSeeder');
        //$this->call('SellersTableSeeder');
        //$this->call('SettingsTableSeeder');  

         DB::statement('SET FOREIGN_KEY_CHECKS = 1');

         //Model::guard();

    }
}
