<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomejabJobAssignphotographers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    DB::unprepared("
	    DROP PROCEDURE IF EXISTS `HomeJab_Job_AssignPhotographers`;
		CREATE PROCEDURE `HomeJab_Job_AssignPhotographers`(IN `p_JobId` BIGINT, IN `p_Photographers` LONGTEXT, IN `p_UserIdsAVLDATETIME` LONGTEXT)
		BEGIN
		
		BEGIN
		DECLARE v_RESULT INT DEFAULT 0;
		DECLARE v_SCH_START_DATE DATETIME(3);
		DECLARE v_SCH_END_DATE DATETIME(3) ;
		DECLARE v_ScheduleForHours TINYINT UNSIGNED ;
		DECLARE v_UsrAvlDateTime DATETIME(3);
		DECLARE v_IDX INT ;
		DECLARE v_SLICE TEXT ;
		DECLARE v_string LONGTEXT;
		DECLARE v_delimeter CHAR(1);
		DECLARE v_I INT ;
		DECLARE v_RC INT;
		DECLARE v_PHOTOGRAPHERID INT;
		DECLARE v_JobScheduleId BIGINT ;
		DECLARE v_JobLogId BIGINT;
		
		
		DECLARE exit handler for sqlexception
		BEGIN
		
		GET DIAGNOSTICS CONDITION 1
		@p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
		
		SET v_RESULT = 0;
		SELECT v_RESULT as Result;
		ROLLBACK;
		END;
		
		DECLARE exit handler for sqlwarning
		BEGIN
		
		GET DIAGNOSTICS CONDITION 1
		@p1 = RETURNED_SQLSTATE, @p2 = MESSAGE_TEXT;
		
		SET v_RESULT = 0;
		SELECT v_RESULT as Result;
		ROLLBACK;
		END;
		
		START TRANSACTION;
		    SET v_IDX = 1 ;
		    SET @row_number = 0;
		
		      CALL sp_split_data(p_UserIdsAVLDATETIME,',');
		
		        DROP TEMPORARY TABLE IF EXISTS USERSWITHDATETIME;
		              CREATE TEMPORARY TABLE USERSWITHDATETIME ( USERID INT , USRAVLDATETIME VARCHAR(100));
		         INSERT INTO USERSWITHDATETIME
		         SELECT  TRIM(TRAILING '$' FROM (SUBSTR(string,1,LOCATE('$',string)))),SUBSTR(string,LOCATE('$',string)+1,(CHAR_LENGTH(RTRIM(string))- CHAR_LENGTH(RTRIM(SUBSTR(string,1,LOCATE('$',string))))))
		         FROM split_string;
		
		        
		       CALL sp_split_data(p_Photographers,',');
		
					 DROP TEMPORARY TABLE IF EXISTS USERIDS;
		             CREATE TEMPORARY TABLE USERIDS ( ROWID INT , USERID INT );
					 INSERT INTO USERIDS
					 SELECT (@row_number := @row_number + 1),string
					 FROM split_string ORDER BY string;
		
		      UPDATE tbl_jobuser
					       SET  IsActive	= 0,
								UpdatedOn	= UTC_TIMESTAMP()
					WHERE JobId = p_JobId and  UserId IN (SELECT string FROM split_string);
		
					Update tbl_jobs
					set CreatedOn = NOW()
					WHERE JobId = p_JobId;
		
					 SET v_I = 1;
					 SELECT MAX(ROWID) INTO v_RC FROM USERIDS;
		
		      IF EXISTS (SELECT JobScheduleId FROM tbl_jobschedule WHERE  JobId = p_JobId)THEN
		        SELECT JobScheduleId INTO v_JobScheduleId
		        FROM tbl_jobschedule WHERE  JobId = p_JobId;
		
		      ELSE
		         SET v_JobScheduleId = NULL;
		
		      END IF;
		
		      BEGIN
		        WHILE  v_I <= v_RC
					 DO
						SELECT USERID INTO v_PHOTOGRAPHERID
						FROM USERIDS
						WHERE ROWID = v_I;
		
						IF EXISTS (SELECT 1 FROM tbl_jobuser WHERE JobId = p_JobId AND JobScheduleId = v_JobScheduleId AND USERID = v_PHOTOGRAPHERID)
						THEN
							 SELECT DATE_FORMAT(USRAVLDATETIME, \"%Y-%m-%d %H:%i:%s \") INTO v_UsrAvlDateTime
							 FROM USERSWITHDATETIME
							 WHERE USERID = v_PHOTOGRAPHERID;
		
							 UPDATE tbl_jobuser
									SET CreatedOn = DATE_FORMAT(v_UsrAvlDateTime, \"%Y-%m-%d %H:%i:%s \") ,
										IsActive = 1
							 WHERE JobId= p_JobId AND UserId = v_PHOTOGRAPHERID AND JobScheduleId = v_JobScheduleId;
						END IF;
		
		
						IF NOT EXISTS (SELECT 1 FROM tbl_jobuser WHERE JobId = p_JobId AND JobScheduleId = v_JobScheduleId AND USERID = v_PHOTOGRAPHERID)
						THEN
							SET v_UsrAvlDateTime = NULL;
							IF EXISTS (SELECT 1 FROM USERIDS WHERE USERID = v_PHOTOGRAPHERID) THEN
							  SELECT DATE_FORMAT(USRAVLDATETIME, \"%Y-%m-%d %H:%i:%s \") INTO v_UsrAvlDateTime
							  FROM USERSWITHDATETIME
							  WHERE USERID = v_PHOTOGRAPHERID;
							END IF;
		
							
							
							
							
							
		
							INSERT INTO tbl_jobuser
							(
								JobId,UserId,JobScheduleId,IsActive,CreatedBy,CreatedOn
							)
							VALUES
							(
								p_JobId,v_PHOTOGRAPHERID,v_JobScheduleId,1,0,IFNULL(DATE_FORMAT(v_UsrAvlDateTime, \"%Y-%m-%d %H:%i:%s \"),v_SCH_START_DATE)
							);
		
							INSERT INTO tbl_joblog
							(
								JobId,JobScheduleId,`Description`,CreatedBy,CreatedOn
							)
							VALUES
							(
								p_JobId,v_JobScheduleId,CONCAT(' Job has been assigned to ',
								(SELECT  NAME FROM tbl_users WHERE UserId = v_PHOTOGRAPHERID),' at ',NOW()),0,NOW()
							);
		
		
							SET v_JobLogId =  LAST_INSERT_ID();
		
							INSERT INTO tbl_notifications
							(
								JobLogId,UserId,IsRead
							)
							VALUES
							(
								v_JobLogId,v_PHOTOGRAPHERID,0
							);
		
							UPDATE tbl_jobschedule
								SET StatusID = 2
							WHERE  JobScheduleId = v_JobScheduleId;
						END IF;
						SET v_I = v_I + 1;
		        END WHILE;
		       END;
		      
		      
					 IF NOT EXISTS ( SELECT 1 FROM USERIDS)
					 THEN
						UPDATE tbl_JobSchedule
							SET
								StatusID = 6
						WHERE JobId = p_JobId;
		
						INSERT INTO tbl_JobLog
						(
							JobId,JobScheduleId,`Description`,CreatedBy,CreatedOn
						)
						VALUES
						(	p_JobId,(SELECT JobScheduleId FROM tbl_JobSchedule WHERE JobId = p_JobId),
							'Job moved to Urgent as Photographers are not available',0,UTC_TIMESTAMP()
						);
					 END IF;
		
		
				SET v_RESULT = 1;
		
		COMMIT;
		SELECT v_RESULT as Result;
		END;
		");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    DB::unprepared("DROP PROCEDURE IF EXISTS `HomeJab_Job_AssignPhotographers`");
    }
}
