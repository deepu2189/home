<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchCategoryHasPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_category_has_packages', function (Blueprint $table) {
            $table->integer('id', true);           
            $table->integer('search_category_id');
            $table->integer('package_id');
            $table->timestamps();
             
            $table->foreign('search_category_id', 'fk_search_category_has_packages_search_categories')->references('id')->on('search_categories');
            //$table->foreign('package_id', 'fk_search_category_has_packages_packages')->references('id')->on('packages'); 
        });

      
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('search_category_has_packages', function (Blueprint $table) {
                 $table->dropForeign('fk_search_category_has_packages_search_categories');
                // $table->dropForeign('fk_search_category_has_packages_packages');
        });
        Schema::dropIfExists('search_category_has_packages');
    }
}
