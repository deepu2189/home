<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->integer('id', true);           
            $table->string('name', 256);
            $table->integer('preview_image_id')->nullable();            
            $table->string('short_description', 512)->nullable();
            $table->text('long_description', 65535)->nullable();
            $table->string('template_description')->nullable();
            $table->string('delivered_text', 256)->nullable();
            $table->float('price', 10);
            $table->integer('shooting_time')->default(3);
            $table->boolean('ask_price')->default(0);
            $table->integer('position')->default(0);
            $table->boolean('enabled')->default(1);
            $table->boolean('edit_assets')->default(true);
            $table->boolean('for_imported')->default(false);
            $table->integer('infusionsoft_id')->nullable()->default(null);            
            $table->string('package_type');
            $table->string('example_slug', 250)->nullable()->default(null);
            $table->float('payment_to_photographer', 10);
            $table->string('package_instructions', 512);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
