<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     * author @dpk
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('agent_id')->index('fk_properties_agents1_idx')->nullable();
            $table->integer('seller_id')->index('fk_properties_sellers1_idx')->nullable();
            $table->integer('order_id')->index('fk_properties_orders1_idx')->nullable();
            $table->string('title', 255)->nullable();
            $table->string('type', 128)->nullable();
            $table->string('address', 128)->nullable();
            $table->string('slug', 250)->unique();
            $table->string('zip', 128)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('state',255)->nullable();
            $table->integer('beds')->nullable();
            $table->integer('baths')->nullable();   
            $table->integer('half_baths')->nullable();
            $table->integer('area')->nullable();
            $table->float('price', 10)->nullable(); 
            $table->string('owner_phone', 255)->nullable();
            $table->string('owner_email', 255)->nullable();
            $table->integer('views_amount')->nullable();
            $table->boolean('published')->nullable();
            $table->string('map_longitude', 255)->nullable();
            $table->string('map_latitude', 255)->nullable();
            $table->longText('description')->nullable();            
            $table->integer('preview_id')->nullable();
            $table->boolean('agent_notified')->default(false);
            $table->dateTime('last_zip_online')->nullable();
            $table->dateTime('last_zip_print')->nullable();
            $table->boolean('imported')->default(false)->nullable();
            $table->boolean('media_created_notification')->default(false)->nullable();
            $table->integer('imported_id')->nullable();
            $table->string('redirect_url_mls',256)->nullable();
            $table->string('redirect_url_normal', 256)->nullable(); 
            $table->timestamps();                   

            // assigning forign keys 
            $table->foreign('agent_id', 'fk_properties_agents1')->references('id')->on('agents')->onUpdate('NO ACTION')->onDelete('NO ACTION');  
            $table->foreign('seller_id', 'fk_properties_sellers1')->references('id')->on('sellers')->onUpdate('NO ACTION')->onDelete('NO ACTION');          
            //$table->foreign('order_id', 'fk_properties_orders1')->references('id')->on('orders')->onUpdate('NO ACTION')->onDelete('NO ACTION');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
                $table->dropForeign('fk_properties_agents1');
                $table->dropForeign('fk_properties_sellers1');
                //$table->dropForeign('fk_properties_orders1');
        });
        Schema::dropIfExists('properties');
    }
}
