<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {


            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('profile_image_id')->nullable()->index('fk_users_images1_idx');
                $table->integer('profile_video_id')->nullable()->index('fk_users_video1_idx');
                $table->integer('logo_image_id')->nullable()->index('fk_users_logo_images1_idx');
                $table->string('firstname');
                $table->string('lastname');
                $table->string('slug');
                $table->string('email')->unique();
                $table->string('password', 60);
                $table->string('company_name')->nullable();
                $table->string('contact_phone', 256)->nullable();
                $table->string('assistants_emails', 255)->nullable();
                $table->text('contact_text', 65535)->nullable();
                $table->text('bio', 65535)->nullable();
                $table->enum('has_referral_amount', ['0', '1'])->default('0')->comment('0=> Off, 1=> On');
                $table->string('account_type', 45)->default('rea');
                $table->boolean('account_verified')->default(0);
                $table->string('remember_token', 100)->nullable();
                $table->string('confirmation_code')->nullable();
                $table->integer('imported_id')->nullable();
                $table->boolean('imported')->default(0);
                $table->integer('referral_amount')->nullable()->comment('Amount of referred sigunps');
                $table->integer('infusionsoft_id')->nullable();
                $table->string('stripe_id', 128)->nullable();
                $table->string('city')->nullable();
                $table->string('referral_code', 45)->nullable()->default(null);
                $table->string('referral_code_referenced', 45)->nullable()->default(null);
                $table->integer('commission_percent')->nullable();
                $table->decimal('current_wallet_amount', 11, 2)->default(0);
                $table->boolean('mortgage')->nullable();
                $table->timestamps();

                //$table->foreign('profile_image_id', 'fk_users_images1')->references('id')->on('images')->onUpdate('NO ACTION')->onDelete('NO ACTION');
                //$table->foreign('logo_image_id', 'fk_users_logo_images1')->references('id')->on('images')->onUpdate('NO ACTION')->onDelete('NO ACTION');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //$table->dropForeign('fk_users_images1');
            //$table->dropForeign('fk_users_logo_images1');
        });
        Schema::dropIfExists('users');
    }
}
