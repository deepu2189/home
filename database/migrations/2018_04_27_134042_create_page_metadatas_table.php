<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageMetadatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_metadatas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url', 256);
            $table->string('title', 256)->nullable()->default(null);
            $table->string('description', 256)->nullable()->default(null);
            $table->string('keywords', 256)->nullable()->default(null);
            $table->string('new_url', 256)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_metadatas');
    }
}
