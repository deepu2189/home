<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable();
            $table->integer('position');
            $table->timestamps();
        });
     
         DB::table('home_properties')->insert(
             array(
                            array('id' => 1, 'position' => 1),
                            array('id' => 2, 'position' => 2),
                            array('id' => 3, 'position' => 3),
                            array('id' => 4, 'position' => 4),
                            array('id' => 5, 'position' => 5),
                            array('id' => 6, 'position' => 6),
             )
         );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_properties');
    }
}
