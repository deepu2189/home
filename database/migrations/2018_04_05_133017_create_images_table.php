<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id')->unsigned()->index('fk_images_users1_idx');
            $table->string('type', 45);
            $table->string('original_name', 256);
            $table->string('app_name', 256);
            $table->string('extension', 45);
            $table->boolean('external_storage')->default(0);
            $table->string('external_url', 256)->nullable();
            $table->timestamps();

            // Add Foreign Key
            $table->foreign('user_id', 'fk_images_users1')->references('id')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropForeign('fk_images_users1');
        });
        Schema::dropIfExists('images');
    }
}
