<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateSearchCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_categories', function (Blueprint $table) {
            $table->integer('id', true);            
            $table->text('title');
            $table->text('subtitle');
            $table->timestamps();

        });

        $date = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->format(Config::get('app.datetime_format'));

        $subtitle = "Lore ipsum dolor sit amet, Lore ipsum dolor sit amet, Lore ipsum dolor sit amet, Lore ipsum dolor sit amet, Lore ipsum dolor sit amet.";
        $titles = ['VIDEO WALKTHROUGH', 'HDR PHOTOGRAPHY', 'AERIAL VIDEOS', '3D VIRTUAL TOURS'];
        $array = array();
        for ($i = 0; $i < 4; $i++) {
            $tmp = [
                'title' => $titles[$i],
                'subtitle' => $subtitle,
                'created_at' => $date,
                'updated_at' => $date
            ];
            array_push($array, $tmp);
        }

        DB::table('search_categories')->insert($array);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_categories');
    }
}
