<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

use Intervention\Image\Facades\Image;
use Carbon\Carbon;

/**
 *
 * @author Deepak Thakur
 * @since 0.1
 * @package HomeJab
 * @subpackage Models
 */
class ImageFile extends Model
{
     /**
     * Internal path
     */
    const PATH = 'assets/images';

    /**
     * Table name
     *
     * @var type
     */
    protected $table = 'images';
    
    /**
     * Sizes of images
     *
     * @var type
     */
    public static $sizes = [
        'original' => 'original',
        'small' => 'small',
        'tiny' => 'tiny'
    ];
    
    /**
     * Types of images
     *
     * @var type
     */
    public static $types = [
        'profile' => 'profile',
        'logo' => 'logo',
        'client'=> 'client'
    ];
    
    /**
     * Validation rules on store
     *
     * @var array
     */
    public static $rulesOnStore = [
        'type' => 'required|in:profile,logo,package,client',
        'original_name' => 'required',
    ];
    
    /**
     * Internal data storage
     *
     * @var type
     */
    private $data;

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];


    /**
     * The ImageFile belongs to a User
     *
     * @return type
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the base file path to the image. It depends on image type
     *
     * @return type
     */
    public function getBaseFilePath()
    {
        if (!$this->type || !$this->user_id) {
            return null;
        }

        $basePath = public_path(self::PATH . DIRECTORY_SEPARATOR . $this->type . DIRECTORY_SEPARATOR . $this->user_id. '/' .  $this->id);

        return $basePath;
    }

    /**
     * Get the base file url to the image. Ite depends on image type
     *
     * @return string
     */
    public function getBaseUrlPath()
    {
        if (!$this->type || !$this->user_id) {
            return null;
        }

        $baseUrl = self::PATH . '/' . $this->type . '/' . $this->user_id . '/' .  $this->id;
      
        return $baseUrl;
    }

    /**
     * Get the file path to the image based on size
     *
     * @param type $checkExists
     * @param type $size
     * @return string
     */
    public function getFilePath($checkExists = true, $size = 'original')
    {
        if (!$this->app_name || !$this->extension) {
            return null;
        }

        $path = $this->getBaseFilePath() . DIRECTORY_SEPARATOR . $this->app_name . '_' . $size . '.' . $this->extension;
        if ($checkExists && !file_exists($path)) {
            return null;
        }

        return $path;
    }

    /**
     * Get the file url to the image based on size
     *
     * @param type $size
     * @return type
     */
    public function getFileUrl($size = 'original')
    {




        if (!$this->app_name || !$this->extension) {
            return null;
        }

        $path = $this->getFilePath(true, $size);
        if (!$path) {
            return null;
        }
           
        return url($this->getBaseUrlPath() . '/' . $this->app_name . '_' . $size . '.' . $this->extension);
    }

    /**
     * Set image data of base64 file to be used at save
     *
     * @param type $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Get image data of base64 file to be used at save
     *
     * @param type $data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Initializes the model
     */
    public static function boot()
    {
        parent::boot();
        static::created(function ($image) {

            $filename   = $image->getFilePath(false, 'original');
            $dirname    = dirname($filename);
            //Create folder container for image
            if (!file_exists($dirname)) {
                mkdir($dirname, 0777, true);
            }
            //Put base64 data to real file
            file_put_contents($image->getFilePath(false, 'original'), $image->getData());

            // create instances for small and tiny
            $medium     = Image::make($image->getFilePath(false, 'original'));
            $small      = Image::make($image->getFilePath(false, 'original'));
            $tiny       = Image::make($image->getFilePath(false, 'original'));

            // resize the image to a width of 300 and constrain aspect ratio (auto height)
            $medium->resize(512, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $small->resize(256, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $tiny->resize(128, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            //Save image
            $small->save($image->getFilePath(false, 'medium'));
            $small->save($image->getFilePath(false, 'small'));
            $tiny->save($image->getFilePath(false, 'tiny'));
        });

        static::deleted(function ($image) {
            //Deletes entire basepath dir
            $path = $image->getBaseFilePath();
            if (is_dir($path) === true) {
                $files = glob($path . '/*'); // get all file names

                foreach ($files as $file) { // iterate files
                    if (is_file($file)) {
                        unlink($file); // delete file
                    }
                }

                return rmdir($path);
            } else if (is_file($path) === true) {
                return unlink($path);
            }
        });
    }

    /**
     * Accesor for created at date
     *
     * @param type $date
     * @return type
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param type $date
     * @return type
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Check if the given user is the owner of the image
     *
     * @param type $user
     * @return bool
     */
    public function isOwner($user)
    {
        return $this->user_id == $user->id;
    }
}
