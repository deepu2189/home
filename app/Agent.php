<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Event;
use App\Events\ReaOrSellerContactFormSubmit;

use Log;

/**
 *
 * @author Nicolas Taglienti <nicolas@serfe.com>
 * @author Nicolas Taglienti <nicolas@serfe.com>
 * @since 0.1
 * @package HomeJab
 * @subpackage Models
 */
class Agent extends Model
{


    public static $rulesOnSendContactEmail = [
        'email' => 'required|email',
        'userId' => 'required',
        //'propertyId' => 'required',
        'message' => 'required',
        'name' => 'required',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'agents';

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates    =   ['created_at', 'updated_at'];

    /**
     * The Agent belongs to a User
     *
     * @return type
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /*
     * Save buyer that has contacted the rea
     *
     */
    public static function registerBuyerOnInfusionSoft($data)
    {
        Event::fire(new ReaOrSellerContactFormSubmit($data));
    }

    /**
     * Get agent data by agent_id
     * @param integer $id
     * @param \HomeJab\Model\Agent
     */
    public static function getAgent($id)
    {
        return self::find($id);
    }

    /**
     * Get user_id by agent_id
     * @param integer $id
     * @return integer
     */
    public static function getUserId($id)
    {
        return self::query()->where('id', $id)->value('user_id');
    }


    public static function getAgentsVideos($last_mediafile_added, $limit)
    {
        Log::info('LAST ID ADDED', [$last_mediafile_added]);
        return self::query()
            ->join('users', 'users.id', '=', 'agents.user_id')
            ->where('users.profile_video_id', '>', $last_mediafile_added)
            ->orderBy('users.profile_video_id', 'ASC')
            ->limit($limit)
            ->get(['users.id','users.profile_video_id']);
    }

    public static function quantityAgentsWithVideo($last_mediafile_added)
    {
        return self::query()
            ->join('users', 'users.id', '=', 'agents.user_id')
            ->where('users.profile_video_id', '<>', 'NULL')
            ->where('users.profile_video_id', '>', $last_mediafile_added)
            ->orderBy('users.profile_video_id')
            ->count();
    }
}
