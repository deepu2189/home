<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class PageMetadata extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'page_metadatas';
    
    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    public static $rules = [
        'url'           => 'required'
    ];
    
        
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'title',
        'description',
        'keywords'
    ];
    
    protected $fillableOnUpdate = [
        'title',
        'description',
        'keywords'
    ];
    
    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
    
    /**
     * Add or Update page_metadata
     *
     * @param array $data
     * @return boolean
     */
    public static function addOrUpdatePageMetadata($data)
    {
        DB::beginTransaction();
        
        $page_metadata = PageMetadata::where('url', '=', $data['url'])->first();
        
        if (!$page_metadata) {
            $page_metadata = new PageMetadata();
        }
        
        $page_metadata->fill($data);
        
        if ($page_metadata->save()) {
            DB::commit();
            return $page_metadata;
        } else {
            DB::rollback();
            return false;
        }
    }
    
    /**
     * Returns the metadata for a page
     *
     * @param type $url
     * @return type
     */
    public function getByUrl($url)
    {
        $metadata = $this->where('url', $url)->first();
        if ($metadata) {
            return $metadata;
        } else {
            // return default values
            return $this->where('url', 'default')->first();
        }
    }
    
    /**
     * Search old property's url on metadata, and return it if it is found
     *
     * @param type $slug
     * @return boolean
     */
    public static function searchPropertyOldUrlBySlug($slug)
    {
        $new_url = PageMetadata::where('url', '=', '/property/view/'.$slug)->first(['new_url']);
                
        if ($new_url) {
            $explode = explode("/", $new_url->new_url);
            $new_slug = array_pop($explode);
            
            $property = Property::where('slug', '=', $new_slug)->first();
            
            if ($property) {
                return $property;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /**
     * Update metadata new url from all properties old urls
     *
     * @param type $old_slug
     * @param type $new_slug
     * @return boolean
     */
    public static function updateOldPropertyUrls($old_slug, $new_slug)
    {
        $metadata = PageMetadata::where('url', '=', '/property/view/'.$old_slug)
                ->orWhere('new_url', '=', '/property/view/'.$old_slug)
                ->get(['*']);
        
        foreach ($metadata as $meta) {
            $meta->new_url = '/property/view/'.$new_slug;
            $meta->save();
        }
        
        return true;
    }
    
    
    /**
     * Get all old url's of property by slug
     *
     * @param string $slug
     * @return boolean
     */
    public static function getAllOldUrlsBySlug($slug)
    {
        $old_urls = PageMetadata::where('new_url', '=', '/property/view/' . $slug)->get(['url']);
        $old_slugs = [];

        foreach ($old_urls as $url) {
            $explode = explode("/", $url->url);
            $slug = array_pop($explode);
            array_push($old_slugs, $slug);
        }

        return $old_slugs;
    }
}
