<?php
namespace App;

use App\Observers\CustomeObservers;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Mail;


class JabUsersPayment extends Model
{
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'Createdon';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'Updatedon';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'FullName', 'BankRoutingNo','AccountNo','UserId', 'CreatedBy', 'CreatedOn', 'UpdatedBy', 'UpdatedOn'
    ];

    public static $rulesOnUserPaymentDetails = [
        'FullName'        =>  'required',
        'BankRoutingNo'   =>  'required|numeric',
        'AccountNo'       =>  'required|numeric',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $table = "tbl_userspaymentinfo";

    protected $primaryKey = 'PaymentId';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new CustomeObservers(1));

        parent::boot();
    }
}
?>
