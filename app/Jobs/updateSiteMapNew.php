<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Roumen\Sitemap\Sitemap;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use App\Agent;
use App\Property;
use App\Mediafile;
use App\Package;

class updateSiteMapNew implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // create new sitemap object
        $dateTime = date('Y-m-d H:i:s');

        $sitemap = App::make("sitemap");

        // add items to the sitemap (url, datetime, priority, freq)
        $statucUrl = Config::get('app.static_url');
        if ($statucUrl[strlen($statucUrl)-1] == '/') {
            $statucUrl = substr($statucUrl, 0, strlen($statucUrl)-1);
        }
        $sitemap->add($statucUrl, $dateTime, '1.0', 'daily');
        $sitemap->add($statucUrl . '/prices', $dateTime, '0.9', 'monthly');
        $sitemap->add($statucUrl . '/properties/list', $dateTime, '0.9', 'monthly');
        $sitemap->add($statucUrl . '/register', $dateTime, '0.9', 'monthly');
        $sitemap->add($statucUrl . '/login', $dateTime, '0.9', 'monthly');
        $sitemap->add($statucUrl . '/frequently-asked-questions', $dateTime, '0.9', 'monthly');
        $sitemap->add($statucUrl . '/jobs', $dateTime, '0.9', 'monthly');
        $sitemap->add($statucUrl . '/faq', $dateTime, '0.9', 'monthly');
        $sitemap->add($statucUrl . '/sitemap', $dateTime, '0.9', 'monthly');
        $sitemap->add($statucUrl . '/3D', $dateTime, '0.9', 'monthly');

        $sitemap->store('xml', 'sitemaps/sitemap-general');
        $sitemap->addSitemap(secure_url('sitemaps/sitemap-general.xml'));
        $sitemap->model->resetItems();

        // get all agents from db
        $agents = Agent::orderBy('created_at', 'desc')->get();

        // add every agent to the sitemap
        foreach ($agents as $agent) {
            $sitemap->add($statucUrl . '/agent/' . $agent->id, $agent->updated_at, '0.9', 'monthly');
        }

        $sitemap->store('xml', 'sitemaps/sitemap-agents');
        $sitemap->addSitemap(secure_url('sitemaps/sitemap-agents.xml'));
        $sitemap->model->resetItems();

        $packages = Package::orderBy('created_at', 'desc')->get();
        // add every package to the sitemap
        foreach ($packages as $package) {
            $sitemap->add($statucUrl . '/order/package/' . $package->id, $package->updated_at, '0.9', 'monthly');
        }

        $sitemap->store('xml', 'sitemaps/sitemap-packages');
        $sitemap->addSitemap(secure_url('sitemaps/sitemap-packages.xml'));
        $sitemap->model->resetItems();

        // get all properties from db
        $properties = Property::orderBy('created_at', 'desc')->where('published', '=', true)->where('imported', '=', false)->get();

        $linksCount = 0;
        $sitemapCounter = 0;

        // add every property to the sitemap
        foreach ($properties as $property) {
            $images = [];
            $videos = [];
            if ($linksCount >= 30000) {
                // generate new sitemap file
                $sitemap->store('xml', 'sitemaps/sitemap-properties-'.$sitemapCounter);
                // add the file to the sitemaps array
                $sitemap->addSitemap(secure_url('sitemaps/sitemap-properties-'.$sitemapCounter.'.xml'));
                // reset items array (clear memory)
                $sitemap->model->resetItems();
                // reset the counter
                $linksCount = 0;
                // count generated sitemap
                $sitemapCounter++;
            }

            if ($property->imported) {
                $URL['redirect_url_mls'] = Config::get('app.old_domain_mls_seo') . $property->redirect_url_mls;
                //$URL['redirect_url_normal'] = Config::get('app.old_domain_seo') . $property->redirect_url_normal;

                $mediafiles_ids = Property::find($property->id)->mediafiles()->allRelatedIds();
                $mediafiles = Mediafile::whereIn('id', $mediafiles_ids)->where('published', '=', true)->get();


                foreach ($mediafiles as $mediafile) {
                    if (isset($mediafile->external_url) && $mediafile->external_url != '') {
                        if ($mediafile->type == Mediafile::$videoType) {
                            $videos[] = array(
                                'url' => $mediafile->external_url,
                                'title' => $this->purgeXmlString($mediafile->original_name),
                                'caption' => $this->purgeXmlString($mediafile->original_name),
                                'description' => $this->purgeXmlString($mediafile->original_name)
                            );
                        } elseif ($mediafile->type == Mediafile::$TreeDTourType) {
                            //do not include 3D videos
                        } else {
                            $images[] = array(
                                'url' => $mediafile->external_url,
                                'title' => $this->purgeXmlString($mediafile->original_name),
                                'caption' => $this->purgeXmlString($mediafile->original_name),
                                'description' => $this->purgeXmlString($mediafile->original_name)
                            );
                        }
                    }
                }
                $sitemap->add($URL['redirect_url_mls'], $property->updated_at, '0.9', 'daily', $images, [], [], $videos);
                $linksCount++;
                $linksCount+=count($videos);
                $linksCount+=count($images);
                //$sitemap->add($URL['redirect_url_normal'], $property->updated_at, '0.9', 'daily', $images, [], [], $videos);
                //$linksCount++; $linksCount+=count($videos); $linksCount+=count($images);
            } else {

                $mediafiles_ids = Property::find($property->id)->mediafiles()->allRelatedIds();
                $mediafiles = Mediafile::whereIn('id', $mediafiles_ids)->where('published', '=', true)->get();

                foreach ($mediafiles as $mediafile) {
                    if (isset($mediafile->external_url) && $mediafile->external_url != '') {
                        if ($mediafile->type == Mediafile::$videoType) {
                            $video = array(
                                'url' => $mediafile->external_url,
                                'title' => $this->purgeXmlString($property->title . " - " . $mediafile->original_name),
                                'caption' => $this->purgeXmlString($property->title. " - " . $mediafile->original_name),
                                'description' => $this->purgeXmlString("Videofile: ".$mediafile->original_name . " - " . $property->description)
                            );
                            $metadata = json_decode($mediafile->metadata, true);
                            if (array_key_exists('thumbnail', $metadata)) {
                                $video['thumbnail_loc'] = $this->purgeXmlString($metadata['thumbnail']);
                            }
                            if (array_key_exists('profiles', $metadata) && count($metadata['profiles']) > 0) {
                                $temp = array_pop($metadata['profiles']);
                                $video['content_loc'] = $this->purgeXmlString($temp['url']);
                            }
                            /*if (array_key_exists('embebed', $metadata)) {
                                $temp = array_pop($metadata['embebed']);
                                $video['player_loc'] = $this->purgeXmlString($temp['url']);
                            }*/
                            $videos[] = $video;
                        } elseif ($mediafile->type == Mediafile::$TreeDTourType) {
                            //do not include 3D videos
                        } else {
                            $images[] = array(
                                'url' => $mediafile->external_url,
                                'title' => $this->purgeXmlString($property->title . " - " . $mediafile->original_name),
                                'caption' => $this->purgeXmlString($property->title . " - " . $mediafile->original_name),
                                'description' => $this->purgeXmlString("Videofile: ".$mediafile->original_name . " - " . $property->description)
                            );
                        }
                    }
                }




                $sitemap->add($statucUrl . '/property/view/' . $property->slug, $property->updated_at, '0.9', 'daily', $images, [], [], $videos);
                $linksCount++;
                $linksCount+=count($videos);
                $linksCount+=count($images);
                //$sitemap->add(Config::get('app.mls_url') . '/property/view/' . $property->slug, $property->updated_at, '0.9', 'daily', $images, [], [], $videos);
                //$linksCount++; $linksCount+=count($videos);$linksCount+=count($images);


            }



        }





        // you need to check for unused items
        if (!empty($sitemap->model->getItems())) {
            // generate sitemap with last items
            $sitemap->store('xml', 'sitemaps/sitemap-properties-'.$sitemapCounter);
            // add sitemap to sitemaps array
            $sitemap->addSitemap(secure_url('sitemaps/sitemap-properties-'.$sitemapCounter.'.xml'));
            // reset items array
            $sitemap->model->resetItems();
        }
        // generate your sitemap (format, filename)
        $sitemap->store('sitemapindex', 'sitemap');



        // this will generate file sitemap.xml to your public folder
    }


    /**
     * Delete all non printable characters
     *
     * @param type $string
     * @return type
     */
    private function purgeXmlString($string)
    {
        return  preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $string);
    }
}
