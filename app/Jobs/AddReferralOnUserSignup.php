<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Config;
use App\User;
use App\UserReferral;
use App\WalletTransaction;
use Carbon\Carbon;


class AddReferralOnUserSignup implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * User that has registered and need to send email
     *
     * @var \App\User
     */
    protected $code;
    
    /**
     *
     * @var array
     */
    protected $registered_user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($code, $registered_user_id)
    {
        $this->code = $code;
        $this->registered_user_id = $registered_user_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userReferral = User::where('referral_code', '=', $this->code)
                            ->where('account_verified', '=', 1)
                            ->get(['has_referral_amount', 'referral_amount', 'account_verified', 'referral_code', 'id'])
                            ->first();
        
        if ($userReferral) {
            $referralId = $userReferral->id;

            if ($userReferral->has_referral_amount == 0) {
                $referral_amount = 0.0;
            } else {
                if (empty($userReferral->referral_amount) || is_null($userReferral->referral_amount)) {
                    $referral_amount = User::getReferralAmountSettings();
                } else {
                    $referral_amount = $userReferral->referral_amount;
                }
            }


            if ($this->registered_user_id != $referralId) {
                if (UserReferral::addReferralSignup(
                    $this->registered_user_id,
                    $referralId,
                    $referral_amount
                )) {

                    //Send notification email that user has been signed up to homejab referral code
                    $userSignup = User::find($this->registered_user_id);
                    if (!is_null($userSignup)) {
                        //$job = (new SendReferralNotificationEmail($userReferral, $userSignup))->onQueue(Config::get('aws.sqs.email'));
                        //$this->dispatch($job);
                        
                        dispatch((new SendReferralNotificationEmail($userReferral, $userSignup))->onQueue(Config::get('aws.sqs.email')));


                        //Send notification email that wallet amount has been credited
                        if ($referral_amount > 0.0) {
                            $walletData = array(
                                'type' => WalletTransaction::$referralSignupCommissionType,
                                'amount' => $referral_amount
                            );

                         //  $job2 = (new SendEmailToCreditOwner($userReferral, $walletData, $userSignup))->onQueue(Config::get('aws.sqs.email'));
                         //  $this->dispatch($job2);

                        dispatch((new SendEmailToCreditOwner($userReferral, $walletData, $userSignup))->onQueue(Config::get('aws.sqs.email')));

                        }
                    }
                }
            }
        }
    }
}
