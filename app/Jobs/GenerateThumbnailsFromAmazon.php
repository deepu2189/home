<?php

namespace App\Jobs;

use App\Mediafile;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\App;

use ErrorException;
use Exception;
use Log;

use Illuminate\Support\Facades\Config;

/**
 * Background job that downloads the file from amazon
 * and generate the corresponding thumbnails
 *
 * @author Nicolas Taglienti <nicolas@serfe.com>
 * @since 0.7
 * @package HomeJab
 * @subpackage eWarpIntegration
 */
class GenerateThumbnailsFromAmazon extends Job implements ShouldQueue
{
    
    use InteractsWithQueue, SerializesModels;
    

    /**
     * Mediafile model
     *
     * @var MediaFile
     */
    private $mediafile = null;

    /**
     * Property Id
     *
     * @var integer
     */
    private $propertyId = null;
    
    /**
     *
     * @var boolean
     */
    private $homePicture = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mediafile, $propertyId, $homePicture = false)
    {
        //set initial data
        $this->mediafile = $mediafile;
        $this->propertyId = $propertyId;
        $this->homePicture = $homePicture;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
                if (Mediafile::generateMediaFileThumbnailFromAmazon($this->mediafile, $this->propertyId, $this->homePicture)) {
                return true;
            } else {
                return false;
            }
        } catch (\Intervention\Image\Exception\NotReadableException $e) {
            $basePath       = public_path('temp' . DIRECTORY_SEPARATOR);
            $path           = $basePath . $this->mediafile->original_name;
            if (file_exists($path)) {
                unlink($path);
            }
            
            Log::error("Mediafile uploaded on incorrect type section");
            Log::error($e->getMessage());
            return true;
        }
    }
}
