<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

use Mail;

/**
 * Sends an email notification that a new referral was created
 *
 * @author Esteban Zeller <esteban@serfe.com>
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.2.0
 * @package HomeJab
 * @subpackage Jobs
 */
class SendReferralNotificationEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Propertythat has registered and need to send email
     *
     * @var \HomeJab\Models\User
     */
    protected $userReferral;
    
    /**
     * Amount of credit granted on the referral
     * @var float
     */
    // protected $amount;


    /**
    * User login with referral code
    */
    protected $userSignup;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\User $userReferral, $userSignup)
    {
        $this->userReferral = $userReferral;
        $this->userSignup = $userSignup;
    }

    /**
     * Execute the job.
     *
     * Send an email to the user for account verified
     *
     * @return void
     */
    public function handle()
    {
        $to = $this->userReferral->email;
        $data = array(
            'username' => $this->userReferral->firstname . ' ' . $this->userReferral->lastname,
            'user_signup' => $this->userSignup,
        );
        Mail::send('emails.user.referral_email', $data, function ($message) use ($to) {
            $message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
            $message->to($to)->subject(trans('emails.referral_email.subject'));
        });
    }
}
