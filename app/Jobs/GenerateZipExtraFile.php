<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use App\Transformers\AWSPathTransformer;
use App\Order;
use App\Mediafile;   
use ZipArchive;
use Codeception\Util\FileSystem;
use Storage;
use Log;

use Carbon\Carbon;

/**
 * Generates a compressed file for the property extrafiles.
 * Only applies to SU.
 * @author Deepak Thakur
 * @package HomeJab
 * @subpackage Jobs
 */

class GenerateZipExtraFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Property where the files are neede of
     * @var integer
     */
    public $order_id = null;

    /**
     * User wich requested the file
     * @var type
     */
    public $user_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_id, $user_id)
    {
        $this->order_id = $order_id;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $category = Mediafile::$categoryExtraFiles;
        $order = Order::find($this->order_id);
            // Generate zip filename
            $zip_filename = $this->order_id.'-Extra-Files'.'.zip';
            // Check if the file exists on amazon
            $this->amazons3 = App::make('aws')->createClient('s3', ['region' => Config::get('aws.credentials.region')]);

            $aws_keyname = AWSPathTransformer::getCompressedExtraFilesPath($zip_filename);
        if (!$this->amazons3->doesObjectExist(Config::get('aws.credentials.bucket'), $aws_keyname)) {
            $amazon_zip_path = $this->generateZipExtraFile($zip_filename, $order, $aws_keyname);
        } else {
            $amazon_zip_path = $this->generateZipExtraFile($zip_filename, $order, $aws_keyname);
        }
            // Send the email
        if (intval($this->user_id) > 0) {
            $email_job = new SendZipReadyEmail(
                $amazon_zip_path,
                $this->user_id,
                $category,
                $this->order_id
            );
            $email_job->onQueue(Config::get('aws.sqs.email'));
            $this->dispatch($email_job);
        }
        return true;
    }

    /**
     * Generates correctly the file with all the elements
     *
     * @param string $zip_filename
     * @param \HomeJab\Models\Property $property
     * @param string $aws_keyname
     * @throws Exception
     * @throws ErrorCopyFileToAmazonException
     * @return string AWS S3 url for download
     */
    private function generateZipExtraFile($zip_filename, $order, $aws_keyname)
    {
        $extrafiles = $order
                ->extraFiles()
                ->get(['external_url', 'metadata']);
        if (count($extrafiles) <= 0) {
            // There is no pictures to make for this job. Release to make it afterwards
            Log::info("No extra files for the property ".$order->id." to generate zip from. Will retry in 30 seconds");
            $this->release(30);
            return true;
        }

        // Define the temporal folder where the files will be stored
        $storage_folder = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR;

        $download_folder = $this->order_id;//.$this->category;
        if (!Storage::disk('local')->makeDirectory($download_folder)) {
            Log::error("Could not genetate the temporal download folder for the property");
            throw new \Exception("Could not genetate the temporal download folder for the property");
        }

        $download_list = [];

        // Iterate over the files
        foreach ($extrafiles as $extrafile) {
            $original_url = $extrafile->external_url;

            $filename = $this->getFileName($original_url);
            if (env('APP_ENV') === 'local' || env('APP_ENV') === 'testing') {
                $download_list[] = $storage_folder.DIRECTORY_SEPARATOR.'.gitignore';
            } else {
                try {
                    // Download the file from the remote URL
                    $contents = false;
                    $client = new \GuzzleHttp\Client();
                    $contents = $client->request('GET', $original_url);

                    if ($contents === false) {
                        Log::error("There was an error triying to download the file : " . $original_url);
                        throw new \Exception("There was an error triying to download the file : " . $original_url);
                    }

                    // Save the file to a location
                    $download_list[] = $storage_folder.$download_folder.DIRECTORY_SEPARATOR.$filename;
                    Storage::disk('local')->put($download_folder.DIRECTORY_SEPARATOR.$filename, $contents->getBody()->getContents());
                } catch (ErrorException $e) {
                    // Could not open the file
                    Log::error($e->getMessage());
                    throw new \Exception("Could not copy the file from the original location. Check error log.".$e->getMessage());
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    Log::error($e->getMessage());
                    throw new \Exception("Could not download the file from the original location. Client response was: ".$e->getMessage());
                } catch (\GuzzleHttp\Exception\TransferException $e) {
                    Log::error($e->getMessage());
                    throw new ErrorCopyFileToAmazonException("Could get the file from the original location. Check error log.".$e->getMessage());
                } catch (\GuzzleHttp\Exception\ConnectException $e) {
                    Log::error($e->getMessage());
                    throw new ErrorCopyFileToAmazonException("Could get the file from the original location. Check error log.".$e->getMessage());
                }
            }
        }
        unset($extrafiles);

        // ZIP Filename - Defined in requerimient

        $zip_filename_path = $storage_folder . $zip_filename;
        // Create the Zipped file
        $zipper = new ZipArchive();
        if ($zipper->open($zip_filename_path, ZIPARCHIVE::CREATE) !== true) {
            Log::error("Could not create the zip file: ".$zipper->getStatusString());
            throw new \Exception("Could not create the zip file: ".$zipper->getStatusString());
        }
        if (count($download_list) <= 0) {
            Log::error("There is no file downloaded to be compressed!");
            throw new \Exception("There is no file downloaded to be compressed!");
        }
        foreach ($download_list as $file) {
            $filename = $this->getFileName($file);
            if (!file_exists($file)) {
                Log::error("Could not find the file ". $file. " to add to the zip file");
                throw new \Exception("Could not find the file ". $file. " to add to the zip file");
            }
            if (!is_readable($file)) {
                Log::error("Could not read the file ". $file. " to add to the zip file");
                throw new \Exception("Could not read the file ". $file. " to add to the zip file");
            }
            if (!$zipper->addFile($file, $filename)) {
                Log::error("Could not add the file ". $filename. " to the zip file");
                throw new \Exception("Could not add the file ". $filename. " to the zip file");
            }
        }
        if ($zipper->numFiles <= 0) {
            Log::error("Empty zip file!");
            throw new \Exception("Empty zip file!");
        }
        if (!$zipper->close()) {
            Log::error("Could not create the zip file (close): ".$zipper->getStatusString());
            throw new \Exception("Could not create the zip file (close): ".$zipper->getStatusString());
        }
        unset($zipper);
        unset($download_list);
        // Remove the temporal folder
        if (Storage::disk('local')->deleteDirectory($download_folder) !== true) {
            Log::error("Could not delete the temporal download folder location");
            throw new \Exception("Could not delete the temporal download folder location");
        }

        $amazon_zip_path = null;
        try {
                $upload = $this->amazons3->upload(
                    Config::get('aws.credentials.bucket'),
                    $aws_keyname,
                    fopen($zip_filename_path, "r"),
                    'public-read'
                    // Posibble permissions found in documentation:
                    // private | public-read | public-read-write | aws-exec-read
                    // | authenticated-read | bucket-owner-read
                    // | bucket-owner-full-control
                );
            $amazon_zip_path = $upload['ObjectURL'];
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw new ErrorCopyFileToAmazonException("Could not upload the file to the final location. Check error log.");
        }

        $order->save();

        // If upload was successfully, remove the zip file to save space
        if (!Storage::disk('local')->delete($zip_filename)) {
            Log::error("Could not delete the temporal file: ". $zip_filename_path);
            throw new \Exception("Could not delete the temporal file: ". $zip_filename_path);
        }

        return $amazon_zip_path;
    }

    /**
     * Extracts the filename from a path
     *
     * @param string $file_path
     * @return string
     */
    private function getFileName($file_path)
    {
        $parts = preg_split('/\//', $file_path);
        $filename = array_pop($parts);
        $filename = urldecode($filename);
        return $filename;
    }
}
