<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Mediafile;
use App\JobHasMediaFile;
use App\PropertyHasMediaFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Log;

/**
 * Removes failed uploaded files from the database and amazon s3 storage
 *
 * @author Deepak Thakur
 * @since 0.8
 * @package HomeJab
 * @subpackage Jobs
 */

class DeleteMediafilesById implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * List of mediafiles to remove
     *
     * @var array
     */
    public $mediafile_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mediafile_id)
    {
        $this->mediafile_id = $mediafile_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $mediaFile = Mediafile::withTrashed()->find($this->mediafile_id);

        if ($mediaFile) {
            if ($mediaFile->key == '') {
                return true;
            }

           // DB::reconnect();
            DB::beginTransaction();

            $amazons3 = App::make('aws')->createClient('s3', ['region' => Config::get('aws.credentials.region')]);

            $metadata = json_decode($mediaFile->metadata, true);

            $objectKeys = [];
            $objectKeys[] = ['Key' => $mediaFile->key];
            $transformer = new \App\Transformers\AWSPathTransformer();

            if (isset($metadata['resolutions'])) {
                foreach ($metadata['resolutions'] as $mediaFile) {
                    if (isset($mediaFile['url'])) {
                        $url = $transformer->getValuesFromUrl($mediaFile['url']);
                        //generate keys without "https://s3-eu-west-1.amazonaws.com/alfa.homejab/"
                        $key = $url['Key'];
                        $objectKeys[] = ['Key' => $key];
                    }
                }
            }

            if ($amazons3->deleteObjects([
                    'Bucket' => Config::get('aws.credentials.bucket'),
                    'Delete' => [
                        'Objects' => $objectKeys
                    ]
                ])) {
            } else {
                Log::error("Could not delete the amazon key " . $key);
                DB::rollback();
                return false;
            }

            JobHasMediaFile::where('mediafile_id', '=', $this->mediafile_id)->delete();
            PropertyHasMediaFile::where('mediafile_id', '=', $this->mediafile_id)->delete();

            if (MediaFile::withTrashed()->where('id', '=', $this->mediafile_id)->forceDelete()) {
                DB::commit();
                return true;
            } else {
                DB::rollback();
                Log::error('Could not delete the mediafile #'.$this->mediafile_id);
                return false;
            }
        }
    }
}
