<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Config;
use Mail;

/**
 * Sends an email notification when an upload file fail
 *
 * @author Deepak Thakur
 * @since 0.2
 * @package HomeJab
 * @subpackage Jobs
 */
class SendUploadFailedNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Propertythat has registered and need to send email
     *
     * @var \App\Property
     */
    protected $job_id;
    /**
     * Propertythat has registered and need to send email
     *
     * @var \App\Property
     */
    protected $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($job_id, $data)
    {
        $this->job_id = intval($job_id);
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * Send an email to the user for account verified
     *
     * @return void
     */
    public function handle()
    {
        $job = \App\AllJob::findOrFail($this->job_id);
            
        // Get the order user creator

        if ($job->photographer_id) {
                $hjph = \App\Photographer::find($job->photographer_id);
                $user = \App\User::find($hjph->user_id);
                
                $hjph_email = $user->email;
                $hjph_name = $user->firstname . ' ' . $user->lastname;
                
                Mail::send(
                    'emails.job.upload_failed',
                    [
                        'job' => $job,
                        'order_number' => $job->order_id,
                        'app_static_url' => Config::get('app.static_url'),
                        'body_url'  => 'backend/photographer/upload',
                    ],
                    function ($m) use ($hjph_email, $hjph_name) {
                        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                        $m->to(
                            $hjph_email,
                            $hjph_name
                        )->subject(trans('emails.upload_failed.subject'));
                    }
                );
        }
        
        
        $supportEmail =  env('QUEUE_FAILED_NOTIFICATION_EMAIL', 'serfepruebas@gmail.com');
        
        $reponse = $this->data;
        
        Mail::send(
            'emails.job.upload_failed_support',
            [
                        'job' => $job,
                        'order_number' => $job->order_id,
                        'app_static_url' => Config::get('app.static_url'),
                        'body_url'  => 'backend/photographer/upload',
                        'data'  => $reponse,
                    ],
            function ($m) use ($supportEmail, $hjph_name) {
                        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                        $m->to(
                            $supportEmail,
                            $hjph_name
                        )->subject(trans('emails.upload_failed.subject'));
            }
        );
    }
}
