<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Property;
use Illuminate\Support\Facades\Config;

use Mail;

/**
 * Sends an email notification that a property was created
 *
 * @author Deepak Thakur
 * @since 0.2
 * @package HomeJab
 * @subpackage Jobs
 */

class SendPropertyCreated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
/**
     * Propertythat has registered and need to send email
     *
     * @var \App\Property
     */
    protected $property;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Property $property)
    {
        $this->property = $property;
    }

    /**
     * Execute the job.
     *
     * Send an email to the user for account verified
     *
     * @return void
     */
    public function handle()
    {
        if ($this->property->agent_notified) {
            // This avoids sending twice the notification
            return true;
        }
        
        // Get the order user creator
        $agent_email = $this->property->Agent->User->email;
        $agent_name = $this->property->Agent->User->getFullNameAttribute();

        Mail::send(
            'emails.property.property_created',
            [
                'property' => $this->property,
                'app_static_url'=> Config::get('app.static_url'),
                'mls_url' => Config::get('app.mls_url')
            ],
            function ($m) use ($agent_email, $agent_name) {
                $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $m->to(
                    $agent_email,
                    $agent_name
                )->subject(trans('emails.property_created.subject'));
            }
        );
        $assistants =  $this->property->Agent->User->getAssistantsEmails();
        //mail to the agent assistants
        if (count($assistants) > 0) {
            foreach ($assistants as $assistant) {
                Mail::send('emails.property.property_created', [
                'property' => $this->property,
                'app_static_url' => Config::get('app.static_url'),
                'mls_url' => Config::get('app.mls_url')
                    ], function ($m) use ($assistant) {
                        $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                        $m->to($assistant)->subject(trans('emails.property_created.subject'));
                    });
            }
        }
        
        $this->property->agent_notified = true;
        $this->property->published = true;
        $this->property->save();
        return true;
    }
}
