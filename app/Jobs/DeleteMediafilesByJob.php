<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Mediafile;
use App\JobHasMediaFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Log;

/**
 * Removes failed uploaded files from the database and amazon s3 storage
 *
 * @author Deepak Thakur
 * @since 0.4
 * @package HomeJab
 * @subpackage Jobs
 */

class DeleteMediafilesByJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

     /**
     * Job Id that will have it's mediafiles removed
     * @var integer
     */
    public $job_id = null;

    /**
     *
     * @var array
     */
    public $mediafiles = [];
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($job_id, $mediafiles)
    {
        $this->job_id = $job_id;
        $this->mediafiles = $mediafiles;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->mediafiles as $mediafile_relation) {
            $mediafile_id = $mediafile_relation->mediafile_id;

            $jobHasMediaFile = JobHasMediaFile::find($mediafile_relation->id);
            
            if (!$jobHasMediaFile) {
                // May be was deleted on another iteration
                continue;
            }

            DB::beginTransaction();

            try {
                if (!$jobHasMediaFile->where('mediafile_id', $mediafile_id)->delete()) {
                    DB::rollback();
                    Log::error("Could not delete the relation between the mediafile ".$mediafile_id." and the job from the database.");
                    throw new \Exception("Could not delete the Mediafile ".$mediafile_id." from the database");
                }


                $mediaFile = Mediafile::find($mediafile_id);
                $key = $mediaFile->key;
                if (!$mediaFile->delete()) {
                    DB::rollback();
                    Log::error("Could not delete the mediafile #".$mediafile_id." from the database.");
                    throw new \Exception("Could not delete the Mediafile ".$mediafile_id." from the database");
                }

                if (env('APP_ENV') !== 'testing' && env('APP_ENV') !== 'local') {
                    $amazons3 = App::make('aws')->createClient(
                        's3',
                        [
                            'region' => Config::get('aws.credentials.region'),
                        ]
                    );

                    $amazons3->deleteObjects([
                        'Bucket' => Config::get('aws.credentials.bucket'),
                        'Delete' => [
                            'Objects' => [
                                ['Key' => $key]
                            ]
                        ]
                    ]);

                    if ($amazons3->doesObjectExist(Config::get('aws.credentials.bucket'), $key)) {
                        Log::error("Could not delete the amazon key ".$key);
                    }
                }

                DB::commit();
            } catch (\Exception $e) {
                throw new \Exception("Could not delete the Mediafile ".$mediafile_id." - Exception: ".$e->getMessage());
            }
        }
        
        return true;
    }
}
