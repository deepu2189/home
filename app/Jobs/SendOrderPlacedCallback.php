<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use App\CallbackLog;
use App\CallbackStatus;
use App\Order;
use App\Package;
use Log;

/**
 * This callback will be called when an order is placed.
 *
 * @author Deepak Thakur
 * @since 1.0
 * @package HomeJab
 * @subpackage Jobs
 */

class SendOrderPlacedCallback implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Order identifier
     * @var type
     */
    public $order_id = null;

    /**
     * Transformer for the order data into callback data
     * @var \HomeJab\Transformers\OrderCallbackTransformer
     */
    private $OrderCallbackTransformer = null;


    /**
     * Url to send data
     * @var
     */
    private $url = null;

    /**
     * Flag to double callback
     * @var
     */
    public $double_callback = null;

    /**
     * Flag to order 3D
     * @var
     */
    public $order_3d = null;

    /**
     * Data to send callback
     * when order is 3D.
     * @var
     */
    public $data = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_id, $double_callback = false, $order_3d = false, $data = null)
    {
        $this->order_id = $order_id;
        $this->OrderCallbackTransformer = new \App\Transformers\OrderCallbackTransformer();
        $this->url = Config::get('app.callback_url');
        $this->double_callback = $double_callback;
        $this->order_3d = $order_3d;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!Config::get('app.callback_url')) {
            if ($this->attempts() >= 4) {
                Log::error("Callback for job #".$this->order_id." was not sent because the CALLBACK_URL is not configured. Giving up.\n");
                return true;
            }
            throw new \Exception("Callback url is not configured. Check CALLBACK_URL on environment.");
        }

        $callback_type=CallbackStatus::$typeOrderPlaced;

        if ($this->order_3d && $this->double_callback && !is_null($this->data)) {
            $this->sendCallback($this->data);
            return;
        } else {
            $order = Order::find($this->order_id);
            if (!$order) {
                throw new \Exception("Order #".$this->order_id." not found when trying to send order data on callback");
            }

            //*****Verify if package is 3D***//
            $assets = $order->package->assetTypes;
            $package_3d = false;
            $have_addons = false;
            $double_callback = (boolean) DB::table('settings')->where('text', '=', 'double_callback_enabled')->get(['value'])[0]->value;

            $task = "";
            $task3D = "";

            foreach ($assets as $asset) {
                if ($asset->enabled_3d) {
                    $package_3d = true;
                    $task3D = $asset->name;
                } else {
                    if (empty($task)) {
                        $task = $asset->name;
                    } else {
                        $task = $task . " - " . $asset->name;
                    }
                }
            }

            if (count($assets) > 1) {
                $have_addons = true;
            }

            if ($package_3d && $have_addons && $double_callback) {
                $data1 = $this->OrderCallbackTransformer->transformData($order, $package_3d, $task, $callback_type);
                $this->sendCallback($data1, $callback_type);
                $data2 = $this->OrderCallbackTransformer->transformData($order, $package_3d, $task3D, $callback_type);
                $callback3D = new SendOrderPlacedCallback($this->order_id, $double_callback, $package_3d, $data2);
                $callback3D->onQueue(Config::get('aws.sqs.default'));
                $this->dispatch($callback3D);
            } else {
                if ($package_3d) {
                    if ($have_addons) {
                        $task = $task3D . " - " . $task;
                    } else {
                        $task = $task3D;
                    }
                }
                $data = $this->OrderCallbackTransformer->transformData($order, $package_3d, $task, $callback_type);
                $this->sendCallback($data, $callback_type);
            }
        }
    }

     /**
    * Send callback Schedulling app
    */
    private function sendCallback($data, $callback_type)
    {
        //file_put_contents('order_data.txt',print_r($data,true));die;

        $order = Order::find($this->order_id);

        if (!$order) {
            throw new \Exception("Order #" . $this->order_id . " not found when trying to send order data on callback");
        }

        $callback_status = CallbackStatus::where('order_id', '=', $this->order_id)->where('type', '=', $callback_type)->first();
        $callback_status_id = $callback_status['id'];

        if (!$callback_status) {
            $callback_status = CallbackStatus::addCallbackStatus($this->order_id, $callback_type);
            $callback_status_id = $callback_status['callback_status_id'];
        }
        //Send Data
        $client = new \GuzzleHttp\Client();
        $response = null;
        try {
           $response = $client->request('POST', $this->url, ['json' => $data]);
           //echo"<pre>";print_r($response->getBody()->getContents());die;
            //file_put_contents('order_response.txt',print_r($response->getBody()->getContents(),true));die;
            //echo"<pre>";print_r($response->getBody()->getContents());die;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            Log::error("Error in client request for callback when order #".$this->order_id." was placed");
            Log::error($e->getMessage());
            Log::error(print_r($response, true));
        }

        $this->logCallback($response, $data, $callback_type, $callback_status_id);

        if (!is_null($response) && ($response->getStatusCode() == 200 || $response->getStatusCode() == 201)) {
            // Store the response in the order itself
            $callback_sent = true;
            $callback_success = (new \DateTime())->format('Y-m-d H:i:s');
            CallbackStatus::updateCallbackStatus(
                $this->order_id,
                $callback_type,
                $callback_sent,
                $callback_success
            );

            return $response->getStatusCode();
        } else if ($this->attempts() < 5) {
            // make time interval
            $seconds = 0;
            for ($i = 1; $i <= $this->attempts(); $i++) {
                $seconds += $i * 300;
            }

            if ($seconds) {
                $this->release($seconds);
            } else {
                // never will enter here, but to done to avoid errors
                $this->release(300);
            }
        } else if ($this->attempts() >= 5) {
            $callback_sent = false;
            $callback_success =null;
            $callback_failed_at = (new \DateTime())->format('Y-m-d H:i:s');
             CallbackStatus::updateCallbackStatus(
                 $this->order_id,
                 $callback_type,
                 $callback_sent,
                 $callback_success,
                 $callback_failed_at
             );
            if (!$order->save()) {
                Log::error("Could not update the job status for the order #".$this->order_id." - Plase do it manually");
            }
            throw new \Exception("Error in client request for callback when order #".$this->order_id." was placed. Giving up.");
        }
    }

    /**
     * Stores in the log model
     * @param type $response
     * @param type $original_data
     * @param type $callback_type
     * @param type $callback_status_id
     */
    private function logCallback($response, $original_data, $callback_type, $callback_status_id)
    {
        if (!is_null($response)) {
            CallbackLog::addCallbackLog(
                $this->order_id,
                [
                    'response' => [
                        'code' => $response->getStatusCode(),
                        'body' => $response->getBody()->read(1000000),
                    ],
                    'data' => $original_data,
                    'url' => $this->url
                ],
                $this->attempts(),
                $callback_type,
                $callback_status_id
            );
        } else {
            CallbackLog::addCallbackLog(
                $this->order_id,
                [
                    'response' => [
                        'code' => null,
                        'body' => null,
                    ],
                    'data' => $original_data,
                    'url' => $this->url
                ],
                $this->attempts(),
                $callback_type,
                $callback_status_id
            );
        }
    }
}
