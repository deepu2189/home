<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\eWarpIntegration\Exceptions\ErrorCopyFileToAmazonException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use App\Transformers\AWSPathTransformer;

use App\Property;
use App\Mediafile;
use App\Jobs\SendZipReadyEmail;
use App\Jobs\SendMediafilesReadyEmail;

use ZipArchive;
use Codeception\Util\FileSystem;
use Storage;
use Illuminate\Support\Facades\DB;
use Log;
use App\Jobs\Job;

use Carbon\Carbon;

class GenerateZipFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Property where the files are neede of
     * @var integer
     */
    public $property_id = null;

    /**
     * Category with the file must be generated
     * @var string
     */
    public $category = null;

    /**
     * True if both zip files werw requested originally
     * @var bool
     */
    public $requested_both = null;

    /**
     * User wich requested the file
     * @var type
     */
    public $user_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($property_id, $category, $user_id, $requested_both = false)
    {



        $this->property_id = $property_id;
        $this->category = $category;
        $this->user_id = $user_id;
        if ($category == Mediafile::$photoTypeBoth) {
            $this->requested_both = true;
        } else {
            $this->requested_both = $requested_both;
        }


    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $property = Property::find($this->property_id);

        if ($this->requested_both && $property->media_created_notification) {
            return true;
        }

        // If the category is both, dispatch a new job of category print with requested_both = true
        if ($this->category == Mediafile::$photoTypeBoth) {
            // Dispatch job GenerateZipFile for category print and specify that both were requested originally
            $background_job_print = new GenerateZipFile($this->property_id, Mediafile::$photoTypePrint, $this->user_id, $this->requested_both);
            $background_job_print->onQueue(Config::get('aws.sqs.zip'));
            $this->dispatch($background_job_print);
            // Dispatch job GenerateZipFile for category online and specify that both were requested originally
            $background_job_online = new GenerateZipFile($this->property_id, Mediafile::$photoTypeOnline, $this->user_id, $this->requested_both);
            $background_job_online->onQueue(Config::get('aws.sqs.zip'));
            $this->dispatch($background_job_online);
            return true;
        } else {
            // Get all the mediafiles from the property

            // Generate zip filename
            $zip_filename = $this->property_id.'-'.$this->category.'-'.str_slug($property->title).'.zip';

            // Check if the file exists on amazon
            $this->amazons3 = App::make('aws')->createClient('s3', ['region' => Config::get('aws.credentials.region')]);

            $aws_keyname = AWSPathTransformer::getCompressedPropertyPath($zip_filename);

            if (!$this->amazons3->doesObjectExist(Config::get('aws.credentials.bucket'), $aws_keyname)) {
                Log::info($this->category . ' Zip file for property #' . $property->id .  ' does not exist and it will be generated');
                $amazon_zip_path = $this->generateZipFile($zip_filename, $property, $aws_keyname);
            } else {
                //Validate zip file generation date agains last modification date of the mediafiles
                $lastMediafileOnline = $property->mediafiles()->where('photo_type', '=', 'online')->orderBy('updated_at', 'DESC')->first();
                $lastMediafilePrint = $property->mediafiles()->where('photo_type', '=', 'print')->orderBy('updated_at', 'DESC')->first();

                // Validate the last entry frot the files on each category
                if (($this->category === Mediafile::$photoTypeOnline && !is_null($property->last_zip_online) && ($lastMediafileOnline->updated_at < $property->last_zip_online))
                    ||
                    ($this->category === Mediafile::$photoTypePrint && !is_null($property->last_zip_print) && ($lastMediafilePrint->updated_at < $property->last_zip_print))
                ) {
                    Log::info($this->category . ' Zip file for property #' . $property->id .  ' already exist');
                    $amazon_zip_path = $this->amazons3->getObjectUrl(Config::get('aws.credentials.bucket'), $aws_keyname);
                } else {
                    // We need to update the file
                    Log::info($this->category . ' Zip file for property #' . $property->id .  ' will be regenerate');

                    $amazon_zip_path = $this->generateZipFile($zip_filename, $property, $aws_keyname);
                }
            }
            // Send the email
            //      If only one zip was required ORIGINALLY, send the email
            if (!$this->requested_both) {
                if (intval($this->user_id) > 0) {
                    dispatch(new SendZipReadyEmail(
                        $amazon_zip_path,
                        $this->user_id,
                        $this->category,
                        $this->property_id
                    ))->onQueue(Config::get('aws.sqs.email'));
                }
            } else {
                //  If both zip files were required ORIGINALLY, check if they are uploaded to amazon and send the email
                if (!is_null($property->last_zip_print)
                    && !is_null($property->last_zip_online)
                    && !$property->media_created_notification) {
                    // Get amazon zip path for the two files
                    // Print
                    $zip_filename_print = $this->property_id.'-'.Mediafile::$photoTypePrint.'-'.str_slug($property->title).'.zip';
                    $aws_keyname_print = AWSPathTransformer::getCompressedPropertyPath($zip_filename_print);
                    $amazon_zip_path_print = Mediafile::convertAWSUrlToNiceUrl($this->amazons3->getObjectUrl(Config::get('aws.credentials.bucket'), $aws_keyname_print));
                    // Online
                    $zip_filename_online = $this->property_id.'-'.Mediafile::$photoTypeOnline.'-'.str_slug($property->title).'.zip';
                    $aws_keyname_online = AWSPathTransformer::getCompressedPropertyPath($zip_filename_online);
                    $amazon_zip_path_online = Mediafile::convertAWSUrlToNiceUrl($this->amazons3->getObjectUrl(Config::get('aws.credentials.bucket'), $aws_keyname_online));

                    if (intval($this->user_id) > 0) {
                        // Send email
                        $email_job = new SendMediafilesReadyEmail(
                            $amazon_zip_path_print,
                            $amazon_zip_path_online,
                            $this->user_id,
                            $this->property_id
                        );
                        $email_job->onQueue(Config::get('aws.sqs.email'));
                        $this->dispatch($email_job);
                    }
                    $property->media_created_notification = true;
                    $property->save();
                }
            }
        }
        return true;
    }

    /**
     * Generates correctly the file with all the elements
     *
     * @param string $zip_filename
     * @param \HomeJab\Models\Property $property
     * @param string $aws_keyname
     * @throws Exception
     * @throws ErrorCopyFileToAmazonException
     * @return string AWS S3 url for download
     */
    private function generateZipFile($zip_filename, $property, $aws_keyname)
    {
        if ($this->category === Mediafile::$photoTypePrint) {
            $mediafiles = $property
                ->mediafiles()
                ->where([
                    'type' => Mediafile::$imageType,
                    'photo_type' => $this->category
                ])
                ->get(['external_url', 'metadata']);
        }

        if ($this->category === Mediafile::$photoTypeOnline) {
            $mediafiles = $property
                ->mediafiles()
                ->where([
                    'type' => Mediafile::$imageType,
                    'photo_type' => $this->category,
                    'published' => true
                ])
                ->get(['external_url', 'metadata']);
        }

        if (count($mediafiles) <= 0) {
            // There is no pictures to make for this job. Release to make it afterwards
            Log::info("No mediafiles for the property ".$property->id." to generate zip from");
            $this->release(30);
            return true;
        }

        // Define the temporal folder where the files will be stored
        $storage_folder = storage_path() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR;

        $download_folder = $this->property_id.$this->category;
        if (!Storage::disk('local')->makeDirectory($download_folder)) {
            Log::error("Could not genetate the temporal download folder for the property");
            throw new \Exception("Could not genetate the temporal download folder for the property");
        }

        $download_list = [];

        // Iterate over the files
        foreach ($mediafiles as $mediafile) {
            $original_url = $mediafile->external_url;

            if ($this->category == Mediafile::$photoTypePrint) {
                $metadata = json_decode($mediafile->metadata, true);
                if (array_key_exists('resolutions', $metadata)) {
                    foreach ($metadata['resolutions'] as $resolution) {
                        if (array_key_exists('resizeRule', $resolution) && $resolution['resizeRule'] == 'ORIGINAL') {
                            $original_url = $resolution['url'];
                        }
                    }
                }
            }

            $filename = $this->getFileName($original_url);

            if ( env('APP_ENV') === 'testing') {
                $download_list[] = $storage_folder.DIRECTORY_SEPARATOR.'.gitignore';
            } else {
                try {
                    // Download the file from the remote URL
                    $contents = false;
                    $client = new \GuzzleHttp\Client();
                    $contents = $client->request('GET', $original_url);

                    if ($contents === false) {
                        Log::error("There was an error triying to download the file : " . $original_url);
                        throw new \Exception("There was an error triying to download the file : " . $original_url);
                    }

                    // Save the file to a location
                    $download_list[] = $storage_folder.$download_folder.DIRECTORY_SEPARATOR.$filename;
                    Storage::disk('local')->put($download_folder.DIRECTORY_SEPARATOR.$filename, $contents->getBody()->getContents());
                } catch (ErrorException $e) {
                    // Could not open the file
                    Log::error($e->getMessage());
                    Log::error("Could not copy the file from the original location. Check error log. file:".$original_url);
                    throw new \Exception("Could not copy the file from the original location. Check error log.".$e->getMessage());
                } catch (\GuzzleHttp\Exception\ClientException $e) {
                    Log::error($e->getMessage());
                    Log::error("Could not copy the file from the original location. Check error log. file:".$original_url);
                    throw new \Exception("Could not download the file from the original location. Client response was: ".$e->getMessage());
                } catch (\GuzzleHttp\Exception\TransferException $e) {
                    Log::error($e->getMessage());
                    Log::error("Could not copy the file from the original location. Check error log. file:".$original_url);
                    throw new ErrorCopyFileToAmazonException("Could get the file from the original location. Check error log.".$e->getMessage());
                } catch (\GuzzleHttp\Exception\ConnectException $e) {
                    Log::error($e->getMessage());
                    Log::error("Could not copy the file from the original location. Check error log. file:".$original_url);
                    throw new ErrorCopyFileToAmazonException("Could get the file from the original location. Check error log.".$e->getMessage());
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                    Log::error("Could not copy the file from the original location. Check error log. file:".$original_url);
                    throw new ErrorCopyFileToAmazonException($e->getMessage());
                }
            }
        }
        unset($mediafiles);

        // ZIP Filename - Defined in requerimient

        $zip_filename_path = $storage_folder . $zip_filename;
        // Create the Zipped file
        $zipper = new ZipArchive();
        if ($zipper->open($zip_filename_path, ZIPARCHIVE::CREATE) !== true) {
            Log::error("Could not create the zip file: ".$zipper->getStatusString());
            throw new \Exception("Could not create the zip file: ".$zipper->getStatusString());
        }
        if (count($download_list) <= 0) {
            Log::error("There is no file downloaded to be compressed!");
            throw new \Exception("There is no file downloaded to be compressed!");
        }
        foreach ($download_list as $file) {
            $filename = $this->getFileName($file);
            if (!file_exists($file)) {
                Log::error("Could not find the file ". $file. " to add to the zip file");
                throw new \Exception("Could not find the file ". $file. " to add to the zip file");
            }
            if (!is_readable($file)) {
                Log::error("Could not read the file ". $file. " to add to the zip file");
                throw new \Exception("Could not read the file ". $file. " to add to the zip file");
            }
            if (!$zipper->addFile($file, $filename)) {
                Log::error("Could not add the file ". $filename. " to the zip file");
                throw new \Exception("Could not add the file ". $filename. " to the zip file");
            }
        }
        if ($zipper->numFiles <= 0) {
            Log::error("Empty zip file!");
            throw new \Exception("Empty zip file!");
        }
        if (!$zipper->close()) {
            Log::error("Could not create the zip file (close): ".$zipper->getStatusString());
            throw new \Exception("Could not create the zip file (close): ".$zipper->getStatusString());
        }
        unset($zipper);
        unset($download_list);
        // Remove the temporal folder
        if (Storage::disk('local')->deleteDirectory($download_folder) !== true) {
            Log::error("Could not delete the temporal download folder location");
            throw new \Exception("Could not delete the temporal download folder location");
        }

        $amazon_zip_path = null;
        try {
            $upload = $this->amazons3->upload(
                Config::get('aws.credentials.bucket'),
                $aws_keyname,
                fopen($zip_filename_path, "r"),
                'public-read'
            // Posibble permissions found in documentation:
            // private | public-read | public-read-write | aws-exec-read
            // | authenticated-read | bucket-owner-read
            // | bucket-owner-full-control
            );
            $amazon_zip_path = $upload['ObjectURL'];
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw new ErrorCopyFileToAmazonException("Could not upload the file to the final location. Check error log.");
        }


        // Update the correspondent flag on the property
        if ($this->category === Mediafile::$photoTypeOnline) {
            $property->last_zip_online = Carbon::now();
        } else if ($this->category === Mediafile::$photoTypePrint) {
            $property->last_zip_print = Carbon::now();
        }
        $property->save();

        // If upload was successfully, remove the zip file to save space
        if (!Storage::disk('local')->delete($zip_filename)) {
            Log::error("Could not delete the temporal file: ". $zip_filename_path);
            throw new \Exception("Could not delete the temporal file: ". $zip_filename_path);
        }

        return $amazon_zip_path;
    }

    /**
     * Extracts the filename from a path
     *
     * @param string $file_path
     * @return string
     */
    private function getFileName($file_path)
    {
        $parts = preg_split('/\//', $file_path);
        $filename = array_pop($parts);
        $filename = urldecode($filename);
        return $filename;
    }
}
