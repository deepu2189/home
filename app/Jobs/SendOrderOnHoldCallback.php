<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;

use App\CallbackLog;
use App\CallbackStatus;
use App\Order;
use App\User;

/**
 * This callback will be called when an rescheduled order is requested.
 *
 * @author Deepak Thakur
 * @since 1.3.0
 * @package HomeJab
 * @subpackage Jobs
 */

class SendOrderOnHoldCallback implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    

    /**
     * Order identifier
     * @var type
     */
    public $order_id = null;
    
    public $user_id = null;

    /**
     * Transformer for the order data into callback data
     * @var \HomeJab\Transformers\OrderCallbackTransformer
     */
    private $OrderCallbackTransformer = null;
    /**
     * Url to send data
     * @var
     */
    private $url = null;

    /**
     * Preferred time
     * @var
     */
    public $preferred_time = null;

    /**
     * Preferred date
     * @var
     */
    public $preferred_date = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_id, $user_id)
    {
        $this->order_id = $order_id;
        $this->OrderCallbackTransformer = new App\Transformers\OrderCallbackTransformer();
        $this->url = Config::get('app.callback_url_on_hold');
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!Config::get('app.callback_url_on_hold')) {
            if ($this->attempts() >= 4) {
                Log::error("Callback for job #".$this->order_id." was not sent because the CALLBACK_URL_ON_HOLD is not configured. Giving up.\n");
                return true;
            }
            throw new \Exception("Callback url is not configured. Check CALLBACK_URL_ON_HOLD on environment.");
        }

        $order = Order::find($this->order_id);

        if (!$order) {
            throw new \Exception("Order #" . $this->order_id . " not found when trying to send order data on callback");
        }
        
        $callback_type=CallbackStatus::$typeOrderOnHold;

        $OrderCallbackTransformer = new \HomeJab\Transformers\OrderCallbackTransformer();
      
        $user = User::find($this->user_id);
        $data = $OrderCallbackTransformer->transformData($order, null, null, $callback_type, null, $user);
        $this->sendCallback($data, $callback_type);
    }



    /**
     * Send callback Schedulling app
     *
     * @param type $data
     * @param type $callback_type
     * @return type
     * @throws \Exception
     */
    private function sendCallback($data, $callback_type)
    {

        $order = Order::find($this->order_id);
        
        if (!$order) {
            throw new \Exception("Order #".$this->order_id." not found when trying to send order data on callback");
        }
        if ($this->attempts()===1) {
            $callback_status=CallbackStatus::addCallbackStatus($this->order_id, $callback_type, $this->attempts());
            $callback_status_id=$callback_status['callback_status_id'];
        } else {
            $callback_status = CallbackStatus::where('order_id', '=', $this->order_id)->where('type', '=', $callback_type)->orderBy('id', 'DESC')->first();
            $callback_status_id=$callback_status['id'];
        }
        
        //Send Data
        $client = new \GuzzleHttp\Client();
        $response = null;
        try {
            $response = $client->request('POST', $this->url, ['json' => $data]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            Log::error("Error in client request for callback when order #".$this->order_id." was put on holdd");
            Log::error($e->getMessage());
            Log::error(print_r($response, true));
        }
        
        $this->logCallback($response, $data, $callback_type, $callback_status_id);

        if (!is_null($response) && ($response->getStatusCode() == 200 || $response->getStatusCode() == 201)) {
            // Store the response in the order itself
            $callback_sent = true;
            $callback_success = (new \DateTime())->format('Y-m-d H:i:s');
                 
            CallbackStatus::updateCallbackStatus(
                $this->order_id,
                $callback_type,
                $callback_sent,
                $callback_success
            );
  
            return $response->getStatusCode();
        } else if ($this->attempts() < 5) {
            // make time interval
            $seconds = 0;
            for ($i = 1; $i <= $this->attempts(); $i++) {
                $seconds += $i * 300;
            }
            
            if ($seconds) {
                $this->release($seconds);
            } else {
                // never will enter here, but to done to avoid errors
                $this->release(300);
            }
        } else if ($this->attempts() >= 5) {
            $callback_sent = false;
            $callback_success =null;
            $callback_failed_at = (new \DateTime())->format('Y-m-d H:i:s');
            CallbackStatus::updateCallbackStatus(
                $this->order_id,
                $callback_type,
                $callback_sent,
                $callback_success,
                $callback_failed_at
            );
            if (!$order->save()) {
                Log::error("Could not update the job status for the order #".$this->order_id." - Plase do it manually");
            }
            throw new \Exception("Error in client request for callback when order #".$this->order_id." was put on hold. Giving up.");
        }
    }

    /**
     * Stores in the log model
     * @param type $response
     * @param type $original_data
     * @param type $callback_type
     * @param type $callback_status_id
     */
    private function logCallback($response, $original_data, $callback_type, $callback_status_id)
    {
        if (!is_null($response)) {
            CallbackLog::addCallbackLog(
                $this->order_id,
                [
                    'response' => [
                        'code' => $response->getStatusCode(),
                        'body' => $response->getBody()->read(1000000),
                    ],
                    'data' => $original_data,
                    'url' => $this->url
                ],
                $this->attempts(),
                $callback_type,
                $callback_status_id
            );
        } else {
            CallbackLog::addCallbackLog(
                $this->order_id,
                [
                    'response' => [
                        'code' => null,
                        'body' => null,
                    ],
                    'data' => $original_data,
                    'url' => $this->url
                ],
                $this->attempts(),
                $callback_type,
                $callback_status_id
            );
        }
    }

}
