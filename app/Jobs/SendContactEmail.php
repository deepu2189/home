<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Mockery\CountValidator\Exception;
use App\Mail\sendEmailMailable;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\Property;

use Log;
use Illuminate\Support\Facades\Config;



class SendContactEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    /**
     * User that has registered and need to send email
     *
     * @var \HomeJab\Models\User
     */
    protected $user;
    /**
     *
     * @var array
     */
    protected $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userEmail      = $this->user->email;
        $userFullname   = $this->user->fullname;
        $user           = $this->user;
        $data           = $this->data;

        $propertyName = false;

        if (array_key_exists('propertyId', $data)) {
            $property = Property::find($data['propertyId']);
            $propertyName ='';
            $propertySlug ='';
            if ($property) {
                if ($property->title) {
                    $propertyName = '#'.$property->id . ' - ' . $property->title;
                } else {
                    $propertyName = '#'.$property->id . ' - ' . $property->address;
                }
                $propertySlug = $property->slug;
            }
        }

        Mail::send(
            'emails.agent.contact_email',
            [
                'data' => $data,
                'user' => $user,
                'propertyName' => $propertyName,
                'propertySlug' => $propertySlug,
                'app_static_url'=> Config::get('app.static_url'),
                'mls_url'       => Config::get('app.mls_url')
            ],
            function ($m) use ($userEmail, $userFullname) {
                $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $m->to(
                    $userEmail,
                    $userFullname
                )->subject(trans('emails.contact_email.subject'));
            }
        );

        return true;
    }
}
