<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Agent;
use App\Property;
use App\Jobs\Job;
use Mail;
use Log;

/**
 * Send email to owner with analytics data
 *
 * @author Deepak Thakur
 * @since 0.9
 * @package HomeJab
 * @subpackage Email
 */
class SendPropertyStatistics implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Agent identifier
     *
     * @var integer
     */
    private $agentId;
    
    /**
     * Property identifier
     *
     * @var integer
     */
    private $propertyId;
    
    /**
     * Array with analytics data of current period
     *
     * @var array
     */
    private $currentPeriodData;
    
    /**
     * Current period to compare
     *
     * @var string
     */
    private $currentPeriod;
    
    /**
     * Array with analytics data of past period
     *
     * @var array
     */
    private $pastPeriodData;
    
    /**
     * Past period to compare
     *
     * @var string
     */
    private $pastPeriod;
    
    /**
     * Email to send the statistics
     *
     * @var string
     */
    private $emailTo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($agentId, $propertyId, $currentPeriodData, $pastPeriodData, $currentPeriod, $pastPeriod, $emailTo)
    {
        // Set parameters
        $this->agentId           = $agentId;
        $this->propertyId        = $propertyId;
        $this->currentPeriodData = $currentPeriodData;
        $this->pastPeriodData    = $pastPeriodData;
        $this->currentPeriod     = $currentPeriod;
        $this->pastPeriod        = $pastPeriod;
        $this->emailTo           = $emailTo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Get property data
        $property           = Property::find($this->propertyId);
        $propertyAddress    = $property->address;
        
        // Get agent data
        if ($property->Agent) {
            $agentEmail = $property->Agent->User->email;
            $agentName  = $property->Agent->User->getFullNameAttribute();
        } else if ($property->Seller) {
            $agentEmail = $property->Seller->User->email;
            $agentName  = $property->Seller->User->getFullNameAttribute();
        } else {
            throw new \Exception("Property without agent or seller associated");
        }
         
        Mail::send(
            'emails.property.send_analytics_statistics',
            [
                'agentName'         => $agentName                       ,
                'propertyAddress'   => $propertyAddress                 ,
                'currentPeriod'     => strtoupper($this->currentPeriod) ,
                'pastPeriod'        => strtoupper($this->pastPeriod)    ,
                'currentPeriodData' => $this->currentPeriodData         ,
                'pastPeriodData'    => $this->pastPeriodData
            ],
            function ($m) use ($agentEmail, $agentName, $propertyAddress) {
                $m->from($agentEmail, $agentName);
                $m->to(
                    $this->emailTo
                )->subject(trans('emails.email_analytics_data.subject') . " " . $propertyAddress);
            }
        );
    }
}
