<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\User;
use App\Property;

use Illuminate\Support\Facades\Config;
use Mail;

class SendZipReadyEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Path to the file in AWS S3 storage
     *
     * @var string
     */
    private $aws_path = null;

    /**
     * User identifier that requested the file
     *
     * @var integer
     */
    private $user_id = null;

    /**
     * Category of the files
     *
     * @var string
     */
    private $category = null;

    /**
     * Property Identifier
     *
     * @var integer
     */
    private $property_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($aws_path, $user_id, $category, $property_id)
    {
        $this->aws_path = $aws_path;
        $this->user_id = $user_id;
        $this->category = $category;
        $this->property_id = $property_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //pr($this->aws_path);

        $user = User::findOrFail($this->user_id);
        $user_name = $user->fullname;
        $user_email = $user->email;


        $property = Property::findOrFail($this->property_id);
        $property_title = $property->title;
        try {

            Mail::send(
                'emails.mediafiles.zip_created',
                [
                    'aws_path' => $this->aws_path,
                    'user_name' => $user_name,
                    'category' => $this->category,
                    'property_title' => $property_title,
                    'app_static_url' => Config::get('app.static_url')
                ],
                function ($m) use ($user_email, $user_name) {
                    $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $m->to(
                        $user_email,
                        $user_name
                    )->subject(trans('emails.zip_created.subject'));
                }
            );

        } catch (\Exception $e) {


        }


        $assistants = $user->getAssistantsEmails();
        
        if (count($assistants) > 0) {
            foreach ($assistants as $assistant) {

                Mail::send('emails.mediafiles.zip_created', [
                    'aws_path' => $this->aws_path,
                    'user_name' => $user_name,
                    'category' => $this->category,
                    'property_title' => $property_title,
                    'app_static_url' => Config::get('app.static_url')
                ], function ($m) use ($assistant) {
                    $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $m->to($assistant)->subject(trans('emails.zip_created.subject'));
                });
            }

        }

        return true;
    }
}
