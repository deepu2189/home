<?php
/**
 * Created by PhpStorm.
 * User: OD13
 * Date: 3/22/2019
 * Time: 1:34 PM
 */

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Mockery\CountValidator\Exception;
use App\Mail\sendEmailMailable;
use Illuminate\Support\Facades\Mail;

use Log;
use Illuminate\Support\Facades\Config;

class AllPropertiesSendContactEmail implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 *
	 * @var array
	 */
	protected $data;


	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data         = $this->data;
		echo "<pre>"; print_r($data);
		$propertyName = false;

//		$html = View('emails.agent.allproperties_email', ['data' => $data, 'app_static_url'=> Config::get('app.static_url')])->render();
//		file_put_contents('AllPropertyContactEmail.html',$html); exit;
		Mail::send(
			'emails.agent.allproperties_email',
			[
				'data' => $data,
				'app_static_url'=> Config::get('app.static_url')

			],
			function ($m) use ($data) {
				/*$m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
				$m->to(
					$data['agentEmail'],
					$data['agentFirstName']." ".$data['agentLastName']
				)->subject(trans('allproperties_emails.contact_email.subject'));*/
				$m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
				$m->to(
					'homejabagent1@gmail.com',
					'Agent1'." ".'Homejab'
				)->subject(trans('allproperties_emails.contact_email.subject'));
			}
		);
		return true;
	}

}