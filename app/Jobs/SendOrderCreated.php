<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;

use App\Order;
use App\AllJob as JobModel;
use App\User;
use Mail;

/**
 * Email sent when a new order is created
 *
 * @author Deepak Thakur
 * @since 0.1
 * @package HomeJab
 * @subpackage Emails
 */

class SendOrderCreated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    
    /**
     * User that has registered and need to send email
     *
     * @var \HomeJab\Models\Order
     */
    protected $order;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * Send an email to the user for account verified
     *
     * @return void
     */
    public function handle()
    {


        // Bring all SU users
        $su_users = User::getAllByAccountType(User::$account_type['su']['key']);
        //Create recipents email
        $recipents = [];
        /*foreach ($su_users as $su_user) {
            $recipents[$su_user->email] = $su_user->getFullNameAttribute();
        }*/



        $recipents[Config::get('app.email_contact_on_place_order')] =  Config::get('app.full_name_on_place_order');

        // Get the order user creator
        $order_owner_email  = null;
        $order_owner_name   = null;
        $user_type          = null;
        if ($this->order->Agent) {
            $order_owner_email = $this->order->Agent->User->email;
            $order_owner_name = $this->order->Agent->User->getFullNameAttribute();
            $user_type = 'Agent';
            $user_type_lowercase = 'agent';
        } else if ($this->order->Seller) {
            $order_owner_email = $this->order->Seller->User->email;
            $order_owner_name = $this->order->Seller->User->getFullNameAttribute();
            $user_type = 'Seller';
            $user_type_lowercase = 'seller';
        }



        Mail::send(
            'emails.order.thank_you_order_placed',
            [
                'order' => $this->order,
                'job' => $this->order->jobs()->first(),
                'timing_labels' => JobModel::$schedulingTime,
                'app_static_url' => Config::get('app.static_url'),
                'user_type' => $user_type,
                'user_fullname' => $order_owner_name,
                'user_type_lowercase' => $user_type_lowercase
            ],
            function ($m) use ($order_owner_email, $order_owner_name) {
                $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $m->to(
                    $order_owner_email,
                    $order_owner_name
                )->subject(trans('emails.order_placed.agent_mail_subject'));
            }
        );

        if ($this->order->Agent) {
            $assistants =  $this->order->Agent->User->getAssistantsEmails();
            if (count($assistants) > 0) {
                foreach ($assistants as $assistant) {
                    Mail::send(
                        'emails.order.thank_you_order_placed',
                        [
                        'order' => $this->order,
                        'job' => $this->order->jobs()->first(),
                        'timing_labels' => JobModel::$schedulingTime,
                        'app_static_url' => Config::get('app.static_url'),
                        'user_type' => $user_type,
                        'user_fullname' => $order_owner_name,
                        'user_type_lowercase' => $user_type_lowercase
                        ],
                        function ($m) use ($assistant) {
                            $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                            $m->to($assistant)->subject(trans('emails.order_placed.agent_mail_subject'));
                        }
                    );
                }
            }
        }

        $stripeInfo = Order::getStripeInfo($this->order->id);
        $stripeLink = Config::get('app.stripe_url') . $stripeInfo['id'];

        //If we have SU user then send email for all
       // pr($recipents,1);
        if (!empty($recipents)) {
            Mail::send(
                'emails.order.order_placed_for_su',
                [
                    'order' => $this->order,
                    'job' => $this->order->jobs()->first(),
                    'timing_labels' => JobModel::$schedulingTime,
                    'app_static_url' => Config::get('app.static_url'),
                    'stripeLink' => $stripeLink,
                    'user_type' => $user_type,
                    'user_fullname' => $order_owner_name,
                    'user_type_lowercase' => $user_type_lowercase
                ],
                function ($m) use ($recipents) {
                    $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                    $m->to($recipents)->subject(trans('emails.order_placed.subject'));
                }
            );
        }
        return true;
    }
}
