<?php

namespace App\Jobs;

use App\User;
use App\Property;
use App\AllJob as JobModel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;


use Mail;

/**
 * Email sent when all jobs from the orders are finished
 *
 * @author Guillermo Mione <guillermo@serfe.com>
 * @since 0.8
 * @package HomeJab
 * @subpackage Workers
 */
class SendMediafilesReadyEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    /**
     * Path to the print file in AWS S3 storage
     *
     * @var string
     */
    private $aws_path_print = null;

        /**
     * Path to the online file in AWS S3 storage
     *
     * @var string
     */
    private $aws_path_online = null;
    
    /**
     * User identifier that requested the file
     *
     * @var integer
     */
    private $user_id = null;
        
    /**
     * Property Identifier
     *
     * @var integer
     */
    private $property_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($aws_path_print, $aws_path_online, $user_id, $property_id)
    {
        $this->aws_path_print = $aws_path_print;
        $this->aws_path_online = $aws_path_online;
        $this->user_id = $user_id;
        $this->property_id = $property_id;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!Config::get('app.email_to_jobs')) {
            throw new \Exception("Jobs email is not configured. Check for EMAIL_TO_JOBS on enviroment.");
        }
        if (!Config::get('app.support_email')) {
            throw new \Exception("Support email is not configured. Check for SUPPORT_EMAILS on enviroment.");
        }
        
        $user = User::findOrFail($this->user_id);
        $user_name = $user->fullname;
        $user_email = $user->email;

        $property = Property::findOrFail($this->property_id);
        $property_address = $property->address;
        $property_slug = $property->slug;
        $order_id = $property->order_id;
        
        if ($property->agent_notified) {
            return true;
        }
                
        if (JobModel::thereAreReditJobs($order_id)) {
            $layout = 'emails.mediafiles.all_mediafiles_ready_after_redit';
            $property_address = 'Your Re-Edit on ' . $property->address . ' is Complete!';
        } else {
            $layout = 'emails.mediafiles.all_mediafiles_ready';
        }
                        
        Mail::send(
            $layout,
            [
                'aws_path_print' => $this->aws_path_print,
                'aws_path_online' => $this->aws_path_online,
                'property_slug' => $property_slug,
                'order_id' => $order_id,
                'app_static_url'=> Config::get('app.static_url'),
                'mls_url' =>  Config::get('app.mls_url'),
                'support_email' => Config::get('app.support_email')
            ],
            function ($m) use ($user_email, $user_name, $property_address) {
                $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $m->to(
                    $user_email,
                    $user_name
                )->subject($property_address);
            }
        );
        
        $assistants = $user->getAssistantsEmails();

        if (count($assistants) > 0) {
            foreach ($assistants as $assistant) {
                Mail::send($layout, [
                    'aws_path_print' => $this->aws_path_print,
                    'aws_path_online' => $this->aws_path_online,
                    'property_slug' => $property_slug,
                    'order_id' => $order_id,
                    'app_static_url'=> Config::get('app.static_url'),
                    'mls_url' =>  Config::get('app.mls_url'),
                    'support_email' => Config::get('app.support_email')
                        ], function ($m) use ($assistant, $property_address) {
                            $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                            $m->to($assistant)->subject($property_address);
                        });
            }
        }
        
        // Copy email to jobs@homejab.com
        Mail::send(
            $layout,
            [
                'aws_path_print' => $this->aws_path_print,
                'aws_path_online' => $this->aws_path_online,
                'property_slug' => $property_slug,
                'order_id' => $order_id,
                'app_static_url'=> Config::get('app.static_url'),
                'mls_url' =>  Config::get('app.mls_url'),
                'support_email' => Config::get('app.support_email')
            ],
            function ($m) use ($property_address) {
                $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $m->to(
                    Config::get('app.email_to_jobs')
                )->subject($property_address);
            }
        );
        
        $property->agent_notified = true;
        $property->save();
        
        return true;
    }
}
