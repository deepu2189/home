<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use App\JabMessage;
use App\JobLog;
use App\Jobs;
use GuzzleHttp\Client;

class JabJobSchedule extends Authenticatable
{
    use Notifiable;

    protected $table = "tbl_jobschedule";

    protected $primaryKey = 'JobScheduleId';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'JobId','HJJobID','ScheduleDate','ScheduleTime','StatusID','IsActive','PackagePay','PackageInstructions'
    ];

    public static $rulesOnUpdateJobSchedule = [
        'JobId'             =>  'required',
        'StartDateTime'     =>  'required',
        'UserId'            =>  'required',
    ];
    public static $rulesOnReSchedule = [
        'JobId'             =>  'required',
        'ScheduleDateTime'  =>  'required',
        'PhotographerId'    =>  'required',
    ];


    public static function checkJobScheduleExistOrNot($HJJobID)
    {
        if($HJJobID == '')
        {
            return false;
        }
        $Jobschedule = self::where('HJJobID',$HJJobID)->get();
        if(count($Jobschedule) > 0) {
            return true;
        }
        else
        {
            return false;
        }

    }
	public static function getJobIdByHJJobID($HJJobID)
	{
		if($HJJobID == '')
			return false;

		return self::where('HJJobID',$HJJobID)->first();
	}
	public static function changeStatusByJobId($POST)
	{
		if($POST == '')
			return false;

		$updateDetails = array();
		if(isset($POST['StatusID']) && $POST['StatusID'] != '')
			$updateDetails['StatusID'] = $POST['StatusID'];

		return self::where('JobId',$POST['JobId'])->update($updateDetails);
	}
	public static function getJobscheduleDetails(){

		 $arrjobscedule =     DB::table('tbl_jobschedule')
			          ->select('tbl_jobschedule.StatusID','tbl_jobschedule.Jobid','tbl_jobschedule.ScheduleDate','tbl_jobschedule.ScheduleTime','tbl_jobs.CreatedOn')
			          ->leftJoin('tbl_jobs','tbl_jobschedule.JobId', '=', 'tbl_jobs.JobId')
			          ->where('tbl_jobschedule.StatusID',2)
			          ->where(function($q) {
				          $q->where('tbl_jobs.IsRescheduled', null)
							   ->orWhere('tbl_jobs.IsRescheduled', '');
			          })
			          ->whereRaw('TIMESTAMPDIFF(MINUTE,tbl_jobs.CreatedOn,NOW()) >= 30')
			          ->get();
		 return $arrjobscedule;
	}
}
?>