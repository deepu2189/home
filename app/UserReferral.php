<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class UserReferral extends Model
{
    /**
     *
     * @var type
     */
    public static $saleType = 'sale';
    
    /**
     *
     * @var type
     */
    public static $sigunUpType = 'referral';

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at'];

    /**
     * Save Referral Tab For User
     *
     * @param type $uId
     * @param type $referralId
     * @param type $referralAmount
     * @return boolean
     */
    public static function saveReferralTab($uId, $referralId, $referralAmount)
    {

        $userReferral = new UserReferral();
        $userReferral->user_id = $uId;
        $userReferral->referral_id = $referralId;
        $userReferral->amount = $referralAmount;
        if ($userReferral->save()) {
            return true;
        }
        return false;
    }


    /**
     * The Referral belongs to a User
     *
     * @return type
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Accesor for created at date
     *
     * @param type $date
     * @return type
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.date_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param type $date
     * @return type
     */
    public function getTransferDateAttribute($date)
    {
        if (!empty($date)) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.date_format'));
        } else {
            return 'Not yet transferred';
        }
    }
    
    /**
     *
     * @param type $user_id
     * @return type
     */
    public static function getReferredUsersByUserId($user_id)
    {
        $countReferral = 0;
        $totalEarning = 0;
        $refUsers = array();
        $allReferral = UserReferral::orderBy('user_referrals.id', 'desc')->where('referral_id', $user_id)->leftJoin('users', 'users.id', '=', 'user_referrals.user_id')->get();
        if (count($allReferral)) {
            foreach ($allReferral as $referral) {
                $refUser = [
                    'firstname' => $referral['firstname'],
                    'lastname' => $referral['lastname'],
                    'amount' => $referral['amount'],
                    'type' => $referral['type']
                ];
                $refUsers[] = $refUser;
                $countReferral++;
                $totalEarning += $referral['amount'];
            }
        }
        return [
            $refUsers,
            $countReferral,
            $totalEarning
        ];
    }
    
    /**
     *
     * @param type $registered_user_id
     * @param type $referralId
     * @param type $referral_amount
     * @return boolean
     */
    public static function addReferralSignup($registered_user_id, $referralId, $referral_amount)
    {
       
        $newUserReferral = new UserReferral();
        $newUserReferral->user_id = $registered_user_id;
        $newUserReferral->referral_id = $referralId;
        $newUserReferral->amount = $referral_amount;
        $newUserReferral->type = UserReferral::$sigunUpType;

       // pr($newUserReferral,1);

        if ($newUserReferral->save()) {

           
            if ($referral_amount > 0.0) {
                // Update wallet amount on user Referral
                if (WalletTransaction::addCreditForUserSignUpReferral($referralId, $referral_amount)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     *
     * @param type $user_id
     * @return type
     */
    public static function getReferralByUserId($user_id)
    {
        return self::where('type', '=', 'referral')
                   ->where('user_id', '=', $user_id)
                   ->get(['referral_id'])
                   ->first();
    }
    
    /**
     *
     * @param type $registered_user_id
     * @param type $referralId
     * @param type $referral_amount
     * @return boolean
     */
    public static function addSaleReferral($user_id, $partner_user_id, $referral_amount, $order_id)
    {
        $newUserReferral = new UserReferral();
        $newUserReferral->user_id = $user_id;
        $newUserReferral->referral_id = $partner_user_id;
        $newUserReferral->amount = $referral_amount;
        $newUserReferral->type = UserReferral::$saleType;
        if ($newUserReferral->save()) {
            // Update wallet amount on user Referral
            if (WalletTransaction::addCreditForOrderPlacing($partner_user_id, $referral_amount, $user_id, $order_id)) {
                return true;
            }
        }
        return false;
    }
}
