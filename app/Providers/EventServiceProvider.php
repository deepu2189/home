<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Queue;

/**
 *  EVENTS
 */
use App\Events\ContactAdded;
use App\Events\PhotographerAdded;
use App\Events\MortgageContactAdded;
use App\Events\CustomerAdded;
use App\Events\OrderAdded;
use App\Events\PackageAdded;
use App\Events\ProductAdded;
use App\Events\ProductOptionAdded;
use App\Events\OrderPlaced;
use App\Events\SubscriptionRequested;
use App\Events\ContactMsgSent;
use App\Events\OrderPlacedWithOwnerInfo;
use App\Events\ProductUpdated;
use App\Events\ReaOrSellerContactFormSubmit;
use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Events\PropertyCreated;
use App\Events\OrderDeleted;
use App\Events\OrderUpdated;
use App\Events\PageMetadataChanged;
use App\Events\Home3DTranslationsEdited;
use App\Events\OrderCompleted;
use App\Events\PackageTemplateEdited;
use App\Events\AddonImageChanged;
use App\Events\SiteMapChanged;
use App\Events\PropertyUpdated;
use App\Events\EwarpQuantityFail;
use App\Events\EwarpPlaceOrderFail;
use App\Events\AddPromotionToWallet;
use App\Events\EwarpOnlinePhoto;

/**
 *  HANDLERS
 */
use App\Handlers\Events\AddMortgageContact;
use App\Handlers\Events\AddContactWhenPhotographerAdded;
use App\Handlers\Events\AddContact;
use App\Handlers\Events\AddCustomer;
use App\Handlers\Events\AddOrder;
use App\Handlers\Events\AddPackage;
use App\Handlers\Events\AddProduct;
use App\Handlers\Events\AddProductOption;
use App\Handlers\Events\AddContactWhenOrderPlaced;
use App\Handlers\Events\AddContactWhenSubsRequested;
use App\Handlers\Events\AddContactWhenMsgSent;
use App\Handlers\Events\AddContactWhenReaOrSellerContactFormSubmit;
use App\Handlers\Events\SaveUpdatedProduct;
use App\Handlers\Events\AddOrderWhenPlaced;
use App\Handlers\Events\AddContactWhenUserCreated;
use App\Handlers\Events\SaveUpdatedUser;
use App\Handlers\Events\UpdateSiteMap;
use App\Handlers\Events\AddPropertyPageMetadata;
use App\Handlers\Events\AddUserPageMetadata;
use App\Handlers\Events\AddPackageMetadata;
use App\Handlers\Events\DeleteInfusionSoftOrder;
use App\Handlers\Events\UpdateOrder;
use App\Handlers\Events\AddOrderCompletedTag;
use App\Handlers\Events\RemovePrerenderCache;
use App\Handlers\Events\CalculateReferralComissionOnOrderPlaced;
use App\Handlers\Events\AddPropertyPageMetadataToAllOrders;
use App\Handlers\Events\RemoveFbCacheWhenPropertyUpdated;
use App\Handlers\Events\UpdateAssistantsEmailsWebhook;
use App\Handlers\Events\SendEmailToNotifyEwarpQuantityWrong;
use App\Handlers\Events\SendEmailWhenEwarpOrderPlaceFail;
use App\Handlers\Events\AddAmountToWallet;
use App\Handlers\Events\AddPromotionTagToUser;
use App\Handlers\Events\SendEmailWhenOnlinePhotoMissing;

/**
 * Event service providers for the application
 */
class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        PhotographerAdded::class => [
            AddContactWhenPhotographerAdded::class
        ],
        MortgageContactAdded::class => [
            AddMortgageContact::class,
        ],
        ContactAdded::class => [
            AddContact::class,
        ],
        ProductAdded::class => [
            AddProduct::class,
            //UpdateSiteMap::class,
            AddPackageMetadata::class,
        ],
        OrderPlaced::class => [
            AddOrderWhenPlaced::class,
           // UpdateSiteMap::class,
            CalculateReferralComissionOnOrderPlaced::class,
        ],
        OrderPlacedWithOwnerInfo::class => [
            AddContactWhenOrderPlaced::class,
        ],
        ContactMsgSent::class => [
             AddContactWhenMsgSent::class,
        ],
        SubscriptionRequested::class => [
            AddContactWhenSubsRequested::class,
        ],
        ProductUpdated::class => [
            AddPackageMetadata::class,
            SaveUpdatedProduct::class,
            //::class,
            RemovePrerenderCache::class
        ],
        UserCreated::class => [
            AddUserPageMetadata::class,
            AddContactWhenUserCreated::class,
           // UpdateSiteMap::class
        ],
        UserUpdated::class => [
            AddUserPageMetadata::class,
            SaveUpdatedUser::class,
            //UpdateSiteMap::class,
            RemovePrerenderCache::class,
            UpdateAssistantsEmailsWebhook::class
        ],
        ReaOrSellerContactFormSubmit::class => [
            AddContactWhenReaOrSellerContactFormSubmit::class,
        ],
        SiteMapChanged::class => [
          //  UpdateSiteMap::class
        ],
        PropertyCreated::class => [
            AddPropertyPageMetadata::class,
            //UpdateSiteMap::class
        ],
        PropertyUpdated::class => [
            RemovePrerenderCache::class,
            RemoveFbCacheWhenPropertyUpdated::class,
            AddPropertyPageMetadata::class,
           // UpdateSiteMap::class
        ],
        OrderDeleted::class => [
            DeleteInfusionSoftOrder::class
        ],
        OrderUpdated::class => [
            UpdateOrder::class,
        ],
        OrderCompleted::class => [
           AddOrderCompletedTag::class,
        ],
        PageMetadataChanged::class => [
            RemovePrerenderCache::class
        ],
        PackageTemplateEdited::class => [
            AddPropertyPageMetadataToAllOrders::class
        ],
        AddonImageChanged::class => [
            RemovePrerenderCache::class
        ],
        Home3DTranslationsEdited::class => [
            RemovePrerenderCache::class
        ],
        HomePageTranslationsEdited::class => [
            RemovePrerenderCache::class
        ],
        EwarpPlaceOrderFail::class => [
            SendEmailWhenEwarpOrderPlaceFail::class
        ],
        EwarpQuantityFail::class => [
            SendEmailToNotifyEwarpQuantityWrong::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
