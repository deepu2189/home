<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Config;
use Mail;
use Illuminate\Support\Facades\View;
use GuzzleHttp\Client;

class Jobs extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';

    protected $table = "tbl_jobs";

	protected $primaryKey = 'JobId';

    //public $timestamps = ['CreatedOn','UpdatedOn'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'HJJobID','HJPackageId','HJPackageName','ScheduleDate','ScheduleSlot','PropertyZip','PropertyState','PropertyCity','PropertyAddress','AddressLocation','ScheduleStartTime','ScheduleForHours','AssistantEmails'
    ];

    public static $rulesOnAddJob = [
        'HJJobID'           =>  'required',
        'HJPackageId'       =>  'required',
        'HJPackageName'     =>  'required',
        'ScheduleDate'      =>  'required',
        'ScheduleSlot'      =>  'required',
        'PropertyZip'       =>  'required',
        'PropertyState'     =>  'required',
        'PropertyCity'      =>  'required',
        'PropertyAddress'   =>  'required',
        'ScheduleStartTime' =>  'required',
        'ScheduleForHours'  =>  'required'
    ];
    public static $rulesOnStartJob = [
        'UserId'            =>  'required',
        'JobId'             =>  'required',
        'StartDateTime'     =>  'required'
    ];
    public static $ruleonRescheduleJob = [
		'RescheduledTimeRequested'  =>  'required',
	];
    public static function GetJobDeatailsByUserIdResponseJson($ret,$error=''){

        $new_array = array();

        if($ret != '' && count($ret) > 0)
        {
            foreach ($ret as $key=>$val)
            {
                $new_array[] = array(
                    "Jobid"=> is_object($val)?$val->Jobid:null ,
                    "HJJobID"=> is_object($val)?$val->HJJobID:null,
                    "HJPackageName"=> is_object($val)?$val->HJPackageName:null,
                    "PackagePay"=> is_object($val)?$val->packagepay:null,
                    "ContactName"=> is_object($val)?$val->ContactName:null,
                    "ContactEmail"=> is_object($val)?$val->ContactEmail:null,
                    "ContactPhone"=> is_object($val)?$val->ContactPhone:null,
                    "ScheduleDate"=> is_object($val)?$val->ScheduleDate:null,
                    "PropertyAddress"=> is_object($val)?$val->PropertyAddress:null,
                    "ScheduleTime"=> is_object($val)?$val->ScheduleTime:null,
                    "StatusID"=> is_object($val)?$val->StatusID:null,
                    "Error"=> $error != ''?$error:null,
                );

            }

        }
        else{
            $new_array[] = array(
                "Jobid"=> null,
                "HJJobID"=> null,
                "HJPackageName"=> null,
                "PackagePay"=> null,
                "ContactName"=> null,
                "ContactEmail"=> null,
                "ContactPhone"=> null,
                "ScheduleDate"=> null,
                "PropertyAddress"=> null,
                "ScheduleTime"=> null,
                "StatusID"=> null,
                "Error"=> $error != ''?$error:null,
            );
        }

        return response()->json($new_array);
    }
    public static function getInfoByID($jobid)
    {
        if($jobid == '')
            return false;

        return Jobs::find($jobid);
    }

    public static function checkHJJobIDExistOrNot($HJJobID)
    {
        if($HJJobID == '')
        {
            return false;
        }

        $Jobs = self::where('HJJobID',$HJJobID)->get();

        if(count($Jobs) > 0)
        	return true;
        else
            return false;
    }
	public static function AddJobs($POST)
    {
	    #Get address location from property address using google API
	    $property_address = str_replace(' ', '+', $POST['PropertyAddress']);
	    $url              = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&libraries=drawing,geometry,places&key='.env("GOOGLE_API_KEY").'&address='.$property_address;
	    $client           = new Client();
	    $response         = $client->GET($url);

	    $objAddressLoc = json_decode($response->getBody()->getContents());

	    $address_location           = $objAddressLoc->results[0]->geometry->location;

	    if(is_array($POST['AssistantsEmails']))
            $POST['AssistantsEmails'] = implode($POST['AssistantsEmails']);

        $post["HJJobID"] = $POST['HJJobID'];
        $post["HJPackageId"] = $POST['HJPackageId'];
        $post["HJPackageName"] = $POST['HJPackageName'];
        $post["ContactName"] = $POST['ContactName'];
        $post["ContactPhone"] = $POST['ContactPhone'];
        $post["ContactEmail"] = $POST['ContactEmail'];
        $post["ContactOwnerName"] = $POST['ContactOwnerName'];
        $post["ContactOwnerPhone"] = $POST['ContactOwnerPhone'];
        $post["ContactOwnerEmail"] = $POST['ContactOwnerEmail'];
        $post["ScheduleDate"] = $POST['ScheduleDate'];
        $post["ScheduleSlot"] = $POST['ScheduleSlot'];
        $post["Comments"] = $POST['Comments'];
        $post["MustHaveShots"] = $POST['MustHaveShots'];
        $post["PropertyZip"] = $POST['PropertyZip'];
        $post["PropertyState"] = $POST['PropertyState'];
        $post["PropertyCity"] = $POST['PropertyCity'];
        $post["PropertyAddress"] = $POST['PropertyAddress'];
        $post["AddressLocation"] = 'POINT('.$address_location->lng.' '.$address_location->lat.')';
        $post["ScheduleStartTime"] = $POST['ScheduleStartTime'];
        $post["ScheduleForHours"] = $POST['ScheduleForHours'];
        $post["paymentToPhotographer"] = $POST['PaymentToPhotographer'];
        $post["packageInstructions"] = $POST['PackageInstructions'];
        $post["AssistantEmails"] = $POST['AssistantsEmails'];
        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_Homejab_SaveJobDetails('.rtrim($params,','  ).')');
        return $ret;

    }
	public static function UpdateAssistantEmail($POST)
    {
        if(!is_array($POST))
            return false;

	    self::where('ContactEmail',$POST['ContactEmail'])->update( ['AssistantEmails' => $POST['AssistantEmails']]);
	    return true;
    }
	public static function GetJobListingByUserID($POST)
    {
        if(!is_array($POST))
            return false;

        $post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
        $post["PageNumber"] = (isset($POST['PageNumber']) && $POST['PageNumber'] != '')?$POST['PageNumber']:0;
        $post["PageSize"]   = (isset($POST['PageSize']) && $POST['PageSize'] != '')?$POST['PageSize']:0;
        $post["StatusID"]   = (isset($POST['StatusID']) && $POST['StatusID'] != '')?$POST['StatusID']:0;


        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }
        $ret = DB::select('call SP_HomeJob_API_GetJobListingByUserID('.rtrim($params,','  ).')');
        return Jobs::hydrate($ret);

    }
	/*public static function AddJobs($POST)
	{

		if(!is_array($POST))
			return false;

		DB::beginTransaction();


		if (strtoupper(LTRIM(RTRIM($POST['ScheduleStartTime']))) == 'ANYTIME')
		{
			# Get UserShiftStartTime
			$UserShiftStartime = AppSettings::select('UserShiftStartTime')->first();
			$ScheduleStartTime = $UserShiftStartime;
		}
		else {
			$timestamp = strtotime($POST['ScheduleStartTime']);
			$ScheduleStartTime = date('H:i:s.u',$timestamp);
		}

		$POST['ScheduleStartTime'] = $ScheduleStartTime;
		$POST['ScheduleForHours'] = is_numeric($POST['ScheduleForHours'])?$POST['ScheduleForHours']:'0';
		$POST['AssistantsEmails'] = implode(',',$POST['AssistantsEmails']);


		$objjobs = new Jobs();
		$objjobs->fill($POST);
		$JobData = $objjobs->save();

		if ($JobData) {

			# add job schedule
			//echo"<pre>";print_r($POST);die;
			# set post data

			$_post['JobId'] = $objjobs->JobId;
			$_post['HJJobID'] = $POST['HJJobID'];
			$_post['ScheduleDate'] = $POST['ScheduleDate'];
			$_post['ScheduleTime'] = $ScheduleStartTime;
			$_post['StatusID'] = 1;
			$_post['IsActive'] = 1;
			$_post['PackagePay'] = $POST['PaymentToPhotographer'];
			$_post['PackageInstructions'] = $POST['PackageInstructions'];


			$objJobSchedule = new JobSchedule();
			$objJobSchedule->fill($_post);

			$JobSchData = $objJobSchedule->save();
			$objJobSchedule->JobScheduleId;

			# add job log
			$_post = array();
			$_post['JobId'] = $objjobs->JobId;
			$_post['JobScheduleId'] = $objJobSchedule->JobScheduleId;
			$_post['Description'] = 'New Job Fetch from homejab.com at '.date('Y-m-d H:i:sA');
			$_post['CreatedBy'] = 0;
			$_post['CreatedOn'] = date('Y-m-d H:i:s');

			$objJoblog= new JobLog();
			$objJoblog->fill($_post);

			$JobLogData = $objJoblog->save();
			//echo $objJoblog->LogId;die;

			DB::commit();
			return $JobData;
		}
		DB::rollback();

		return false;
	}*/
	public static function GetJobDetailsBy_UserId_JobID($POST)
    {
        if(!is_array($POST))
            return false;

        $post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
        $post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_HomeJob_API_GetJobDetailsBy_UserID_JobID('.rtrim($params,','  ).')');
        //echo"<pre>";print_r(Jobs::hydrate($ret));die;

        return Jobs::hydrate($ret);
    }
    public static function getJobUserNameFromJobId($jobid)
    {
        if($jobid != '')
        {
           return  DB::table('tbl_jobusertime')
            ->select('tbl_users.Name')
            ->leftJoin('tbl_users','tbl_users.UserId', '=', 'tbl_jobusertime.UserId')
            -> where('tbl_jobusertime.JobId',$jobid)
            ->first();
        }
    }
    public static function AcceptJob($POST)
    {
        if(!is_array($POST))
            return false;


        $post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
        $post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;
        $post["AcceptDateTime"]= $POST['AcceptDateTime'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_HomeJob_API_AcceptJob('.rtrim($params,','  ).')');
        return $ret[0]->Result;
    }
    public static function SendEmailToCustomerAgentForAccpetedJob($jobid)
    {
        if($jobid == '')
            return false;

        $objJobs = self::select('HJJobID','ContactEmail', 'ContactOwnerEmail', 'AssistantEmails','PropertyAddress','ScheduleDate','ScheduleStartTime')->where('JobId',$jobid)->first();
        if(is_object($objJobs))
        {
            # get job user from job id
            $objJobUser = Jobs::getJobUserNameFromJobId($jobid);
            $arrJobs['HJJobID'] = $objJobs->HJJobID;
            $arrJobs['ContactEmail'] = $objJobs->ContactEmail;
            $arrJobs['ContactOwnerEmail'] = $objJobs->ContactOwnerEmail;
            $arrJobs['AssistantEmails'] = $objJobs->AssistantEmails;
            $arrJobs['PropertyAddress'] = $objJobs->PropertyAddress;
            $arrJobs['ScheduleDate'] = $objJobs->ScheduleDate;
            $arrJobs['ScheduleStartTime'] = $objJobs->ScheduleStartTime;
            $arrJobs['JobUser'] = $objJobUser->Name;
            $arrJobs['ScheduleDateTime']=date('d-m-Y h:i A',strtotime($arrJobs['ScheduleDate'].' '.$arrJobs['ScheduleStartTime']));

            # send email
	        try{
		        $send_email = Mail::send('emails.CustomerAgentEmail', ['arrJobs' => $arrJobs], function ($m) use ($arrJobs) {
			        $ToAddress = [$arrJobs->ContactEmail.','.$arrJobs->ContactOwnerEmail.','.$arrJobs->AssistantEmails,Config('constants.JOB_CONFIRM_RECIVER_EMAILID')];
			        $ToAddressList = Custom::getToAddressList($ToAddress);
			        $m->from(Config('constants.DEFAULT_FROM_EMAIL_ADDRESS'));
			        $m->to($ToAddressList)->subject(' Your shoot has been confirmed / ' . $arrJobs['PropertyAddress']);
		        });
	        }
	        catch(\Exception $e)
	        {
		        $error_content = "JobId : $jobid <br/>
									Email Type :  SendEmail To Customer Agent For AccpetedJob<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
		        file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),true);
	        }
        }
    }
    public static function SendEmailToPhotographersForAcceptedJob($jobid)
    {
        if($jobid == '')
            return false;
        $objJobs = self::select('HJJobID','HJPackageName','PropertyAddress','ScheduleDate','ScheduleSlot')->where('JobId',$jobid)->first();
        if(is_object($objJobs))
        {
	        $arrJobAssignList=Jobs::JobAssignDetailForMail($jobid);
	        #$html = View('emails.PhotographersJobAccepted', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList])->render();
//	        file_put_contents('declineJobEmail.html',$html); die;
            # send email
            $objJobs->ScheduleDate = date('d-m-Y',strtotime($objJobs->ScheduleDate));
	        try{
		        $send_email = Mail::send('emails.PhotographersJobAccepted', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList], function ($m) use ($objJobs) {
			        $ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
			        $ToAddressList = Custom::getToAddressList($ToAddress);
			        $m->from(Config('constants.DEFAULT_FROM_EMAIL_ADDRESS'));
			        $m->to($ToAddressList)->subject('Job Accepted - ' . $objJobs->HJJobID);
		        });
	        }
	        catch(\Exception $e)
	        {
		        $error_content = "JobId : $jobid <br/>
									Email Type :  SendEmail To Photographers For AcceptedJob<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
		        file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),true);
	        }
        }
    }
    public static function SendMessageToCustomerForAccpetedJob($jobid)
    {
        if($jobid == '')
            return false;

        $objJobs = self::select('ContactPhone','ContactOwnerPhone','PropertyAddress','ScheduleDate','ScheduleStartTime')->where('JobId',$jobid)->first();

        if(is_object($objJobs))
        {
            $objJobuser = Jobs::getJobUserNameFromJobId($jobid);

            # set twillo credential
            $sid = env('TWILLO_ACCOUNT_SID');
            $token = env('TWILLO_AUTH_TOKEN');
            $client = new Client($sid, $token);

            $message_body = "Your HomeJab shoot has been confirmed & scheduled with ".$objJobuser->Name."\r\nDate & Time: ".date("d-m-Y h:i A",strtotime($objJobs->ScheduleDate.' '.$objJobs->ScheduleStartTime))."\r\nProperty Address :".$objJobs->PropertyAddress."\r\nIf you need to cancel or reschedule,\r\nplease email us: ".\Config('constants.SALES_EMAIL_ADDRESS');
            // Use the client to do fun stuff like send text messages!
            if(isset($objJobs->ContactPhone) && $objJobs->ContactPhone != '' )
            {
                try{
                    $client->messages->create(
                        // the number you'd like to send the message to
                        '+1'.preg_replace('/[^0-9]/', '',$objJobs->ContactPhone),
                        array(
                            // A Twilio phone number you purchased at twilio.com/console
                            'from' => env('TWILLO_FROM_NUMBER'),
                            // the body of the text message you'd like to send
                            'body' => $message_body
                        )
                    );
                }
                catch(\Exception $e)
                {
                    $error_content = "Controller name : Send SMS <br/>
                                Mobile Number:".$objJobs->ContactPhone."<br/>
                                Date & Time:".date('Y-m-d H:i:s')." <br/>
                                Error Message : ".$e->getMessage()."<hr/>";

                    file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),true);
                }
            }
            if(isset($objJobs->ContactOwnerPhone) && $objJobs->ContactOwnerPhone != '' )
            {
                try{
                    $client->messages->create(
                    // the number you'd like to send the message to
                        '+1'.preg_replace('/[^0-9]/', '',$objJobs->ContactOwnerPhone),
                        array(
                            // A Twilio phone number you purchased at twilio.com/console
                            'from' => env('TWILLO_FROM_NUMBER'),
                            // the body of the text message you'd like to send
                            'body' => $message_body
                        )
                    );
                }
                catch(\Exception $e)
                {
                    $error_content = "Controller name : Send SMS <br/>
                                Mobile Number:".$objJobs->ContactPhone."<br/>
                                Date & Time:".date('Y-m-d H:i:s')." <br/>
                                Error Message : ".$e->getMessage()."<hr/>";

                    file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true));
                }
            }
        }
    }
	public static function DeclineJob($POST)
	{
		if(!is_array($POST))
			return false;

		$post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
		$post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;
		$post["DeclineDateTime"]= $POST['DeclineDateTime'];
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= "'".addslashes(stripslashes(trim($val)))."'".",";
		}

		$ret = DB::select('call SP_HomeJob_API_DeclineJob('.rtrim($params,','  ).')');
		return $ret[0]->Result;
	}
    public static function SendEmailToPhotographersForDeclinedJob($jobid){
	    if($jobid == '')
		    return false;
	    $objJobs = self::select('HJJobID','HJPackageName','PropertyAddress','ScheduleDate','ScheduleSlot')->where('JobId',$jobid)->first();
	    if(is_object($objJobs))
	    {
	    	$arrJobAssignList=Jobs::HomeJobJobDeclineDetailForMail($jobid);
	    	#$html = View('emails.PhotographersJobDecline', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList])->render();
		    #file_put_contents('declineJobEmail.html',$html);  die;
		    # send email
		    $objJobs->ScheduleDate = date('d-m-Y',strtotime($objJobs->ScheduleDate));
		       try{
			    $send_email = Mail::send('emails.PhotographersJobDecline', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList], function ($m) use ($objJobs) {
				    $ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
				    $ToAddressList = Custom::getToAddressList($ToAddress);
				    $m->from(Config('constants.DEFAULT_FROM_EMAIL_ADDRESS'));
				    $m->to($ToAddressList)->subject('Job Declined - ' . $objJobs->HJJobID);
			    });
		    }
		    catch(\Exception $e)
		    {
			    $error_content = "JobId : $jobid <br/>
									Email Type :  SendEmail To Photographers For Declined Job<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
			    file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),true);
		    }
	    }
    }
	public static function HomeJobJobAssignDetailForMail($jobid){
    	if($jobid == '')
			return false;

		$post["JobId"]  = $jobid;
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= addslashes(stripslashes(trim($val)));
		}
		$ret = DB::select('call SP_HomeJob_JobAssignDetailsForMail('.rtrim($params,','  ).')');
		return $ret;
    }
    public static function HomeJobJobDeclineDetailForMail($jobid){
    	if($jobid == '')
			return false;

		$post["JobId"]  = $jobid;
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= addslashes(stripslashes(trim($val)));
		}
		$ret = DB::select('call SP_HomeJob_JobDeclineDetailsForMail('.rtrim($params,','  ).')');
		return $ret;
    }
	public static function JobAssignDetailForMail($jobid){
		if($jobid == '')
			return false;

		$post["JobId"]  = $jobid;
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= addslashes(stripslashes(trim($val)));
		}
		$ret = DB::select('call JobAssignDetailsForMail('.rtrim($params,','  ).')');
		return $ret;
	}
    public static function EndJob($POST){
	    if(!is_array($POST))
		    return false;
	    $post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
	    $post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;
	    $post["EndDateTime"]= $POST['EndDateTime'];
	    $params = '';
	    foreach($post as $key=>$val)
	    {
		    $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
	    }
	    $ret = DB::select('call SP_HomeJob_API_EndJob('.rtrim($params,','  ).')');
	    return $ret[0]->Result;
    }

	public static function SendEmailToPhotographersForEndJob($jobid){
    	if($jobid == '')
			return false;
		$objJobs = self::select('HJJobID','HJPackageName','PropertyAddress','ScheduleDate','ScheduleSlot')->where('JobId',$jobid)->first();
		if(is_object($objJobs))
		{
			$arrJobAssignList=Jobs::HomeJobJobAssignDetailForMail($jobid);
			/*$html = View('emails.PhotographersJobClosed', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList])->render();
			file_put_contents('jobclosedEmail.html',$html);  die;*/
			# send email
			$objJobs->ScheduleDate = date('d-m-Y',strtotime($objJobs->ScheduleDate));

			try{
				
				$send_email = Mail::send('emails.PhotographersJobClosed', ['objJobs' => $objJobs], function ($m) use ($objJobs) {
					$ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
					$ToAddressList = Custom::getToAddressList($ToAddress);
					$m->from(Config('constants.DEFAULT_FROM_EMAIL_ADDRESS'));
					$m->to($ToAddressList)->subject('Job Closed - ' . $objJobs->HJJobID);
				});
				echo $send_email;die;
			}
			catch(\Exception $e)
			{
				$error_content = "JobId : $jobid <br/>
									Email Type :  SendEmail To Photographers For Closed Job<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
				file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),true);
			}
		}
	}
	public static function StartJob($POST){
		if(!is_array($POST))
			return false;
		$post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
		$post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;
		$post["StartDateTime"]= $POST['StartDateTime'];
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= "'".addslashes(stripslashes(trim($val)))."'".",";
		}
		$ret = DB::select('call SP_Homejab_API_StartJob ('.rtrim($params,','  ).')');

		return $ret;
	}
	public static function AssignPhotographer($JobId,$ScheduleDateTime,$LoggedUserId,$RankID = 1){

		$retvalue = true
	;	# Call store procedure for get available photographers
		$arr_avilablephtographers = DB::select('call HomeJab_Job_GetAvailablePhotographers('."'".$JobId."',"."'".$ScheduleDateTime."',"."'".$LoggedUserId."',"."'".$RankID."')");

		if(is_array($arr_avilablephtographers)  && count($arr_avilablephtographers) > 0){

			$drivetimelist = '';
			$useridlist    = '';
			foreach($arr_avilablephtographers as $key=>$val){
				$post =array();
				# Now set array for calculate drive time
				$post['source_address'] =	$val->UserAddress;
				$post['destination_address'] =	$val->JobAddress;
				# Create Html File On server With This Detail
				$calculate_drivetime=Jobs::CalculatetDriveTime($post);

				$drivetime_detail = "jobid : ".$JobId." <br/>
									total available photographer : ".count($arr_avilablephtographers)." <br/>
									user address : ".$val->UserAddress." <br/>
		                            job address: ".$val->JobAddress." <br/>
		                            drivetime in hour : ".$calculate_drivetime." <br/>";
				file_put_contents('GetDistance.html', print_r($drivetime_detail, true), true);

				if($calculate_drivetime > 0)
				{
					$drivetime = 50;
					if($calculate_drivetime <= $drivetime)
					{
						# Set comma seprated userlist and time
						$drivetimelist .= $val->UserId.'$'.$val->UserAvailabilityTime.',';
						$useridlist    .= $val->UserId.',';
					}
				}
			}
			#add push notification functionality

			# Call store procedure to assign job to photograhers
			$arrdata['Photographers'] = rtrim($drivetimelist,',');
			$arrdata['UserIdsAVLDATETIME'] = rtrim($useridlist,',');
			$arrdata['JobId'] = $JobId;
			$retvalue = Jobs::AssignJobtoPhotographers($arrdata);
		}
		if($ScheduleDateTime == '')
		{
			Jobs::SendEmailToAssignJobPhotographers($JobId);
		}
		return $retvalue;
	}
	public static function CalculatetDriveTime($post){

		$source_address = $post['source_address'];
		$destination_address = $post['destination_address'];
		$driveTimeurl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$source_address."&destinations=".$destination_address."&mode=driving&language=us-en&sensor=false&key=".env("GOOGLE_API_KEY");

		# Get drive time using google api
		$client = new Client();

		$response = $client->GET($driveTimeurl);

		$api_data = $response->getBody()->getContents();

		if(isset($api_data) && $api_data != '')
		{
			$decode_api_data = json_decode($api_data,true);
			if($decode_api_data['status'] == 'OK')
			{
				# Now Calculate drive time in hours
				$drive_time = $decode_api_data['rows'][0]['elements'][0]['duration']['value']/60;
				$retvalue = floor($drive_time);
			}
		}
		if(isset($retvalue) && $retvalue > 0)
			return $retvalue;
		else
			return false;
	}
	public static function SendEmailToAssignJobPhotographers($JobId)
	{
		if($JobId == '')
			return false;

		# Get job details from jobid
		$objJobs = self::select('HJJobID','HJPackageName','PackagePay','PropertyAddress','ScheduleDate','ScheduleSlot')->where('JobId',$JobId)->first();

		if(is_object($objJobs))
		{
			# Get job assign details by jobid
			$arrJobAssignList = Jobs::HomeJobJobAssignDetailForMail($JobId);
			#$html = View('emails.AssignJobPhotographer', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList])->render();
			#file_put_contents('AssignJobPhotographerEmail.html',$html); die;

			# send email
			$objJobs->ScheduleDate = date('d-m-Y',strtotime($objJobs->ScheduleDate));
			try{
				$send_email = Mail::send('emails.AssignJobPhotographer', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList], function ($m) use ($objJobs) {

					$ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
					$ToAddressList = Custom::getToAddressList($ToAddress);
					$m->from(Config('constants.DEFAULT_FROM_EMAIL_ADDRESS'));
					$m->to($ToAddressList)->subject('Job Created - ' . $objJobs->HJJobID);
				});
			}
			catch(\Exception $e)
			{
				$error_content = "JobId : $JobId <br/>
									Email Type :  SendEmail To AssignJobPhotographers<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
				file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),true);
			}
		}
	}
	public static function AssignJobtoPhotographers($POST){

		if(!is_array($POST))
			return false;

		$post["JobId"]              = $POST["JobId"];
		$post["Photographers"]      = $POST["Photographers"];
		$post["UserIdsAVLDATETIME"] = $POST["UserIdsAVLDATETIME"];

		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= "'".addslashes(stripslashes(trim($val)))."'".",";
		}
		# Call store procedure to assign photograhers for perticular job
		$ret = DB::select('call HomeJab_Job_AssignPhotographers ('.rtrim($params,','  ).')');
		return $ret;
	}
}