<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class AppSettings extends Authenticatable
{
    use Notifiable;

    protected $table = "tbl_appsettings";

    protected $primaryKey = 'AppSettingsId';

    public $timestamps = false;
}
?>