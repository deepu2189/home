<?php

namespace App\Validators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

use Log;


use App\PropertyHasMediaFile;
use App\AssetType;
use App\Order;
use App\Property;
use App\Mediafile;
use App\OrderHasExtraFile;

class CustomValidator extends Validator
{

    /**
    * Function to validate that ewarp_product_id exist in request
    */
    public function validateEwarpProduct($attribute, $value, $parameters, $validator)
    {
        foreach ($value as $asset) {
            if (!array_key_exists('ewarp_product_id', $asset)) {
                return false;
            }
        }
        return true;
    }

    /**
    * Function to validate that ewarp_product_id is active
    */
    public function validateEwarpProductExist($attribute, $value, $parameters, $validator)
    {
        
        foreach ($value as $asset) {
            if (!array_key_exists('ewarp_product_id', $asset)) {
                return false;
            }

            if (!AssetType::where('ewarp_product_id', '=', $asset['ewarp_product_id'])->first()) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param type $attribute
     * @param type $value
     * @param type $parameters
     * @param type $validator
     * @return boolean
     */
    public function validateAssetTypeExists($attribute, $value, $parameters, $validator)
    {
        foreach ($value as $asset) {
            if (!array_key_exists('asset_type_id', $asset)) {
                return false;
            }

            $assetType = AssetType::where('id', '=', $asset['asset_type_id'])->first();

            if (!$assetType) {
                return false;
            }

            $olderFiles = count($asset['property_mediafiles']);
            $extraFiles = count($asset['extra_files']);


            //Normal aaset types trying to replace older files is not allowed
            if ($olderFiles != 0 && !$assetType->rework) {
                $validator->addReplacer('assetTypeExists', function ($message, $attribute, $rule) use ($validator) {
                    $message = "The asset type selected don't allows re-edits with older files";
                    return $message;
                });

                return false;
            }

            //Added work with asset type of rework is not allowed
            if ($olderFiles == 0 && $extraFiles > 0 && $assetType->rework) {
                $validator->addReplacer('assetTypeExists', function ($message, $attribute, $rule) use ($validator) {
                    $message = "The asset type selected is invalid for this operation";
                    return $message;
                });

                return false;
            }
        }
        return true;
    }
    

    /**
    * Function to validate re edit
    */
    public function validateMediafiles($attribute, $value, $parameters, $validator)
    {

        $order_id = array_get($validator->getData(), $parameters[0], null);
        
        $order = Order::find($order_id);
        
        if (!$order) {
            return false;
        }

        $property_id = $order->property->id;

        foreach ($value as $asset) {
            if (!array_key_exists('property_mediafiles', $asset)) {
                return false;
            }

            $mediafiles = $asset['property_mediafiles'];
            foreach ($mediafiles as $id) {
                $file = Mediafile::find($id);
                
                
                if (!$file) {
                    return false;
                }
                
                $relation = PropertyHasMediaFile::where('property_id', '=', $property_id)
                    ->where('mediafile_id', '=', $id)
                    ->first();

                if (!$relation || $file->copied_to_amazon == 0 || !$file->key) {
                    $validator->addReplacer('mediafilesExists', function ($message, $attribute, $rule, $id) use ($validator, $id) {
                        $message = "The mediafile #:id does not exist or has not yet been uploaded to amazon";
                        return str_replace(':id', $id, $message);
                    });
                    
                    return false;
                }
            }
        }
        
        return true;
    }

    public function orderIsComplete($attribute, $value, $parameters, $validator)
    {
        $order = Order::find($value);

        if ($order) {
            return $order->isCompleted();
        }

        return false;
    }

    /**
    * Function to validate extra files
    */
    public function validateExtraFiles($attribute, $value, $parameters, $validator)
    {
        $order_id = array_get($validator->getData(), $parameters[0], null);
        
        $order = Order::find($order_id);

        if (!$order) {
            return false;
        }
        
        $property_id = $order->property->id;
        
        foreach ($value as $asset) {
            if (!array_key_exists('extra_files', $asset)) {
                return false;
            }
            
            $mediafiles = $asset['extra_files'];
            foreach ($mediafiles as $id) {
                $file = Mediafile::find($id);
                
                if (!$file) {
                    return false;
                }
                
                $relation = OrderHasExtraFile::where('order_id', '=', $order_id)
                ->where('mediafile_id', '=', $id)
                ->first();
                

                if (!$relation || $file->copied_to_amazon == 0 || !$file->key) {
                    $validator->addReplacer('mediafilesExists', function ($message, $attribute, $rule, $id) use ($validator, $id) {
                        $message = "The extra file #:id does not exist or has not yet been uploaded to amazon";
                        return str_replace(':id', $id, $message);
                    });
                    
                    return false;
                }
            }
        }
        
        return true;
    }
}
