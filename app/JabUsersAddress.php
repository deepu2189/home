<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Observers\CustomeObservers;


class JabUsersAddress extends Model
{
    use Notifiable;

    const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Zip', 'State', 'City', 'Address', 'UserId', 'AddressLocation','CreatedBy', 'CreatedOn', 'UpdatedBy', 'UpdatedOn'
    ];


    protected $table = "tbl_usersaddress";

    protected $primaryKey = 'Address_Id';

    //public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new CustomeObservers());

        parent::boot();
    }
    public function getUserAddressInfoByJobId($jobid)
    {
        if($jobid == '')
            return false;

       return $this->newQuery()->leftJoin('tbl_users AS users','tbl_usersaddress.UserId','=','users.UserId')
                        ->leftJoin('tbl_jobuser As ju','users.UserId','=','ju.UserId')
                        ->where('ju.JobId','=',$jobid)->first();



    }
}
?>