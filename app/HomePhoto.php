<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

/**
* Class to handle the job
*
* @author Deepak Thakur
*/

class HomePhoto extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'home_photos';

    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'mediafile_id',
        'position'
    ];
    
    
    /**
     *
     */
    static public $fillableOnUpdate = [
        'position',
        'mediafile_id',
    ];

    /**
     * Relation with mediafiles
     *
     * @return type
     */
    public function mediafile()
    {
        return $this->belongsTo('App\Mediafile');
    }

    /**
     * returns the next position to insert
     * @return type
     */
    public static function getPositionToAdd()
    {
        return self::get(['position'])->max('position') + 1;
    }
    
    /**
     * update only the fillable fields
     *
     * @param type $data
     * @return boolean
     */
    public function partialUpdate($data)
    {
        DB::beginTransaction();

        $this->fill($data);

        if ($this->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    /**
     * update the position
     *
     * @param type $data
     * @return boolean
     */
    public static function updatePositions($data)
    {
        DB::beginTransaction();       
        foreach ($data as $photo) {            
            $homePhoto = HomePhoto::find($photo['id']);
            
            if (!$homePhoto) {
                DB::rollback();
                return false;
            }
            if (!$homePhoto->partialUpdate($photo)) {
                DB::rollback();
                return false;
            }
        }
        DB::commit();
        return true;
    }
}
