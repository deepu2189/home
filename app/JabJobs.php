<?php

namespace App;

use App\Http\Controllers\PushNotificationController;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Config;
use Mail;
use Illuminate\Support\Facades\View;
use GuzzleHttp\Client;
use Twilio\Rest\Client as TwilloClient;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Observers\CustomeObservers;
//use Illuminate\Support\Facades\Config;

class JabJobs extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';

    protected $table = "tbl_jobs";

	protected $primaryKey = 'JobId';


    //public $timestamps = ['CreatedOn','UpdatedOn'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'HJJobID','HJPackageId','HJPackageName','ScheduleDate','ScheduleSlot','PropertyZip','PropertyState','PropertyCity','PropertyAddress','AddressLocation','ScheduleStartTime','ScheduleForHours','AssistantEmails'
    ];

    public static $rulesOnAddJob = [
        'HJJobID'           =>  'required',
        'HJPackageId'       =>  'required',
        'HJPackageName'     =>  'required',
        'ScheduleDate'      =>  'required',
        'ScheduleSlot'      =>  'required',
        //'PropertyZip'       =>  'required',
        //'PropertyState'     =>  'required',
        //'PropertyCity'      =>  'required',
        'PropertyAddress'   =>  'required',
        'ContactName'       =>  'required',
        'ContactPhone'      =>  'required',
        'ContactEmail'      =>  'required',
        'ScheduleStartTime' =>  'required',
        'ScheduleForHours'  =>  'required'
    ];
    public static $rulesOnStartJob = [
        'UserId'            =>  'required',
        'JobId'             =>  'required',
        'StartDateTime'     =>  'required|date'
    ];
	public static $rulesOnAcceptJob = [
		'AcceptDateTime'     =>  'required|date'
	];
	public static $rulesOnDeclineJob = [
		'DeclineDateTime'     =>  'required|date'
	];
	public static $rulesOnEndJob = [
		'EndDateTime'         =>  'required|date'
	];
    public static $rulesOnUpdateOrderDetails = [
        'ContactName'       =>  'required',
        'ContactPhone'      =>  'required',
        //'PropertyZip'       =>  'required',
        //'PropertyState'     =>  'required',
        'PropertyAddress'   =>  'required',
        'ContactEmail'      =>  'required',
        //'PackagePay'        =>  'required',
        //'PackageInstructions' =>  'required'
    ];
    public static $rulesonreschedulejob = [
        'JobId'             => 'required',
        'ScheduleDateTime'  => 'required',
        'PhotographerId'    => 'required',
        'LoggedUserId'      => 'required',

    ];
    public static $rulesonstatusreport = [
        'Type'      => 'required',
    ];

    /**
     * Relation with Job log
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function JobsHasLog()
    {
        return $this->hasMany('App\JabJobLog', 'JobId');
    }
    /**
     * Relation with Job Schedule
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function JobsHasSchedule()
    {
        return $this->hasOne('App\JabJobSchedule', 'JobId');
    }
    /*public static $ruleonRescheduleJob = [
		'RescheduledTimeRequested'  =>  'required|date_format:H:i',
	];*/

    public function JabJobUser(){
        return $this->hasMany('App\JabJobUser','JobId')->where('IsActive','=',1);
    }

	public function JabJobUserForAll(){
		return $this->hasMany('App\JabJobUser','JobId');
	}


	public static function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s.u', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s.u', $date)->format(Config::get('app.datetime_format'));
    }


    public static function GetJobDeatailsByUserIdResponseJson($ret,$error=''){

        $new_array = array();

        if($ret != '' && count($ret) > 0)
        {
            foreach ($ret as $key=>$val)
            {
                $new_array[] = array(
                    "Jobid"=> is_object($val)?$val->Jobid:null ,
                    "HJJobID"=> is_object($val)?$val->HJJobID:null,
                    "HJPackageName"=> is_object($val)?$val->HJPackageName:null,
                    "PackagePay"=> is_object($val)?$val->packagepay:null,
                    "ContactName"=> is_object($val)?$val->ContactName:null,
                    "ContactEmail"=> is_object($val)?$val->ContactEmail:null,
                    "ContactPhone"=> is_object($val)?$val->ContactPhone:null,
                    "ScheduleDate"=> is_object($val)?$val->ScheduleDate:null,
                    "PropertyAddress"=> is_object($val)?$val->PropertyAddress:null,
                    "ScheduleTime"=> is_object($val)?$val->ScheduleTime:null,
                    "StatusID"=> is_object($val)?$val->StatusID:null,
                    "Error"=> $error != ''?$error:null,
                );

            }

        }
        else{
            $new_array[] = array(
                "Jobid"=> null,
                "HJJobID"=> null,
                "HJPackageName"=> null,
                "PackagePay"=> null,
                "ContactName"=> null,
                "ContactEmail"=> null,
                "ContactPhone"=> null,
                "ScheduleDate"=> null,
                "PropertyAddress"=> null,
                "ScheduleTime"=> null,
                "StatusID"=> null,
                "Error"=> $error != ''?$error:null,
            );
        }

        return response()->json($new_array);
    }
    public static function getInfoByID($jobid)
    {
        if($jobid == '')
            return false;

        return JabJobs::find($jobid);
    }

    public static function  checkHJJobIDExistOrNot($HJJobID)
    {
        if($HJJobID == '')
        {
            return false;
        }

        $Jobs = self::where('HJJobID',$HJJobID)->get();

        if(count($Jobs) > 0)
        	return true;
        else
            return false;
    }
	public static function AddJobs($POST)
    {
         #Get address location from property address using google API
	    $property_address = str_replace(' ', '+', $POST['PropertyAddress']);
	    $url              = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&libraries=drawing,geometry,places&key='.env("GOOGLE_API_KEY").'&address='.$property_address;
	    $client           = new Client();
	    $response         = $client->GET($url);
		 $objAddressLoc = json_decode($response->getBody()->getContents());

	    if(count($objAddressLoc->results) > 0)
	        $address_location           = $objAddressLoc->results[0]->geometry->location;

	    if(isset($POST['AssistantsEmails']) && is_array($POST['AssistantsEmails']))
            $POST['AssistantsEmails'] = implode(',',$POST['AssistantsEmails']);


	    $date = explode('-',$POST['ScheduleDate']);

	    /*if(strlen($date[2]) == 2);
        {
            $date[2] = '20' . $date[2];
            $POST['ScheduleDate'] = implode('-',$date);
        }*/

        $post["HJJobID"] = $POST['HJJobID'];
        $post["HJPackageId"] = $POST['HJPackageId'];
        $post["HJPackageName"] = $POST['HJPackageName'];
        $post["ContactName"] = isset($POST['ContactName'])?$POST['ContactName']:'';
        $post["ContactPhone"] = isset($POST['ContactPhone'])?$POST['ContactPhone']:'';
        $post["ContactEmail"] = isset($POST['ContactEmail'])?$POST['ContactEmail']:'';
        $post["ContactOwnerName"] = isset($POST['ContactOwnerName'])?$POST['ContactOwnerName']:'';
        $post["ContactOwnerPhone"] = isset($POST['ContactOwnerPhone'])?$POST['ContactOwnerPhone']:'';
        $post["ContactOwnerEmail"] = isset($POST['ContactOwnerEmail'])?$POST['ContactOwnerEmail']:'';
        $post["ScheduleDate"] = date('Y-m-d',strtotime($POST['ScheduleDate']));//'20'.$date[2]."-".$date[0]."-".$date[1];
        $post["ScheduleSlot"] = $POST['ScheduleSlot'];
        $post["Comments"] = isset($POST['Comments'])?$POST['Comments']:'';
        $post["MustHaveShots"] = isset($POST['MustHaveShots'])?$POST['MustHaveShots']:'';
        $post["PropertyZip"] = $POST['PropertyZip'];
        $post["PropertyState"] = $POST['PropertyState'];
        $post["PropertyCity"] = $POST['PropertyCity'];
        $post["PropertyAddress"] = $POST['PropertyAddress'];
        $post["AddressLocation"] = count($objAddressLoc->results) > 0 ? 'POINT('.$address_location->lng.' '.$address_location->lat.')':'';
        $post["ScheduleStartTime"] = ($POST['ScheduleStartTime'] != 'anytime' ? date('H:i:s',strtotime($POST['ScheduleStartTime'])): 'anytime');
        $post["ScheduleForHours"] = $POST['ScheduleForHours'];
        $post["paymentToPhotographer"] = isset($POST['PackagePay'])?$POST['PackagePay']:isset($POST['PaymentToPhotographer'])?$POST['PaymentToPhotographer']:'';
        $post["packageInstructions"] = isset($POST['PackageInstructions'])?$POST['PackageInstructions']:'';
        $post["AssistantEmails"] = isset($POST['AssistantsEmails'])?$POST['AssistantsEmails']:'';

        //echo"<pre>";print_r($post);die;
        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }
        //echo $params;die;
        $ret = DB::select('call SP_Homejab_SaveJobDetails('.rtrim($params,','  ).')');

		//echo"<pre>";print_r($ret);die;
        return $ret;

    }
	public static function UpdateAssistantEmail($POST)
    {
        if(!is_array($POST))
            return false;

	    self::where('ContactEmail',$POST['ContactEmail'])->update( ['AssistantEmails' => $POST['AssistantEmails']]);
	    return true;
    }
	public static function GetJobListingByUserID($POST)
    {
        if(!is_array($POST))
            return false;

        $post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
        $post["PageNumber"] = (isset($POST['PageNumber']) && $POST['PageNumber'] != '')?$POST['PageNumber']:0;
        $post["PageSize"]   = (isset($POST['PageSize']) && $POST['PageSize'] != '')?$POST['PageSize']:0;
        $post["StatusID"]   = (isset($POST['StatusID']) && $POST['StatusID'] != '')?$POST['StatusID']:0;


        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }
        $ret = DB::select('call SP_HomeJob_API_GetJobListingByUserID('.rtrim($params,','  ).')');
        return JabJobs::hydrate($ret);

    }
	/*public static function AddJobs($POST)
	{

		if(!is_array($POST))
			return false;

		DB::beginTransaction();


		if (strtoupper(LTRIM(RTRIM($POST['ScheduleStartTime']))) == 'ANYTIME')
		{
			# Get UserShiftStartTime
			$UserShiftStartime = AppSettings::select('UserShiftStartTime')->first();
			$ScheduleStartTime = $UserShiftStartime;
		}
		else {
			$timestamp = strtotime($POST['ScheduleStartTime']);
			$ScheduleStartTime = date('H:i:s.u',$timestamp);
		}

		$POST['ScheduleStartTime'] = $ScheduleStartTime;
		$POST['ScheduleForHours'] = is_numeric($POST['ScheduleForHours'])?$POST['ScheduleForHours']:'0';
		$POST['AssistantsEmails'] = implode(',',$POST['AssistantsEmails']);


		$objjobs = new Jobs();
		$objjobs->fill($POST);
		$JobData = $objjobs->save();

		if ($JobData) {

			# add job schedule
			//echo"<pre>";print_r($POST);die;
			# set post data

			$_post['JobId'] = $objjobs->JobId;
			$_post['HJJobID'] = $POST['HJJobID'];
			$_post['ScheduleDate'] = $POST['ScheduleDate'];
			$_post['ScheduleTime'] = $ScheduleStartTime;
			$_post['StatusID'] = 1;
			$_post['IsActive'] = 1;
			$_post['PackagePay'] = $POST['PaymentToPhotographer'];
			$_post['PackageInstructions'] = $POST['PackageInstructions'];


			$objJobSchedule = new JobSchedule();
			$objJobSchedule->fill($_post);

			$JobSchData = $objJobSchedule->save();
			$objJobSchedule->JobScheduleId;

			# add job log
			$_post = array();
			$_post['JobId'] = $objjobs->JobId;
			$_post['JobScheduleId'] = $objJobSchedule->JobScheduleId;
			$_post['Description'] = 'New Job Fetch from homejab.com at '.date('Y-m-d H:i:sA');
			$_post['CreatedBy'] = 0;
			$_post['CreatedOn'] = date('Y-m-d H:i:s');

			$objJoblog= new JobLog();
			$objJoblog->fill($_post);

			$JobLogData = $objJoblog->save();
			//echo $objJoblog->LogId;die;

			DB::commit();
			return $JobData;
		}
		DB::rollback();

		return false;
	}*/
	public static function GetJobDetailsBy_UserId_JobID($POST)
    {
        if(!is_array($POST))
            return false;

        $post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
        $post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_HomeJob_API_GetJobDetailsBy_UserID_JobID('.rtrim($params,','  ).')');
        //echo"<pre>";print_r(Jobs::hydrate($ret));die;

        return JabJobs::hydrate($ret);
    }
    public static function getJobUserNameFromJobId($jobid)
    {
        if($jobid != '')
        {
           return  DB::table('tbl_jobusertime')
            ->select('tbl_users.Name')
            ->leftJoin('tbl_users','tbl_users.UserId', '=', 'tbl_jobusertime.UserId')
            -> where('tbl_jobusertime.JobId',$jobid)
            ->first();
        }
    }
    public static function AcceptJob($POST)
    {
	    # set twillo credential
        if(!is_array($POST))
            return false;

		$post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
        $post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;
        $post["AcceptDateTime"]= date('Y-m-d H:i:s',strtotime($POST['AcceptDateTime']));

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }
		$ret = DB::select('call SP_HomeJob_API_AcceptJob('.rtrim($params,','  ).')');
        return $ret[0]->Result;
    }
    public static function SendEmailToCustomerAgentForAccpetedJob($jobid)
    {
	    /*$data = array('name'=>"Matrix");
	    $send=Mail::send('emails.layout', $data, function($message) {
		    $message->to('social.oequal@gmail.com', 'Tutorials Points')->subject
		    ('Laravel HTML Testing Mails');
		    $message->from('no-reply@matrixm.info','Matrix');
	    });*/
        if($jobid == '')
            return false;

        $objJobs = self::select('HJJobID','ContactEmail', 'ContactOwnerEmail', 'AssistantEmails','PropertyAddress','ScheduleDate','ScheduleStartTime')->where('JobId',$jobid)->first();

        if(is_object($objJobs))
        {
            # get job user from job id
            $objJobUser = JabJobs::getJobUserNameFromJobId($jobid);
            $arrJobs['HJJobID'] = $objJobs->HJJobID;
            $arrJobs['ContactEmail'] = $objJobs->ContactEmail;
            $arrJobs['ContactOwnerEmail'] = $objJobs->ContactOwnerEmail;
            $arrJobs['AssistantEmails'] = $objJobs->AssistantEmails;
            $arrJobs['PropertyAddress'] = $objJobs->PropertyAddress;
            $arrJobs['ScheduleDate'] = $objJobs->ScheduleDate;
            $arrJobs['ScheduleStartTime'] = $objJobs->ScheduleStartTime;
            $arrJobs['JobUser'] = $objJobUser->Name;
            $arrJobs['ScheduleDateTime']=date('d-m-Y h:i A',strtotime($arrJobs['ScheduleDate'].' '.$arrJobs['ScheduleStartTime']));
			 # send email

	        //$ToAddress = $arrJobs['ContactEmail'].','.$arrJobs['ContactOwnerEmail'].','.$arrJobs['AssistantEmails'].','.Config('constants.JOB_CONFIRM_RECIVER_EMAILID');
			try{
				$arrToAddressList = array();
				if($arrJobs['ContactEmail'] != '')
					array_push($arrToAddressList,$arrJobs['ContactEmail']);
				if($arrJobs['ContactOwnerEmail'] != '')
					array_push($arrToAddressList,$arrJobs['ContactOwnerEmail']);
				if($arrJobs['AssistantEmails'] != '')
					array_push($arrToAddressList,$arrJobs['AssistantEmails']);

				array_push($arrToAddressList,Config('constants.JOB_CONFIRM_RECIVER_EMAILID'));
				foreach($arrToAddressList as $address)
				{
					$ToAddressList = '';
					$send_email = Mail::send('emails.CustomerAgentEmail', ['arrJobs' => $arrJobs], function ($m) use ($arrJobs,$address) {
						//$ToAddress = [$arrJobs['ContactEmail'].','.$arrJobs['ContactOwnerEmail'].','.$arrJobs['AssistantEmails'].','.Config('constants.JOB_CONFIRM_RECIVER_EMAILID')];
						$ToAddressList = Custom::getToAddressList($address);
						//$m->from(Config('constants.DEFAULT_FROM_EMAIL_ADDRESS'));
						$m->from(Config('mail.from.address'), Config('mail.from.name'));
						$m->to($ToAddressList)->subject(' Your shoot has been confirmed / ' . $arrJobs['PropertyAddress']);
					});
				}
			}
	        catch(\Exception $e)
	        {
	        	 $error_content = "JobId : $jobid <br/>
									Email Type :  SendEmail To Customer Agent For AccpetedJob<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
		        file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),FILE_APPEND);
	        }
        }
    }
    public static function SendEmailToPhotographersForAcceptedJob($jobid)
    {
    	 if($jobid == '')
            return false;
        $objJobs = self::select('HJJobID','HJPackageName','PropertyAddress','ScheduleDate','ScheduleSlot')->where('JobId',$jobid)->first();
        if(is_object($objJobs))
        {
	        $arrJobAssignList=JabJobs::JobAssignDetailForMail($jobid);
	        //echo"<pre>";print_r($arrJobAssignList);die;
	        #$html = View('emails.PhotographersJobAccepted', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList])->render();
//	        file_put_contents('declineJobEmail.html',$html); die;
            # send email
            $objJobs->ScheduleDate = date('d-m-Y',strtotime($objJobs->ScheduleDate));
	        try{
		        $send_email = Mail::send('emails.PhotographersJobAccepted', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList], function ($m) use ($objJobs) {
			        $ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
			        $ToAddressList = Custom::getToAddressList($ToAddress);
			        $m->from(Config('mail.from.address'), Config('mail.from.name'));
			        $m->to($ToAddressList)->subject('Job Accepted - ' . $objJobs->HJJobID);
		        });
	        }
	        catch(\Exception $e)
	        {
		        $error_content = "JobId : $jobid <br/>
									Email Type :  SendEmail To Photographers For AcceptedJob<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
		        file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),FILE_APPEND);
	        }
        }
    }
    public static function SendMessageToCustomerForAccpetedJob($jobid)
    {
    	if($jobid == '')
            return false;

        $objJobs = self::select('ContactPhone','ContactOwnerPhone','PropertyAddress','ScheduleDate','ScheduleStartTime')->where('JobId',$jobid)->first();

        if(is_object($objJobs))
        {
            $objJobuser = JabJobs::getJobUserNameFromJobId($jobid);

            # set twillo credential
            $sid = env('TWILLO_ACCOUNT_SID');
            $token = env('TWILLO_AUTH_TOKEN');
            $client = new TwilloClient($sid, $token);
			$message_body = "Your HomeJab shoot has been confirmed & scheduled with ".$objJobuser->Name."\r\nDate & Time: ".date("d-m-Y h:i A",strtotime($objJobs->ScheduleDate.' '.$objJobs->ScheduleStartTime))."\r\nProperty Address: ".$objJobs->PropertyAddress."\r\nIf you need to cancel or reschedule, please email us: ".\Config('constants.SALES_EMAIL_ADDRESS');
            // Use the client to do fun stuff like send text messages!
            if(isset($objJobs->ContactPhone) && $objJobs->ContactPhone != '' )
            {
            	try{
                	$message = $client->messages->create(
                        // the number you'd like to send the message to
                        '+1'.preg_replace('/[^0-9]/', '',$objJobs->ContactPhone),
                        array(
                            // A Twilio phone number you purchased at twilio.com/console
                            'from' => env('TWILLO_FROM_NUMBER'),
                            // the body of the text message you'd like to send
                            'body' => $message_body
                        )
                    );

                }
                catch(\Exception $e)
                {
                	$error_content = "Controller name : Send SMS <br/>
                                Mobile Number:".$objJobs->ContactPhone."<br/>
                                Date & Time:".date('Y-m-d H:i:s')." <br/>
                                Error Message : ".$e->getMessage()."<hr/>";

                    file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),FILE_APPEND);
                }
            }
            if(isset($objJobs->ContactOwnerPhone) && $objJobs->ContactOwnerPhone != '' )
            {
                try{
                    $client->messages->create(
                    // the number you'd like to send the message to
                        '+1'.preg_replace('/[^0-9]/', '',$objJobs->ContactOwnerPhone),
                        array(
                            // A Twilio phone number you purchased at twilio.com/console
                            'from' => env('TWILLO_FROM_NUMBER'),
                            // the body of the text message you'd like to send
                            'body' => $message_body
                        )
                    );
                }
                catch(\Exception $e)
                {
                    $error_content = "Controller name : Send SMS <br/>
                                Mobile Number:".$objJobs->ContactPhone."<br/>
                                Date & Time:".date('Y-m-d H:i:s')." <br/>
                                Error Message : ".$e->getMessage()."<hr/>";

                    file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,FILE_APPEND));
                }
            }
        }
    }
	public static function DeclineJob($POST)
	{
		if(!is_array($POST))
			return false;

		$post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
		$post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;
		$post["DeclineDateTime"]= date('Y-m-d H:i:s',strtotime($POST['DeclineDateTime']));
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= "'".addslashes(stripslashes(trim($val)))."'".",";
		}

		$ret = DB::select('call SP_HomeJob_API_DeclineJob('.rtrim($params,','  ).')');
		return $ret[0]->Result;
	}
    public static function SendEmailToPhotographersForDeclinedJob($jobid){

		if($jobid == '')
		    return false;
	    $objJobs = self::select('HJJobID','HJPackageName','PropertyAddress','ScheduleDate','ScheduleSlot')->where('JobId',$jobid)->first();
	    if(is_object($objJobs))
	    {
	    	$arrJobDeclineList=JabJobs::HomeJobJobDeclineDetailForMail($jobid);
	    	#$html = View('emails.PhotographersJobDecline', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList])->render();
		    #file_put_contents('declineJobEmail.html',$html);  die;
		    # send email
		    $objJobs->ScheduleDate = date('d-m-Y',strtotime($objJobs->ScheduleDate));
		       try{
			    $send_email = Mail::send('emails.PhotographersJobDecline', ['objJobs' => $objJobs,'arrJobDeclineList'=>$arrJobDeclineList], function ($m) use ($objJobs) {
				    $ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
				    $ToAddressList = Custom::getToAddressList($ToAddress);
				    $m->from(Config('mail.from.address'), Config('mail.from.name'));
				    $m->to($ToAddressList)->subject('Job Declined - ' . $objJobs->HJJobID);
			    });
		    }
		    catch(\Exception $e)
		    {
			    $error_content = "JobId : $jobid <br/>
									Email Type :  SendEmail To Photographers For Declined Job<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
			    file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),FILE_APPEND);
		    }
	    }
    }
	public static function HomeJobJobAssignDetailForMail($jobid){
    	if($jobid == '')
			return false;

		$post["JobId"]  = $jobid;
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= addslashes(stripslashes(trim($val)));
		}
		$ret = DB::select('call SP_HomeJob_JobAssignDetailsForMail('.rtrim($params,','  ).')');
		return $ret;
    }
    public static function HomeJobJobDeclineDetailForMail($jobid){
    	if($jobid == '')
			return false;

		$post["JobId"]  = $jobid;
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= addslashes(stripslashes(trim($val)));
		}
		$ret = DB::select('call SP_HomeJob_JobDeclineDetailsForMail('.rtrim($params,','  ).')');
		return $ret;
    }
	public static function JobAssignDetailForMail($jobid){
		if($jobid == '')
			return false;

		$post["JobId"]  = $jobid;
		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= addslashes(stripslashes(trim($val)));
		}
		$ret = DB::select('call JobAssignDetailsForMail('.rtrim($params,','  ).')');
		return $ret;
	}
    public static function EndJob($POST){
	    if(!is_array($POST))
		    return false;
	    $post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
	    $post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;
	    $post["EndDateTime"]= $POST['EndDateTime'];
	    $params = '';
	    foreach($post as $key=>$val)
	    {
		    $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
	    }
	    $ret = DB::select('call SP_HomeJob_API_EndJob('.rtrim($params,','  ).')');
	    return $ret[0]->Result;
    }

	public static function SendEmailToPhotographersForEndJob($jobid){
    	if($jobid == '')
			return false;
		$objJobs = self::select('HJJobID','HJPackageName','PropertyAddress','ScheduleDate','ScheduleSlot')->where('JobId',$jobid)->first();
		if(is_object($objJobs))
		{

			/*$html = View('emails.PhotographersJobClosed', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList])->render();
			file_put_contents('jobclosedEmail.html',$html);  die;*/
			# send email
			$arrJobAssignList=JabJobs::HomeJobJobAssignDetailForMail($jobid);
			$objJobs->ScheduleDate = date('d-m-Y',strtotime($objJobs->ScheduleDate));

			try{

				$send_email = Mail::send('emails.PhotographersJobClosed', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList], function ($m) use ($objJobs) {
					$ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
					$ToAddressList = Custom::getToAddressList($ToAddress);
					$m->from(Config('mail.from.address'), Config('mail.from.name'));
					$m->to($ToAddressList)->subject('Job Closed - ' . $objJobs->HJJobID);
				});
			}
			catch(\Exception $e)
			{
				$error_content = "JobId : $jobid <br/>
									Email Type :  SendEmail To Photographers For Closed Job<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
				file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),FILE_APPEND);
			}
		}
	}
	public static function StartJob($POST){
		if(!is_array($POST))
			return false;
		$post["UserId"]     = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:0;
		$post["JobId"]      = (isset($POST['JobId']) && $POST['JobId'] != '')?$POST['JobId']:0;
		$post["StartDateTime"]= date('Y-m-d H:i:s',strtotime($POST['StartDateTime']));

		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= "'".addslashes(stripslashes(trim($val)))."'".",";
		}
		$ret = DB::select('call SP_Homejab_API_StartJob ('.rtrim($params,','  ).')');

		return $ret;
	}
	public static function AssignPhotographer($JobId,$ScheduleDateTime,$LoggedUserId,$RankID = 1){

		if($JobId == '')
			return false;

		$retvalue = true;

		if($ScheduleDateTime == '')
			$ScheduleDateTime = 'null';
		else
			$ScheduleDateTime = "'".$ScheduleDateTime."'";

		if($LoggedUserId == '')
			$LoggedUserId = 'null';
		else
			$LoggedUserId = "'".$LoggedUserId."'";

		//echo 'call HomeJab_Job_GetAvailablePhotographers('."'".$JobId."',".$ScheduleDateTime.",".$LoggedUserId.","."'".$RankID."'".')';die;
		# Call store procedure for get available photographers
		$arr_avilablephtographers = DB::select('call HomeJab_Job_GetAvailablePhotographers('."'".$JobId."',".$ScheduleDateTime.",".$LoggedUserId.","."'".$RankID."'".')');

		if(is_array($arr_avilablephtographers)  && count($arr_avilablephtographers) > 0){

			$drivetimelist = '';
			$useridlist    = '';
			foreach($arr_avilablephtographers as $key=>$val){
					$post =array();
				# Now set array for calculate drive time
				$post['source_address'] =	$val->UserAddress;
				$post['destination_address'] =	$val->JobAddress;
				# Create Html File On server With This Detail
				$calculate_drivetime=JabJobs::CalculatetDriveTime($post);
				$drivetime_detail = "jobid : ".$JobId." <br/>
									total available photographer : ".count($arr_avilablephtographers)." <br/>
									user address : ".$val->UserAddress." <br/>
		                            job address: ".$val->JobAddress." <br/>
		                            drivetime in hour : ".$calculate_drivetime." <br/><hr>";
				file_put_contents('GetDistance.html', print_r($drivetime_detail, true), FILE_APPEND );
				//echo $val->UserId;
				if($calculate_drivetime > 0)
				{
					$drivetime = 50;
					if($calculate_drivetime <= $drivetime)
					{
						# Set comma seprated userlist and time
						$drivetimelist .= $val->UserId.'$'.date('Y-m-d H:i:s',strtotime($val->UserAvailabilityTime)).',';
						$useridlist    .= $val->UserId.',';
					}
				}
			}

			//echo rtrim($useridlist,',');;die;
			# Call store procedure to assign job to photograhers
			$arrdata['JobId'] = $JobId;
			$arrdata['Photographers'] = rtrim($useridlist,',');
			$arrdata['UserIdsAVLDATETIME'] = rtrim($drivetimelist,',');

			$retvalue = JabJobs::AssignJobtoPhotographers($arrdata);
			# Send Push notification to perticular user for job
			if($retvalue[0]->Result == 1)
			{
				$arruseids = explode(',',rtrim($useridlist,','));

				if(count($arruseids) > 0)
				{
					$post = array();
					foreach($arruseids as $key=>$val)
					{
						$post['UserID']         = $val;
						$post['JobID']          = $JobId;
						$post['Message']        = 'Job has been assigned to you';
						$post['click_action']   = 'OPEN_HOMEJAB_NOTIFICATION_ACTIVITY';
						$post['ShowDetailsButton']= true;

						JabUser::SendPushNotificationToUser($post);
					}
				}
			}
		}
		if($ScheduleDateTime == '' || $ScheduleDateTime = 'null')
		{
			JabJobs::SendEmailToAssignJobPhotographers($JobId);
		}
		return $retvalue;
	}

	public function ReScheduleAssignPhotographer($JobId,$ScheduleDateTime,$PhotographerId,$LoggedUserId)
    {
        if($JobId == '')
            return false;

        $post['JobID']              = $JobId;
        $post['ScheduleDateTime']   = $ScheduleDateTime;
        $post['PhotographerId']     = $PhotographerId;
        $post['LoggedUserId']       = $LoggedUserId;

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }
        $ret = DB::select('call HomeJab_Job_ReScheduleAssignPhotographer ('.rtrim($params,','  ).')');
        //echo"<pre>";print_r('call HomeJab_Job_ReScheduleAssignPhotographer ('.rtrim($params,','  ).')');

        if(isset($ret[0]->Result) && $ret[0]->Result == 1){

            #fetch jobs detail based on jobid
            $objjabjobs = $this->newQuery()->where('JobId', $JobId)->get()->first();

            $PhotographerIds = explode(',', $PhotographerId);

            foreach ($PhotographerIds as $key => $val) {

                #send push notification to user
                $post['UserID'] = $val;
                $post['JobID'] = $JobId;
                $post['Message'] = "Order $objjabjobs->HJJobID has been Rescheduled";
                $post['click_action'] = 'OPEN_HOMEJAB_NOTIFICATION_ACTIVITY';
                $post['ShowDetailsButton'] = true;

                JabUser::SendPushNotificationToUser($post);
            }

            #send mail
            if (is_object($objjabjobs)) {

                $arrJobAssignList = JabJobs::HomeJobJobAssignDetailForMail($JobId);

                //$html = View('emails.AssignJobPhotographer', ['objJobs' => $objjabjobs,'arrJobAssignList'=>$arrJobAssignList])->render();
                //file_put_contents('AssignJobPhotographerEmail.html',$html); die;

                $send_email = Mail::send('emails.AssignJobPhotographer', ['objJobs' => $objjabjobs, 'arrJobAssignList' => $arrJobAssignList], function ($m) use ($objjabjobs) {

                    $ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
                    $ToAddressList = Custom::getToAddressList($ToAddress);
	                $m->from(Config('mail.from.address'), Config('mail.from.name'));
                    $m->to($ToAddressList)->subject('Job Created - ' . $objjabjobs->HJJobID);
                });
            }
        }
        return $ret;
    }

	public static function CalculatetDriveTime($post){

		$source_address = $post['source_address'];
		$destination_address = $post['destination_address'];
		$driveTimeurl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$source_address."&destinations=".$destination_address."&mode=driving&language=us-en&sensor=false&key=".env("GOOGLE_API_KEY");

		# Get drive time using google api
		$client = new Client();

		$response = $client->GET($driveTimeurl);

		$api_data = $response->getBody()->getContents();

		if(isset($api_data) && $api_data != '')
		{
			$decode_api_data = json_decode($api_data,true);
			if($decode_api_data['status'] == 'OK')
			{
				# Now Calculate drive time in hours
				$drive_time = $decode_api_data['rows'][0]['elements'][0]['duration']['value']/60;
				if($drive_time < 1)
					$retvalue = ceil($drive_time);
				else
					$retvalue = floor($drive_time);
			}
		}
		if(isset($retvalue) && $retvalue > 0)
			return $retvalue;
		else
			return false;
	}
	public static function SendEmailToAssignJobPhotographers($JobId)
	{
		if($JobId == '')
			return false;

		# Get job details from jobid
		$objJobs = self::select('HJJobID','HJPackageName','PackagePay','PropertyAddress','ScheduleDate','ScheduleSlot')->where('JobId',$JobId)->first();

		if(is_object($objJobs))
		{
			# Get job assign details by jobid
			$arrJobAssignList = JabJobs::HomeJobJobAssignDetailForMail($JobId);
			# send email
			$objJobs->ScheduleDate = date('d-m-Y',strtotime($objJobs->ScheduleDate));
			/*$html = View('emails.AssignJobPhotographer', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList])->render();
			file_put_contents('AssignJobPhotographerEmail.html',$html); die;*/
			try{
				$send_email = Mail::send('emails.AssignJobPhotographer', ['objJobs' => $objJobs,'arrJobAssignList'=>$arrJobAssignList], function ($m) use ($objJobs) {

					$ToAddress = Config('constants.JOB_CREATE_RECEIVER_EMAILID');
					$ToAddressList = Custom::getToAddressList($ToAddress);
					$m->from(Config('mail.from.address'), Config('mail.from.name'));
					$m->to($ToAddressList)->subject('Job Created - ' . $objJobs->HJJobID);
				});
			}
			catch(\Exception $e)
			{
				$error_content = "JobId : $JobId <br/>
									Email Type :  SendEmail To AssignJobPhotographers<br/>
									Controller name : Send Email <br/>
	                                Date & Time:".date('Y-m-d H:i:s')." <br/>
	                                Error Message : ".$e->getMessage()."<br/>";
				file_put_contents('HomeJabMailigErrorLog.html',print_r($error_content,true),FILE_APPEND);
			}
		}
	}
	public static function AssignJobtoPhotographers($POST){

		if(!is_array($POST))
			return false;

		$post["JobId"]              = $POST["JobId"];
		$post["Photographers"]      = $POST["Photographers"];
		$post["UserIdsAVLDATETIME"] = $POST["UserIdsAVLDATETIME"];

		$params = '';
		foreach($post as $key=>$val)
		{
			$params .= "'".addslashes(stripslashes(trim($val)))."'".",";
		}
		# Call store procedure to assign photograhers for perticular job
		$ret = DB::select('call HomeJab_Job_AssignPhotographers ('.rtrim($params,','  ).')');

		return $ret;

	}


    public function updateOrderDetailByJobId($POST,$jobid){

        if(!is_array($POST))
            return false;

        #Get address location from property address using google API
        $address = str_replace(' ', '+', $POST['PropertyAddress']);
        $url              = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&libraries=drawing,geometry,places&key='.env("GOOGLE_API_KEY").'&address='.$address;
        $client           = new Client();
        $response         = $client->GET($url);

        $objAddressLoc = json_decode($response->getBody()->getContents());

        $address_location           = $objAddressLoc->results[0]->geometry->location;

        $post['jobid']             = $jobid;
        $post['Email']             = $POST['Email'];
        $post['ContactName']       = $POST['ContactName'];
        $post['ContactPhone']      = $POST['ContactPhone'];
        $post['ContactOwnerPhone'] = isset($POST['ContactOwnerPhone'])?$POST['ContactOwnerPhone']:'';
        $post['PropertyZip']       = $POST['PropertyZip'];
        $post['PropertyState']     = $POST['PropertyState'];
        $post['PropertyAddress']   = $POST['PropertyAddress'];
        $post['ContactEmail']      = $POST['ContactEmail'];
        $post['ContactOwnerName']  = isset($POST['ContactOwnerName'])?$POST['ContactOwnerName']:'';
        $post['ContactOwnerEmail'] = isset($POST['ContactOwnerEmail'])?$POST['ContactOwnerEmail']:'';
        $post['Comments']          = isset($POST['Comments'])?$POST['Comments']:'';
        $post['MustHaveShots']     = isset($POST['MustHaveShots'])?$POST['MustHaveShots']:'';
        $post['PackagePay']        = isset($POST['PackagePay'])?$POST['PackagePay']:'';
        $post['PackageInstructions'] = isset($POST['PackageInstructions'])?$POST['PackageInstructions']:'';
        $post['AddressLocation']   = 'POINT('.$address_location->lng.' '.$address_location->lat.')';

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_UpdateOrderDetails('.rtrim($params,','  ).')');

        return $ret;
    }

    public function GetJobsDetailByStatusId($statusid = ''){

	    $query = $this->newQuery('tbl_jobschedule.JobId','tbl_jobschedule.HJJobID','tbl_jobs.HJPackageName','tbl_jobs.PropertyAddress','tbl_jobs.ContactName','tbl_jobs.ContactOwnerEmail','tbl_jobschedule.ScheduleDate','tbl_jobschedule.ScheduleTime')
                ->join('tbl_jobschedule','tbl_jobschedule.JobId','=','tbl_jobs.JobId')
		        ->join('tbl_status','tbl_status.StatusId','=','tbl_jobschedule.StatusID')
                ->where('tbl_jobschedule.IsActive','=',1);

	    if($statusid != '')
	        $query->where('tbl_jobschedule.StatusID','=',$statusid);

	    return $query;
    }

    public function DeleteJob($POST){
        if(!is_array($POST))
            return false;

        $post["JobId"]      = $POST['JobId'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_Homejab_DeleteJob('.rtrim($params,','  ).')');

        return $ret;
    }

    public function GetAllJobReportsOld($POST,$status)
    {
        if (!is_array($POST))
            return false;

        if($status == Config('constants.JobForNextDay'))
        {
            $post["StartDate"]  = (isset($POST['StartDate']) && $POST['StartDate'] != '')?$POST['StartDate']:date('Y-m-d H:i:s',strtotime('+1 days'));
            $post["EndDate"]    = (isset($POST['EndDate']) && $POST['EndDate'] != '')?$POST['EndDate']:date('Y-m-d H:i:s',strtotime('+1 days'));
            $post["StatusId"]   = (isset($POST['StatusId']) && $POST['StatusId'] != '')?$POST['StatusId']:3;
        }
        elseif ($status == Config('constants.JobsForMonth'))
        {
            $post["StartDate"] = (isset($POST['StartDate']) && $POST['StartDate'] != '') ? $POST['StartDate'] : date('Y-m-d H:i:s');
            $post["EndDate"] = (isset($POST['EndDate']) && $POST['EndDate'] != '') ? $POST['EndDate'] : date('Y-m-d H:i:s');
            $post["StatusId"] = (isset($POST['StatusId']) && $POST['StatusId'] != '') ? $POST['StatusId'] : 8;
        }
        elseif ($status == Config('constants.PayPerUser'))
        {
            $post["StartDate"] = (isset($POST['StartDate']) && $POST['StartDate'] != '') ? $POST['StartDate'] : date('Y-m-d H:i:s');
            $post["EndDate"] = (isset($POST['EndDate']) && $POST['EndDate'] != '') ? $POST['EndDate'] : date('Y-m-d H:i:s');
            $post["StatusId"] = (isset($POST['StatusId']) && $POST['StatusId'] != '') ? $POST['StatusId'] : 8;
        }
        elseif ($status == Config('constants.StatusReport'))
        {
            $post["StartDate"] = (isset($POST['StartDate']) && $POST['StartDate'] != '') ? $POST['StartDate'] : '';
            $post["EndDate"] = (isset($POST['EndDate']) && $POST['EndDate'] != '') ? $POST['EndDate'] : date('Y-m-d H:i:s');
            $post["StatusId"] = (isset($POST['StatusId']) && $POST['StatusId'] != '') ? $POST['StatusId'] : 6;
        }

        $post["UserId"] = (isset($POST['UserId']) && $POST['UserId'] != '')?$POST['UserId']:'';
        $post["Type"]       = $status;
       /* $post["PageNumber"] = (isset($POST['PageNumber']) && $POST['PageNumber'] != '')?$POST['PageNumber']:1;
        $post["PageSize"]   = (isset($POST['PageSize']) && $POST['PageSize'] != '')?$POST['PageSize']:100;
        */
        $params = '';
        foreach ($post as $key => $val) {
            $params .= "'" . addslashes(stripslashes(trim($val))) . "'" . ",";
        }



        $ret = DB::select('call SP_HomeJob_Reports_GetAllJobs1(' . rtrim($params, ',') . ')');


        return $ret;
    }
    public function GetAllJobReports($POST,$status)
    {
        if (!is_array($POST))
            return false;

        $post = array();
        if($status == Config('constants.JobForNextDay'))
        {
            if(!isset($POST['NotCheckDate']))
            {
                $post["StartDate"] = (isset($POST['StartDate']) && $POST['StartDate'] != '') ? $POST['StartDate'] : date('Y-m-d', strtotime('+1 days'));
                $post["EndDate"] = (isset($POST['StartDate']) && $POST['StartDate'] != '') ? $POST['StartDate'] : date('Y-m-d', strtotime('+1 days'));
            }
            $post["StatusId"]   = (isset($POST['StatusId']) && $POST['StatusId'] != '')?$POST['StatusId']:3;
        }
        elseif ($status == Config('constants.JobsForMonth'))
        {
            if(!isset($POST['NotCheckDate']))
                $post["StartDate"] = (isset($POST['StartDate']) && $POST['StartDate'] != '') ? $POST['StartDate'] : date('Y-m-d');
            $post["StatusId"] = (isset($POST['StatusId']) && $POST['StatusId'] != '') ? $POST['StatusId'] : 8;
        }
        elseif ($status == Config('constants.PayPerUser')) {

            if (!isset($POST['NotCheckDate'])){
                $post["StartDate"] = (isset($POST['StartDate']) && $POST['StartDate'] != '') ? $POST['StartDate'] : date('Y-m-d');
                $post["EndDate"] = (isset($POST['EndDate']) && $POST['EndDate'] != '') ? $POST['EndDate'] : date('Y-m-d');
            }
            $post["StatusId"] = (isset($POST['StatusId']) && $POST['StatusId'] != '') ? $POST['StatusId'] : 8;
        }
        elseif ($status == Config('constants.StatusReport')) {
            if (!isset($POST['NotCheckDate'])){
                $post["StartDate"] = (isset($POST['StartDate']) && $POST['StartDate'] != '') ? $POST['StartDate'] : date('Y-m-d');
                $post["EndDate"] = (isset($POST['EndDate']) && $POST['EndDate'] != '') ? $POST['EndDate'] : date('Y-m-d');
            }
            $post["StatusId"] = (isset($POST['StatusId']) && $POST['StatusId'] != '') ? $POST['StatusId'] : 6;
        }

        $query = $this->newQuery()
            ->select('tbl_jobs.JobId','tbl_jobs.HJJobId','tbl_jobs.ContactName','tbl_jobs.ContactPhone','tbl_jobs.ContactEmail','tbl_jobs.MustHaveShots','tbl_jobs.ContactOwnerPhone','tbl_jobs.ContactOwnerEmail','tbl_jobs.ScheduleSlot','JS.ScheduleTime','tbl_jobs.ScheduleForHours','tbl_jobs.PropertyAddress','tbl_jobs.Comments','JS.PackagePay','JS.PackageInstructions','tbl_jobs.HJPackageName','JS.ScheduleDate','Jst.Status')
            ->leftJoin('tbl_jobschedule AS JS','tbl_jobs.JobId','=','JS.JobId')
            ->leftJoin('tbl_status AS Jst','JS.StatusID','=','Jst.StatusId');

        if(isset($post['StatusId']))
            $query->whereIn('JS.StatusID',[$post['StatusId']]);

        $arrStatus = explode(',',$post['StatusId']);

        if($status != Config('constants.StatusReport') || ($status == Config('constants.StatusReport') && !in_array(4,$arrStatus) && !in_array(6,$arrStatus) && !in_array(7,$arrStatus) && !in_array(9,$arrStatus)))
        {

            $query->select('tbl_jobs.JobId','tbl_jobs.HJJobId','tbl_jobs.ContactName','tbl_jobs.ContactPhone','tbl_jobs.ContactEmail','tbl_jobs.MustHaveShots','tbl_jobs.ContactOwnerPhone','tbl_jobs.ContactOwnerEmail','tbl_jobs.ScheduleSlot','JS.ScheduleTime','tbl_jobs.ScheduleForHours','tbl_jobs.PropertyAddress','tbl_jobs.Comments','JS.PackagePay','JS.PackageInstructions','tbl_jobs.HJPackageName','JS.ScheduleDate','Jst.Status','U.Name','JU.UpdatedOn')
                ->leftJoin('tbl_jobuser AS JU','JS.JobId','=','JU.JobId')
                ->leftJoin('tbl_users AS U','JU.UserId','=','U.UserId')
                ->where('JS.IsActive','=',1)
                ->where('JU.IsActive','=',1);
        }
        elseif($status == Config('constants.StatusReport') && (in_array(4,$arrStatus) || in_array(7,$arrStatus) || in_array(9,$arrStatus)))
        {
            $query->where('JS.IsActive','=',1);
        }
        elseif($status == Config('constants.StatusReport'))
        {

            $query->select('tbl_jobs.JobId','tbl_jobs.HJJobId','tbl_jobs.ContactName','tbl_jobs.ContactPhone','tbl_jobs.ContactEmail','tbl_jobs.MustHaveShots','tbl_jobs.ContactOwnerPhone','tbl_jobs.ContactOwnerEmail','tbl_jobs.ScheduleSlot','JS.ScheduleTime','tbl_jobs.ScheduleForHours','tbl_jobs.PropertyAddress','tbl_jobs.Comments','JS.PackagePay','JS.PackageInstructions','tbl_jobs.HJPackageName','JS.ScheduleDate','Jst.Status','U.Name','JU.UpdatedOn')
                ->leftJoin('tbl_jobuser AS JU','JS.JobId','=','JU.JobId')
                ->leftJoin('tbl_users AS U','JU.UserId','=','U.UserId')
                ->where('JS.IsActive','=',1)
                ->where(function($q) {
                    $q->where('JU.IsActive', null)
                        ->orWhere('JU.IsActive', '0');
                });
        }
        if($status == Config('constants.PayPerUser'))
        {
	        $query->select('tbl_jobs.JobId','tbl_jobs.HJJobId','tbl_jobs.ContactName','tbl_jobs.ContactPhone','tbl_jobs.ContactEmail','tbl_jobs.MustHaveShots','tbl_jobs.ContactOwnerPhone','tbl_jobs.ContactOwnerEmail','tbl_jobs.ScheduleSlot','JS.ScheduleTime','tbl_jobs.ScheduleForHours','tbl_jobs.PropertyAddress','tbl_jobs.Comments','JS.PackagePay','JS.PackageInstructions','tbl_jobs.HJPackageName','JS.ScheduleDate','Jst.Status','JUT.CloseTime AS UpdatedOn');
			$query->leftJoin('tbl_jobusertime AS JUT','JU.JobId','=','JUT.JobId')
                ->where('U.UserId','=',isset($POST['UserId'])?$POST['UserId']:'0');
        }
        if(isset($post['StartDate']) || isset($post['EndDate']))
        {
            if($status == Config('constants.JobsForMonth'))
            {
                $query->whereRaw('MONTH(JS.ScheduleDate) = MONTH("'.$post['StartDate'].'")')
                    ->whereRaw('YEAR(JS.ScheduleDate) = YEAR("'.$post['StartDate'].'")') ;
            }
            elseif($status == Config('constants.PayPerUser'))
            {
                $query->whereBetween('JUT.CloseTime',[$post["StartDate"],$post['EndDate']]);


            }
            else
            {
                $query->whereBetween('JS.ScheduleDate',[$post["StartDate"],$post['EndDate']]);
            }
        }


        if(isset($POST['JobId']))
            $query->where('tbl_jobs.JobId','=',$POST['JobId']);
        /*echo"<pre>";print_r($query->toSql());
        echo"<pre>";print_r($query->getBindings());*/

        return $query;
    }
}
