<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Mail;

class JabUserPackage extends Model
{
    const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'HJPackageId', 'HJPackageName', 'HJPackageType', 'UserId', 'CreatedBy', 'CreatedOn', 'UpdatedBy', 'UpdatedOn', 'HJPackageShortDescription', 'HJPackagePrice'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $table = "tbl_userspackage";

    protected $primaryKey = 'PackageId';

    //public $timestamps = false;

    public static $rulesOnAssignPackages = [
        'package_list'      => 'required',
        'photographer_list' => 'required'

    ];

    public static  $rulesOnAssignPackage = [
        'package_list'  => 'required'
    ];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function JabUser(){
        return $this->belongsTo('App\JabUser', 'UserId',UserId);
    }

    public function CreateorUpdate($id,$POST){
        print_r($id); die;
        $record = self::where('HJPackageId',$id)->get();
        if (is_null($record)) {
            return self::create($data);
        } else {

            return $record->update($data);
        }
    }
}