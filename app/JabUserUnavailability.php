<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class JabUserUnavailability extends Authenticatable
{
    use Notifiable;

    protected $table = "tbl_usersunavailability";

    public $timestamps = false;

    public static $rulesOnGetUnavailablity = [
        'UserId'          =>  'required',
        'StartDate'       =>  'required',
        'EndDate'         =>  'required',
    ];
    public static $rulesOnDeleteUnavailablity = [
        'UserUnavailabilityID'  =>  'required',
        'UserId'                =>  'required',
    ];
	public static $rulesOnAddEditUnavailablity = [
		'UserUnavailabilityID'  =>  'required',
		'StartDate'             =>  'required',
		'StartTime'             =>  array(
			'required',
			//'regex:/^(([0-1][0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?)$/'
            //'regex:/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/'
		),
		'EndDate'               =>  'required',
		'EndTime'               =>  array(
			'required',
			//'regex:/^(([0-1][0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?)$/'
			//'regex:/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/'
		)
	];
    public static function GetUnavailablityResponseJson($ret,$error=''){
        $retval = '';
        foreach($ret as $key => $val)
        {
            $retval .= response()->json([
                "UserUnavailabilityID"=> is_object($val)?$val->UserUnavailabilityID:'0' ,
                "UserId"=> is_object($val)?$val->UserId:null,
                "StartDate"=> is_object($val)?$val->StartDate:null,
                "StartTime"=> is_object($val)?$val->StartTime:null,
                "EndDate"=> is_object($val)?$val->EndDate:null,
                "EndTime"=> is_object($val)?$val->EndTime:null,
                "Notes"=> is_object($val)?$val->Notes:null,
                "IsDeletePossible"=> is_object($val)?$val->IsDeletePossible:null,
                "Error"=> $error,
                "JobID"=> is_object($val)?$val->JobId:null,
                "Address"=> is_object($val)?$val->propertyaddress:null,
            ]);
            echo"<pre>";print_r(123131);die;
        }
        return $retval;
    }
    public static function GetUnavailability($POST)
    {
        if(!is_array($POST))
            return false;

		$post['UserId']             = $POST['UserId'];
        $post['StartDate']          = date("Y-m-d", strtotime($POST['StartDate']));
        $post['EndDate']            = date("Y-m-d", strtotime($POST['EndDate']));

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_HomeJab_API_Get_Unavailablity('.rtrim($params,','  ).')');

        return JabUserUnavailability::hydrate($ret);
    }
    public static function DeleteUnavailability($POST)
    {
        if(!is_array($POST))
            return false;

        $post['UserUnavailabilityID'] = $POST['UserUnavailabilityID'];
        $post['UserId']               = $POST['UserId'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_HomeJab_API_Delete_Unavailablity('.rtrim($params,','  ).')');
        if(isset($ret[0]->Result))
            return $ret[0]->Result;
        else
            return false;
    }
    public static function AddEditUserUnavailability($POST)
    {
        if(!is_array($POST))
            return false;

        $post['UserUnavailabilityID'] = $POST['UserUnavailabilityID'];
        $post['UserId']               = $POST['UserId'];
        $post['StartDate']            = date('Y-m-d',strtotime(str_replace('-','/',$POST['StartDate'])));
        $post['StartTime']            = date('H:i',strtotime($POST['StartTime']));
        $post['EndDate']              = date('Y-m-d',strtotime(str_replace('-','/',$POST['EndDate'])));
        $post['EndTime']              = date('H:i',strtotime($POST['EndTime']));
        $post['Notes']                = $POST['Notes'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }
        //echo 'call SP_HomeJob_API_AddEdit_Unavailablity('.rtrim($params,','  ).')';die;
		$ret = DB::select('call SP_HomeJob_API_AddEdit_Unavailablity('.rtrim($params,','  ).')');

        if(isset($ret[0]->Result))
            return $ret[0]->Result;
        else
            return false;
    }
	public static function updateActiveFieldByJobId($jobid)
	{
		if($jobid  == '')
			return false;

		return self::where('JobId',$jobid)->update(array('IsActive'=>0));
	}
	public function GetUnavailabilityList($userid,$POST){
        $sdate = date('Y-m-d H:i:s.u');
        $edate = date("Y-m-d H:i:s.u",strtotime("+1 days +20 minutes +30 second"));

        if(isset($POST['date']) && $POST['date'] != ''){
            $startdate  = $POST['date'];
        }else{
            $startdate = date('Y-m-d',strtotime($sdate));
        }

        $enddate  = date('Y-m-d',strtotime($edate));

        $st =  date('H:i:s.u',strtotime($sdate));
        $et =  date('H:i:s.u',strtotime($edate));

        /*return DB::select(DB::raw("select tbl_usersunavailability.UserUnavailabilityID, tbl_usersunavailability.StartDate, tbl_usersunavailability.StartTime, tbl_usersunavailability.EndDate, tbl_usersunavailability.EndTime,IF(tbl_usersunavailability.StartDate < '$startdate','$sdate',CONCAT(tbl_usersunavailability.StartDate,' ', tbl_usersunavailability.StartTime)) AS Startdate,IF(tbl_usersunavailability.EndDate > '$edate','$edate',CONCAT(tbl_usersunavailability.EndDate ,' ', tbl_usersunavailability.EndTime)) AS Enddate
                                                      from tbl_usersunavailability where tbl_usersunavailability.UserId = '$userid' AND tbl_usersunavailability.StartDate >= '$startdate' OR tbl_usersunavailability.EndDate <= '$enddate' AND tbl_usersunavailability.StartTime >= '$st' AND tbl_usersunavailability.EndTime <= '$et' AND tbl_usersunavailability.IsActive = 1 "));*/
        return DB::select(DB::raw("select tbl_usersunavailability.UserUnavailabilityID, tbl_usersunavailability.StartDate, tbl_usersunavailability.StartTime, tbl_usersunavailability.EndDate, tbl_usersunavailability.EndTime,IF(tbl_usersunavailability.StartDate < '$startdate','$sdate',CONCAT(tbl_usersunavailability.StartDate,' ', tbl_usersunavailability.StartTime)) AS Startdate,IF(tbl_usersunavailability.EndDate > '$edate','$edate',CONCAT(tbl_usersunavailability.EndDate ,' ', tbl_usersunavailability.EndTime)) AS Enddate,tbl_usersunavailability.Notes 
                                                      from tbl_usersunavailability where tbl_usersunavailability.UserId = '$userid' AND tbl_usersunavailability.IsActive = 1 "));


    }
}
?>