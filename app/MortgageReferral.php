<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Model for the mortgage referrals
 *
 * @author Deepak Thakur
 * @since 1.0
 * @pacakge HomeJab
 * @subpackage Models
 */

class MortgageReferral extends Model
{
    /**
     *
     * @var array
     */
    public static $rulesOnRegister = [
        'firstname'    => 'required',
        'lastname'     => 'required',
        'email'        => 'required|email|unique:mortgage_referrals',
        'phone_number' => 'required',
        'json_data'    => 'required'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id',
        'firstname',
        'lastname',
        'email',
        'phone_number',
        'json_data'
    ];

    /**
     * Register mortgage_referral
     *
     * @param type $data
     */
    public static function register($data)
    {
        DB::beginTransaction();

        $mortgage_referral = new MortgageReferral();
        $newData['firstname'] = $data['firstname'];
        unset($data['firstname']);
        $newData['lastname'] = $data['lastname'];
        unset($data['lastname']);
        $newData['email'] = $data['email'];
        unset($data['email']);
        $newData['phone_number'] = $data['phone'];
        unset($data['phone']);
        $newData['property_id'] = $data['property_id'];
        unset($data['property_id']);

        $newData['json_data'] = json_encode($data);
        $mortgage_referral->fill($newData);

        if ($mortgage_referral->save()) {
            DB::commit();
            return $mortgage_referral;
        }

        DB::rollback();

        return false;
    }
}
