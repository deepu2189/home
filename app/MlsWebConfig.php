<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class MlsWebConfig extends Authenticatable
{
	use Notifiable;

	/*const CREATED_AT = 'CreatedOn';
	const UPDATED_AT = 'UpdatedOn';*/

	protected $table = "mls_web_config";

	public $timestamps = false;

	public function getAllConfig()
	{

		global $config;

		$arrconfig = $this->newQuery()->get();

		foreach($arrconfig as $key=>$val)
		{
			$config[$val['config_name']] = stripslashes($val['config_value']);
		}
		return $config;
	}
}
?>