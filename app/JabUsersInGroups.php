<?php
namespace App;

use App\Observers\CustomeObservers;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Mail;


class JabUsersInGroups extends Model
{
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'Createdon';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'Updatedon';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'UserID', 'GroupId','CreatedBy', 'CreatedOn', 'UpdatedBy', 'UpdatedOn'
    ];

    public static $rulesOnAssignGroup = [
        'group_list'      => 'required|regex:/^[\d\s,]*$/',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $table = "tbl_usersingroups";

    protected $primaryKey = 'UserGroupId';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new CustomeObservers());

        parent::boot();
    }
    public function JabUser(){
        return $this->belongsTo('App\JabUser', 'UserId',UserId);
    }

}
?>