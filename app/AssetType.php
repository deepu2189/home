<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

/**
 * Model to manage the asset types
 *
 * @author Deepak Thakur 
 * @since 0.3
 * @package HomeJab
 * @subpackage Models
 */
class AssetType extends Model
{
    /**
     * Validation rules for creation
     * @var array
     */
    public static $rulesOnStore = [
        'name'              => 'required|unique:asset_types|string',
        'description'       => 'required|string',
        'instructions'      => 'required|string',
        'enabled'           => 'required|boolean',
        'enabled_3d'        => 'boolean',
        'ewarp_product_id'  => 'string',
        'variant_1'         => 'string',
        'unit'              => 'string',
    ];
    
    /**
     * Validation rules for update
     * @var array
     */
    public static $rulesOnUpdate = [
        'name'             => 'required|string',
        'description'      => 'string',
        'instructions'     => 'string',
        'enabled_3d'       => 'boolean',
        'ewarp_product_id' => 'string',
        'variant_1'        => 'string',
        'unit'             => 'string',
        'enabled'          => 'boolean'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    /**
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'instructions',
        'enabled_3d',
        'ewarp_product_id',
        'unit',
        'variant_1',
        'enabled'
    ];
    
    /**
     * Fields that can be updated through and edit
     * @var array
     */
    public static $fillableOnUpdate = [
        'name',
        'description',
        'instructions',
        'enabled_3d',
        'ewarp_product_id',
        'unit',
        'variant_1',
        'enabled',
    ];

    /**
     * Relation with packages
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function packages()
    {
        return $this->belongsToMany('App\Package', 'package_has_asset_types');
    }
    
    
    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
    
    /**
     * Creates a AssetType
     *
     * @param array $data
     * @returns boolean
     */
    public static function add($data)
    {
        DB::beginTransaction();

        $asset = new AssetType();
        $asset->fill($data);

        if ($asset->save()) {
            DB::commit();
            return $asset;
        }
        DB::rollback();

        return false;
    }
    
    /**
     * Checks if a asset type will trigger a job with ewarp integration
     *
     * @param integer $id
     * @return boolean
     */
    public static function needsEWarpProcess($id)
    {
        $myself = self::where('id', $id)->get(['ewarp_product_id'])->first();
        if (is_null($myself->ewarp_product_id)) {
            return false;
        } else {
            return true;
        }
    }
}
