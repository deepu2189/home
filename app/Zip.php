<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;

/**
 * Model for zips
 *
 * @author Deepak Thakur
 * @since 1.2.1
 * @pacakge HomeJab
 * @subpackage Models
 */
class Zip extends Model
{
   /**
    * Constructor.
    *
    */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    /**
    * Rules to add a new location
    * @var array
    */
    public static $rulesAddZip = [
        'zip_code' => 'required|regex:/\b\d{5}\b/',
        'lat' => 'required',
        'lng' => 'required',
        'city_name' => 'required'
    ];

    public static $rulesOnGetLocationsByRadius = [
        'lat' => 'required',
        'lng' => 'required',
    ];



    /**
    * The database table used by model.
    *
    * @var string
    */
    protected $table = 'zips';

    /**
    * The attributes that table
    *
    * @var string
    */
    protected $fillable =[
        'zip_code',
        'lat',
        'lng',
        'city_name'
    ];

    /**
    * Function to add a new zip code
    */
    public static function addZipCode($data)
    {
        
        //Validations
        DB::beginTransaction();
       
        $location = new Zip();
        $location->fill($data);

        if ($location->save()) {
            DB::commit();
            return $location;
        }

        DB::rollback();

        return false;
    }

    /**
    * Function to delete zip code
    */
    public static function deleteZipCode($zip)
    {
       
        $entity = Zip::where('zip_code', '=', $zip);

        DB::beginTransaction();
        if ($entity->delete()) {
            DB::commit();
            return true;
        } else {
            DB::rollback();
            return false;
        }
    }

    public function getLocationByRadius($inputs, $radius){

      return $results = DB::select('SELECT
          id,zip_code,lat,lng, (
            3959 * acos (
              cos ( radians('.$inputs['lat'].') )
              * cos( radians( lat ) )
              * cos( radians( lng ) - radians('.$inputs['lng'].') )
              + sin ( radians('.$inputs['lat'].') )
              * sin( radians( lat ) )
            )
          ) AS distance
        FROM zips
        HAVING distance < '.$radius.'
        ORDER BY distance
        LIMIT 0 , 1');

    }
}
