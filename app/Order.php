<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Transaction;
use App\Property;
use App\OrderItem;
use Illuminate\Support\Facades\Config;
use App\Events\SiteMapChanged;
use Illuminate\Database\Eloquent\SoftDeletes;
use Event;
use App\Events\OrderPlaced;
use App\Events\OrderPlacedWithOwnerInfo;
use App\Events\OrderUpdated;
use Log;

/**
 * Model to manage the orders
 *
 * @author Esteban Zeller <esteba@serfe.com>
 * @author Deepak Thakur
 * @since 0.1
 * @package HomeJab
 * @subpackage Models
 */

class Order extends Model
{
    use SoftDeletes;

    /**
     * Constuctor. Inicializes status list with traductions
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        self::$statusList = [
            'new' => trans('order.new'),
            'completed' => trans('order.completed'),
            'on_hold' => trans('order.on_hold'),
            'scheduled' => trans('order.scheduled'),
            'redit' => trans('order.redit')
        ];
    }

    /**
     * Table name
     * @var string
     */
    protected $table = 'orders';

    /**
     * Validation rules for the order placing
     * @var array
     */
    public static $rulesOnPlaceOrder = [
        'agent_id' => 'required_without:seller_id',
        'seller_id' => 'required_without:agent_id',
        'package_id' => 'required',
        'amount' => 'required|numeric|min:0'
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'trial_ends_at',
        'suscription_ends_at',
        'deleted_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agent_id',
        'seller_id',
        'package_id',
        'amount',
        'status',
        'manually',
        'must_have_shots'
    ];

    /**
     * Possible status for an order
     * @var array
     */
    public static $statusList = [
            //We initialize this variable in the constructor
            //To do the Translations
    ];
    public static $statusNew = 'new';
    public static $statusPaid = 'paid';
    public static $statusOnHold = 'on_hold';

    /**
     * Related Agent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }

    /**
     * Related Seller
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seller()
    {
        return $this->belongsTo('App\Seller');
    }

    /**
     * Related Package
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function package()
    {
        return $this->belongsTo('App\Package');
    }

    /**
     * Relation with Job
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs()  
    {
        return $this->hasMany('App\AllJob');
    }

    /**
     * Relation with Transaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transaction()
    {
        return $this->hasMany('App\Transaction');
    }

    /**
     * Relation with Property
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function property()
    {
        return $this->hasOne('App\Property');
    }

    /**
     * Relation with CallbackStatus
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // @codingStandardsIgnoreLine
    public function callback_status()
    {
        return $this->hasMany('App\CallbackStatus');
    }

    /**
     * Relation with Order Items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderItem()
    {
        return $this->hasMany('App\OrderItem');
    }

    /**
     * Relation with extra Files
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function extraFiles()
    {
        return $this->belongsToMany('App\Mediafile', 'orders_has_extra_files')->withTrashed();
    }

    /**
     * Relation with Redit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function redits()
    {
        return $this->hasMany('App\Redit');
    }

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getTrialEndsAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getSuscriptionEndsAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Places a new order
     *
     * @param array $data
     */
    public static function place($data)
    {
        DB::beginTransaction();

        $order = new Order();

        $order->fill($data);

        $order->status = self::$statusNew;

        if (!$order->save()) {
            DB::rollback();
            return false;
        }

        $response = null;

        $package = Package::findOrFail($data['package_id']);

        if (!$data['manually']) {
            $asset_types = $package->assetTypes()->where('enabled', true)->get();
        } else {
            // order placed by SU, so can contain a disabled package with disabled assets
            $asset_types = $package->assetTypes()->get();
        }



        foreach ($asset_types as $asset_type) {
            $data['job']['order_id'] = (int) $order->id;
            $data['job']['amount'] = $data['amount'];
            $data['job']['asset_type_id'] = (int) $asset_type->id;
            $job = new \App\AllJob();
            if (!$job->createFromOrder($data['job'])) {
                DB::rollback();
                $response['success'] = false;
                return $response;
            }
        }

        // Creates the property unpublished
        if (Property::createFromOrder($data, $order->id) === false) {
            DB::rollback();
            $response['success'] = false;
            return $response;
        }

        // We need to calculate the total amount for the order
        $stripe_total = floatval($package->price);

        if (array_key_exists('additionals', $data) && count($data['additionals']) > 0) {
            // See if we have a payment with wallet transaction



            foreach ($data['additionals'] as $additional) {
                if ($additional['type'] === OrderItem::$walletDiscountType) {
                    $stripe_total -= floatval($additional['amount']);
                }
            }
        } else {

            // Create the array to handle the always present items
            $data['additionals'] = [];
        }

        // Order items will be always present now
        if (OrderItem::insertOrderItems($data['additionals'], $order->id) === false) {
            DB::rollback();
            $response['success'] = false;
            $response['message'] = "There where an error trying to create the order items";
            return $response;
        }

        $user = null;
        if ($order->Agent) {
            $user = $order->Agent->User;
        } else if ($order->Seller) {
            $user = $order->Seller->User;
        }

        if (WalletTransaction::createFromOrderItems($data['additionals'], $user->id, $order->id) === false) {
            DB::rollback();
            $response['success'] = false;
            $response['message'] = "There where an error trying to create the wallet transactions";
            return $response;
        }

        if (!$data['manually'] && $stripe_total > 0.0) {

            $response = Transaction::addChargeFromOrder($data, $order->id);

            if ($response['success'] === false) {
                $response['order_data'] = $data;
                //$response['message'] = "There where an error trying to create the transaction record";
                DB::rollback();
                return $response;
            }
        } else if (array_key_exists('doBackendPayment', $data) && $data['doBackendPayment'] && $stripe_total > 0.0) {
            $user = null;
            $package = $order->package;

            $payment_data = [
                'amount' => $stripe_total,
                'package_id' => $package->id,
                'package_name' => $package->name
            ];

            if ($order->Agent) {
                $user = $order->Agent->User;

                $payment_data['agent_id'] = $order->agent_id;
                $payment_data['agent_email'] = $user->email;
            } else if ($order->Seller) {
                $user = $order->Seller->User;

                $payment_data['seller_id'] = $order->seller_id;
                $payment_data['seller_email'] = $user->email;
            }

            // if agent/seller is not on stripe, payment is not done

            if ($user->stripe_id !== null) {
                $payment_data['payment']['card']['name'] = $user->email;

                $response = Transaction::addChargeFromOrder($payment_data, $order->id);

                if ($response['success'] === false) {
                    $response['message'] = "There where an error trying to create the stripe transaction and record";
                    DB::rollback();
                    return $response;
                }
            }
        }

        // update the order total to match the amount when using credit from the wallet
        $order->amount = $stripe_total;
        if (!$order->save()) {
            $response['message'] = "There where an error when trying to update the order total";
            DB::rollback();
            return $response;
        }

        DB::commit();

        $response['success'] = true;
        $response['order_data'] = $order;
        return $response;
    }

    /**
     * Communication to Stripe and return information from order_id
     *
     * @param array $order_id
     */
    public static function getStripeInfo($order_id)
    {

        $transaction = Transaction::where('order_id', '=', $order_id)->first();

        if ($transaction) {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            return (\Stripe\Charge::retrieve($transaction['stripe_id']));
        }
        return false;
    }

    /**
     * Sets an order as completed
     *
     * @param integer $order_id
     * @return boolean
     */
    public static function markAsFinished($order_id)
    {
        DB::beginTransaction();
        try {
            $order = self::find($order_id);
            $order->status = 'completed';
            if ($order->save()) {
                DB::commit();
                return true;
            }
            DB::rollback();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            DB::rollback();
            return false;
        }
    }

    public function markAsRedit()
    {
        $this->status = 'redit';
        if ($this->save()) {
            return true;
        }
        return false;
    }

    /**
     * Creates a contact on InfusionSoft with owner info
     *
     * @param integer $data = order_id
     * @return undefined
     */
    public static function createOwnerContactOnIS($data)
    {
        Event::fire(new OrderPlacedWithOwnerInfo($data));
    }

    /**
     * Creates a order on InfusionSoft
     *
     * @param integer $data = order_id
     * @return undefined
     */
    public static function createOrderOnIS($data)
    {
        Event::fire(new OrderPlaced($data));
    }

    /*
     * Updated Order in InfusionSoft
     *
     */

    public static function updateOrderInfusionSoft($data)
    {
        Event::fire(new OrderUpdated($data));
    }

    /*
     * get order data by order_id
     *
     */

    public static function getOrder($id)
    {
        return self::find($id);
    }

    /*
     * migrate order
     *
     */

    public static function migrateAgentSeller($id, $array_seller_agent)
    {
        $order = self::find($id);
        $order->seller_id = $array_seller_agent['seller_id'];
        $order->agent_id = $array_seller_agent['agent_id'];
        if ($order->save()) {
            return true;
        }
        return false;
    }

    /**
     * delete the order an all related entries
     *
     * @param type $id
     */
    public static function deleteOrderById($id)
    {
        //needed information
        $order = Order::find(intval($id));

        if (!$order) {
            return false;
        }

        $jobs = $order->jobs;

        DB::beginTransaction();

        // Delete Property
        if (!$order->property->delete()) {
            DB::rollback();
            return false;
        }

        // Delete Jobs relations and mediafiles
        foreach ($jobs as $job) {
            $mediafileRelations = JobHasMediaFile::where('job_id', '=', $job->id)->get();

            if ($mediafileRelations) {
                foreach ($mediafileRelations as $media) {
                    if (!$media->delete()) {
                        DB::rollback();
                        return false;
                    }
                }
            }

            if (!$job->delete()) {
                DB::rollback();
                return false;
            }
        }

        // Transactions Job
        $transactions = $order->transaction;
        if ($transactions) {
            foreach ($transactions as $transaction) {
                if (!$transaction->delete()) {
                    DB::rollback();
                    return false;
                }
            }
        }

        // Delete Order
        if (!$order->delete()) {
            DB::rollback();
            return false;
        }

        DB::commit();
        return true;
    }

    /**
     * Check if the order amount is equal to additionals plus package price
     * If the order has no total, it is calculated
     *
     * @param int $package_id
     * @param array $additionals
     * @param double $order_total
     * @return type
     */
    // @codingStandardsIgnoreLine
    public static function checkOrderTotal($package_id, $additionals = null, $order_total)
    {
               
        $calculated_total = 0;
        if ($additionals) {
            foreach ($additionals as $additional) {
                if ($additional['type'] === OrderItem::$chargeType) {
                    $calculated_total += $additional['amount'];
                } else if ($additional['type'] === OrderItem::$discountType) {
                    $calculated_total -= $additional['amount'];
                } else if ($additional['type'] === OrderItem::$walletDiscountType) {
                    // We do not use as discount as the order will be paid in full wit credit from the user
                    $calculated_total -= $additional['amount'];
                }
            }
        }

        $package = Package::find($package_id);
        
        $calculated_total += $package->price;       
        if ($calculated_total == $order_total) {
            return true;
        }
        return false;
    }

    /**
     * Return orders completed in period:[8pm yesterday - 10am today]
     *
     * @return integer
     */
    public static function getCompletedOrders()
    {
        //create range dates in local timezone, set hour and then convert to UTC to compare
        $timezone = Carbon::now()->timezoneName;
        $today_limit = Carbon::today($timezone);
        $today_limit->hour = 10;
        $today_limit->setTimezone('UTC');

        $yesterday_to_find = Carbon::yesterday($timezone);
        $yesterday_limit = Carbon::yesterday($timezone);
        $yesterday_limit->hour = 20;
        $yesterday_limit->setTimezone('UTC');

        // get all orders that has been completed
        // yesterday_to_find is a datetime with yesterday day and 00:00:00 hour
        $orders = self::query()->where('updated_at', '>=', $yesterday_to_find)->where('status', 'completed')->get();

        $completed_orders = fopen("completed_orders_from_" . $yesterday_to_find->toDateString() . "_to_" . $today_limit->toDateString() . ".txt", "w");

        $inserted = 0;
        $title_seted = false;

        $completed_order_redits = "";
        $completed_order_normal = "";
        $count_redits = 0;
        $count_normals = 0;

        if (count($orders) > 0) {
            foreach ($orders as $order) {
                //convert updated date to UTC
                $updated_at = new \DateTime($order->updated_at, new \DateTimeZone($timezone));
                $order_date = Carbon::instance($updated_at)->setTimezone('UTC');

                if ($order_date >= $yesterday_limit && $order_date <= $today_limit) {
                    $property = Property::where('order_id', '=', $order->id)->first(['address']);
                    
                    $txt = "Order id: " . $order->id . " Address: " . $property->address . "\r\n";
                    if (!$order->lastStateWasRedit()) {
                        $completed_order_normal .= $txt;
                        $count_normals++;
                    } else {
                        $completed_order_redits .= $txt;
                        $count_redits++;
                    }

                    $inserted++;
                }
            }
        }
        
        if (!$inserted) {
            fclose($completed_orders);
            unlink("completed_orders_from_" . $yesterday_to_find->toDateString() . "_to_" . $today_limit->toDateString() . ".txt");
            
            return [
                'inserted' => $inserted,
                'normals'  => $count_normals,
                'redits'   => $count_redits
            ];
        }
        
        $header_completed = "** Completed orders from " . $yesterday_limit->toDateString() . " 8pm to " . $today_limit->toDateString() . " 10am period ** \r\n";
        $header_redit = "** Completed Redit orders from " . $yesterday_limit->toDateString() . " 8pm to " . $today_limit->toDateString() . " 10am period ** \r\n";
        fwrite($completed_orders, $header_completed);
        fwrite($completed_orders, $completed_order_normal);
        fwrite($completed_orders, $header_redit);
        fwrite($completed_orders, $completed_order_redits);

        fclose($completed_orders);

        return [
            'inserted' => $inserted,
            'normals'  => $count_normals,
            'redits'   => $count_redits
        ];
    }

    /**
    * Function to return if the last state
    * before to finish was redit
    *
    */
    public function lastStateWasRedit()
    {

        $lastJob = $this->jobs()->orderBy('updated_at', 'DESC')->first();
        if ($lastJob && $lastJob->redit) {
            return true;
        }

        return false;
    }

    /**
     * Change order status
     * @param type $order_id
     * @param type $status
     * @return boolean
     */
    public static function changeStatus($order_id, $status)
    {
        $order = self::find($order_id);
        $order->status = $status;
        if ($order->save()) {
            return true;
        }
        return false;
    }

    /**
     * Verified if the order is completed.
     * @return boolean
     */
    public function isCompleted()
    {

        if (strcmp($this->status, 'completed') !== 0) {
            return false;
        }
        return true;
    }
}
