<?php
/**
 * Created by PhpStorm.
 * User: OD13
 * Date: 2/23/2019
 * Time: 1:11 PM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Rutorika\Sortable\BelongsToSortedManyTrait;
use Log;
use Event;
use Illuminate\Support\Facades\Hash;

class JabListhubListingMaster extends Model
{
	protected $table = "listhub_listing_master";

	public function getAllDataFromAddress($prop_address){
		$query = $this->newQuery()->where('street','LIKE','%'.$prop_address.'%')->OrderBy('listing_date','DESC');

		return $query->first();
	}
}
