<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class Custom extends Authenticatable
{
    use Notifiable;

    /*check api key*/
    public static function checkApiKey($request)
    {
        $header_api_key = $request->header('HomeJab_Authentication_Ticket');
    }
    /* check email validation*/
    public  static function CheckValidateEmail($email)
    {
        if($email == '')
            return false;

        if(!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email))
            return false;
        else
            return true;
    }
    public static function checkjsonData($jsonData){
        if($jsonData->headers->get('Content-Type') != 'application/json')
            return false;

        if($jsonData->getContent() == '')
            return false;

        if(is_array(json_decode($jsonData->getContent(), true)))
        {
            return json_decode($jsonData->getContent(), true);
        }
    }
    public static function getToAddressList($ToAddress){
//	    echo Config::get('constants.DEFAULT_TO_ADDRESS_EMAIL'); die;
		if(strpos($_SERVER['HTTP_HOST'],'project') != false){
		     return Config::get('constants.DEFAULT_TO_ADDRESS_EMAIL');
		}else{
			return $ToAddress;
		}
	}
	public static function getAddressLocationByAddress($address)
    {
        if($address == '')
            return false;


        #Get address location from property address using google API
        $address = str_replace(' ', '+', $address);
        $url              = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&libraries=drawing,geometry,places&key='.env("GOOGLE_API_KEY").'&address='.$address;
        $client           = new Client();
        $response         = $client->GET($url);

        $objAddressLoc = json_decode($response->getBody()->getContents());

        return $objAddressLoc;

    }
}