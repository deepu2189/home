<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Model for the Callback status
 *
 * @author Deepak Thakur
 * @since 1.3.0
 * @package HomeJab
 * @subpackage Models
 */
class CallbackStatus extends Model
{
    /**
     * Constuctor. Inicializes status list with traductions
     *
     * @param array $attributes
    */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        self::$typeList=[
            'order_placed' => trans('callback_status.order_placed'),
            'rescheduled' => trans('callback_status.rescheduled'),
            'on_hold' => trans('callback_status.on_hold')
        ];
    }
    
    /**
     * Name for the table
     * @var string
     */
    protected $table = 'callback_status';
    
       /**
     * Validation rules for creation
     * @var array
     */
    public static $rulesOnStore = [
        'order_id'                  => 'required|integer',
        'type'                      => 'required|string',
        'callback_sent'             => 'boolean',
        'callback_succes'           => 'datetime',
        'callback_failed_at'        => 'datetime'

    ];
    
    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['callback_succes','callback_failed_at','created_at', 'updated_at'];

    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'order_id',
        'type',
        'callback_sent',
        'callback_succes',
        'callback_failed_at'
    ];
    
    /**
     * Possible type for an CallbackStatus
     * @var array
     */
    public static $typeList = [
        //We initialize this variable in the constructor
        //To do the Translations
    ];

    /**
     * Order Placed type definition
     * @var string
     */
    public static $typeOrderPlaced = 'order_placed';

    /**
     * Order Rescheduled type definition
     * @var string
     */
    public static $typeOrderRescheduled = 'rescheduled';

    /**
     * Order put on hold type definition
     * @var string
     */
    public static $typeOrderOnHold ='on_hold';
    
    /**
     * Relation with Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * Add callback status
     * @param type $order_id
     * @param type $type
     * @param type $callback_sent
     * @param type $callback_success
     * @param type $callback_failed_at
     * @return boolean
     */
    public static function addCallbackStatus($order_id, $type, $callback_sent = 0, $callback_success = null, $callback_failed_at = null)
    {
        DB::beginTransaction();
        $callback_status = new CallbackStatus();

        $callback_status->order_id = $order_id;
        $callback_status->type = $type;
        $callback_status->callback_sent = $callback_sent;
        $callback_status->callback_success = $callback_success;
        $callback_status->callback_failed_at = $callback_failed_at;
        
        if ($callback_status->save()) {
            DB::commit();
            $response['success'] = true;
            $response['callback_status_id'] = $callback_status->id;
            return $response;
        } else {
            DB::rollback();
            $response['success'] = false;
            return $response;
        }
    }
    
    /**
     * Update callback status
     * @param type $order_id
     * @param type $type
     * @param type $callback_sent
     * @param type $callback_success
     * @param type $callback_failed_at
     * @return boolean
     */
    public static function updateCallbackStatus($order_id, $type, $callback_sent = 0, $callback_success = null, $callback_failed_at = null)
    {
        $callback_status= CallbackStatus::where('order_id', '=', $order_id)->where('type', '=', $type)->orderBy('id', 'DESC')->first();
           
        if (!$callback_status) {
            return false;
        }
        
        DB::beginTransaction();
        $callback_status->order_id = $order_id;
        $callback_status->type = $type;
        $callback_status->callback_sent = $callback_sent;
        $callback_status->callback_success = $callback_success;
        $callback_status->callback_failed_at = $callback_failed_at;
        
        if ($callback_status->save()) {
            DB::commit();
            $response['success'] = true;
            $response['callback_status_id'] = $callback_status->id;
            return $response;
        } else {
            DB::rollback();
            $response['success'] = false;
            return $response;
        }
    }
}
