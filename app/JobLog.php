<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class JobLog extends Authenticatable
{
    use Notifiable;

    protected $table = "tbl_joblog";

    protected $primaryKey = 'LogId';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'JobId', 'JobScheduleId', 'Description', 'CreatedBy', 'CreatedOn'
    ];

    public static function Insert($POST)
    {
	    # add job log
	    $_post                  = array();
	    $_post['JobId']         = $POST["JobId"];
	    $_post['JobScheduleId'] = $POST["JobScheduleId"];
	    $_post['Description']   = $POST["Description"];
	    $_post['CreatedBy']     = $POST["CreatedBy"];
	    $_post['CreatedOn']     = $POST["CreatedOn"];
	    $_post['LogId']         = $POST["LogId"];
	    $_post['UpdatedBy']     = $POST["UpdatedBy"];
	    $_post['UpdatedOn']     = $POST["UpdatedOn"];

	    $objJoblog = new JobLog();
	    $objJoblog->fill($_post);

	    $JobLogData = $objJoblog->save();
    }
}
?>