<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class JabMessage extends Authenticatable
{
    use Notifiable;

    /*const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';*/

    protected $table = "tbl_message";

    protected $primaryKey = 'MessageId';
    public $timestamps = false;

	protected $fillable = [
		'JobId','SenderId','ReceiverId','MessageText','Messagetime','IsRead','isClosed'
	];

    public static $rulesOnGetMessage = [
        'JobID'             =>  'required',
        //'UserID'            =>  'required',
    ];
    public static $rulesOnGetNotification = [
        //'UserID'            =>  'required'
    ];
    public static $rulesOnMarkAllRead = [
        'JobID'             =>  'required',
        'UserId'            =>  'required',
    ];
    public static $rulesOnSendMessage = [
        'JobID'             =>  'required',
        'SenderID'          =>  'required',
        'MessageText'       =>  'required',
        'SentTime'          =>  'required',
    ];
    public function SenderJabuser()
    {
        return $this->belongsTo('App\JabUser', 'SenderId');
    }
    public function ReceiverJabuser()
    {
        return $this->belongsTo('App\JabUser', 'ReceiverId');
    }
    public static function GetMessage($POST)
    {
        if(!is_array($POST))
            return false;

        $post['JobID']      = $POST['JobID'];
        $post['UserID']     = $POST['UserID'];
        $post['PageNumber'] = isset($POST['PageNumber'])?$POST['PageNumber']:1;
        $post['PageSize']   = isset($POST['PageSize'])?$POST['PageSize']:10;

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_GetMessageByJobID('.rtrim($params,','  ).')');

        return JabMessage::hydrate($ret);

    }
    public static function GetNotificationList($POST)
    {
        if(!is_array($POST))
            return false;

        $post['UserId']     = $POST['UserId'];
        $post['PageNumber'] = isset($POST['PageNumber'])?$POST['PageNumber']:1;
        $post['PageSize']   = isset($POST['PageSize'])?$POST['PageSize']:10;

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_GetNotificationListByUser('.rtrim($params,','  ).')');

        return JabMessage::hydrate($ret);

    }
    public static function GetNotificationCount($POST)
    {
        if(!is_array($POST))
            return false;

        $post['UserID']     = $POST['UserID'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_GetUnreadMessageCountByUserId('.rtrim($params,','  ).')');

        return $ret;

    }
    public static function MarkAllReadByJobIDUserID($POST)
    {
        if(!is_array($POST))
            return false;

        $post['JobID']      = $POST['JobID'];
        $post['UserID']     = $POST['UserId'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_MarkAllRead('.rtrim($params,','  ).')');

        return $ret;

    }
    public static function MarkReadByMessageId($POST)
    {
        if(!is_array($POST))
            return false;

        $post['MessageId']  = $POST['MessageId'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_MarkRead('.rtrim($params,','  ).')');

        return $ret;

    }
    public static function  SendMessage($POST)
    {
        if(!is_array($POST))
            return false;

        $post['JobID']      = $POST['JobID'];
        $post['SenderID']   = $POST['SenderID'];
        $post['MessageText']= $POST['MessageText'];
        $post['SentTime']   = date('Y-m-d H:i:s', strtotime($POST['SentTime']));

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_SendMessageByJobID('.rtrim($params,','  ).')');

        return $ret;
    }

    public function getUnReadNotificationListForWeb(){

        return $this->newQuery()->select('tbl_message.MessageText','tbl_message.isClosed','tbl_message.Messagetime','tbl_jobs.JobID','tbl_users.Name','tbl_jobs.HJJobID')
                         ->join('tbl_users','tbl_message.SenderId', '=','tbl_users.UserId')
                         ->join('tbl_jobs','tbl_message.JobId', '=','tbl_jobs.JobId')
                         ->where('tbl_message.ReceiverId',0)
                         ->where('tbl_message.IsRead',0)
                         ->groupBy('tbl_message.JobId')
                         ->orderBy('tbl_message.JobId','desc')->get();
    }

    public function getAllNotificationListForWeb($POST){

            return $this->newQuery()->select('tbl_message.JobID','tbl_message.Messagetime','tbl_message.MessageText','tbl_message.IsRead','tbl_message.isClosed','tbl_users.Name','tbl_jobs.HJJobID','tbl_users.Name','tbl_users.UserId')
                ->join('tbl_users','tbl_users.UserId','=','tbl_message.SenderId')
                ->join('tbl_jobs','tbl_jobs.JobId','=','tbl_message.JobID');
    }

    public function UpdateIsCloseByJobId($isclosed,$jobid){
        /*DB::beginTransaction();


//        $objjabmessage = JabMessage::find($jobid);
        $objjabmessage = $this->newQuery()->whereIn('MessageId',$message_ids->implode('MessageId', ', '));

        $objjabmessage[0]->isClosed = $isclosed;

        if ($objjabmessage->save()) {
            DB::commit();
            return $objjabmessage;
        }
        DB::rollback();

        return false;*/
        if($jobid == '')
            return false;

        return $this->newQuery()->where('JobId', '=', $jobid)->update(['IsClosed' => $isclosed]);

    }
}
?>