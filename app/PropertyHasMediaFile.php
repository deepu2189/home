<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Rutorika\Sortable\BelongsToSortedManyTrait;
use Illuminate\Support\Collection;
use App\Library\SortableTrait;
use Log;

class PropertyHasMediaFile extends Model
{
    use SortableTrait;


     /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'property_has_mediafiles';

    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'property_id',
        'mediafile_id',
        'position'
    ];
    
    /**
    * Field to group by when ordered
    *
    * @var type
    */
    protected static $sortableGroupField = 'property_id';


    /**
     * Relation with property
     *
     * @return type
     */
    public function property()
    {
        return $this->hasMany('App\Property', 'property_id');
    }


    /**
     * Relation with mediafiles
     *
     * @return type
     */
    public function mediafiles()
    {
        return $this->hasMany('App\Mediafile', 'mediafile_id');
    }
        
    
    /**
     * Accesor for created at date
     *
     * @param type $date
     * @return type
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param type $date
     * @return type
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
    
    public static function getPositionToAdd($property_id)
    {
        return self::where('property_id', $property_id)->get(['position'])->max('position') + 1;
    }

    /**
    * Function to get Object with a position determinated.
    * @param $property_id.
    * @param $position.
    */
    public function getObjectWithPosition($property_id, $position)
    {
        return self::where('property_id', $property_id)->where('position', $position)->get(['*'])->first();
    }
    
    /**
     * Set new order for all property's mediafiles when a mediafile is deleted
     * @param type $property_id
     */
    public static function orderPropertyMediafiles($property_id)
    {
        $rows = self::where('property_id', $property_id)->orderBy('position', 'Asc')->get(['*']);
                
        
        DB::beginTransaction();
        $pos = 1;
        foreach ($rows as $row) {
            $mediafile = Mediafile::find($row->mediafile_id);
            if ($mediafile) {
                $row->position = $pos;
                
                if (!$row->save()) {
                    DB::rollback();
                    return false;
                }
                
                $pos++;
            }
        }
        
        DB::commit();
        return true;
    }

    /**
    * Function to map custom queries to array
    * @param $object: array with object stdClass inside.
    * @return $array: array with keys - values;
    */
    public function mappingToArray($object)
    {
        $array = collect($object)->map(function ($x) {
            return (array) $x;
        })->toArray();
        $array = array_shift($array);

        return $array;
    }

    public function mappingToArrayAll($object, $key)
    {
        $array = array();
        $tmp = json_decode(json_encode($object), true);
        foreach ($tmp as $o) {
            array_push($array, $o[$key]);
        }
        return $array;
    }


    /**
    * Function to get the position of the last entity added.
    * @param  $batch_id
    * @param  $property_id
    * @return $position: Position of last entity added.
    */
    public function lastPositionAdding($batch_id, $property_id)
    {
        
        
        $position = DB::table('property_has_mediafiles')
                     ->select(DB::raw('max(position) AS position'))
                     ->join('mediafiles', 'mediafiles.id', '=', 'property_has_mediafiles.mediafile_id')
                     ->where('property_id', $property_id)
                     ->where('mediafiles.batch_id', $batch_id)
                     ->get();



        $position = $this->mappingToArray($position);

        
        return $position['position'];
    }

    /**
    * Function to get the new position of the object proccessing
    * according to asset_type id of the batch_id.
    * @param  $batch_id
    * @param  $order_id
    * @return $position:.
    */
    public function getNewPositionMedia($batch_id, $order_id, $property_id)
    {

        $queryAsset =

            DB::table('all_jobs')
                     ->select(DB::raw('asset_types.id AS asset_id'))
                     ->join('asset_types', 'asset_types.id', '=', 'all_jobs.asset_type_id')
                     ->where('all_jobs.batch_id', '=', $batch_id)
                     ->get();

        //pr($queryAsset);

        $queryPackage = DB::table('orders')
                     ->where('orders.id', '=', $order_id)
                     ->get(['package_id']);
        //pr($queryPackage);

        //Mapping to array
        $queryAsset = $this->mappingToArray($queryAsset);
        $queryPackage = $this->mappingToArray($queryPackage);

         $asset_id = $queryAsset['asset_id'];
         $package_id = $queryPackage['package_id'];

        //Get Position of the current asset proccessed
        $getAssets = DB::table('package_has_asset_types')
                    ->select(DB::raw('position'))
                    ->where('asset_type_id', '=', $asset_id)
                    ->where('package_id', '=', $package_id)
                    ->get();



        $getAssets = $this->mappingToArray($getAssets);
        $position_asset = $getAssets['position'];

        $lastEntity = array();

        //If the position_asset is > 1, calculate the last object adding.
        if ($position_asset > 1) {
            //Asset types ids with position <= current.
            $assetTypeIds = DB::table('package_has_asset_types')
                            ->where('package_id', '=', $package_id)
                            ->where('position', '<', $position_asset)
                            ->get(['asset_type_id']);


            $assetTypeIds = $this->mappingToArrayAll($assetTypeIds, 'asset_type_id');

            $batchIds = DB::table('all_jobs')
                        ->select('batch_id')
                        ->where('order_id', $order_id)
                        ->whereIn('asset_type_id', $assetTypeIds)
                        ->get();

//            pr($property_id);
//            pr($package_id);
//            pr($position_asset);
//            pr($assetTypeIds);
//            pr($batchIds);

           $batchIds = $this->mappingToArrayAll($batchIds, 'batch_id');

            $res = DB::table('property_has_mediafiles')
                ->join('mediafiles', 'mediafiles.id', '=', 'property_has_mediafiles.mediafile_id')
                ->where('property_has_mediafiles.property_id', '=', $property_id)
                ->whereIn('mediafiles.batch_id', array_values($batchIds))
                ->orderBy('position', 'desc')
                ->take(1)
                ->get(['property_has_mediafiles.property_id','property_has_mediafiles.position']);


            $lastEntity = $this->mappingToArray($res);

        }

        return $lastEntity;
    }

    /**
     * Function to insert the actual object after tha last entity founded.
     * @param mixed $entity Array containing the property_id and position of the last object inserted.
     */
    public function saveInPosition($entity)
    {
        $afterTo = $this->getObjectWithPosition($entity['property_id'], $entity['position']);

        if (($this->position - $afterTo->position) == 1) {
            return true;
        } else {
            try {
                $this->moveAfter($afterTo);
                return true;
            } catch (\Exception $e) {

                Log::info($e->getMessage());
            }
        }

    }

    /**
     *
     * @param integer $property_id
     * @param type $position
     * @return boolean
     */
    public function insertBefore($property_id, $position)
    {
        $beforeTo = $this->getObjectWithPosition($property_id, $position);

        if ($this->mediafile_id == $beforeTo->mediafile_id) {
            return true;
        } else {
            $this->moveBefore($beforeTo);
            return true;
        }
    }

      /**
    * Function to return the last mediafile_id
    * with photo_type 'online'.
    */
    public static function getPreviewImage($property_id)
    {
        $mediafile_id = DB::table('property_has_mediafiles')
            ->join('mediafiles', 'mediafiles.id', '=', 'property_has_mediafiles.mediafile_id')
            ->where('property_has_mediafiles.property_id', '=', $property_id)
            ->where('mediafiles.photo_type', '=', 'online')
            ->orderBy('position', 'asc')
            ->take(1)
            ->get(['property_has_mediafiles.mediafile_id']);

        if (count($mediafile_id) > 0) {
            $mediafile_id = json_decode(json_encode($mediafile_id), true)[0];
            return $mediafile_id['mediafile_id'];
        } else {
            return null;
        }
    }
}
