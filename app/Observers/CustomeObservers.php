<?php

namespace App\Observers;
use Illuminate\Support\Facades\Auth;

class CustomeObservers {


    public function __construct(){

        //$user = Auth::guard('admin')->user();
	    $user = Auth::guard('api')->user();

        if($user != '')
            $this->userID = $user->UserId;

        //echo"<pre>";print_r($user);die;
       // echo"<pre>";print_r($user->UserId);die;
       // $this->userID = $user->UserId;
        //echo"<pre>";print_r($user);
        //$this->userID = 1;
    }

    public function saving($model)
    {
        $model->UpdatedBy = $this->userID;
        //$model->UpdatedOn = date('Y-m-d H:i:s');
    }

    public function saved($model)
    {
        $model->UpdatedBy = $this->userID;
        //$model->UpdatedOn = date('Y-m-d H:i:s');
    }


    public function updating($model)
    {
        $model->UpdatedBy = $this->userID;
        //$model->UpdatedOn = date('Y-m-d H:i:s');

    }

    public function updated($model)
    {
        $model->UpdatedBy = $this->userID;
        //$model->UpdatedOn = date('Y-m-d H:i:s');
    }


    public function creating($model)
    {
        $model->CreatedBy = $this->userID;
        //$model->CreatedOn = date('Y-m-d H:i:s');
    }

    public function created($model)
    {
        $model->CreatedBy = $this->userID;
        //$model->CreatedOn = date('Y-m-d H:i:s');
    }


    public function removing($model)
    {
        //$model->purged_by = $this->userID;
    }

    public function removed($model)
    {
        //$model->purged_by = $this->userID;
    }
}