<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HomeOurWork extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'home_properties';

    /**
     * Validation rules for update
     * @var array
     */
    public static $rulesOnUpdate = [
        'porperty_id' => 'numeric',
        'position' => 'numeric',
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     *
     * @var array
     */
    protected $fillable = [
        'property_id',
        'position',
    ];

    /**
     * Fields that can be updated through and edit
     * @var array
     */
    public static $fillableOnUpdate = [
        'property_id',
        'position',
    ];

    /**
     * The home_property belongs to a Property
     *
     * @return type
     */
    public function property()
    {
        return $this->belongsTo('App\Property');
    }

    /**
     * Get our work data by ourWork_id
     * @param integer $id
     * @param \HomeJab\Model\OurWork
     */
    public static function getOurWork($id)
    {
        return self::find($id);
    }

    /**
     * delete the property from our work
     *
     * @param type $id
     */
    public static function deletePropertyFromOurWork($id)
    {
        //needed information
        $our_work = HomeOurWork::find(intval($id));
        
        if (!$our_work) {
            return false;
        }

        DB::beginTransaction();

        // Delete Property
        $our_work->property_id = null;       
        if ($our_work->save()) {
            DB::commit();
            $response['success'] = true;
            return $response;
        } else {
            DB::rollback();
            $response['success'] = false;
            return $response;
        }
    }
}
