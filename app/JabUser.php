<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Observers\CustomeObservers;

class JabUser extends Authenticatable
{
    use HasApiTokens,Notifiable;

    const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Name', 'Email','HomeJabId','Password','PhoneNo','RoleId','RankId','IsActive','CreatedBy','CreatedOn','UpdatedBy','UpdatedOn'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = "tbl_users";

    protected $primaryKey = 'UserId';

    //public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public static $rulesOnAddPhotographer = [
        'Name'          =>  'required',
        'Zip'           =>  'required',
        'Address'       =>  'required',
        'Email'         =>  'required|email',
        'Password'      =>  'required',
        'HomeJabId'     =>  'required',
        'PhoneNo'       =>  'required',
    ];
    public static $rulesOnUpdatePhotographer = [
        'Name'          =>  'required',
        'Zip'           =>  'required',
        'Address'       =>  'required',
        'Email'         =>  'required|email',
        'Password'      =>  'required',
        'HomeJabId'     =>  'required',
        'PhoneNo'       =>  'required',
    ];

    public function JabRole(){
        return $this->belongsTo('App\JabRole', 'RoleId');
    }

    public function JabRanks(){
        return $this->belongsTo('App\JabRanks', 'RankId');
    }

    public function JabJobUser(){
        return $this->hasMany('App\JabJobUser', 'UserId');
    }
    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new CustomeObservers());

        parent::boot();
    }
    public static function getUserByEmailIdAndPassword($POST)
    {
        return self::select('UserID', 'HomeJabId','Name','RoleId','ProfilePic')->where('Email',$POST['Email'])
            ->where('Password',$POST['Password'])

            ->where('IsActive',1)
            ->where('RoleId',3)->get();

    }
    public static function LoginErrorResponseJson($error){
        return response()->json([
            'UserID' => null,
            'HomeJabId' => null,
            'Name' => null,
            'RoleId' => null,
            'Error'=>$error,
            'ProfilePic' => null,
        ]);
    }
    public static function UpdateUserDeviceTypeAndToken($POST,$user_id)
    {

        if($user_id == '')
            return false;

        $updateDetails = array();
        if(isset($POST['DeviceType']) && $POST['DeviceType'] != '')
            $updateDetails['DeviceType']    = $POST['DeviceType'];

        if(isset($POST['DeviceToken']) && $POST['DeviceToken'] != '')
            $updateDetails['DeviceToken']   = $POST['DeviceToken'];


        return  self::where('UserId',$user_id)->update($updateDetails);
    }
    public static function UpdateProfilePicByUserId($POST)
    {
        if(!isset($POST['EmployeeID']) && $POST['EmployeeID'] == '')
            return false;

        $post = array();
        $post['ProfilePic'] = $POST['ProfilePic'];

        return self::where('UserId',$POST['EmployeeID'])->update($post);
    }
    public static function getInfoByUserID($userid)
    {
        if($userid == '')
            return false;

        return  self::where('UserID',$userid)->first();
    }
    public static function SendPushNotificationToUser($POST)
    {
        if(!is_array($POST) && count($POST) <= 0)
            return false;

        $objUsersDetails = JabUser::getInfoByUserID($POST['UserID']);

        if( is_object($objUsersDetails))
        {
            # set post for notification.
            $post = array();
            $post['DeviceType']         = $objUsersDetails->DeviceType;
            $post['DeviceToken']        = $objUsersDetails->DeviceToken;
            $post['UserID']             = isset($POST['UserID'])?$POST['UserID']:$objUsersDetails->UserId;
            $post['JobID']              = isset($POST['JobID'])?$POST['JobID']:'';
            $post['HJJobID']            = isset($POST['HomeJabId'])? $POST['HomeJabId'] :$objUsersDetails->HomeJabId;
            $post['Message']            = isset($POST['Message'])?$POST['Message']:'';
            $post['click_action']       = isset($POST['click_action'])?$POST['click_action']:'';
            $post['ShowDetailsButton']  = isset($POST['ShowDetailsButton'])?$POST['ShowDetailsButton']:'';
            $post['isMessage']          = isset($POST['isMessage'])?$POST['isMessage']:'';
			if($objUsersDetails->DeviceType != '' && strtoupper($objUsersDetails->DeviceType) == "ANDROID")
            {
                JabUser::SendNotificationOnAndroid($post);
            }
            elseif($objUsersDetails->DeviceType != '' && strtoupper($objUsersDetails->DeviceType) == "IOS")
            {
                JabUser::SendNotificationOnIOS($post);
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    public static function SendNotificationOnAndroid($POST)
    {
        if(!is_array($POST) && count($POST) <= 0)
            return false;
        try
        {
            $objpNotification = new PushNotification('fcm');
            $retvalue         = $objpNotification->setMessage(
                ['notification' =>
                    [
                        'title'     => 'Homejab',
                        'body'      => $POST['Message'],
                        'icon'      => '@drawable/logo_icon',
                        'sound'     => 'default',
                        'click_action' => $POST['click_action']
                    ],
                    'data' =>
                        [
                            "JobID"        => $POST['JobID'],
                            "HJJobID"      => $POST['HJJobID'],
                            "ShowDetailsButton"=> $POST['ShowDetailsButton'],
                            "isMessage"    => $POST['isMessage']
                        ]
                ])->setDevicesToken($POST['DeviceToken'])->send()->getFeedback();
            return $retvalue;
            /*	[UserID] => 15
        [JobID] => 28
        [Message] => Please accept the job in the queue:13174
        [HomeJabId] => 13174
        [click_action] => OPEN_HOMEJAB_NOTIFICATION_ACTIVITY*/
        }
        catch(Exception $e)
        {
            return $e->getMessage();
        }
    }
    public static function SendNotificationOnIOS($POST)
    {
        if(!is_array($POST) && count($POST) <= 0)
            return false;
        try
        {
            $objpNotification = new PushNotification('apn');

            $retvalue = $objpNotification->setMessage(
                ['aps' =>
                    [
                        'alert' =>
                            [
                                'title' => 'Homejab',
                                'body' => $POST['Message']
                            ],
                        'sound' => 'default',
                        'badge' => 1
                    ],
                    'extraPayLoad' =>
                        [
                            "JobID" => $POST['JobID'],
                            "HJJobID" => $POST['HJJobID'],
                            "ShowDetailsButton" => $POST['ShowDetailsButton'],
                            "isMessage" => $POST['isMessage']
                        ]
                ])->setDevicesToken($POST['DeviceToken'])->send()->getFeedback();

            return $retvalue;
        }
        catch(Exception $e)
        {
            return $e->getMessage();
        }
    }

    public static function getAllActiveUser(){
        return  self::where('RoleId',3)->where('IsActive',true)->get();
    }

    public function getAllPhotographers()
    {
        return $this->newQuery()
            ->select('tbl_users.UserId','tbl_users.Email','tbl_users.Name','tbl_users.PhoneNo','tbl_users.HomeJabId','tbl_users.Password','tbl_users.ProfilePic','tbl_users.IsActive','UA.Address','UA.state','UA.city','UA.Zip','tbl_users.RankId','R.Rank')
            ->leftJoin('tbl_usersaddress As UA','tbl_users.UserId', '=', 'UA.UserId')
            ->leftJoin('tbl_rank AS R','tbl_users.RankId', '=', 'R.RankId')
            ->where('tbl_users.RoleId',3);

    }

    public function getAllActivePhotographers()
    {

        return $this->newQuery()
            ->select('tbl_users.Email','tbl_users.Name','tbl_users.PhoneNo','UA.Address','UA.state','UA.city','UA.Zip','R.Rank')
            ->leftJoin('tbl_usersaddress As UA','UA.UserId', '=', 'tbl_users.UserId')
            ->leftJoin('tbl_rank AS R','R.RankId', '=', 'tbl_users.RankId')
            ->where('tbl_users.RoleId',3);
    }
    public function getAllActivePhotographersByGroupId($groupId)
    {
	    $query = $this->newQuery()
	         ->where('tbl_users.RoleId',3)
	         ->where('tbl_users.IsActive',1);

	    if($groupId != 'All')
		    $query->leftJoin('tbl_usersingroups As UIG','tbl_users.UserId', '=', 'UIG.UserId')->where('UIG.GroupId','=',$groupId);

	    return $query->get();

    }


    public function checkIfuserExists($email,$id=false){
        $query = $this->newQuery()->where('Email',$email);

        if($id > 0)
            $query->where('UserId','!=',$id);

        $info = $query->get();

        if(count($info) > 0)
            return true;
        else
            return false;
    }

    public function InsertPhotographers($POST){

        DB::beginTransaction();
        # Insert record in tbl_user
        $post['Email']      = $POST['Email'];
        $post['Name']       = $POST['Name'];
        $post['HomeJabId']  = $POST['HomeJabId'];
        $post['PhoneNo']    = $POST['PhoneNo'];
        $post['Password']   = $POST['Password'];
        $post['RoleId']     = (isset($POST['RoleId']) && $POST['RoleId'] != '')?$POST['RoleId']:3;
        $post['IsActive']   = (isset($POST['IsActive']) && $POST['IsActive'] != '')?$POST['IsActive']:1;

        $objjabuser = new JabUser();
        $objjabuser->fill($post);
        $jabuser = $objjabuser->save();

        if(!$objjabuser->save()){
            DB::rollBack();
            return false;
        }
        if($jabuser == true)
        {
            # Add data in tbl_useraddresses
            $post = array();

            #Get address location from property address using google API
            $address = str_replace(' ', '+', $POST['Address']);
            $url              = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&libraries=drawing,geometry,places&key='.env("GOOGLE_API_KEY").'&address='.$address;
            $client           = new Client();
            $response         = $client->GET($url);
			//echo $url;
            $objAddressLoc = json_decode($response->getBody()->getContents());

            $address_location           = $objAddressLoc->results[0]->geometry->location;

            $post['Zip']                = $POST['Zip'];
            $post['State']              = $POST['State'];
            $post['City']               = $POST['City'];
            $post['Address']            = $POST['Address'];
            $post['AddressLocation']    = DB::raw("ST_GeomFromText('POINT($address_location->lng $address_location->lat)')");
            $post['UserId']             = $objjabuser->UserId;


            $objjabuseraddress = new JabUsersAddress();
            $objjabuseraddress->fill($post);

            if(!$objjabuseraddress->save()){
                DB::rollBack();
                return false;
            }


            if($objjabuseraddress->save() == true){

                //$html = View('emails.PhotographerAdded', ['arrPhotographer' => $POST])->render();
                //file_put_contents('PhotographerAdded.html',$html); die;
                $send_email = Mail::send('emails.PhotographerAdded', ['arrPhotographer' => $POST], function ($m) use ($POST) {
                    $ToAddress = $POST['Email'];
                    $ToAddressList = Custom::getToAddressList($ToAddress);
	                $m->from(Config('mail.from.address'), Config('mail.from.name'));
                    $m->to($ToAddressList)->subject('New Photographer Added');
                });

            }
            DB::commit();

            return $objjabuser->UserId;

        }


    }
    public function UpdatePhotographers($POST,$UserId){

        DB::beginTransaction();
        # get userinfo from id
        $userinfo=self::select('UserId','Password')->where('UserId',$UserId)->first();

        # update record in tbl_user
        $post['Email']      = $POST['Email'];
        $post['Name']       = $POST['Name'];
        $post['HomeJabId']  = $POST['HomeJabId'];
        $post['PhoneNo']    = $POST['PhoneNo'];
        $post['Password']   = $POST['Password'];

        if(isset($POST['RoleId']) && $POST['RoleId'] != '')
            $post['RoleId']     = $POST['RoleId'];

        if(isset($POST['IsActive']) && $POST['IsActive'] != '')
            $post['IsActive'] = $POST['IsActive'];

        # Check Password Change or not, if changed then send email
        if($userinfo->Password != $post['Password'] ){
            $send_email = Mail::send('emails.PhotographerDetailsChanged', ['arrPhotographer' => $post], function ($m) use ($post) {
                $ToAddress = $post['Email'];
                $ToAddressList = Custom::getToAddressList($ToAddress);
	            $m->from(Config('mail.from.address'), Config('mail.from.name'));
                $m->to($ToAddressList)->subject(' Your Details Updated');
            });
        }

        $objjabuser = JabUser::find($userinfo->UserId);
        $objjabuser->fill($post);
        $photographher = $objjabuser->save();


        if(!$objjabuser->save()){
            DB::rollBack();
            return false;
        }

        if($photographher == true)
        {
            # Now update record in tbl_useraddress.
            $addressid = DB::table('tbl_usersaddress')->select('Address_Id')->where('UserId',$userinfo->UserId)->first();

            #Get address location from property address using google API
            $address = str_replace(' ', '+', $POST['Address']);
            $url              = 'https://maps.googleapis.com/maps/api/geocode/json?sensor=false&libraries=drawing,geometry,places&key='.env("GOOGLE_API_KEY").'&address='.$address;
            $client           = new Client();
            $response         = $client->GET($url);

            $objAddressLoc = json_decode($response->getBody()->getContents());
            if(isset($objAddressLoc->results[0]))
	        {
		        $address_location        = $objAddressLoc->results[0]->geometry->location;
		        $post['AddressLocation'] = DB::raw("ST_GeomFromText('POINT($address_location->lng $address_location->lat)')");
	        }
	            $post['Zip']            = $POST['Zip'];
            $post['State']          = $POST['State'];
            $post['City']           = $POST['City'];
            $post['Address']        = $POST['Address'];

            $objjabuseraddress = JabUsersAddress::find($addressid->Address_Id);
            $objjabuseraddress->fill($post);

            if(!$objjabuseraddress->save()){
                DB::rollBack();
                return false;
            }

            DB::commit();

            return $objjabuser->UserId;
        }

    }

    public function UpdateUserRankId($rank_id,$user_id){
        DB::beginTransaction();

        $objjabuser = JabUser::find($user_id);
        $objjabuser->RankId = $rank_id;


        if ($objjabuser->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();

        return false;
    }

    public function ActiveInactive($activate,$user_id){

        DB::beginTransaction();

        $objjabuser = JabUser::find($user_id);
        $objjabuser->IsActive = $activate;

        if ($objjabuser->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();

        return false;


    }
}