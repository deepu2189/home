<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Rutorika\Sortable\BelongsToSortedManyTrait;
use Log;
use Event;
use App\Events\propertyCreated;


use Illuminate\Support\Facades\Hash;

/**
 * Model to manage the Redit Orders
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since
 * @package HomeJab
 * @subpackage Models
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $Order
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $User
 */
class ReditHasJob extends Model
{
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'redit_has_jobs';



    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Fields that can be filled in a mass assignation
     *
     * @var array
     */
    protected $fillable = [
        'redit_id',
        'all_job_id',
    ];

     /**
     * Relation with jobs
     *
     * @return type
     */
    public function job()
    {
        return $this->hasMany('App\AllJob', 'all_job_id');
    }

     /**
     * Relation with jobs
     *
     * @return type
     */
    public function redit()
    {
        return $this->hasMany('App\Redit', 'redit_id');
    }
}
