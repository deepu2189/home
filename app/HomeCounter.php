<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class that handles the Home Counters
 *
 *
 * @author Dario Grau <dario@serfe.com>
 * @since
 * @package HomeJab
 * @subpackage Models
 */
class HomeCounter extends Model
{
    /**
     * Table name
     * @var string
     */
    protected $table = 'home_counters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rea',
        'videos',
        'photographs',
        'us_cities'
    ];

    /**
     * Validation rules for creation
     * @var array
     */
    public static $rulesOnStore = [
        'rea'      => 'required|integer|min:0',
        'videos'      => 'required|integer|min:0',
        'photographs' => 'required|integer|min:0',
        'us_cities'   => 'required|integer|min:0'
    ];

    /**
     * Validation rules for update
     *
     * @var array
     */
    public static $rulesOnUpdate = [
        'rea'         => 'required|integer|min:0',
        'videos'      => 'required|integer|min:0',
        'photographs' => 'required|integer|min:0',
        'us_cities'   => 'required|integer|min:0'
    ];


    /**
     * Validation rules for update
     *
     * @var array
     */
    public static $fillableOnUpdate = [
        'videos',
        'photographs',
        'us_cities'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * Updates the first entry of home_counters database
     *
     * @param type $data
     * @return boolean
     */
    public static function updateHomeCounters($data)
    {
        $homeCounter = HomeCounter::findOrFail(1);
        $homeCounter->fill($data);
        DB::beginTransaction();
        if ($homeCounter->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }

    /**
     * Gets the recommended values for home counters
     *
     * @return (object)|\HomeJab\Models\HomeCounter
     */
    public static function getRecommendedCounters()
    {

        $matchREA = ['rea', 'seller'];
        $rea = User::whereIn('account_type', $matchREA)->count();

        $property_pictures = Mediafile::where('published', '=', true)
            ->where('type', '=', Mediafile::$imageType)
            ->where('photo_type', '', Mediafile::$photoTypeOnline)
            ->count();

        $video_amount = Mediafile::where('type', '=', Mediafile::$videoType)
            ->where('published', '=', true)
            ->where('origin', '!=', Mediafile::$originPhotographer)
            ->count();

        $us_cities = Property::distinct()->count('zip');

        $data = array(
            'rea' => $rea,
            'videos' => $video_amount,
            'photographs' => $property_pictures,
            'us_cities' => $us_cities
        );

        $counters = new HomeCounter();
        $counters->fill($data);

        return $counters;
    }
}
