<?php

/**
 * @param bool $status
 * @param string $statusCode
 * @param string $message
 * @param array $errors
 * @param array $result
 * @return \Illuminate\Http\JsonResponse
 */

 
function apiResponse($status, $statusCode, $message = null, $errors = [], $result = [])
{
    $response = ['success' => $status, 'status_code' => $statusCode];
    if ($message != "") {
        $response['message'] = $message;
    }
    if (count($errors) > 0) {
        $response['errors'] = $errors;
    }
    if (count($result) > 0) {
        $response['result'] = $result;
    }

    return \Response::json($response, $statusCode, [], JSON_NUMERIC_CHECK);
    //return response()->json($response, $statusCode, [JSON_NUMERIC_CHECK]);
}

function createToken($userId = 1)
{
    return sha1(uniqid($userId . time(), true)) . sha1(uniqid($userId . time(), true));
}


/**
 * conversion utc time to country specific time zone depending upon which country user is belong to
 *
 * @param $utcDate
 * @param string $format
 * @return bool|string
 */
function getLocalTimeZone($utcDate, $format = null)
{
    $currentTimezone = date_default_timezone_get();
    $dateFormat = ($format != "") ? $format : 'Y-m-d H:i:s';
    if ($currentTimezone != '') {
        $date = new \DateTime($utcDate, new DateTimeZone('UTC'));
        $date->setTimezone(new DateTimeZone($currentTimezone));
        return $date->format($dateFormat);
    } else {
        $date = new \DateTime($utcDate, new DateTimeZone('UTC'));
        return $date->format($dateFormat);
    }
}

/**
 * @param string $localDate
 * @param string $format
 * @return bool|string
 */
function getUtcDate($localDate, $format = null)
{
    $currentTimezone = date_default_timezone_get();
    $format = ($format == "") ? 'Y-m-d H:i:s' : $format;
    if ($currentTimezone != '') {
        $date = new \DateTime($localDate, new DateTimeZone($currentTimezone));
        $date->setTimezone(new DateTimeZone('UTC'));
        return $date->format($format);
    }
}

/**
 * decimal for interger
 * @param $number
 * @param string $separator
 * @return string
 */
function twoDecimal($number, $separator = '')
{
    return number_format($number, 2, '.', $separator);
}

function pr($data, $exit = 0)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    if ($exit) {
        exit;
    }
}

function pageSize()
{
    return \Config::get('constants.PAGINATION_LIMIT');
}

function allInputs()
{
   return $all = \Request::all();   
}