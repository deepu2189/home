<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class JobUser extends Authenticatable
{
	use Notifiable;

	protected $table = "tbl_jobuser";

	protected $primaryKey = 'JobUserId';

	public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'JobId','JobScheduleId','UserId','IsActive','CreatedBy','CreatedOn','UpdatedBy','UpdatedOn'
	];

	public static function updateActiveFieldByJobId($jobid)
	{
		if($jobid  == '')
			return false;

		return self::where('JobId',$jobid)->update(array('IsActive'=>0));
	}

}
?>