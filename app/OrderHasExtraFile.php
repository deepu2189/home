<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;

class OrderHasExtraFile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders_has_extra_files';

    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'order_id',
        'mediafile_id',
    ];

    /**
     * Relation with Order
     *
     * @return type
     */
    public function order()
    {
        return $this->hasOne('App\Order', 'id', 'order_id');
    }

    /**
     * Relation with mediafiles
     *
     * @return type
     */
    public function mediafiles()
    {
        return $this->hasOne('App\Mediafile', 'id', 'mediafile_id');
    }

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
}
