<?php

namespace App\VimeoIntegration\Objects;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Config\Repository;
use App\VimeoIntegration\Lib\VimeoIntegration;

use Log;

/**
 * Class with utils function
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.5.0
 * @package HomeJab
 * @subpackage Objects
 */
class CommonFunction
{

    /**
    * Function to get file content lengh
    * @param {string} link: Resource link
    */
    public static function sizeFile($link)
    {
        $client = new \GuzzleHttp\Client();
        $response = null;
        $size = null;
        try {
            $response = $client->request('HEAD', $link);

            $size = null;
            if ($response->hasHeader('content-length')) {
                $size = $response->getHeader('content-length')[0];
            }
        } catch (\GuzzleHttp\Exception\TransferException $e) {
            Log::error($e->getMessage());
            Log::error(print_r($response, true));
        }
        
        return $size;
    }

    /**
    * Function to generate ramdom string
    * @param {int} length: lengh of string.
    * @param {string} keyspace: set of possible values.
    */
    // @codingStandardsIgnoreLine
    public static function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    /**
    * Function to check the scope. Compare scope provided with the resultant.
    * @param {array} scope: array of scope provided.
    * @return {boolean}
    */
    public static function checkScope($scope)
    {
        
        if (count(array_diff(VimeoIntegration::$scopes, $scope))>0) {
            return false;
        }
        return true;
    }

    /**
    * Function to get parameters &redirect_url.
    * @param {string} url: url where extract parameter redirect_url.
    * @return {string} link: url to redirect.
    */
    public static function formatUrl($url)
    {
        $link = substr($url, 0, strpos($url, '&redirect_url='));
        return $link;
    }
}
