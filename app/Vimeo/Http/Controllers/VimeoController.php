<?php

namespace App\VimeoIntegration\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\RestfulController;
use App\VimeoIntegration\Facades\Vimeo;
use App\VimeoIntegration\Lib\VimeoIntegration;
use App\VimeoIntegration\Models\VimeoAccounts;
use App\VimeoIntegration\Objects\CommonFunction;
use App\User;
use App\Mediafile;
use Illuminate\Support\Facades\DB;

use App\VimeoIntegration\Exceptions\ExceptionVimeoAPI;
use ErrorException;
use Exception;

use Log;

/**
 * Vimeo Controller class
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.5.0
 */
class VimeoController extends RestfulController
{

    /**
     * Main account of the system name
     * @var string
     */
    private $mainAccount;

    /**
     * Pointer to the main account
     * @var \HomeJab\VimeoIntegraton\Lib\Vimeo
     */
    private $vimeoMain;

    public function __construct()
    {
        //Get master account
        $this->mainAccount = VimeoAccounts::getMasterAccount();

        $this->vimeoMain = new VimeoIntegration($this->mainAccount);
    }

    /**
     * @api {post} /vimeo/changeStatus/:id Change status account
     * @apiGroup Vimeo
     * @apiPermission SU
     * @apiParam {int} id identifier account
     * @apiSuccess (200 OK) {integer} code=200 Code of operation
     * @apiSuccess (200 OK) {string} message Message to show to the user
     * @apiSampleRequest off
     */
    /**
     * Changes the status of the account
     * @return {json}
     */
    public function changeStatus($id)
    {
        $user_id = null;

        try {
            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];
        } catch (\Exceptions $e) {
            return $this->respondForbidden(trans('vimeo.forbidden'));
        }

        $user = User::find($user_id);
        if ($user->isAdmin()) {
            //Search account
            $vimeoAccount = VimeoAccounts::getAccountById($id);
            if (!is_null($vimeoAccount) && strcmp($vimeoAccount->name, 'main') == 0) {
                $data = [
                    "message" => trans('vimeo.master_account')
                ];
                return $this->respondUnprocesableEntry($data);
            }
            if (!is_null($vimeoAccount)) {
                //Check that upload request is available
                try {
                    $vimeoIntegration = new VimeoIntegration($vimeoAccount);
                    $quota = $vimeoIntegration->checkQuota();
                    if ($quota['space']['max'] == 0) {
                        $data = [
                            "message" => trans('vimeo.no_upload_permission')
                        ];
                        return $this->respondUnprocesableEntry($data);
                    }

                    $vimeoAccount->enabled = !$vimeoAccount->enabled;
                    if ($vimeoAccount->save()) {
                        $msg = ($vimeoAccount->enabled) ? trans('vimeo.account_enable') : trans('vimeo.account_disable') ;
                            return $this->respond([
                                'message'   => $msg,
                                'data'      => ""
                            ]);
                    }
                } catch (ExceptionVimeoAPI $e) {
                    Log::error($e->getMessage());
                    return $this->respondUnprocesableEntry(trans('vimeo.fail_api'));
                }
            } else {
                return $this->respondNotFound();
            }
        } else {
            return $this->respondForbidden(trans('vimeo.invalid_profile'));
        }
    }




    /**
    * Return a new url to upload video from FrontEnd
     *
     * @param Request $request
     * @return {json}
     * @api {post} /vimeo/uploadVideo Return a new url to upload video from FrontEnd
     * @apiGroup Vimeo
     * @apiPermission Admin, Rea, Seller
     * @apiParam {string} size: file size to upload in bytes
     * @apiSuccessParam (200 OK) {object}
     */
    public function uploadVideo(Request $request)
    {
        $user_id = null;

        try {
            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];
        } catch (\Exception $e) {
            return $this->respondForbidden(trans('vimeo.forbidden'));
        }

        $user = User::find($user_id);
        if ($user->isAdmin() || $user->isAgent() || $user->isSeller()) {
            if (!$request->exists('size')) {
                return $this->respondBadRequest(trans('vimeo.missing_parameter'));
            }

            $size = $request->get('size');

            try {
                //1 - Get account with free space
                $account = VimeoAccounts::getAccountWithQuota($size);

               // pr($account,1);

                $vimeo = new VimeoIntegration($account);

                //2- Generate ticket
                $ticket = $vimeo->generateTicket();
               
                $ticket['account_id'] = $account->id;

                return $this->respond($ticket);
            } catch (ExceptionVimeoAPI $e) {
                Log::error($e->getMessage());
                return $this->respondUnprocesableEntry(trans('vimeo.fail_api'));
            }
        } else {
            return $this->respondForbidden(trans('vimeo.invalid_profile'));
        }
    }



     /**
     * @param Request $request
     * @return {json}
     * @api {get} /vimeo/getAccounts  Return a summary of all accounts
     * @apiGroup Vimeo
     * @apiPermission Admin
     * @apiSuccessParam (200 OK) {object}
     */
    public function getAccounts()
    {

        $user_id = null;

        try {
            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];
        } catch (\Exception $e) {
            return $this->respondForbidden(trans('vimeo.forbidden'));
        }

        $user = User::find($user_id);
        if ($user->isAdmin()) {
            // 1 - Get all accounts and validate access token
            $allAccounts = VimeoAccounts::getAllConnections();

            // 2 - Get quota of accounts and take the first account with space.
            $allQuotas = array();
            foreach ($allAccounts as $account) {
                try {
                    $vimeo = new VimeoIntegration($account);
                    $quota = $vimeo->checkQuota();
                    $quota['id'] = $account->id;
                    $quota['name'] = $account->name;
                    $quota['enabled'] = (boolean) $account->enabled;
                    array_push($allQuotas, $quota);
                } catch (ExceptionVimeoAPI $e) {
                    Log::error($e->getMessage(). " - " . $e->getCode());
                    //return $this->respondUnprocesableEntry(trans('vimeo.fail_api'));
                }
            }

            if (count($allQuotas) > 0) {
                 $data = [
                    'data' => $allQuotas,
                    'message' => trans('vimeo.accounts')
                 ];
                 return $this->respond($data);
            } else {
                //Not found
                return $this->respondNotFound(trans('vimeo.not_found'));
            }
        } else {
            //No allow
            return $this->respondForbidden(trans('vimeo.invalid_profile'));
        }
    }


    /**
     *
     * @param Request $request
     * @return {json}
     * @api {post} /vimeo/createAccount Create new vimeo account.
     * @apiGroup Vimeo
     * @apiPermission Admin
     * @apiSuccessParam (200 OK) {object}
     * @apiParam {string} client_id: Identifier vimeo account. Unique.
     * @apiParam {string} client_secret: vimeo secret app.
     * @apiParam {string} access_token: Token.
     */
    public function createAccount(Request $request)
    {
        $user_id = null;

        try {
            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];
        } catch (\Exception $e) {
            return $this->respondForbidden(trans('vimeo.forbidden'));
        }

        $user = User::find($user_id);
        if ($user->isAdmin()) {
            $validator = \Validator::make($request->all(), VimeoAccounts::$rulesOnCreate);

            if ($validator->fails()) {
                return $this->respondValidationFailed(trans('vimeo.validation_fails'), $validator->errors());
            }

            DB::beginTransaction();
            try {
                $newAccount = new VimeoAccounts();
                $lastid = VimeoAccounts::getLastId();
                $newAccount->name = 'slave-'.$lastid;
                $newAccount->enabled = false;
                $newAccount->client_secret = $request->get('client_secret');
                $newAccount->client_id = $request->get('client_id');
                $newAccount->access_token = $request->get('access_token');
                if ($newAccount->save()) {
                    Db::commit();
                    return $this->respond([
                            'message' => trans('vimeo.new_account')
                    ]);
                }
            } catch (\Exception $e) {
                DB::rollback();
                Log::error($e->getMessage() . $e->getFile() . $e->getLine());
                return $this->respondNotFound(trans('vimeo.error'));
            }
        } else {
            return $this->respondForbidden(trans('vimeo.invalid_profile'));
        }
    }


    /**
     *
     * @param Request $request
     * @return {json}
     * @api {post} /vimeo/validateNewAccount Validate vimeo account. It must be called after /vimeo/createAccount
     * @apiGroup Vimeo
     * @apiPermission Admin
     * @apiSuccessParam (200 OK) {object}
     * @apiParam {string} client_id: Identifier vimeo account. Unique.
     * @apiParam {string} client_secret: vimeo secret app.
     * @apiParam {string} access_token: Token.
     */
    public function validateNewAccount(Request $request)
    {
          $user_id = null;

        try {
            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];
        } catch (\Exception $e) {
            return $this->respondForbidden(trans('vimeo.forbidden'));
        }

        $user = User::find($user_id);
        if ($user->isAdmin()) {
            $validator = \Validator::make($request->all(), VimeoAccounts::$rulesOnValidate);

            if ($validator->fails()) {
                return $this->respondValidationFailed(trans('vimeo.validation_fails'), $validator->errors());
            }

            DB::beginTransaction();
            $newAccount = VimeoAccounts::getAccountByClientId($request->get('client_id'));
            try {
                $vimeoIntegration = new VimeoIntegration($newAccount);
                $dataUser = $vimeoIntegration->checkAccount();
                if ($dataUser) {
                    //save link account.
                    Log::info($dataUser['link']);
                    $newAccount->account_link = $dataUser['link'];
                    if ($newAccount->save()) {
                        DB::commit();
                        return $this->respond([
                                'message' => trans('vimeo.account_added')
                        ]);
                    }
                }
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                $newAccount->delete();
                DB::commit();
                $data = [
                    'message' =>  trans('vimeo.error') . ' ' .  $e->getMessage(),
                    'errors' => $e->getMessage()
                ];

                return $this->respondUnprocesableEntry($data);
            }
        } else {
            return $this->respondForbidden(trans('vimeo.invalid_profile'));
        }
    }


        /**
        *
        * @param Request $request
        * @return {json}
        * @api {post} /vimeo/deleteTicket Delete Ticket created for a specific account.
        * @apiGroup Vimeo
        * @apiPermission Admin Agent Seller
        * @apiSuccessParam (200 OK) {object}
        * @apiParam {string} account_id: vimeo account id (local database).
        * @apiParam {string} complete_uri: ticket's uri.
        */
    public function deleteTicket(Request $request)
    {
        try {
            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];
        } catch (\Exception $e) {
            return $this->respondForbidden(trans('vimeo.forbidden'));
        }

        $user = User::find($user_id);
        if ($user->isAdmin() || $user->isAgent() || $user->isSeller()) {
            if (!$request->exists('account_id')) {
                return $this->respondBadRequest(trans('vimeo.missing_parameter'));
            }

            $uri = $request->get('complete_uri');
            $account_id = $request->get('account_id');


                //1 - Get account with free space
                $account = VimeoAccounts::getAccountById($account_id);

                $vimeo = new VimeoIntegration($account);

                //2- Delete ticket
                $delete = $vimeo->deleteTicket($uri);


                return $this->respond($delete);
        } else {
            return $this->respondForbidden(trans('vimeo.invalid_profile'));
        }
    }


        /**
        *
        * @param type $id
        * @return type
        * @api {post} /vimeo/regenerateLinks Regenerate download links for a specific video.
        * @apiGroup Vimeo
        * @apiPermission Admin Agent Seller
        * @apiSuccessParam (200 OK) {object}
        * @apiParam {string} account_id: vimeo account id (local database).
        * @apiParam {string} complete_uri: ticket's uri.
        */
    public function regenerateLinks($id)
    {
        try {
            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];
        } catch (\Exception $e) {
            return $this->respondForbidden(trans('vimeo.forbidden'));
        }

        try {
            $user = User::find($user_id);
            if ($user->isAdmin() || $user->isAgent() || $user->isSeller()) {
                if (!$id) {
                    return $this->respondBadRequest(trans('vimeo.missing_parameter'));
                }

                $mediafile = Mediafile::find($id);

                if ($mediafile) {
                    $account = $this->mainAccount;
                    $vimeo = new VimeoIntegration($account);
                    $metadata_old = json_decode($mediafile->metadata);
                    $videoId= $metadata_old->video_id;

                    $video = $vimeo->getInformationVideo($videoId);

                    foreach ($video['download'] as $download) {
                        if ($download['quality']==='source') {
                            $url=$download['link'];
                            break;
                        }
                    }

                    //Update metadata
                    $metadata_new = array(
                        'download' => $video['download'],
                        'files' => $video['files'],
                        'video_id' => $videoId,
                        'account_id' => $metadata_old->account_id
                    );

                    $mediafile->metadata = json_encode($metadata_new);
                    $mediafile->save();

                    return $url;
                }
            } else {
                return $this->respondForbidden(trans('vimeo.invalid_profile'));
            }
        } catch (\Exception $e) {
                Log::error($e->getMessage());
                $data = [
                    'message' =>  trans('vimeo.error') . ' ' .  $e->getMessage(),
                    'errors' => $e->getMessage()
                ];

                return $this->respondUnprocesableEntry($data);
        }
    }

    /*
    * REQUEST NEW ACCOUNT INTEGRATION
    */

    // /**
    //  * NO USED
    //  * @param Request $request
    //  * @return {json}
    //  * @api {get} /vimeo/newAccount  Return a link to authorization new account
    //  * @apiGroup Vimeo
    //  * @apiPermission Admin
    //  * @apiSuccessParam (200 OK) {object}
    //  */
    // public function authNewAccount(){
    //     $user_id = null;

    //     try {
    //         $user_id = Authorizer::getResourceOwnerId();
    //     } catch (\LucaDegasperi\OAuth2Server\Exceptions\NoActiveAccessTokenException $e) {
    //         return $this->respondForbidden("Action not allowed");
    //     }

    //     $user = User::find($user_id);
    //     if ($user->isAdmin()) {
    //         $url = Config::get('app.vimeo_redirect_uri');
    //         try{
    //             $link = $this->vimeoMain->authNewAccount($url);
    //         } catch(ExceptionVimeoAPI $e){
    //             Log::error($e->getMessage());
    //             return $this->respondUnprocesableEntry("Fail communication with vimeo API.");
    //         }
    //         if($link){
    //             return $this->respond([
    //                 'data' => [
    //                     'link' => $link
    //                 ]
    //             ]);
    //         }else{
    //             return $this->respondNotFound('Error to comunnication with vimeo');
    //         }
    //     }else{
    //         return $this->respondForbidden("Action not allowed");
    //     }
    // }

    // /**
    // * NO USED
    // * @param Request $request
    // * @return {json}
    // * @api {post} /vimeo/getAccess Validate account and generate accessToken
    // * @apiGroup Vimeo
    // * @apiPermission Admin
    // * @apiSuccessParam (200 OK) {object}
    // * @apiParam {string} code: code to generate an access token with vimeo api.
    // */
    // public function getAccess(Request $request){

    //     $user_id = null;

    //     try {
    //         $user_id = Authorizer::getResourceOwnerId();
    //     } catch (\LucaDegasperi\OAuth2Server\Exceptions\NoActiveAccessTokenException $e) {
    //         return $this->respondForbidden("Action not allowed");
    //     }

    //     $user = User::find($user_id);
    //     if ($user->isAdmin()) {


    //         if($request->has('code')){
    //             $code = $request->get('code');
    //         }else{
    //             return $this->respondBadRequest();
    //         }

    //         $url = Config::get('app.vimeo_redirect_uri');
    //         $token = $this->vimeoMain->generateToken($code, $url);
    //         if(!is_null($token)){
    //             //TODO save new aacount in database
    //             $account = new VimeoAccounts();
    //             $account->name = $token['user']['name'];
    //             $account->access_token = $token['access_token'];
    //             $account->account_link = $token['user']['link'];
    //             $account->user_id = substr($token['user']['uri'],7);
    //             $account->enabled = true;

    //             //Validate scope
    //             if(CommonFunction::checkScope(explode(" ", $token['scope']))){
    //                 if($account->save()){
    //                     return $this->respond("The account was added successfully");
    //                 }
    //             }else{
    //                 $this->respondBadRequest("The scope is invalid");
    //             }

    //         }else{
    //             return $this->respondNotFound('Error vimeo');
    //         }
    //     }else{
    //         return $this->respondForbidden("Action not allowed");
    //     }
    // }
}
