<?php
namespace App\VimeoIntegration\Exceptions;

use Exception;

/**
 * This exception is called when the communication with the API fail.
 *
* @author Andres Estepa <andres@serfe.com>
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Exception
 */
class ExceptionVimeoAPI extends Exception
{

     /**
     * Creates an exception when the API call fail
     *
     * @param string $message
     * @param integer $code
     * @param type $previous
     */
    public function __construct($message, $code = null, $previous = null)
    {
        if (is_array($message)) {
            $code = $message['status'];
            $message = $message['error'];
        }
        parent::__construct($message, $code, $previous);
    }
}
