<?php

namespace App\VimeoIntegration\Lib;

use App\VimeoIntegration\Facades\Vimeo;
use App\VimeoIntegration\Models\VimeoAccounts;
use App\VimeoIntegration\Objects\CommonFunction;
use App\VimeoIntegration\Exceptions\ExceptionVimeoAPI;
use App\VimeoIntegration\Jobs\SendEmailVimeoAccountLimit;

use Log;

/**
 * Handle integration with vimeo
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.5.0
 */
class VimeoIntegration
{
    /**
     * Account
     */
    private $vimeoAccount;

    private $vimeoConnection;


    /**
     * All permission
     */
    public static $scopes = ['public', 'private', 'create', 'edit', 'delete', 'interact', 'upload', 'video_files'];


    public function __construct(VimeoAccounts $account)
    {

        try {
            $this->vimeoAccount = $account;
           
            //Set Connection
            $this->vimeoConnection = Vimeo::connection($this->vimeoAccount->name);
            $this->vimeoConnection->setToken($this->vimeoAccount->access_token);

            //Check quota and send a email if quota usage is upper 80%
            if ($this->vimeoAccount->enabled) {
                $quota = $this->checkQuota();

                $usage = $this->getUsage($quota);
                if ($usage > 80) {
                    $job = (new SendEmailVimeoAccountLimit($this->vimeoAccount->name, $this->vimeoAccount->account_link))->onQueue(Config::get('aws.sqs.vimeo'));
                    $this->dispatch($job);
                }
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * Return percentage usage of account
     */
    public function getUsage($quota)
    {
        $r = 0;
        if ($quota['space']['max'] > 0) {
            $r = (100 * intval($quota['space']['used'])) / intval($quota['space']['max']);
        }
        return $r;
    }


    /**
     * Check if token is valid
     */
    public function checkToken()
    {

        $res = $this->VimeoRequest('/oauth/verify', [], 'GET');

        if ($res && array_key_exists('access_token', $res) && array_key_exists('user', $res)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Generate url to authorization app to handler new account
     * @param {string} url: url to redirect user after request is accepted.
     */
    public function authNewAccount($url)
    {
        $state = CommonFunction::random_str(10);
        $res = $this->vimeoConnection->buildAuthorizationEndpoint($url, $this->scopes, $state);
        return $res;
    }

    /**
     * Function to generate token for a new account
     * @param {string} code
     * @param {string} redirect_uri:
     */
    public function generateToken($code, $redirect_uri)
    {
        //Set token to null (The endpoint need that header have basic authentication and not bearer)
        $this->vimeoConnection->setToken(null);
        //Generate Token
        $token = $this->vimeoConnection->accessToken($code, $redirect_uri);

        if (array_key_exists('access_token', $token['body'])) {
            return $token['body'];
        }
        return null;
    }


    /**
     * Get quota usage of current account
     * @return {int} upload_quota.
     */
    public function checkQuota()
    {
        $res = $this->VimeoRequest('/me', [], 'GET');
        if (array_key_exists('error', $res)) {
            return $res;
        } else {
            if (array_key_exists('upload_quota', $res)) {
                return $res['upload_quota'];
            } else {
                $quota = array(
                    "space" => [
                        "free" => 0,
                        "max" => 0,
                        "used" => 0
                    ]
                );
                return $quota; //0 indicate that uploadQuota is not available: the upload request has not been accepted.
            }
        }
    }

    /**
     * Get information of all videos
     * @return {array}
     */
    public function getVideos()
    {
        return $this->VimeoRequest('/me/videos', [], 'GET');
    }

    /**
     * Check users data.
     */
    public function checkAccount()
    {
        return $this->VimeoRequest('/me', [], 'GET');
    }

    /**
     * Generate ticket to upload video from frontend
     * @return {array}
     */
    public function generateTicket()
    {
        $params = array(
            'type' => 'streaming'
        );
        return $this->VimeoRequest('/me/videos', $params, 'POST');
    }

    /**
     * Get information video when the video is not hosted in master account.
     * @param {int} user_id: user identifier
     * @param {int} video_id: video identifier
     */
    public function getInformationVideoFromUser($user_id, $video_id)
    {
        $res = $this->VimeoRequest('/users/' . $user_id . '/videos/' . $video_id, [], 'GET');
        return $res;
    }

    /**
     * Delete ticket with $uri
     * @param type $uri
     * @return type
     */
    public function deleteTicket($uri)
    {
        return $this->VimeoRequestWithHeaders($uri, [], 'DELETE');
    }

    /**
     * Function to upload video with public link
     * @param {string} link: Video public link.
     * @return undefined.
     */
    public function uploadVideoPull($link)
    {
        $params = array(
            'type' => 'pull',
            'link' => $link
        );
        return $this->VimeoRequest('/me/videos', $params, 'POST');
    }

    /**
     * Check status video.
     * @param {int} video_id: identifier video.
     * @return undefined.
     */
    public function checkStatusVideo($video_id)
    {
        $res = $this->VimeoRequest('/me/videos/' . $video_id, [], 'GET');
        if (array_key_exists('status', $res)) {
            return $res['status'];
        }
        return null;
    }

    /**
     * Return all information of a video
     * @param {int} identifier video.
     */
    public function getInformationVideo($video_id)
    {
        //  $this->vimeoConnection->setToken($this->vimeoAccount->access_token);

        $res = $this->VimeoRequest('/me/videos/' . $video_id, [], 'GET');

        return $res;
    }



    /**
     * Wrapper request vimeo
     * Handler all request to Vimeo Api.
     * @param $path
     * @param $options optional
     * @param $method
     */
    // @codingStandardsIgnoreLine
    public function VimeoRequest($path, $params, $method)
    {
        $result = $this->vimeoConnection->request($path, $params, $method);
        if (array_key_exists('headers', $result) && array_key_exists('X-RateLimit-Remaining', $result['headers'])) {
            Log::info("Vimeo limits: " . $result['headers']['X-RateLimit-Remaining'] . "/" . $result['headers']['X-RateLimit-Limit'] . " - " . $result['headers']['X-RateLimit-Reset']);
        }
        if ($result['status'] >= 200 && $result['status'] < 300) {
            return $result['body'];
        } else {
            $error = [
                'status' => $result['status'],
                'error' => $result['body']['error']
            ];
            //Exception
            throw new ExceptionVimeoAPI($error);
        }
    }

    /**
     *
     * @param type $path
     * @param type $params
     * @param type $method
     * @return type
     * @throws ExceptionVimeoAPI
     */
    // @codingStandardsIgnoreLine
    public function VimeoRequestWithHeaders($path, $params, $method)
    {
        $result = $this->vimeoConnection->request($path, $params, $method);
        if (array_key_exists('headers', $result) && array_key_exists('X-RateLimit-Remaining', $result['headers'])) {
            Log::info("Vimeo limits: " . $result['headers']['X-RateLimit-Remaining'] . "/" . $result['headers']['X-RateLimit-Limit'] . " - " . $result['headers']['X-RateLimit-Reset']);
        }
        if ($result['status'] >= 200 && $result['status'] < 300) {
            return [
                'body' => $result['body'],
                'headers' => $result['headers']
            ];
        } else {
            $error = [
                'status' => $result['status'],
                'error' => $result['body']['error']
            ];
            //Exception
            throw new ExceptionVimeoAPI($error);
        }
    }

    /**
     * Delete video from Vimeo with $video_id
     * @param type $video_id
     * @return type
     */
    public function deleteVideo($video_id)
    {
        $res = $this->VimeoRequest('/videos/' . $video_id, [], 'DELETE');
        return $res;
    }
}
