<?php

namespace App\VimeoIntegration\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Config\Repository;
use App\VimeoIntegration\Facades\Vimeo;
use App\VimeoIntegration\Lib\VimeoIntegration;

use Log;

/**
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.5.0
 * @package HomeJab
 * @subpackage Models
 */
class VimeoAccounts extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'vimeo_accounts';
    
    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
  
    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'name',
        'client_id',
        'client_secret',
        'access_token',
        'enabled',
    ];

    public static $rulesOnCreate = [
        'client_id' => 'required|unique:vimeo_accounts',
        'client_secret' => 'required',
        'access_token' => 'required'
    ];

    public static $rulesOnValidate = [
        'client_id' => 'required',
        'client_secret' => 'required',
        'access_token' => 'required'
    ];
    
    
    /**
    * Return all connection in array
    * with format required by vimeo.php config file.
    */
    public static function getAllConnections()
    {
        return self::all();
    }

    /**
    * Function to return an array with all vimeo connection added to database.
    * @return {array}
    */
    public static function getAllConnectionsInArray()
    {
        
        $allConnection = VimeoAccounts::getAllConnections();
        $formatArray = array();

        foreach ($allConnection as $connection) {
            $tmp = array(
                'client_id' => $connection->client_id,
                'client_secret' => $connection->client_secret,
                'access_token' => $connection->access_token,
            );
            
            $formatArray[$connection->name] = $tmp;
        }

        return $connections = ['vimeo' => [
            'default' => 'main',
            'connections' => $formatArray,
        ] ];
    }

    /**
    * Function to return a repository laravel object with all vimeo
    * connection availables
    * @return {Object} Repository.
    */
    public static function getRepository()
    {
        return new Repository(VimeoAccounts::getAllConnectionsInArray());
    }
    
    /**
    * Function the master vimeo account (always have id=1)
    * @return {Object} account.
    */
    public static function getMasterAccount()
    {
        return VimeoAccounts::find(1);
    }

    /**
    * Return the first VimeoAccount founded with space to storage
    * a file with size $size
    * @param {int} $size: file size.
    */
    public static function getAccountWithQuota($size)
    {
        //1 - Get all accounts and validate access token
        $allAccounts = VimeoAccounts::getAllConnections();

        //2 - Get quota of accounts and take the first account with space.
        $accountWithSpace = null;
        $lastMajorSpace = 0;
        foreach ($allAccounts as $account) {
            if ($account->enabled) {
                $vimeo = new VimeoIntegration($account);
                $quota = $vimeo->checkQuota();
                $space = $quota['space']['free'];
                if ($space > $size) {
                    if ($space > $lastMajorSpace) {
                        $lastMajorSpace = $space;
                        $accountWithSpace = $account;
                    }
                }
            }
        }
        return $accountWithSpace;
    }

    /**
    * Return the id of last account added.
    * @return {int} id.
    */
    public static function getLastId()
    {
        $account = DB::table('vimeo_accounts')->where('id', '>', 1)->max('id');
        if ($account) {
            return $account;
        }

        return 1;
    }

    /**
    * Get account by id.
    * @param {int} id: identifier
    * @return {Object} account.
    */
    public static function getAccountById($id)
    {
        $account = self::find($id);
        return $account;
    }

    /**
    * Get account by Client id.
    * @param {int} client_id: client identifier
    * @return {Object} account.
    */
    public static function getAccountByClientId($client_id)
    {
        $account = VimeoAccounts::where('client_id', '=', $client_id)->first();
        return $account;
    }
}
