<?php

namespace App\VimeoIntegration\Console\Commands;

use Illuminate\Console\Command;
use App\VimeoIntegration\Objects\CommonFunction;
use Illuminate\Support\Facades\Config;

use App\VimeoIntegration\Facades\Vimeo;

use App\Models\Agent;
use App\Models\Mediafile;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\VimeoIntegration\Jobs\UploadVideoToVimeo;

use Log;

/**
 * Get video list
 *
 * @author Andres Estepa
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Console
 */
class VimeoMigrateAgentsVideos extends Command
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vimeo:migrate_agents_videos {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate agents videos from amazon to vimeo';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    
        if (env('APP_ENV') === 'local' || env('APP_ENV') === 'testing') {
            $this->line(print_r("Local or testing environment detected. Command canceled. Please, change environment.", true));
            return true;
        }

         //Last mediafile id proccessed
        $lastID = $this->argument('id');
        if (!$lastID) {
            $lastID = 0;
        }

        $offset = 0;
        $limit = 5;

        $count = Agent::quantityAgentsWithVideo($lastID);
        $this->line(print_r("Total videos: " . $count, true));
        
        while ($limit*$offset <= $count) {
            $agentsVideos = Agent::getAgentsVideos($lastID, $limit);
            Log::info($agentsVideos);

            foreach ($agentsVideos as $agent) {
                $mediafile = Mediafile::find($agent['profile_video_id']);
                

                if (!$mediafile) {
                    Log::info('Mediafile ' . $mediafile->id . 'not found');
                    $this->line(print_r('Mediafile with id' . $mediafile->id . 'not found', true));
                    return;
                }
                
                
                $this->line(print_r("Processing Mediafile with id " . $mediafile->id, true));

                if ($mediafile->type === Mediafile::$profileType) {
                    $omediafile = new Mediafile();
                    $omediafile->external_url = $mediafile->external_url;
                    $omediafile->copied_to_amazon = false;
                    if ($omediafile->save()) {
                        $vimeoUploadJob = (new UploadVideoToVimeo($mediafile->id));
                        $vimeoUploadJob->setUserId($agent['id']);
                        $vimeoUploadJob->onQueue(Config::get('aws.sqs.vimeo'));
                        $this->dispatch($vimeoUploadJob);
                    }
                }
                $lastID = $mediafile->id;
            }

            $offset++;
        }
    }
}
