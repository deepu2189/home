<?php

namespace App\VimeoIntegration\Console\Commands;

use Illuminate\Console\Command;

use App\VimeoIntegration\Facades\Vimeo;
use App\VimeoIntegration\Lib\VimeoIntegration;
use App\VimeoIntegration\Models\VimeoAccounts;
use App\VimeoIntegration\Exceptions\ExceptionVimeoAPI;
use ErrorException;
use Exception;

/**
 * Get Quota
 *
 * @author Andres Estepa
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Console
 */
class VimeoCheckQuota extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vimeo:get_quota';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Return quota';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $allAccounts = VimeoAccounts::getAllConnections();

        //2 - Get quota of accounts and take the first account with space.
        try {
            $allQuotas = array();
            foreach ($allAccounts as $account) {
                $vimeo = new VimeoIntegration($account);
                $quota = $vimeo->checkQuota();
                $quota['id'] = $account->id;
                $quota['name'] = $account->name;
                $quota['enabled'] = (boolean) $account->enabled;
                array_push($allQuotas, $quota);
            }
        } catch (ExceptionVimeoAPI $e) {
            Log::error($e->getMessage());
            return $this->respondUnprocesableEntry("Fail communication with vimeo API.");
        }

        $this->line(print_r($allQuotas, true));
    }
}
