<?php

namespace App\VimeoIntegration\Console\Commands;

use Illuminate\Console\Command;

use App\VimeoIntegration\Facades\Vimeo;
use App\VimeoIntegration\Jobs\SendEmailVimeoAccountLimit;
use App\VimeoIntegration\Models\VimeoAccounts;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\VimeoIntegration\Jobs\UploadVideoToVimeo;

use Illuminate\Support\Facades\Config;

/**
 * Get Quota
 *
 * @author Andres Estepa
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Console
 */
class SendEmailVimeoLimit extends Command
{

     use InteractsWithQueue, SerializesModels, DispatchesJobs;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vimeo:send_email_limit_account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email informing that limit usage has been exceeded';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $vimeoAccount = VimeoAccounts::getMasterAccount();
        $job = (new SendEmailVimeoAccountLimit($vimeoAccount->name, $vimeoAccount->account_link))->onQueue(Config::get('aws.sqs.vimeo'));
        $this->dispatch($job);
    }
}
