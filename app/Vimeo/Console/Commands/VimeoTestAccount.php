<?php

namespace App\VimeoIntegration\Console\Commands;

use Illuminate\Console\Command;

use App\VimeoIntegration\Facades\Vimeo;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

/**
 * Get Quota
 *
 * @author Andres Estepa
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Console
 */
class VimeoTestAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vimeo:create_test_account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert vimeo account test in database (Key, secret and token)';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      

        //Vimeo Main account (email: support@homejab.com)
        $data1 = [
            'id' => 1,
            'name' => 'main',
            'client_id' => '482b1634c572c019c03abde8326808595c8cd9e4',
            'client_secret' => 'j3kU35IaMI3XZWu3DYFPflk/eV6IosZIT5ajIvqPn6MKE/e26UEUg2YO5bw8zbhpZD24JMRKDpetui1AOpkSY05TuZ/fYEwhxk9q+W9erwNjxQLv5e10nQqvjuixAP9+',
            'access_token' => '85d5fa31832ea2f9018f8f91645f8f26',
            'enabled' => true,
            'account_link' => "https://vimeo.com/user41236431",
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
        

        DB::table('vimeo_accounts')->insert($data1);
    }
}
