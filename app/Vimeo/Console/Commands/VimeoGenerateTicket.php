<?php

namespace App\VimeoIntegration\Console\Commands;

use Illuminate\Console\Command;

use App\VimeoIntegration\Facades\Vimeo;

/**
 * Get Quota
 *
 * @author Andres Estepa
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Console
 */
class VimeoGenerateTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vimeo:generate_ticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Return a ticket id to upload video';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = 'main';
        
        Vimeo::setDefaultConnection($name);
        $response = Vimeo::request('/me/videos', [], 'POST');

        $this->line(print_r($response, true));
    }
}
