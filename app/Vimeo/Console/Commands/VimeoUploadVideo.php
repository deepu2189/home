<?php

namespace App\VimeoIntegration\Console\Commands;

use Illuminate\Console\Command;
use App\VimeoIntegration\Objects\CommonFunction;
use App\VimeoIntegration\Facades\Vimeo;
use App\VimeoIntegration\Models\VimeoAccounts;
use App\VimeoIntegration\Lib\VimeoIntegration;

use Log;

/**
 * Get Quota
 *
 * @author Andres Estepa
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Console
 */
class VimeoUploadVideo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vimeo:upload_video {link}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload video from external resource';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $link = "https://cdn-dev-m2.esoftsystems.com/50100037/4857/50000205481/51621/esoft-720p/50000310636-esoft-720p.mp4";
        $link = $this->argument('link');
        $size = CommonFunction::sizeFile($link);
        
        if (!is_null($size)) {
            $this->line(print_r('File size: ' . $size, true));
            //2 - Get account to upload
            $vimeoAccount = VimeoAccounts::getMasterAccount();
            if (!is_null($vimeoAccount)) {
                //$this->line(print_r("The video will be uploaded to " . $vimeoAccount->name, true));
                $vimeoIntegration = new VimeoIntegration($vimeoAccount);
                try {
                    $user_id = 21856338;
                    $ticket = $vimeoIntegration->uploadVideoPull($link);
                    $this->line(print_r($ticket, true));
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
            }
        } else {
            $this->line(print_r('Error: The file size has not been calculated ', true));
        }
    }
}
