<?php

namespace App\VimeoIntegration\Console\Commands;

use Illuminate\Console\Command;
use App\VimeoIntegration\Objects\CommonFunction;

use App\VimeoIntegration\Facades\Vimeo;

use Log;

/**
 * Get video list
 *
 * @author Andres Estepa
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Console
 */
class VimeoGetVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vimeo:get_videos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Return video list';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $name = 'Video Tours';
        
        Vimeo::setDefaultConnection($name);

        $user_id = 21856338;
        $video_id = 210601545;
        // $response = Vimeo::request('/me/videos', ['per_page' => 10], 'GET');
        $response = Vimeo::request('/users/' . $user_id . '/videos/' . $video_id, [], 'GET');
        Log::info($response);
        $this->line(print_r($response, true));
    }
}
