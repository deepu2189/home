<?php

namespace App\VimeoIntegration\Jobs;

use App\Jobs\Job;
use App\Mediafile;

use App\VimeoIntegration\Models\VimeoAccounts;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\VimeoIntegration\Lib\VimeoIntegration;

use App\VimeoIntegration\Exceptions\ExceptionVimeoAPI;

use Log;

/**
 * Background job that check status of video.
 * When the video has been updated publish video.
 *
 * @author Mariana Perez Elena <mariana@serfe.com>
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Job
 */
class DeleteVideoTicket extends Job implements ShouldQueue
{

    use InteractsWithQueue, SerializesModels, Dispatchable;

    /**
     * Mediafile identifier
     *
     * @var integer
     */
    private $mediafile_id = null;

    /**
     * Vimeo Account Id
     *
     * @var \Models\VimeoAccounts
     */
    private $vimeo_account_id = null;
    
    /**
     * Complete uri
     * @var string
     */
    private $complete_uri = null;

     /**
     * Vimeo Integration:
     * Handle request api for $vimeoAccount
     *
     * @var \Lib\VimeoIntegration
     */
    private $vimeoIntegration = null;

    private $user_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mediafile_id, $complete_uri, $vimeo_account_id)
    {
        // we store only the mediafile ID
        $this->mediafile_id = $mediafile_id;
        $this->complete_uri = $complete_uri;
        $this->vimeo_account_id = $vimeo_account_id;
    }


    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($testing = false)
    {
        //1 - Get link
        $mediafile = Mediafile::find($this->mediafile_id);
        if (!$mediafile) {
            Log::error("Mediafile #".$this->mediafile_id." does not exist in database");
            return true;
        }
        


        try {
            $vimeoAccount = VimeoAccounts::find($this->vimeo_account_id);

            if (!$vimeoAccount) {
                Log::error("Invalid account");
                return false;
            }

            $this->vimeoIntegration =  new VimeoIntegration($vimeoAccount);
            $this->vimeoIntegration->deleteTicket($this->complete_uri);
        } catch (ExceptionVimeoAPI $e) {
            Log::info("Fail");
            Log::info($e->getMessage());
            if ($this->attempts() > 6) {
                Log::error("Giving up after 6 retry events to try to delete ticket".$this->complete_uri);
                Log::error($e->getMessage());
                return true;
            } else {
                $this->release(pow(15*$this->attempts(), 2));
                return false;
            }
        }
    }
}
