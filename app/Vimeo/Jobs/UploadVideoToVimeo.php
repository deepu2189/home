<?php

namespace App\VimeoIntegration\Jobs;

use App\Jobs\Job;
use App\Models\Mediafile;
use App\VimeoIntegration\Models\VimeoAccounts;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\VimeoIntegration\Objects\CommonFunction;
use App\VimeoIntegration\Jobs\CheckVideoUpload;
use App\VimeoIntegration\Lib\VimeoIntegration;
use Illuminate\Support\Facades\App;
use App\VimeoIntegration\Exceptions\ExceptionVimeoAPI;
use ErrorException;
use Exception;
use Log;
use Illuminate\Support\Facades\Config;

/**
 * Background job that upload the file from the processed entry in eWarp
 * site to vimeo account.
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.5.0
 * @package HomeJab
 * @subpackage Job
 */
class UploadVideoToVimeo extends Job implements ShouldQueue
{

    use InteractsWithQueue,
        SerializesModels,
        Dispatchable;

    /**
     * Mediafile identifier
     *
     * @var integer
     */
    private $mediafile_id = null;

    /**
     * Vimeo Account
     *
     * @var \Models\VimeoAccounts
     */
    private $vimeoAccount = null;

    /**
     * Vimeo Integration:
     * Handle request api for $vimeoAccount
     *
     * @var \Lib\VimeoIntegration
     */
    private $vimeoIntegration = null;

    /**
     * Display the current user ID
     * @var null|integer
     */
    private $user_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mediafile_id)
    {
        // we store only the mediafile ID
        $this->mediafile_id = $mediafile_id;
    }

    /**
     * Identifier for the user id
     * @param integer $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($testing = false)
    {

        //1 - Get link
        $mediafile = Mediafile::find($this->mediafile_id);
        if (!$mediafile) {
            Log::error("Mediafile #" . $this->mediafile_id . " does not exist in databse");
            return true;
        }

        //2 - Get size of file
        $link = $mediafile->external_url;
        $size = CommonFunction::sizeFile($link);

        if (!is_null($size)) {
            //2 - Get account to upload
            $this->vimeoAccount = VimeoAccounts::getAccountWithQuota($size);
            if (!is_null($this->vimeoAccount)) {
                $this->vimeoIntegration = new VimeoIntegration($this->vimeoAccount);
                try {
                    $data = $this->vimeoIntegration->uploadVideoPull($link);
                    $video_id = substr($data['uri'], strpos($data['uri'], '/', 1) + 1);
                    $vimeoCheckStatus = (new CheckVideoUpload($this->mediafile_id, $video_id, $this->vimeoAccount->id))->onQueue(Config::get('aws.sqs.vimeo'));
                    if (!is_null($this->user_id)) {
                        $vimeoCheckStatus->setUserId($this->user_id);
                    }
                    $this->dispatch($vimeoCheckStatus);
                } catch (ExceptionVimeoAPI $e) {
                    if ($this->attempts() > 6) {
                        Log::error("Giving up after 6 retry events to try to upload mediafile " . $this->mediafile_id);
                        Log::error($e->getMessage());
                        return true;
                    } else {
                        $this->release(pow(15 * $this->attempts(), 2));
                        return false;
                    }
                }
            }
        } else {
            if ($this->attempts() > 5) {
                $job = (new SendEmailErrorGettingFileSize($link))->onQueue(Config::get('aws.sqs.email'));
                $this->dispatch($job);
            }
            $this->release(pow(15 * $this->attempts(), 2));
            return false;
        }
        return true;
    }
}
