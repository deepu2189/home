<?php

namespace App\VimeoIntegration\Jobs;

use App\Jobs\Job;
use App\Mediafile;
use App\User;

use App\VimeoIntegration\Models\VimeoAccounts;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\VimeoIntegration\Objects\CommonFunction;
use App\VimeoIntegration\Lib\VimeoIntegration;
use App\VimeoIntegration\Jobs\DeleteVideoTicket;
use Illuminate\Support\Facades\App;

use App\VimeoIntegration\Exceptions\ExceptionVimeoAPI;
use ErrorException;
use Exception;

use Log;

use Illuminate\Support\Facades\Config;

/**
 * Background job that check status of video.
 * When the video has been updated publish video.
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Job
 */
class CheckVideoUpload extends Job implements ShouldQueue
{

    use InteractsWithQueue, SerializesModels, Dispatchable;

    /**
     * Mediafile identifier
     *
     * @var integer
     */
    private $mediafile_id = null;

    /**
     * Vimeo Account id
     *
     * @var \Models\VimeoAccounts
     */
    private $vimeo_account_id = null;


    /**
    * Vimeo video id.
    */
    private $video_id = null;


     /**
     * Vimeo Integration:
     * Handle request api for $vimeoAccount
     *
     * @var \Lib\VimeoIntegration
     */
    private $vimeoIntegration = null;

    private $user_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mediafile_id, $video_id, $vimeo_account_id)
    {


        // we store only the mediafile ID
        $this->mediafile_id = $mediafile_id;
        $this->video_id = $video_id;
        $this->vimeo_account_id = $vimeo_account_id;

    }

    public function setUserId($user_id)
    {
        $this->user_id = \Auth()->user()->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($testing = false)
    {
        try {
            $vimeoAccount = VimeoAccounts::find($this->vimeo_account_id);

            if (!$vimeoAccount) {
                Log::error("Invalid account.");
                return false;
            }

            $this->vimeoIntegration =  new VimeoIntegration($vimeoAccount);

            $video = $this->vimeoIntegration->getInformationVideo($this->video_id);
            if (array_key_exists('status', $video) && strcmp($video['status'], 'available') !== 0) {

                //$this->release(pow(15*$this->attempts(), 2));
                try{
                    Log::info('In try of attempts ');

                    $this->release(300);
                    Log::info('after try ');
                }
                catch(Exception $e){
                    Log::info('In catch ');
                    Log::info($e->getMessage());
                }


                Log::info('Status not valid when checking for video '.$this->video_id);
                return false;
            } else {
                Log::info('Video uploaded successfully and key is inserted to database '.$this->video_id);
                $mediafile = Mediafile::find($this->mediafile_id);

                //Publish video
                if (!$mediafile) {
                    Log::error("Mediafile #".$this->mediafile_id." does not exist in database");
                    return true;
                }

                $mediafile->type = 'videos';
                /**
                 * The next line is commented to avoid the problem with duplication videos.
                 * Vimeo doesn't keep the original name of the file and if the order
                 * related have multiple jobs each "bacthdelivery" search the video
                 * file by filename which does not match and then duplicate the file.
                 * Commenting this line the original name given by eWarp is keeping and the problem is avoided.
                 * See ticket (https://tracker.serfe.com/view.php?id=53724)
                 */
                // $mediafile->original_name = $video['name'];
                $mediafile->key = $video['uri'];
                $mediafile->storage = "vimeo";
                $mediafile->copied_to_amazon = false;

                $files = $video['files'];
                $download = $video['download'];
                $source = null;
                $source_download = null;

                for ($i = 0; $i < count($download); $i++) {
                    if (strcmp($download[$i]['quality'], 'source') === 0) {
                        $source = $files[$i];
                        $source_download = $download[$i];
                        break;
                    }
                }

                if (!is_null($source) && !is_null($source_download)) {
                    $mediafile->extension = $source['type'];
                    $mediafile->external_storage = true;
                    $mediafile->external_url = $video['link'];
                    $mediafile->md5 = $source['md5'];
                }

                $metadata_old = json_decode($mediafile->metadata);
                $metadata_new = array(
                    'download' => $video['download'],
                    'files' => $video['files'],
                    'video_id' => $this->video_id,
                    'account_id' => $this->vimeo_account_id
                );


                $mediafile->metadata = json_encode($metadata_new);


                if (!is_null($this->user_id)) {
                    $user = User::find($this->user_id);
                    if (!$user) {
                        Log::error('The user '. $this->user_id . ' not found');
                    }
                    $mediafile->type = "profile";
                    $user->profile_video_id = $mediafile->id;
                    $user->save();
                }

                if ($mediafile->save()) {
                    return true;
                }
            }
        } catch (\Exception $e) {
            Log::info("Fail");
            Log::info($e->getMessage());
            if ($this->attempts() > 6) {
                Log::error("Giving up after 6 retry events to try to check status of video_id " . $this->video_id);
                Log::error($e->getMessage());
                return true;
            } else {
                $this->release(pow(15*$this->attempts(), 2));
                return false;
            }
        }
    }
}
