<?php

namespace App\VimeoIntegration\Jobs;

use App\Jobs\Job;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;
use Mail;

/**
 * Background job that send email informing that token has expired.
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Job
 */
class SendEmailVimeoAccountLimit extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     *
     * @var {string} name: account name.
     */
    protected $name;

    /**
    * @var {string} link: user link account
    */
    protected $link;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name, $link)
    {
        $this->name = $name;
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * Send an email to Sales
     *
     * @return void
     */
    public function handle()
    {
        
        
        if (!Config::get('app.support_email')) {
            throw new \Exception("Email support is not configured");
        }

        //send email
        Mail::send(
            'emails.vimeo.vimeo_account_limit',
            [
                'name' => $this->name,
                'link' => $this->link
            ],
            function ($m) {
                $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
                $m->to(
                    Config::get('app.support_email')
                )->subject("Vimeo account limit");
            }
        );
        return true;
    }


    /**
     * Handle a job failure.
     *
     * @return void
     */
    public function failed()
    {
        // Called when the job is failing...
    }
}
