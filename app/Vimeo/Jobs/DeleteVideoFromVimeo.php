<?php

namespace App\VimeoIntegration\Jobs;

use App\Jobs\Job;
use App\VimeoIntegration\Models\VimeoAccounts;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\VimeoIntegration\Lib\VimeoIntegration;

use App\VimeoIntegration\Exceptions\ExceptionVimeoAPI;

use Log;

/**
 * Background job that check status of video.
 * When the video has been updated publish video.
 *
 * @author Mariana Perez Elena <mariana@serfe.com>
 * @since 1.5.0
 * @package HomeJab
 * @subpackage VimeoIntegration/Job
 */
class DeleteVideoFromVimeo extends Job implements ShouldQueue
{

    use InteractsWithQueue, SerializesModels, Dispatchable;
    /**
     * Video identifier
     *
     * @var integer
     */
    private $video_id = null;

    /**
     * Vimeo Account Id
     *
     * @var \Models\VimeoAccounts
     */
    private $vimeo_account_id = null;
    


     /**
     * Vimeo Integration:
     * Handle request api for $vimeoAccount
     *
     * @var \Controllers\VimeoIntegration
     */
    private $vimeoIntegration = null;

    private $user_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($video_id, $vimeo_account_id)
    {
        // we store only the mediafile ID
        $this->video_id = $video_id;
        $this->vimeo_account_id = $vimeo_account_id;
    }


    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($testing = false)
    {
          
        try {
            $vimeoAccount = VimeoAccounts::find($this->vimeo_account_id);

            if (!$vimeoAccount) {
                Log::error("Invalid account");
            }

            $this->vimeoIntegration =  new VimeoIntegration($vimeoAccount);
            $this->vimeoIntegration->deleteVideo($this->video_id);
        } catch (ExceptionVimeoAPI $e) {
            Log::info("Fail");
            Log::info($e->getMessage());
            if ($this->attempts() > 6) {
                Log::error("Giving up after 6 retry events to try to delete video".$this->video_id);
                Log::error($e->getMessage());
                return true;
            } else {
                $this->release(pow(15*$this->attempts(), 2));
                return false;
            }
        }
    }
}
