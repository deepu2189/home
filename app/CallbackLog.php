<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Model for the Callback logs
 *
 * @author Deepak Thakur * 
 * @since 1.0
 * @package HomeJab
 * @subpackage Models
 */

class CallbackLog extends Model
{
    /**
     * Name for the table
     * @var string
     */
    protected $table = 'callback_jobs';
    
    /**
     * Data conversion date
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    
    /**
     * Relation with Jobs
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order()
    {
        return $this->hasMany('App\Order', 'order_id');
    }
    
    /**
     * Saves a callback log
     *
     * @param integer $order_id Order identifier
     * @param string $payload
     * @param integer $attempt Number of attempt for the callback
     */
    public static function addCallbackLog($order_id, $payload, $attempt, $type, $callback_status_id)
    {

        $self = new CallbackLog();
        $self->order_id = $order_id;
        $self->payload = json_encode($payload);
        $self->attempt = $attempt;
        $self->type = $type;
        $self->callback_status_id = $callback_status_id;

        if ($self->save()) {
            return true;
        }
        return false;
    }
}
