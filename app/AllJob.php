<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\eWarpIntegration\Jobs\CreateEWarpOrderFromJob;
use App\PackageHasAssetType;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Event;
use App\Events\EwarpQuantityFail;
use Log;

class AllJob extends Model
{
    use SoftDeletes, DispatchesJobs;

    

    /**
     * Validation rules for the order placing
     * @var array
     */
    public static $rulesOnPlaceOrder = [
        'contact_email' => 'required|email',
        'property_address' => 'required|string'
    ];
    

    /**
     * Validation rules for the job Update
     * @var array
     */
    public static $rulesOnUpdate = [];
    public static $rulesOnScheduleUpdate = [
        'status' => 'required|in:new,assigned,scheduled,in_progress',
        'scheduling_time' => 'required',
        'scheduling_date' => 'required|date_format:Y-m-d'
    ];

    

    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'order_id',
        'contact_email',
        'contact_phone',
        'contact_owner_email',
        'contact_owner_phone',
        'property_address',
        'property_details',
        'quantity',
        'max_files_amount',
        'scheduling_date',
        'scheduling_time',
        'status',
        'asset_type_id',
        'batch_id',
        'comments',
        'finished_by_su',
        'redit',
        'send_to_ewarp_by_su',
        
    ];
    /**
     * Fields that are mass assignable on updates
     *
     * @var type
     */
    public static $fillableOnUpdate = [
        'photographer_id',
        'comments',
        'status',
        'max_files_amount',
        'finished_by_su'
    ];

    /**
     * Labels and keys for database indication for status
     *
     * @var array
     */
    public static   $status = [
        'new' => [ // Just created
            'key' => 'new',
            'label' => 'New'
        ],
        'assigned' => [ // Photographer has stared uploading
            'key' => 'assigned',
            'label' => 'Assigned to photographer'
        ],
        'redit' => [ // Photographer has stared uploading
            'key' => 'redit',
            'label' => 'Re-edit'
        ],
        'scheduled' => [
            'key' => 'scheduled',
            'label' => 'Scheduled for shooting'
        ],
        'in_progress' => [
            'key' => 'in_progress',
            'label' => 'In Progress'
        ],
        'uploaded' => [ // Photographer has finish uploading and was sent to Amazon
            'key' => 'uploaded',
            'label' => 'Uploaded'
        ],
        'processing' => [ // Sent to media processing
            'key' => 'processing',
            'label' => 'Media processing'
        ],
        'finished' => [ // Media processing finished
            'key' => 'finished',
            'label' => 'Finished'
        ]
    ];

    /**
     * Labels and keys for the Scheduling Time
     *
     * @var type
     */
    public static $schedulingTime = [
        'anytime' => [
            'key' => 'anytime',
            'label' => 'Anytime'
        ],
        '8amto11am'=>[
             'key' => '8amto11am',
            'label' => '8 AM - 11 AM'
        ],
        '9amto12pm'=>[
             'key' => '9amto12pm',
            'label' => '9 AM - 12 PM'
        ],
        '10amto1pm'=>[
             'key' => '10amto1pm',
            'label' => '10 AM - 1 PM'
        ],
        '11amto2pm'=>[
             'key' => '11amto2pm',
            'label' => '11 AM - 2 PM'
        ],
        '12pmto3pm'=>[
             'key' => '12pmto3pm',
            'label' => '12 PM - 3 PM'
        ],
        '1pmto4pm'=>[
             'key' => '1pmto4pm',
            'label' => '1 PM - 4 PM'
        ],
        '2pmto5pm'=>[
             'key' => '2pmto5pm',
            'label' => '2 PM - 5 PM'
        ],
        '3pmto6pm'=>[
             'key' => '3pmto6pm',
            'label' => '3 PM - 6 PM'
        ]
    ];
    
    /**
     * Mapping added for the scheduling app. Maps the frontend options to a specific
     * Time for the scheduling app
     * @var array
     */
    public static $scheduling_time_mapping = [
        '8amto11am' => '08:00:00',
        '9amto12pm' => '09:00:00',
        '10amto1pm' => '10:00:00',
        '11amto2pm' => '11:00:00',
        '12pmto3pm' => '12:00:00',
        '1pmto4pm'  => '13:00:00',
        '2pmto5pm'  => '14:00:00',
        '3pmto6pm'  => '15:00:00'
    ];
    
    /**
     * Mapping added for the scheduling app. Maps the frontend options to a specific
     * slot time for the scheduling app
     * @var array
     */
    public static $scheduling_slot_mapping = [
        '8amto11am' => '8am to 11am',
        '9amto12pm' => '9am to 12pm',
        '10amto1pm' => '10am to 1pm',
        '11amto2pm' => '11am to 2pm',
        '12pmto3pm' => '12pm to 3pm',
        '1pmto4pm'  => '1pm to 4pm',
        '2pmto5pm'  => '2pm to 5pm',
        '3pmto6pm'  => '3pm to 6pm'
    ];

    /**
     * Relation with Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * Relation with Photographer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photographer()
    {
        return $this->belongsTo('App\Photographer');
    }

    /**
     * Relation with mediafiles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function mediafiles()
    {
        return $this->belongsToMany('App\Mediafile', 'jobs_has_mediafiles', 'job_id');
    }

    /**
     * Relation with property
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assetType()
    {
        return $this->belongsTo('App\AssetType');
    }
    
    /**
     * Relation with ewarp_logs
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ewarpLogs()
    {
        return $this->hasMany('App\EwarpLog', 'job_id');
    }

    public function redit()
    {
        // return $this->hasOne('App\ReditHasJob');
        return $this->belongsToMany('App\Redit', 'redit_has_jobs', 'job_id');
    }

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Creates a new job and sets the correct status
     *
     * @param array $data
     * @return boolean
     */
    public function createFromOrder($data)
    {
        $this->fill($data);
        $assetId = $data['asset_type_id'];

        $order = Order::find($data['order_id']);
        $packageId = $order->package_id;
        $packageHasAsset = new PackageHasAssetType();
        $amount_originals = $packageHasAsset->getOriginalFilesAmount($packageId, $assetId);
        $amount_edited = $packageHasAsset->getEditedFilesAmount($packageId, $assetId);

        // $this->max_files_amount = $amount_originals;
        $this->max_files_amount = 0;
        $this->quantity = $amount_edited;

        if (!AssetType::needsEWarpProcess($assetId)) {
            $this->status = self::$status['assigned']['key'];
        } else {
            $this->status = self::$status['new']['key'];
        }
        if ($this->save()) {
            return true;
        }
        return false;
    }

    /**
     * Creates a new job and sets the correct status
     *
     * @param array $data
     * @return boolean
     */
    public static function createForRedit($data)
    {
        $jobRedit = AllJob::create($data);
        
        
        $assetType = AssetType::find($data['asset_type_id']);
        
        $jobRedit->max_files_amount = $assetType->minimal_amount;
        
        $jobRedit->status = self::$status['redit']['key'];
        
        if ($jobRedit->save()) {
            return $jobRedit;
        }

        return false;
    }

    

    /**
     * Checks if a job is reserved to a photographer
     *
     * @param integer $job_id
     * @return boolean
     * @throws \BadMethodCallException
     */
    public function isReserved($job_id = null)
    {
        if (is_null($job_id)) {
            throw new \BadMethodCallException("Parameter job id cannot be null");
        }

        $amount = $this->where([
            'id' => $job_id,
            'photographer_id' => null,
        ])->count();

        
        if ($amount > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Sets a job as reserved to a photographer
     *
     * @param integer $job_id
     * @param integer $photographer_id
     * @return boolean
     */
    public function reserve($job_id, $photographer_id)
    {
        DB::beginTransaction();

        $job = $this->find($job_id);

        $job->photographer_id = $photographer_id;
        $job->status = self::$status['assigned']['key'];

        if ($job->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();
        return false;
    }


    /**
     * Sets a job as finish uploaded
     *
     * @param integer $job_id
     * @param boolean $by_su. True if SU user send job to ewarp.
     * @return boolean
     */
    public static function finishUpload($job_id, $by_su = false)
    {
        DB::beginTransaction();

        $job = self::find($job_id);
        $job->status = self::$status['uploaded']['key'];
        if ($by_su) {
            $job->send_to_ewarp_by_su = true;
        }

        if ($job->save()) {
            DB::commit();
            return $job;
        }
        DB::rollback();
        return false;
    }



     /**
     * Check if the given photographer is assigned to the de job
     *
     * @param type $user
     * @return bool
     */
    public function isAssigned($user)
    {
        return $this->photographer_id == $user->photographer->id;
    }

    /**
     * Sets a job as processing mediafiles
     *
     * @param integer $job_id
     * @return boolean
     */
    public static function markAsProcessing($job_id)
    {
        DB::beginTransaction();

        $job = self::find($job_id);
        $job->status = self::$status['processing']['key'];

        if ($job->save()) {
            DB::commit();
            return $job;
        }
        DB::rollback();
        return false;
    }


    /**
     * Sets a job as processed by eWarp and finished
     *
     * @param integer $job_id
     * @return boolean
     */
    public static function markAsFinished($job_id)
    {

        $job = self::findOrFail($job_id);
        $job->status = self::$status['finished']['key'];

        if ($job->save()) {
            return $job;
        }
        return false;
    }


    /**
     * Sets a job as processed by eWarp and finished
     *
     * @param integer $job_id
     * @return boolean
     */
    public static function markAsFinishedByBatchId($batch_id)
    {

        $job = self::where('batch_id', '=', $batch_id)->first();
        if (!is_null($job) && $job->checkJobQuantityFiles()) {
            return self::markAsFinished($job->id);
        } else {
            return false;
        }
    }
    

    public static function hasIncompleteJobs($orderId)
    {
        $order = Order::find($orderId);

        $totalJobs = $order->jobs()->count();
        $completeJobs = $order->jobs()->where('status', '=', self::$status['finished']['key'])->count();

        if ($totalJobs !== $completeJobs) {
            return true;
        }
        return false;
    }

    /**
     * Gets job by batch id
     *
     * @param integer $job_id
     * @return boolean
     */
    public static function getJobByBatchId($batch_id)
    {
        $job = self::where('batch_id', "=", $batch_id)->first();
        
        if (!is_null($job)) {
            return $job;
        } else {
            return false;
        }
    }


    /**
     * Check if exists uncompleted job for an specific order id
     *
     * @param type $order_id
     * @deprecated
     * @return boolean
     */
    public static function areUncompletedJobsByOrderId($order_id)
    {
        // get mediafiles related to the property
        $mediafiles_ids = Property::where('order_id', '=', $order_id)
                ->first()
                ->mediafiles()
                ->allRelatedIds();
        
        // count mediafiles pictures and divide on two (print & online must be counted once)
        $pictures_count = Mediafile::whereIn('id', $mediafiles_ids)
                ->where('type', '=', Mediafile::$imageType)
                ->where('origin', '=', Mediafile::$originEWarp)
                ->count();
        
        $pictures = floor($pictures_count / 2);
        
        /*if ($pictures <= 0) {
            // if there is no pictures, do not send notification
            return true;
        }*/
        
        $videos = Mediafile::whereIn('id', $mediafiles_ids)
                ->where('type', Mediafile::$videoType)
                ->where('origin', '=', Mediafile::$originEWarp)
                ->count();
                        
        $mediafiles_amount = $pictures + $videos;
        
        // Add all requested file quantities of each order job
        $order_total_amount = 0;
        $jobs = self::where('order_id', '=', $order_id)->get(['*']);
        
        foreach ($jobs as $job) {
            if ($job->assetType->ewarp_product_id) {
                $order_total_amount += $job->quantity;
            }
        }
        if ($mediafiles_amount >= $order_total_amount) {
            return false;
        }
        
        return true;
    }


    /**
     *
     * @return boolean
     * @throws \Exception
     */
    public function checkJobQuantityFiles()
    {
        try {
            // get mediafiles related to the property
            $mediafiles_ids = Property::where('order_id', '=', $this->order_id)
            ->first()
            ->mediafiles()
            ->allRelatedIds();




            $mediafiles_count = Mediafile::whereIn('id', $mediafiles_ids)
            ->where('batch_id', '=', $this->batch_id)
            ->where('origin', '=', Mediafile::$originEWarp)
            ->count();

            if ($mediafiles_count >= $this->quantity) {
                return true;
            }



            Event::fire(new EwarpQuantityFail([
                'batch_id' => $this->batch_id,
                'mediafiles_count' => $mediafiles_count,
                'qty_photographer' => $this->getFilesAmount(),
            ]));

            
            return false;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw $e;
        }
    }
    
    /**
     * Check if exists finished job for an specific order id
     *
     * @param type $order_id
     * @return boolean
     */
    public static function areFinishedJobsByOrderId($order_id)
    {
        $amount = self::where('order_id', '=', $order_id)
                ->where('status', [ self::$status['finished']['key']])
                ->count();
        if ($amount > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Check if exists uncompleted jobs with pictures for an specific order id
     *
     * @param type $order_id
     * @return boolean
     */
    public static function areUncompletedPicturesJobsByOrderId($order_id)
    {
        $jobs = self::where('order_id', '=', $order_id);
        
        // check if there are jobs with mediafiles type pictures and not finished
        foreach ($jobs as $job) {
            $mediafiles_ids = $job->mediafiles()->allRelatedIds();
            $mediafiles     = Mediafile::whereIn('id', $mediafiles_ids)->where('type', 'pictures')->count();
            
            if ($mediafiles > 0 && $job->status !== self::$status['finished']['key']) {
                // picture job not finished
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Mutator for contact_owner_email
     *
     * @param varchar $contact_owner_email
     * @return boolean
     */
    public function setContactOwnerEmailAttribute($contact_owner_email = false)
    {
        if (!$contact_owner_email) {
            $contact_owner_email = null;
        }

        $this->attributes['contact_owner_email'] = $contact_owner_email;
    }

    /**
     * Mutator for contact_owner_phone
     *
     * @param varchar $contact_owner_phone
     * @return boolean
     */
    public function setContactOwnerPhoneAttribute($contact_owner_phone = false)
    {
        if (!$contact_owner_phone) {
            $contact_owner_phone = null;
        }

        $this->attributes['contact_owner_phone'] = $contact_owner_phone;
    }
    
    /**
     * Return the agen data for a acertain Job.
     *
     * @param type $jobId
     * @return type
     */
    public static function getAgentByJobId($jobId = null)
    {
        $job    = self::findOrFail($jobId);
        $order  = Order::findOrFail($job->order_id);
        return    Agent::findOrFail($order->agent_id);
    }

    /**
     * Return the jos data for a acertain order_id
     *
     * @param type $orderId
     * @return type
     */
    public static function getJobByOrderId($orderId = null)
    {
        return self::query()->where('order_id', $orderId)->get();
    }
    
 
     /*
     * migrate job
     * 
     */
    public static function migrateAgentSeller($id, $array_seller_agent)
    {
        $job = self::find($id);
        $job->contact_email = $array_seller_agent['contact_email'];
        $job->contact_phone = $array_seller_agent['contact_phone'];
        if ($job->save()) {
            return true;
        }
        return false;
    }

    public static function existJobWithBatchID($batch_id)
    {
        $job = self::where('batch_id', '=', $batch_id)->first();

        if ($job) {
            return $job;
        }

        return false;
    }

    public static function isReditJob($batchId)
    {
        $job = AllJob::where('batch_id', '=', $batchId)->first();

        if ($job->redit()->first()) {
            return true;
        }
        
        return false;
    }


    /**
    * Function to verify if the job is finished
    */
    public function isFinished()
    {
        if (strcmp($this->status, self::$status['finished']['key']) === 0) {
            return true;
        }

        return false;
    }

    /**
    * Function to verify if there are redit jobs for the order
    * @param {int} $orderID
    * @return {boolean}
    */
    public static function thereAreReditJobs($orderId)
    {
        $redits = Redit::where('order_id', '=', $orderId)->get();

        if (count($redits) > 0) {
            return true;
        }

        return false;
    }

    /**
    * Function to get amount of files that belong to job
    * @return {integer} Amount of files.
    */
    public function getFilesAmount()
    {
        $filesAmount = \App\JobHasMediaFile::where('job_id', '=', $this->id)->count();
        return $filesAmount;
    }


    /**
    * Function to send job to ewarp
    * @return {boolean} $by_su. True if SU user force to send the job to eWarp.
    */
    public function sendJobToEwarp($by_su = false)
    {
          //Send to ewarp
        try {
            $job = AllJob::finishUpload($this->id, $by_su);
            if ($job) {

                // Start the eWarp integration
                $bgjob = (new CreateEWarpOrderFromJob($job->id))->onQueue(Config::get('aws.sqs.ewarp'));
                $this->dispatch($bgjob);
                $job->already_queued = true;
                $job->save();

                return $job;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Function to return the number of files to be edited by asset
     *
     * @param Carbon $from Start date
     * @param Carbon $to Stop date
     * @return {Array} $result. Array with amount of files for each asset.
     */
    public static function getMediafilesToBeEdited($from, $to)
    {

        //Asset types accepted by ewarp
        $assetTypesIds = AssetType::whereNotNull('ewarp_product_id')->get(['id']);

        //Jobs in date range with status different to finish and batch id set.
        $jobsIds = Job::where('status', '!=', 'finished')
                        ->whereIn('asset_type_id', $assetTypesIds)
                        ->whereNotNull('batch_id')
                        ->whereBetween('updated_at', array($from, $to))
                        ->orderBy('asset_type_id')
                        ->get();

        
        $mediafilesByAsset = [];

        
        foreach ($jobsIds as $index => $job) {
            $asset = $job->assetType->name;

            $assetTypeId = $job->asset_type_id;
            $packageId = $job->order->package_id;

            $packageHasAsset = PackageHasAssetType::where('asset_type_id', '=', $assetTypeId)
                                              ->where('package_id', '=', $packageId)
                                              ->first();

            
            
            if ($packageHasAsset->relation_n_to_one) {
                if (array_key_exists($asset, $mediafilesByAsset)) {
                    $mediafilesByAsset[$asset]['ratio'] = 1;
                    $mediafilesByAsset[$asset]['amount'] = $mediafilesByAsset[$asset]['amount'] + 1;
                } else {
                    $mediafilesByAsset[$asset] = [ 'amount' => 1, 'ratio' => 1];
                }
            } else {
                $files = $job->mediafiles()->whereNotNull('batch_id')->count();
                if (array_key_exists($asset, $mediafilesByAsset)) {
                    $mediafilesByAsset[$asset]['amount'] = $mediafilesByAsset[$asset]['amount'] + $files;
                } else {
                    $mediafilesByAsset[$asset] = [ 'amount' => $files, 'ratio' => -1];
                }
            }

            if ($mediafilesByAsset[$asset]['ratio'] == -1) {
                $ratio = $packageHasAsset->amount_originals_files / $packageHasAsset->amount_edited_files;
                
                $mediafilesByAsset[$asset]['ratio'] = $ratio;
            }
        }
        return $mediafilesByAsset;
    }

    public static function allJobsFinished($orderId)
    {
        $jobs = self::where('order_id', '=', $orderId)->get();

        // check if there are jobs with mediafiles type pictures and not finished
        foreach ($jobs as $job) {
            if (!$job->isFinished()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Function to verify if the job is being processed
     */
    public function isProcessing()
    {
        if (strcmp($this->status, self::$status['processing']['key']) === 0) {
            return true;
        }

        return false;
    }

    /**
     * Function to verify if the job is uploaded
     */
    public function isUploaded()
    {
        if (strcmp($this->status, self::$status['uploaded']['key']) === 0) {
            return true;
        }

        return false;
    }

    /**
     * Function to verify if the job is uploaded
     */
    public function isRedit()
    {
        if (strcmp($this->status, self::$status['redit']['key']) === 0) {
            return true;
        }

        return false;
    }


}
