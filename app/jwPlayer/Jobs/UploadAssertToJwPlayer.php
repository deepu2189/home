<?php

namespace App\jwPlayerIntegration\Jobs;

use App\Jobs\Job;
use App\Models\Mediafile;
use App\Models\PropertyHasMediaFile;
use App\jwPlayerIntegration\jwPlayer;
use Illuminate\Support\Facades\DB;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use ErrorException;
use Exception;
//use App\eWarpIntegration\Exceptions\ErrorCopyFileToAmazonException;
use Log;
use Illuminate\Support\Facades\Config;

/**
 * Background job that downloads the file from the processed entry in eWarp
 * site and uploads it to jw Player inside the correspondent url.
 *
 * @author Deepak Thakur
 * @since 0.5
 * @package App
 * @subpackage jwPlayerIntegrationIntegration
 */
class UploadAssertToJwPlayer extends Job implements SelfHandling, ShouldQueue
{

    use InteractsWithQueue, SerializesModels;

    /**
     * Mediafile identifier
     *
     * @var integer
     */
    private $mediafile_id = null;
    
    /**
     * Property identifier
     * @var integer
     */
    private $property_id = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mediafile_id, $property_id)
    {
        // we store only the mediafile ID
        $this->mediafile_id = $mediafile_id;
        $this->property_id  = $property_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        
//        $omediafile = Mediafile::findOrFail($this->mediafile_id);
//        $mediafile = $omediafile->replicate();
//        unset($omediafile);
//        unset($mediafile->id);
//        $original_url = $mediafile->external_url;
//        $temporal_file = tempnam(sys_get_temp_dir(), $mediafile->original_name);
//        
//        if (env('APP_ENV') !== 'local' ) {
//            $client = new \GuzzleHttp\Client();
//
//            try {
//                // Download the file from the remote URL
//                $client->request('GET', $original_url,  array('save_to' => $temporal_file));
//                 if (!file_exists($temporal_file)) {
//                    throw new Exception("There was an error triying to download the file from eWarp");
//                }
//            } catch (ErrorException $e) {
//                // Could not open the file
//                Log::error($e->getMessage());
//                throw new ErrorCopyFileToAmazonException("Could not copy the file from the original location. Check error log.");
//            } catch (\GuzzleHttp\Exception\ClientException $e) {
//                Log::error($e->getMessage());
//                throw new ErrorCopyFileToAmazonException("Could not download the file from the original location. Client response was: " . $e->getMessage());
//            }
//        } else {
//            $temporal_file = base_path() . '/tests/_data/video.MP4';
//        }
//        
//        try {
//            $jwInstance = new jwPlayer(Config::get('app.jw_key'), Config::get('app.jw_secret'));
//            $creationResponse = $jwInstance->call('/videos/create', array());
//            if ($creationResponse['status'] === 'error' ) {
//                throw new Exception("Error on video creation: ".$creationResponse['title']." - ". $creationResponse['message']);
//            }
//            $uploadUrl = $jwInstance->getUploadUri($creationResponse);
//        } catch (Exception $ex) {
//            Log::error($ex->getMessage());
//            Log::error('Creation keys fail');
//            throw $ex;
//        }
//        
//        try {
//            $uploadResponse = $jwInstance->upload($uploadUrl, $temporal_file, 'php');
//        } catch (Exception $ex) {
//            Log::error($ex->getMessage());
//            throw $ex;
//        }
//        
//        DB::beginTransaction();
//      
//        $mediafile->metadata = json_encode($uploadResponse);
//        $mediafile->external_url = $jwInstance->getEmbedCode($uploadResponse['media']['key']);
//        $mediafile->key = $uploadResponse['media']['key'];
//        $mediafile->external_storage = false;
//        $mediafile->copied_to_amazon = false;
//        $mediafile->published = true;
//        
//        if ($mediafile->save()) {
//            $property_has_mediafiles = new PropertyHasMediaFile();
//            $property_has_mediafiles->mediafile_id = $mediafile->id;
//            $property_has_mediafiles->property_id = $this->property_id;
//            $property_has_mediafiles->position = $property_has_mediafiles->getPositionToAdd($property_id);
//             
//            if (!$property_has_mediafiles->save()) {
//                DB::rollback();
//                return false;
//            } 
//            
//            DB::commit();
//            return true;
//        }else{
//             DB::rollback();
//             return false;
//        }
    }
}
