<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class JabRoles extends Model
{
    use Notifiable;

    protected $table = "tbl_roles";

    protected $primaryKey = 'RoleId';

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}