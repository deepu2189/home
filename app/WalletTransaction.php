<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use App\User;

class WalletTransaction extends Model
{
    public function __construct(array $attributes = array())
    {
        $this->typeList=[
            self::$placeOrderCommisionType => trans('walletTransaction.order_placed_commision'),
            self::$orderPlacedType => trans('walletTransaction.order_placed'),
            self::$creditConsumtion => trans('walletTransaction.credit_comsumption'),
            self::$creditAdding => trans('walletTransaction.credit_adding'),
            self::$stripeTransactionType => trans('walletTransaction.stripe_transaction'),
            self::$referralSignupCommissionType => trans('walletTransaction.referral_signup_commision')
        ];
        parent::__construct($attributes);
    }


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wallet_transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
        'amount',
        'description',
        'date'
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    
    public $typeList = [
        //We initialize this variable in the constructor
        //To do the Translations
    ];


    public static $typeList2 = [
        //We initialize this variable in the constructor
        //To do the Translations
        "credit_adding" => "Credit Added",
    ];

    /**
     * Transaction type when the commission for an order placed by a referral user is placed
     * @var string
     */
    public static $placeOrderCommisionType = 'order_placed_commision';
    
    /**
     * Transaction type when order placed is registered by the user
     * @var type
     */
    public static $orderPlacedType = 'order_placed';
    
    /**
     * Transaction type when the order placed consumes credit from the user
     * @var string
     */
    public static $creditConsumtion = 'credit_comsumption';
    
    /**
     * Transaction type when the order placed consumes credit from the user
     * @var string
     */
    public static $creditAdding = 'credit_adding';
    
    /**
     * Transaction type when a stripe account element is used
     * @var string
     */
    public static $stripeTransactionType = 'stripe_transaction';
    
    /**
     * Transaction type for register a refferral sigunup commision
     * @var string
     */
    public static $referralSignupCommissionType = 'referral_signup_commision';

    /**
     * The Agent belongs to a User
     *
     * @return type
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.date_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
    
    /**
     * Updates the entries for a user referral element
     *
     * @param integer $user_id
     * @param float $amount
     * @return boolean
     */
    public static function addCreditForUserSignUpReferral($user_id, $amount)
    {
        
        $walletEntry = new WalletTransaction();
        $walletEntry->user_id = $user_id;
        $walletEntry->type = self::$referralSignupCommissionType;
        $walletEntry->amount = $amount;
        $walletEntry->description = "Credit granted for new referral signup";
        $walletEntry->date = Carbon::now();
        
        if ($walletEntry->save()) {
            // Update the user amount
            return User::addToWalletAmount($user_id, $amount);
        }
        return false;
    }
    
    /**
     * Updates the entries for a user referral element
     *
     * @param integer $user_id
     * @param float $amount
     * @return boolean
     */
    public static function addCreditForOrderPlacing($user_id, $amount, $origin_user_id, $order_id)
    {
        
        $walletEntry = new WalletTransaction();
        $walletEntry->user_id = $user_id;
        $walletEntry->type = self::$placeOrderCommisionType;
        $walletEntry->amount = $amount;
        $walletEntry->date = Carbon::now();
        
        $walletEntry->description = "Credit granted on order #".$order_id;
        $walletEntry->description .= " - " . \App\Order::find($order_id)->Package->name." from ";
        $walletEntry->description .= \App\User::getUser($origin_user_id)->fullname;
        
        if ($walletEntry->save()) {
            // Update the user amount
            return User::addToWalletAmount($user_id, $amount);
        }
        return false;
    }


     /**
     * Add credits
     *
     * @param array $data
     * @return boolean
     */
    public static function modifyCreditOfUser($data)
    {
        
        $walletEntry = new WalletTransaction();
        $walletEntry->user_id = $data['id'];
        $walletEntry->description = $data['description'];
        $walletEntry->date = $data['date'];
        $walletEntry->type = $data['type'];
        $walletEntry->amount = $data['amount'];

        if ($walletEntry->save()) {
            // Update
            if ($data['type'] === WalletTransaction::$creditAdding) {
                return User::addToWalletAmount($data['id'], $data['amount']);
            }
            if ($data['type'] === WalletTransaction::$creditConsumtion) {
                return User::removeToWalletAmount($data['id'], $data['amount']);
            }
        };

        return false;
    }
    
    /**
     *
     * @param array $items
     * @param integer $user_id
     * @returns boolean
     */
    public static function createFromOrderItems($items, $user_id, $order_id)
    {
        foreach ($items as $item) {
            if ($item['type'] === \App\OrderItem::$walletDiscountType) {
                $walletEntry = new WalletTransaction();
                $walletEntry->user_id = $user_id;
                $walletEntry->amount = $item['amount'];
                $walletEntry->description = $item['description'];
                $walletEntry->date = Carbon::now();
                $walletEntry->type = self::$creditConsumtion;

                if (!User::removeToWalletAmount($user_id, $item['amount'])) {
                    \Codeception\Util\Debug::debug("Could not update the wallet amount 3");
                    return false;
                }

                $walletEntry->amount *= -1.0;
                $walletEntry->description = "Use of wallet credit on order #" . $order_id;

                if (!$walletEntry->save()) {
                    return false;
                }
            }
        }
        return true;
    }
    

    /**
     * Checks the user credit and compraes with the amount given
     * @param double $amount
     * @param integer $user_id
     * @return boolean
     */
    public static function canPlaceOrderWithWalletCredit($amount, $user_id)
    {
        $user = User::findOrFail($user_id);
        if (floatval($user->current_wallet_amount) >= floatval($amount)) {
            return false;
        }
        return true;
    }
    
    /**
     * Checks that the user has enough credit to reduce
     *
     * @param integer $user_id
     * @param float $amount
     * @return boolean
     */
    public static function checkCreditForOperation($user_id, $amount, $type)
    {

        if ($type === WalletTransaction::$creditConsumtion) {
            if (User::getCurrentWalletStatus($user_id) < $amount) {
                return false;
            }
        }
        return true;
    }
}
