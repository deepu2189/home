<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Rutorika\Sortable\BelongsToSortedManyTrait;
use Log;
use Event;
use App\Events\PropertyCreated;
use App\Events\PropertyUpdated;


use Illuminate\Support\Facades\Hash;

class JabProperties extends Model
{
	use BelongsToSortedManyTrait;
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $table = "properties";

	public $timestamps = false;

	/**
	 * Attributes to auto-transform
	 * @var array
	 */
	protected $casts
		= ['agent_notified' => 'boolean', 'media_created_notification' => 'boolean', 'published' => 'boolean',];

	/**
	 * Fields that can be filled in a mass assignation
	 *
	 * @var array
	 */
	protected $fillable
		= ['title', 'type', 'address', 'slug', 'zip', 'city', 'state', 'beds', 'baths', 'half_baths', 'price', 'area', 'owner_phone', 'owner_email', 'published', 'views_amount', 'map_longitude', 'map_latitude', 'description', 'preview_id', 'agent_id', 'seller_id', 'order_id'];

	/**
	 * Validations rules on update actions
	 *
	 * @var type
	 */
	public static $rulesOnUpdate
		= ['preview_id' => 'sometimes|exists:mediafiles,id,deleted_at,NULL'];

	/**
	 * fields for update
	 *
	 * @var type
	 */
	public static $fillableOnUpdate
		= ['title', 'type', 'address', 'slug', 'zip', 'city', 'state', 'beds', 'baths', 'half_baths', 'price', 'area', 'owner_phone', 'owner_email', 'published', 'views_amount', 'map_longitude', 'map_latitude', 'description', 'preview_id', 'redirect_url_mls', 'redirect_url_normal'];

	public function getAllPropertiesData(){
		return $this->newQuery()->where(function($q)
		{
			$q->whereraw('process_date IS NULL')
			  ->orwhereDate('process_date','<',date('Y-m-d',strtotime("-2 days")));
		})->where(function($q1)
		{
			$q1->whereraw('last_update_date IS NULL')
			   ->orwhereDate('last_update_date','<',date('Y-m-d',strtotime("-5 days")));
		})
//		  ->where('id','=','23303')
		                    ->limit('10')->get();
	}
}