<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Rutorika\Sortable\BelongsToSortedManyTrait;
use Log;
use Event;
use App\Events\PropertyCreated;
use App\Events\PropertyUpdated;


use Illuminate\Support\Facades\Hash;

class Property extends Model
{
    use BelongsToSortedManyTrait;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Attributes to auto-transform
     * @var array
     */
    protected $casts = [
        'agent_notified' => 'boolean',
        'media_created_notification' => 'boolean',
        'published' => 'boolean',
    ];

    /**
     * Fields that can be filled in a mass assignation
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'address',
        'slug',
        'zip',
        'city',
        'state',
        'beds',
        'baths',
        'half_baths',
        'price',
        'area',
        'owner_phone',
        'owner_email',
        'published',
        'views_amount',
        'map_longitude',
        'map_latitude',
        'description',
        'preview_id',
        'agent_id',
        'seller_id',
        'order_id'
    ];

    /**
     * Validations rules on update actions
     *
     * @var type
     */
    public static $rulesOnUpdate = [
        'preview_id' => 'sometimes|exists:mediafiles,id,deleted_at,NULL'
    ];

    /**
     * fields for update
     *
     * @var type
     */
    public static $fillableOnUpdate = [
        'title',
        'type',
        'address',
        'slug',
        'zip',
        'city',
        'state',
        'beds',
        'baths',
        'half_baths',
        'price',
        'area',
        'owner_phone',
        'owner_email',
        'published',
        'views_amount',
        'map_longitude',
        'map_latitude',
        'description',
        'preview_id',
        'redirect_url_mls',
        'redirect_url_normal'
    ];

     /**
     * Relation with agent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
    
    /**
     * Relation with seller
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seller()
    {
        return $this->belongsTo('App\Seller');
    }

    /**
     * Relation with the order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * Relation with mediafiles
     *
     * @return \Rutorika\Sortable\BelongsToSortedMany
     */
    public function mediafiles()
    {
        return $this->belongsToSortedMany(
            'App\Mediafile',
            'position',
            'property_has_mediafiles',
            'property_id',
            'mediafile_id'
        );
    }

    /**
     * Relation with mediafiles
     *
     * @return \Rutorika\Sortable\BelongsToSortedMany
     */
    public function mediafileswithTrashed()
    {
        return $this->belongsToSortedMany(
            'App\Mediafile',
            'position',
            'property_has_mediafiles',
            'property_id',
            'mediafile_id'
        )->withTrashed();
    }


    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
    
    /**
     * Mutator for create slug url
     *
     * @param string $title
     * @return string
     */
    public function setSlugAttribute()
    {
        if ($this->attributes['address']) {
            $slug = str_slug($this->attributes['address'], "-");
            
            $count = Property::where('slug', $slug)->count();
            
            // check if property is updated or created
            if (array_key_exists('id', $this->attributes)) {
                // check if address have been changed
                if ($slug !== $this->attributes['slug']) {
                    // check if address already exists
                    if ($count > 0) {
                        $slug = $slug."-".$this->attributes['id'];
                    }
                    
                    $this->attributes['slug'] = $slug;
                }
            } else {
                // new property
                
                // address already exists
                if ($count > 0) {
                    $slug = $slug."-".Hash::make($slug);
                }
                
                $this->attributes['slug'] = $slug;
            }
        } else {
            // property with no address
            
            //updated
            if (array_key_exists('id', $this->attributes)) {
                $slug = $this->attributes['id'];
                $this->attributes['slug'] = $slug;
            } else {
                // new
                $slug = str_slug($this->attributes['title'], "-");
                
                $count = Property::where('slug', $slug)->count();
                
                if ($count > 0) {
                    $slug = $slug."-".Hash::make($slug);
                }
                    
                $this->attributes['slug'] = $slug;
            }
        }
    }

    /**
     * Creates a proeprty based on the information obtainged by the job
     *
     * @param integer $order_id
     * @return \App\Property
     */
    public static function createFromOrder($data, $order_id)
    {
        
        if (!array_key_exists('map_longitude', $data['property'])) {
            $data['property']['map_longitude']=null;
        }
        if (!array_key_exists('map_latitude', $data['property'])) {
            $data['property']['map_latitude']=null;
        }
        if (!array_key_exists('locality', $data['property']) || is_null($data['property']['locality'])) {
            $data['property']['locality']='';
        }
        if (!array_key_exists('administrative_area_level_1', $data['property'])) {
            $data['property']['administrative_area_level_1']='';
        }
        if (!array_key_exists('type', $data['property'])) {
            $data['property']['type'] = null;
        }

        
        if ($data['agent_id']) {
            $data['agent_id'] = (int) $data['agent_id'];
        } else {
            $data['agent_id'] = null;
        }
        if (array_key_exists('seller_id', $data)) {
            $data['seller_id'] = $data['seller_id'];
        } else {
            $data['seller_id'] = null;
        }
        
        $data = [
            'order_id'       => (int)$order_id,
            'agent_id'       => $data['agent_id'],
            'seller_id'      => $data['seller_id'],
            'address'        => $data['job']['property_address'],
            'owner_email'    => $data['job']['contact_owner_email'],
            'owner_phone'    => $data['job']['contact_owner_phone'],
            'title'          => $data['job']['property_address'],
            'published'      => false,
            'zip'            => $data['property']['postal_code'],
            'city'           => $data['property']['locality'],
            'state'          => $data['property']['administrative_area_level_1'],
            'map_latitude'   => $data['property']['map_latitude'],
            'map_longitude'  => $data['property']['map_longitude'],
            'type'           => $data['property']['type'],
            'agent_notified' => false,
            'media_created_notification' => false
        ];

        $count = 0;
        if ($data['address']) {
            $count = Property::where('slug', str_slug($data['address']))->count();
        }
        
        // if the address already exists, create with temp slug and then update with new id
        if ($count > 0) {
            $data['slug'] = Hash::make($data['address']);
        } else {
            $data['slug'] = $data['address'];
        }
        
        $property = Property::create($data);
        if ($property !== false) {
            if ($count > 0) {
                DB::beginTransaction();
                
                // update created property slug
                $property->slug = $property->address;
                
                if (!$property->save()) {
                    DB::rollback();
                    return false;
                }
                
                DB::commit();
            }
            
           // Event::fire(new PropertyCreated($property->id));
            return true;
        }
        return false;
    }



    /**
     * Update property based on $data received as parametter
     *
     * @param type $data
     * @return boolean
     */
    public function updateProperty($data)
    {
        DB::beginTransaction();

        $this->fill($data);
        
        if ($this->save()) {
            Event::fire(new PropertyUpdated($this->id));
            DB::commit();
            return true;
        }

        DB::rollback();
        return false;
    }

    /**
     * Check if a property exists based on the order ID
     *
     * @param integer $order_id Order identifier
     * @return boolean
     */
    public static function existsByOrderId($order_id)
    {
        $amount = self::where('order_id', $order_id)->count();

        if ($amount === 0) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Mark property as published
     *
     * @param integer $property_id Property identifier
     * @return boolean
     */
    public static function publishProperty($property_id)
    {
        $property = self::find($property_id);
        
        if ($property) {
            DB::beginTransaction();
            $property->published = true;
            
            if (!$property->save()) {
                    DB::rollback();
                    return false;
            }
                
                DB::commit();
                return true;
        }
        
        return false;
    }


    /**
     * Mark property as unpublished
     *
     * @return boolean
    */
    public function unpublishProperty()
    {
        $this->published = false;
        if ($this->save()) {
            return true;
        }

        return false;
    }

    /**
     * get property data
     *
     */
    public static function getProperty($id)
    {
        return self::find($id);
    }
    
    /**
     * get property data by order_id
     *
     */
    public static function getPropertyByOrderId($order_id)
    {
        return self::query()->where('order_id', $order_id)->value('id');
    }
    
    /**
     * migrate order
     *
     */
    public static function migrateAgentSeller($id, $array_seller_agent)
    {
        $property = self::find($id);
        $property->seller_id = $array_seller_agent['seller_id'];
        $property->agent_id = $array_seller_agent['agent_id'];
        if ($property->save()) {
            return true;
        }
        return false;
    }
    
    /**
     * Returns the query to run to get a property by it's slug
     *
     * @param string $slug
     * @return QueryObject
     */
    public static function getBySlugQuery($slug)
    {
        return self::where('slug', $slug)->orWhere('redirect_url_normal', $slug);
    }


    /**
     * Find property by slug in properties and PageMetaData and return id
     *
     * @param string $slug
     * @return id
     */
    public static function getPropertyBySlug($slug)
    {
        $property = Property::getBySlugQuery($slug)->first();
        $id = null;
        if ($property) {
            $id = $property->id;
        } else {
            // search on metadata table to see if it is an old url
            $property = PageMetadata::searchPropertyOldUrlBySlug($slug);
            if ($property) {
                $id = $property->id;
            }
        }
        return $id;
    }


    /**
    */
    public function resetFlagToSendEmail()
    {
        
        $this->agent_notified = false;
        $this->media_created_notification = false;
        if ($this->save()) {
            return true;
        }
        return false;
    }
}
