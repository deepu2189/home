<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

/**
 * Class to handle the job
 *
 * @author Deepak Thakur
 */

class JobHasMediaFile extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'jobs_has_mediafiles';
    
    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
  
    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'job_id',
        'mediafile_id',
    ];

  
    /**
     * Relation with Order
     *
     * @return type
     */
    public function job()
    {
        return $this->hasMany('App\Alljob', 'job_id');
    }
    
    
    /**
     * Relation with mediafiles
     *
     * @return type
     */
    public function mediafiles()
    {
        return $this->hasMany('App\Mediafile', 'mediafile_id');
    }
}
