<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PackageHasAssetType extends Model
{
     /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'package_has_asset_types';

    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'asset_type_id',
        'package_id',
        'position',
        'amount_originals_files',
        'amount_edited_files'
    ];
    
        /**
     * Validation rules for update
     * @var array
     */
    public static $rulesOnUpdate = [
        'asset_type_id',
        'package_id',
        'position',
        'amount_originals_files',
        'amount_edited_files',
        'relation_n_to_one',
    ];
    
    /**
     *
     * @var type
     */
    public static $fillableOnUpdate = [
        'asset_type_id',
        'package_id',
        'position',
        'amount_originals_files',
        'amount_edited_files'
    ];
    /**
    * Field to group by when ordered
    *
    * @var type
    */
    protected static $sortableGroupField = 'package_id';


    /**
     * Relation with assets
     *
     * @return type
     */
    public function asset()
    {
        return $this->belongsTo('App\AssetType', 'asset_type_id');
    }


    /**
     * Relation with package
     *
     * @return type
     */
    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }
        
    
    /**
     * Accesor for created at date
     *
     * @param type $date
     * @return type
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param type $date
     * @return type
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     *
     * @param type $asset_type_id
     * @return type
     */
    public static function getPositionToAdd($asset_type_id)
    {
        return self::where('asset_type_id', $asset_type_id)->get(['position'])->max('position') + 1;
    }
    
    /**
     *
     * @param type $package_id
     * @param type $asset_type_id
     * @return type
     */
    public static function getOriginalFilesAmount($package_id, $asset_type_id)
    {
        $amount = self::where('package_id', $package_id)->where('asset_type_id', $asset_type_id)->get(['amount_originals_files'])->first();

        return $amount->amount_originals_files;
    }
    
        /**
     *
     * @param type $package_id
     * @param type $asset_type_id
     * @return type
     */
    public static function getEditedFilesAmount($package_id, $asset_type_id)
    {

        
        $amount = self::where('package_id', $package_id)->where('asset_type_id', $asset_type_id)->get(['amount_edited_files'])->first();
        return $amount->amount_edited_files;
    }
}
