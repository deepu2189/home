<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class AutoMlsAdmin extends Authenticatable
{
	use Notifiable;

	/*const CREATED_AT = 'CreatedOn';
	const UPDATED_AT = 'UpdatedOn';*/

	protected $table = "mls_sent_prop_notification_to_agent";

	protected $primaryKey = 'ID';
	public $timestamps = false;

	public function DeleteListingDataIfPhoneNumberBlock($config)
	{
		global $db,$config;

		$result = DB::select(DB::raw("DELETE FROM mls_trestle_listing_master WHERE ListAgentMlsId IN
                (SELECT LA.ListAgentMlsId FROM mls_trestle_listing_agent LA WHERE
                (REPLACE(REPLACE(REPLACE(REPLACE(LA.`ListAgentDirectPhone`,')',''),'(',''),'-',''),' ','') IN ( ".rtrim(preg_replace('/[^0-9,]/', '',str_replace(PHP_EOL,',',str_replace('+1','',$config['blocked_phone_number']))),',').")))
                AND is_notification_send = 'No'"));
	}
	public function getTotalListingDataByDate($date)
	{
		$f_select= '';
		if($date != '')
		{
			$arrdate = explode(',',$date);
			foreach($arrdate as $key=>$val)
			{
				$date=date_create($val);
				$format_date = date_format($date,"Y-m-d");
				$f_select .= DB::raw("COUNT(CASE WHEN date(TLM.EntryDateTime) = '".$format_date."' THEN 1 END) AS total_listing".($key+1).",
							COUNT(CASE WHEN TLA.ListAgentDirectPhone != '' AND date(TLM.EntryDateTime) = '".$format_date."' THEN 1 END) As total_sms_day".($key+1).",
							COUNT(CASE WHEN TLA.ListAgentEmail != '' AND date(TLM.EntryDateTime) = '".$format_date."' THEN 1 END) As total_infusionsoft_day".($key+1).",
							COUNT(CASE WHEN TLA.ListAgentDirectPhone != '' AND date(TLM.EntryDateTime) = '".$format_date."' AND TLM.is_notification_send = 'No' THEN 1 END) As total_pending_sms_day".($key+1).",
							COUNT(CASE WHEN TLA.ListAgentEmail != '' AND date(TLM.EntryDateTime) = '".$format_date."' AND TLM.is_notification_send = 'No' THEN 1 END) As total_pending_infusionsoft_day".($key+1).",");
			}
			$result = DB::select(DB::raw("SELECT ".rtrim($f_select,',')." FROM mls_trestle_listing_master TLM
				LEFT JOIN mls_trestle_listing_agent TLA ON TLA.ListAgentMlsId = TLM.ListAgentMlsId"));

			return (array) $result[0];
		}

	}
	public function getTotalSentSMSAndEmailCountByDate($date)
	{
		$f_select = '';
		if($date != '')
		{
			$arrdate = explode(',',$date);
			foreach($arrdate as $key=>$val)
			{
				$date=date_create($val);
				$format_date = date_format($date,"Y-m-d");
				$f_select .= DB::raw("COUNT(CASE WHEN is_sms_send = 'Yes' AND date(SentDateTime) = '".$format_date."' THEN 1 END) AS total_sms_sent_day".($key+1).",
							COUNT(CASE WHEN InfusionSoftId != '0' AND date(SentDateTime) = '".$format_date."' THEN 1 END)AS total_infusionsoft_sent_day".($key+1).",");
			}
		}
		$result = DB::select(DB::raw("SELECT ".rtrim($f_select,',')." FROM ".$this->table));

		return (array) $result[0];
	}
}
?>