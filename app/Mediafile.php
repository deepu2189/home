<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\jwPlayerIntegration\jwPlayer;
use Intervention\Image\Facades\Image;
use App\HomePhoto;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Jobs\DeleteMediafilesById;
use Log;


class Mediafile extends Model
{
     /**
     * Used table
     *
     * @var string
     */
    protected $table = 'mediafiles';
    
    /**
     * include soft delete
     */
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'original_name',
        'key',
        'extension',
        'external_storage',
        'external_url',
        'md5',
        'published',
        'metadata',
        'origin',
        'photo_type',
        'copied_to_amazon',
        'storage'
    ];

    /**
     * Validation rules for creation
     * Possible values are:
     *  - p => Photographer has added this file.
     *  - e => EWarp integration has added this file.
     *  - m => This file was added manually by the SU
     * See static members $origin*
     * @var array
     */
    public static $rulesOnStore = [
        'origin' => 'required|in:p,e,m'
    ];

    /**
     * Validation rules for update
     *
     * @var array
     */
    public static $rulesOnUpdate = [
    ];

            /**
     * Fields that area allowed to be filled at update
     * @var array
     */
    public static $fillableOnUpdateAdmin = [
        'type',
        'original_name',
        'key',
        'extension',
        'external_storage',
        'external_url',
        'published'
    ];

    /**
     * Validation rules for update
     *
     * @var array
     */
    public static $fillableOnUpdate = [
        'type',
        'original_name',
        'key',
        'extension',
        'external_storage',
        'external_url',
    ];


    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Predefined image type for storage in database
     *
     * @var string
     */
    public static $imageType = 'pictures';

    /**
     * Predefined view type for storage on database
     *
     * @var string
     */
    public static $videoType = 'videos';
    /**
     * Predefined view type for storage on database
     *
     * @var string
     */
    public static $profileType = 'profile';
    /**
     * Predefined view type for storage on database
     *
     * @var string
     */
    public static $homePhotosType = 'homePhotos';

    /**
     * Predefined view type for storage on database
     *
     * @var string
     */
    public static $homeVideoType = 'homeVideo';

    /**
     * Predefined view type for storage on database
     *
     * @var string
     */
    public static $videoDownloadableType = 'videos_downloadable';

    /**
     * Predefined view type for storage on database
     *
     * @var string
     */
    public static $TreeDTourType = '3Dtour';

    /**
     * Predefined embed code type for storage on database
     *
     * @var string
     */
    public static $embedType = 'embed';

    /**
     * Predefined extraFiles type for storage in database
     *
     * @var string
     */
    public static $extraFilesType = 'extraFiles';

    /**
     * Defines the origin as Uploaded by photographer
     * @var string
     */
    public static $originPhotographer = 'p';

    /**
     * Defines the origin as eWarp copy processed
     * @var string
     */
    public static $originEWarp = 'e';

    /**
     * Defines the origin as Manual Upload
     * @var string
     */
    public static $originManual = 'm';

    /**
     * Defines the origin as picture resized (online)
     * @var string
     */
    public static $originPictureResized = 'r';

    /**
     * Defines the value for the Category Online
     * @var string
     */
    public static $photoTypeOnline = 'online';

    /**
     * Defnes the value for the Category Print
     * @var string
     */
    public static $photoTypePrint = 'print';

    /**
     * Defnes the value for the Category Both
     * @var string
     */
    public static $photoTypeBoth = 'both';

    /**
     * Defnes the value for the Category Both
     * @var string
     */
    public static $categoryExtraFiles = 'extra files';

    /**
     * The ImageFile belongs to a Photographer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photographer()
    {
        return $this->belongsTo('App\Photographer');
    }

    /**
     * The Mediafile is related to many mediafiles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jobs()
    {
        return $this->belongsToMany('App\AllJob', 'jobs_has_mediafiles', 'mediafile_id', 'job_id');
    }

    /**
     * The Mediafile is related to many properties
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function property()
    {
        return $this->belongsToMany('App\Property', 'property_has_mediafiles', 'mediafile_id');
    }

    /**
     * Relation with property_has_mediafile
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function propertyHasMediaFile()
    {
        return $this->hasOne('App\PropertyHasMediaFile');
    }
    /**
     * Relation with property_has_mediafile
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function homePhoto()
    {
        return $this->hasMany('App\HomePhoto');
    }

        /**
     * Relation with order_has_extra_file
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderHasExtraFile()
    {
        return $this->hasOne('App\OrderHasExtraFile');
    }

    /**
     * Accesor for created at date
     *
     * @param type $date
     * @return type
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param type $date
     * @return type
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accessor for getExternalUrl
     */
    public function getExternalUrlAttribute($url)
    {
        return self::convertAWSUrlToNiceUrl($url);
    }

    /**
     *
     */
    public static function convertAWSUrlToNiceUrl($url)
    {
        $url = str_replace(
            Config::get('aws.credentials.zone').'/'.Config::get('aws.credentials.bucket'),
            Config::get('aws.credentials.bucket'),
            $url
        );
        return $url;
    }

    /**
     * Mark all the mediafiles as copied to eWarp by it's batch number
     *
     * @param long $batch_number
     * @return boolean
     */
    public static function markAsUploadedByBatchNumber($batch_number)
    {
        $affectedRows = self::where('batch_id', '=', $batch_number)->update(['copied_to_ewarp' => true]);
        return ($affectedRows > 0);
    }

    


    /**
     * delete a mediafile from amazon s3
     * @param type $id
     */
    public static function deleteMediafileFromAmazonS3($id)
    {
        $mediafile = self::find($id);
        if ($mediafile) {
            // Move this to a background job for easy processing
            $background_job = new DeleteMediafilesById(intval($mediafile->id));
            $background_job->onQueue(Config::get('aws.sqs.amazon'));
            $this->dispatch($background_job);
        } else {
            return false;
        }
    }


    /**
     * Download the image from amazon s3, generate a thumbnail, upload it to amazon
     * and update the mediafile metadata
     *
     * @param \HomeJab\Models\Mediafile $mediaFile
     */
    public static function generateMediaFileThumbnailFromAmazon(Mediafile $mediaFile, $property_id = 0, $homePicture = false)
    {
        $client = App::make('aws')->createClient(
            's3',
            [ 'region' => Config::get('aws.credentials.region'),]
        );
        $basePath       = public_path('temp' . DIRECTORY_SEPARATOR);
        $path           = $basePath . $mediaFile->original_name;
        $thumbnailPath  = $basePath . 'thumbnail_' . $mediaFile->original_name;
        $previewPath    = $basePath . 'preview_' . $mediaFile->original_name;
        $facebookThumbnailPath = $basePath . 'facebookThumbnail_' . $mediaFile->original_name;
        $twitterThumbnailPath = $basePath . 'twitterThumbnail_' . $mediaFile->original_name;

        if (!file_exists($basePath)) {
            mkdir($basePath, 0777, true);
        }
        try {
            $result = $client->getObject(array(
                'Bucket' => Config::get('aws.credentials.bucket'),
                'Key'    => $mediaFile->key,
                'SaveAs' => $path
            ));
        } catch (\Aws\S3\Exception\S3Exception $e) {
            Log::error($e->getMessage());
            return false;
        }

        if (file_exists($path)) {
            try {
                $image1 = Image::make($path);
                $image1->resize(240, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image1->save($thumbnailPath);

                $image2 = Image::make($path);
                $image2->resize(900, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $image2->save($previewPath);

                //Thumbnail for facebook
                $image3 = Image::make($path);
                list($width, $height) = getimagesize($path);
                if ($width > 600 && $height > 315) {
                    $image3->resize(600, 315);
                }
                $image3->save($facebookThumbnailPath);

                //Thumbnail for twitter
                $image4 = Image::make($path);
                list($width, $height) = getimagesize($path);
                if ($width > 600 && $height > 300) {
                    $image4->resize(600, 300);
                }
                $image4->save($twitterThumbnailPath);
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                throw new \Exception("Could not load preview or thumbnail image. Check error log." . $e->getMessage());
            }

            if ($homePicture) {
                $key_base = self::getNewHomePhotoAWSKey(false);
            } else {
                $key_base = self::getNewFinalAWSKey($property_id, self::$imageType, $mediaFile->photo_type);
            }

             $parts = explode('/', $key_base);
            // The last one will be the filename
             $hash = array_pop($parts);
             $key_baseNew=str_replace($hash, '', $key_base);


            $result = $client->putObject(array(
                'Bucket'        => Config::get('aws.credentials.bucket'),
                'Key'           => $key_baseNew. 'thumbnail' . DIRECTORY_SEPARATOR . $hash . '-' . $mediaFile->original_name,
                'SourceFile'    => $thumbnailPath,
                'ACL'           => 'public-read',
                'CacheControl'  => 'max-age='.(1*365*24*60*60)   // One year
            ));

            $metaData = json_decode($mediaFile->metadata, true);

            list($width, $height) = getimagesize($thumbnailPath);
            if (is_numeric($width) && is_numeric($height)) {
                $metaData['resolutions'][] = ["url" => $result->get('ObjectURL'),
                    "md5" => "",
                    "actualWidth" => $width,
                    "actualHeight" => $height,
                    "resizeRule" => '',
                    "format" => "",
                    "width" => $width,
                    "height" => $height];
                $metaData['thumbnail'] = $result->get('ObjectURL');
            }



            $result = $client->putObject(array(
                'Bucket'        => Config::get('aws.credentials.bucket'),
                'Key'           => $key_baseNew. 'preview' . DIRECTORY_SEPARATOR . $hash . '-' . $mediaFile->original_name,
                'SourceFile'    => $previewPath,
                'ACL'           => 'public-read',
                'CacheControl'  => 'max-age='.(1*365*24*60*60)   // One year
            ));


            list($width, $height) = getimagesize($previewPath);
            if (is_numeric($width) && is_numeric($height)) {
                $metaData['resolutions'][] = ["url" => $result->get('ObjectURL'),
                    "md5" => "",
                    "actualWidth" => $width,
                    "actualHeight" => $height,
                    "resizeRule" => "",
                    "format" => "",
                    "width" => $width,
                    "height" => $height];
                $metaData['preview'] = $result->get('ObjectURL');
            }

            //Facebook thumbnail
             $result = $client->putObject(array(
                'Bucket'        => Config::get('aws.credentials.bucket'),
                'Key'           => $key_baseNew. 'facebookThumbnail' . DIRECTORY_SEPARATOR . $hash . '-' . $mediaFile->original_name,
                'SourceFile'    => $facebookThumbnailPath,
                'ACL'           => 'public-read',
                'CacheControl'  => 'max-age='.(1*365*24*60*60)   // One year
             ));

            list($width, $height) = getimagesize($facebookThumbnailPath);

            $metaData['resolutions'][] = ["url" => $result->get('ObjectURL'),
                "md5"           => "",
                "actualWidth"   => $width,
                "actualHeight"  => $height,
                "resizeRule"    => '',
                "format"        => "",
                "width"         => $width,
                "height"        => $height];
            $metaData['facebookThumbnail'] = $result->get('ObjectURL');

             //Twitter thumbnail
             $result = $client->putObject(array(
                'Bucket'        => Config::get('aws.credentials.bucket'),
                'Key'           => $key_baseNew. 'twitterThumbnail' . DIRECTORY_SEPARATOR . $hash . '-' . $mediaFile->original_name,
                'SourceFile'    => $twitterThumbnailPath,
                'ACL'           => 'public-read',
                'CacheControl'  => 'max-age='.(1*365*24*60*60)   // One year
             ));

            list($width, $height) = getimagesize($twitterThumbnailPath);

            $metaData['resolutions'][] = ["url" => $result->get('ObjectURL'),
                "md5"           => "",
                "actualWidth"   => $width,
                "actualHeight"  => $height,
                "resizeRule"    => '',
                "format"        => "",
                "width"         => $width,
                "height"        => $height];
            $metaData['twitterThumbnail'] = $result->get('ObjectURL');


            list($width, $height) = getimagesize($path);

            if (is_numeric($width) && is_numeric($height)) {
                $metaData['resolutions'][] = ["url" => $mediaFile->external_url,
                    "md5" => "",
                    "actualWidth" => $width,
                    "actualHeight" => $height,
                    "resizeRule" => "",
                    "format" => "",
                    "width" => $width,
                    "height" => $height];
            }

            $mediaFile->metadata = json_encode($metaData);

            unlink($thumbnailPath);
            unlink($previewPath);
            unlink($facebookThumbnailPath);
            unlink($twitterThumbnailPath);
            unlink($path);

            if ($mediaFile->save()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Returns the key based on a job id for a new file in amazon
     *
     *
     * @param integer $job_id
     * @return string
     */
    public static function getNewOriginalAWSKey($job_id)
    {
        $hash = User::generateRandomHash(10);
        $transformer = new \App\Transformers\AWSPathTransformer();
        $url = $transformer->getJobOriginalsUrl($job_id) . $hash;
        return $url;
    }

    /**
     * Returns the key based on a property id and a assets type for a new file in amazon
     *
     *
     * @param integer $property_id
     * @return string
     */
    public static function getNewFinalAWSKey($property_id, $assetType, $photoType)
    {
        $hash = str_random(10);
        $transformer = new \App\Transformers\AWSPathTransformer();
        $url = $transformer->getPropertyUrl($property_id, $assetType, $photoType) . $hash;
        return $url;
    }
    /**
     * Returns the key based on a user id and a assets type for a new file in amazon
     *
     *
     * @param integer $user_id
     * @return string
     */
    public static function getNewUserProfileAWSKey($user_id)
    {
        $hash = User::generateRandomHash(10);
        $transformer = new \App\Transformers\AWSPathTransformer();
        $url = $transformer->getUserUrl($user_id) . $hash;
        return $url;
    }
    /**
     * Returns the key based on a user id and a assets type for a new file in amazon
     *
     *
     * @param integer $add_hash
     * @return string
     */
    public static function getNewHomePhotoAWSKey($add_hash = true)
    {
        $transformer = new \App\Transformers\AWSPathTransformer();
        $url = $transformer->getHomePhotosUrl();
        if ($add_hash) {
            $hash = User::generateRandomHash(10);
            $url .= $hash;
        }
        return $url;
    }

     /**
     * Returns the key based on a order id for a new order extra file in amazon
     *
     * @param integer $order_id
     * @return string
     */
    public static function getExtraFilesAWSKey($order_id)
    {
        $hash = User::generateRandomHash(10);
        $transformer = new \App\Transformers\AWSPathTransformer();
        $url = $transformer->getExtraFilesUrl($order_id) . $hash;
        return $url;
    }


    /**
     * Adds a new 3D tour
     *
     * @param integer $property_id
     * @param string $url
     * @return boolean
     */
    public static function add3DTourToProperty($property_id, $url)
    {
        DB::beginTransaction();

        $data = [
            'type' => self::$TreeDTourType,
            'external_url' => $url,
            'published' => true
        ];

        $mediafiles = Mediafile::create($data);
        if ($mediafiles != false) {
            $property_has_mediafiles = new PropertyHasMediaFile();
            $property_has_mediafiles->mediafile_id = $mediafiles->id;
            $property_has_mediafiles->property_id = $property_id;
            $property_has_mediafiles->position = $property_has_mediafiles->getPositionToAdd($property_id);

            if (!$property_has_mediafiles->save()) {
                DB::rollback();
                return false;
            }

            DB::commit();
            return $mediafiles;
        }

        DB::rollback();
        return false;
    }

    /**
     * Adds a new embed video
     *
     * @param integer $property_id
     * @param string $url
     * @return boolean
     */
    public static function addEmbedVideoToProperty($property_id, $html)
    {
        DB::beginTransaction();

        $json_html = json_encode($html);
        $data = [
            'type' => self::$embedType,
            'metadata' => $json_html,
            'published' => true
        ];

        $mediafiles = Mediafile::create($data);
        if ($mediafiles != false) {
            $property_has_mediafiles = new PropertyHasMediaFile();
            $property_has_mediafiles->mediafile_id = $mediafiles->id;
            $property_has_mediafiles->property_id = $property_id;
            $property_has_mediafiles->position = $property_has_mediafiles->getPositionToAdd($property_id);

            if (!$property_has_mediafiles->save()) {
                DB::rollback();
                return false;
            }

            DB::commit();
            return $mediafiles;
        }

        DB::rollback();
        return false;
    }

    /**
     * Checks if a mediafile exists with the same name and type
     *
     * @param Mediafile $mediafile
     * @return boolean
     */
    // public static function isAlreadyOnDatabase($filename, $type, $property_id, $photo_type)
    // {

    //     $property_ids = Property::where('id', '=', $property_id)->get(['*'])->first()->mediafiles()->getRelatedIds();

    //     if ($type === self::$imageType && $photo_type !== null) {
    //         $count = Mediafile::where([
    //                 'type'          => $type,
    //                 'original_name' => $filename,
    //                 'photo_type'    => $photo_type
    //             ])->whereIn('id', $property_ids)
    //               ->count();
    //     } else {
    //         $count = Mediafile::where([
    //                     'type' => $type,
    //                     'original_name' => $filename
    //         ])->whereIn('id', $property_ids)
    //           ->count();
    //     }

    //     if ($count === 0) {
    //         return false;
    //     }
    //     return true;
    // }

    /**
     * Checks if a mediafile exists with the same name and type
     *
     * @param Mediafile $mediafile
     * @return boolean
     */
    public static function isAlreadyOnDatabase($filename, $type, $property_id, $photo_type, $batch_id)
    {
       // dd('dsdsds');
    //    $property_ids = Property::where('id', '=', $property_id)->get(['*'])->first()->mediafiles()->withTrashed()->allRelatedIds();
        $property_ids = Property::where('id', '=', $property_id)->get(['*'])->first();
//        echo "<pre>";
//    echo 'properties-'.$property_ids->id;
//        echo "<pre>";

        if ($type === self::$imageType && $photo_type !== null) {
            $count = Mediafile::where([
                    'type'          => $type,
                    'original_name' => $filename,
                    'photo_type'    => $photo_type,
                    'batch_id'      => $batch_id,
                ])->whereIn('id', $property_ids)
                  ->count();
        } else {
            $count = Mediafile::where([
                        'type' => $type,
                        'original_name' => $filename,
                        'batch_id'      => $batch_id,
            ])->whereIn('id', $property_ids)
              ->count();
        }

        if ($count === 0) {
            return false;
        }
        return true;
    }


    /**
     * returns the action post data to upload a file to the jwPlayer server
     *
     */
    public static function getJwPlayerCreatePostAction($params = array())
    {
        $jwInstance = new jwPlayer(Config::get('app.jw_key'), Config::get('app.jw_secret'));
        return $jwInstance->call('/videos/create', $params);
    }

    /**
     * Set a mediafile with the home video src
     *
     * @param string $src
     */
    public static function setHomeVideoMediafile($src)
    {
        $video = Mediafile::where('type', '=', Mediafile::$homeVideoType)->first();

        // if it not exists, create it
        if (!$video) {
            $video                  = new Mediafile();
            $video->type            = Mediafile::$homeVideoType;
            $video->original_name   = 'home_page_video';
        }

        $video->external_url = $src;

        if ($video->save()) {
            return true;
        } else {
            return false;
        }
    }

    public static function getPreviewInfo($id)
    {
        
       return Mediafile::select('type')->where('id', $id)->get();
    }


    public static function add($data)
    {
        DB::beginTransaction();

        //pr($data,1);
        $mediaFile = new MediaFile();

        if (!array_key_exists('origin', $data)) {
            $data['origin'] = self::$originPhotographer;
        }

        if (array_key_exists('type', $data)) {
            if ($data['type'] == 'videos') {
                $data['metadata']['profiles'][] = [
                    "url" => $data['external_url'],
                    "md5" => $data['md5'],
                    "profile" => $data['original_name']
                ];

                $data['metadata'] = json_encode($data['metadata']);

            } else if ($data['type'] == 'pictures') {
                list($width, $height) = getimagesize($data['external_url']);

                $metaData['resolutions'][] = ["url" => $data['external_url'],
                    "md5"           => "",
                    "actualWidth"   => $width,
                    "actualHeight"  => $height,
                    "resizeRule"    => "",
                    "format"        => "",
                    "width"         => $width,
                    "height"        => $height];
                $metaData['preview']='/assets/images/DefaultThumbnail.png';
                $metaData['thumbnail']='/assets/images/DefaultThumbnail.png';
                $metaData['facebookThumbnail']='/assets/images/DefaultThumbnail.png';
                $metaData['twitterThumbnail']='/assets/images/DefaultThumbnail.png';
                $data['metadata'] =  json_encode($metaData);
            } else if ($data['type'] == self::$profileType) {
                $data['metadata']['profiles'][] = [
                    "url" => $data['external_url'],
                    "md5" => $data['md5'],
                    "profile" => $data['original_name']
                ];

                $data['metadata'] = json_encode($data['metadata']);
            }
        }

        try {
            $mediaFile->fill($data);
            if ($mediaFile->save()) {


                if (array_key_exists('job_id', $data) && intval($data['job_id']) > 0) {
                    $jobs_has_mediafiles = new JobHasMediaFile();
                    $jobs_has_mediafiles->mediafile_id = $mediaFile->id;
                    $jobs_has_mediafiles->job_id = $data['job_id'];

                    if (!$jobs_has_mediafiles->save()) {
                        DB::rollback();
                        Log::error("Job mediafile relation was not saved");
                        return false;
                    }
                } elseif (intval($data['property_id']) > 0) {
                    $property_has_mediafiles = new PropertyHasMediaFile();
                    $property_has_mediafiles->mediafile_id = $mediaFile->id;
                    $property_has_mediafiles->property_id = $data['property_id'];
                    $property_has_mediafiles->position = $property_has_mediafiles->getPositionToAdd($data['property_id']);

                    if (!$property_has_mediafiles->save()) {
                        DB::rollback();
                        Log::error("Property mediafile relation was not saved");
                        return false;
                    }
                    // Regenerate the zip file
                    $property = Property::find($data['property_id']);
                    $property->last_zip_online = null;
                    if (!$property->save()) {
                        DB::rollback();
                        Log::error("Could not reset the zip flag!");
                        return false;
                    }
                } else if (array_key_exists('order_id', $data) && intval($data['order_id']) > 0) {
                    $order_has_extrafile = new OrderHasExtraFile();
                    $order_has_extrafile->mediafile_id = $mediaFile->id;
                    $order_has_extrafile->order_id = $data['order_id'];

                    if (!$order_has_extrafile->save()) {
                        DB::rollback();
                        Log::error("Order extra mediafile relation was not saved");
                        return false;
                    }
                } else if ($data['type'] == self::$profileType) {
                } else if ($data['type'] == self::$homePhotosType) {
                } else {
                    DB::rollback();
                    Log::error("Mediafile relation was not detected was not saved");
                    return false;
                }


                if ($mediaFile->type === self::$homePhotosType) {
                    $homePhoto = new HomePhoto();
                    $homePhoto->mediafile_id = $mediaFile->id;
                    $homePhoto->position = $homePhoto->getPositionToAdd();
                    if (!$homePhoto->save()) {
                        DB::rollback();
                        Log::error("Job mediafile home photo relation was not saved");
                        return false;
                    }
                }
                DB::commit();
                return $mediaFile;
            }
            DB::rollback();
            Log::error("Mediafile was not saved!");
            return false;
        } catch (\Exception $e) {
            DB::rollback();
            Log::error("Exception: ".$e->getMessage());
            return false;
        }
    }

    
}
