<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class Message extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';

    protected $table = "tbl_message";

    protected $primaryKey = 'JobId';

    public static $rulesOnGetMessage = [
        'JobID'             =>  'required',
        'UserID'            =>  'required',
    ];
    public static $rulesOnGetNotification = [
        'UserID'            =>  'required'
    ];
    public static $rulesOnMarkAllRead = [
        'JobID'             =>  'required',
        'UserId'            =>  'required',
    ];
    public static $rulesOnSendMessage = [
        'JobID'             =>  'required',
        'SenderID'          =>  'required',
        'MessageText'       =>  'required',
        'SentTime'          =>  'required',
    ];
    public static function GetMessage($POST)
    {
        if(!is_array($POST))
            return false;

        $post['JobID']      = $POST['JobID'];
        $post['UserID']     = $POST['UserID'];
        $post['PageNumber'] = isset($POST['PageNumber'])?$POST['PageNumber']:1;
        $post['PageSize']   = isset($POST['PageSize'])?$POST['PageSize']:10;

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_GetMessageByJobID('.rtrim($params,','  ).')');

        return Message::hydrate($ret);

    }
    public static function GetNotificationList($POST)
    {
        if(!is_array($POST))
            return false;

        $post['UserID']     = $POST['UserID'];
        $post['PageNumber'] = isset($POST['PageNumber'])?$POST['PageNumber']:1;
        $post['PageSize']   = isset($POST['PageSize'])?$POST['PageSize']:10;

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_GetNotificationListByUser('.rtrim($params,','  ).')');

        return Message::hydrate($ret);

    }
    public static function GetNotificationCount($POST)
    {
        if(!is_array($POST))
            return false;

        $post['UserID']     = $POST['UserID'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_GetUnreadMessageCountByUserId('.rtrim($params,','  ).')');

        return $ret;

    }
    public static function MarkAllReadByJobIDUserID($POST)
    {
        if(!is_array($POST))
            return false;

        $post['JobID']      = $POST['JobID'];
        $post['UserID']     = $POST['UserId'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_MarkAllRead('.rtrim($params,','  ).')');

        return $ret;

    }
    public static function MarkReadByMessageId($POST)
    {
        if(!is_array($POST))
            return false;

        $post['MessageId']  = $POST['MessageId'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_MarkRead('.rtrim($params,','  ).')');

        return $ret;

    }
    public static function  SendMessage($POST)
    {
        if(!is_array($POST))
            return false;

        $post['JobID']      = $POST['JobID'];
        $post['SenderID']   = $POST['SenderID'];
        $post['MessageText']= $POST['MessageText'];
        $post['SentTime']   = date('Y-m-d H:i:s', strtotime(str_replace('-', '/', $POST['SentTime'])));

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        # Call store procedure
        $ret = DB::select('call SP_HomeJob_API_SendMessageByJobID('.rtrim($params,','  ).')');

        return $ret;
    }

}
?>