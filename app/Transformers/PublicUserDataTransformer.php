<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the object form a user into a representable object
 *
 * @author Nicolas Taglienti <nicolas@serfe.com>
 * @since 0.2
 */
class PublicUserDataTransformer extends Transformer
{

    /**
     * Transforms the object form a user into a representable object
     * Only returns the public data of the user
     *
     * @param Model $user
     * @return mixed
     * @since 0.2
     */
    public function transform(Model $user)
    {

        $publicUserDataTransform = [
            'id'                => (int) $user->id,
            'firstname'         => $user->firstname,
            'lastname'          => $user->lastname,
            'slug'              => $user->slug,
            'email'             => $user->email,
            'contact_phone'     => $user->contact_phone,
            'contact_text'      => $user->contact_text,
            'bio'               => $user->bio,
            'company_name'      => $user->company_name,
            'mortgage'          => $user->mortgage
        ];

        
         $image = $user->profileImage;
         if ($image) {
             $publicUserDataTransform['profile_image'] = [
                 'id'                => (int) $image->id,
                 'type'              => $image->type,
                 'original_name'     => $image->original_name,
                 'app_name'          => $image->app_name,
                 'extension'         => $image->extension,
                 'external_storage'  => (bool) $image->external_storage,
                 'external_url'      => $image->external_url,
                 'sizes'             => [
                     'original'      =>  $image->getFileUrl('original'),
                     'medium'        =>  $image->getFileUrl('medium'),
                     'small'         =>  $image->getFileUrl('small'),
                     'tiny'          =>  $image->getFileUrl('tiny'),
                 ],
                 'created_at'        => $image->created_at,
                 'updated_at'        => $image->updated_at
             ];
         }
         $image = $user->logoImage;
         if ($image) {
             $publicUserDataTransform['logo_image'] = [
                 'id'                => (int) $image->id,
                 'type'              => $image->type,
                 'original_name'     => $image->original_name,
                 'app_name'          => $image->app_name,
                 'extension'         => $image->extension,
                 'external_storage'  => (bool) $image->external_storage,
                 'external_url'      => $image->external_url,
                 'sizes'             => [
                     'original'      =>  $image->getFileUrl('original'),
                     'medium'        =>  $image->getFileUrl('medium'),
                     'small'         =>  $image->getFileUrl('small'),
                     'tiny'          =>  $image->getFileUrl('tiny'),
                 ],
                 'created_at'        => $image->created_at,
                 'updated_at'        => $image->updated_at
             ];
         }

        if ($user->isAgent()) {
            if (!$user->agent) {
                $user->agent();
            }

            $publicUserDataTransform['agent_data'] = [
                'id' => $user->agent->id
            ];
        }

        if ($user->isSeller()) {
            if (!$user->seller) {
                $user->seller();
            }

            $publicUserDataTransform['seller_data'] = [
                'id' => $user->seller->id
            ];
        }

        return $publicUserDataTransform;
    }
}
