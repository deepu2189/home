<?php
namespace App\Transformers;

use App\JabUsersPayment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
    class UsersPaymentTransformer extends Transformer
    {
        public function transform(Model $userspayment)
        {
            $UsersPaymentTransform = [
                'FullName'      => $userspayment->FullName,
                'BankRoutingNo' => $userspayment->BankRoutingNo,
                'AccountNo'     => $userspayment->AccountNo,
                'UserId'        => $userspayment->UserId,
                'CreatedBy'     => $userspayment->CreatedBy,
                'CreatedOn'     => $userspayment->CreatedOn,
                'UpdatedBy'     => $userspayment->UpdatedBy,
                'UpdatedOn'     => $userspayment->UpdatedOn
            ];
            return $UsersPaymentTransform;
        }
    }
?>
