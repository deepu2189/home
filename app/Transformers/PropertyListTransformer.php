<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\Mediafile;
use Illuminate\Support\Facades\Config;
use App\Transformers\AssetTypeTransformer;

use Log;

/**
 * Transformer for the Property
 *
 * @author Nicolas Talienti <nicolas@serfe.com>
 * @since 0.3
 * @package HomeJab
 * @subpackage Transformers
 */
class PropertyListTransformer extends Transformer
{

  

    /**
     *
     * @param AssetTypeTransformer $assetTypeTransformer
     */
    public function __construct(AssetTypeTransformer $assetTypeTransformer)
    {
        $this->assetTypeTransformer = $assetTypeTransformer;
    }

    /**
     * Transforms a property
     *
     * @param Model $property
     * @return array
     */
    public function transform(Model $property)
    {

        $propertyTransform = [
            'id'                    => (int) $property->id,
            'slug'                  => $property->slug,
            'title'                 => $property->title,
            'type'                  => $property->type,
            'price'                 => $property->price,
            'address'               => $property->address,
            'preview_id'            => $property->preview_id,
            'imported'              => (bool) $property->imported,
            'redirect_url_mls'      => $property->redirect_url_mls,
            'redirect_url_normal'   => $property->redirect_url_normal,
            'city'   => $property->city,
            'state'   => $property->state
        ];

      

        $assets = $property->order->package->assetTypes;
        if ($assets) {
            $propertyTransform['asset_types'] = $this->assetTypeTransformer->transformCollection($assets);
        }

        if (intval($property->preview_id) > 0) {
            $mediaPreview = Mediafile::find(intval($property->preview_id));
            if ($mediaPreview) {
                $propertyTransform['preview_Image'] = json_decode($mediaPreview->metadata);
            } else {
                $propertyTransform['preview_Image'] = false;
            }
        } else {
            $propertyTransform['preview_Image'] = false;
        }

        if ($property->imported) {
            $propertyTransform['redirect_url_mls']         = Config::get('app.old_domain_mls_seo') . $property->redirect_url_mls;
            $propertyTransform['redirect_url_normal']      = Config::get('app.old_domain_seo') . $property->redirect_url_normal;
        }

        return $propertyTransform;
    }
}
