<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

use HomeJab\Models\HomeCounter;

/**
 * Transforms the object form a homeCounter into a representable object
 *
 * @author Dario Grau <dario@serfe.com>
 * @since
 */
class HomeCountersTransformer extends Transformer
{

    /**
     * Tramsforms a homeCounter Object into a representation
     *
     * @param Model $counter
     * @return mixed
     * @since
     */
    public function transform(model $homeCounters)
    {

        $homeCountersTransform = [
            'rea'         => (int)   $homeCounters->rea,
            'videos'      => (int)   $homeCounters->videos,
            'photographs' => (int)   $homeCounters->photographs,
            'us_cities'   => (int)   $homeCounters->us_cities
        ];
        
        return $homeCountersTransform;
    }
}
