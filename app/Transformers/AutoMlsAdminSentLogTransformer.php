<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class AutoMlsAdminSentLogTransformer extends Transformer
{
	/**
	 *
	 * @param JabJobLogListTransformer $jobLogListTransformer
	 */
	public function __construct()
	{

	}
	public function transform(Model $log)
	{

		$logTransformer = [
			'total_listing'                 => $log->total_listing,
			'total_sms_sent_day'            => $log->total_sms_sent_day,
			'total_pending_sms_day'         => $log->total_pending_sms_day,
			'total_infusionsoft_sent_day'   => $log->total_infusionsoft_sent_day,
			'total_pending_infusionsoft_day'=> $log->total_pending_infusionsoft_day,
			'date'                          => $log->date,
		];
		return $logTransformer;
	}
}
?>