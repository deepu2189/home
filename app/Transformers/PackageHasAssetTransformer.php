<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * @author Andres <andres@serfe.com>
 * @since 1.2.1
 * @package HomeJab
 * @subpackage Transformers
 */
class PackageHasAssetTransformer extends Transformer
{

     /**
     * Transforms the data to a representable way
     *
     * @param \HomeJab\Models\PackageHasAsset $PackageHasAsset
     * @return array
     */
    public function transform(Model $PackageHasAsset)
    {

        $packageHasAssetTransform = [
            'asset_type_id'    => (int) $PackageHasAsset->asset_type_id,
            'package_id'       => (int) $PackageHasAsset->package_id,
            'position'         => (int) $PackageHasAsset->position,
            'relation'         => (int) $PackageHasAsset->relation_n_to_one,
            'amount_originals_files' => (int) $PackageHasAsset->amount_originals_files,
            'amount_edited_files' => (int) $PackageHasAsset->amount_edited_files,
        ];
        return $packageHasAssetTransform;
    }
}
