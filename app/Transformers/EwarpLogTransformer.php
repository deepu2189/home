<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the Ewarp Log for the frontend
 *
 * @author Guillermo Mione <guillermo@serfe.com>
 * @since 1.0
 * @package HomeJab
 * @subpackage Transformers
 */
class EwarpLogTransformer extends Transformer
{

    /**
     * Transforms Ewarp log
     *
     * @param Model $log
     * @return array
     */
    public function transform(Model $log)
    {

        $logTransform = [
            'id'                    => (int) $log->id,
            'job_id'                => (int) $log->job_id,
            'log'                   => json_decode($log->log),
            'description'           => $log->description,
            'created_at'            => $log->created_at,
            'updated_at'            => $log->updated_at
        ];


      

        return $logTransform;
    }
}
