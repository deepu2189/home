<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

use Log;

/**
 * Generic class for the transformer
 *
 * @author dpk 
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformer
 */
abstract class Transformer
{
    /**
     * Transforms a collection of items
     *
     * @param Collection $items
     * @return array
     */
    public function transformCollection(Collection $items)
    {
        return $items->map(function ($item) {
            return $this->transform($item);
        });
    }


    public function transformCollectionComplex(Collection $items, $callback)
    {
        try {
            return $items->map(function ($item, $callback) {
                return $this->$callback($item);
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' ' . $e->getFile() . ':' . $e->getLine());
            return;
        }
    }

    abstract public function transform(Model $item);
}
