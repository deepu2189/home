<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\PropertyTransformer;
use App\Transformers\UserTransformer;

/**
 * Transforms the data from the user to an array interface
 *
 * @author Deepak Thakur
 * @since 1.2.3
 * @package HomeJab
 * @subpackage Transformers
 */
class MortgageReferralTransformer extends Transformer
{

    /**
     *
     * @param PropertyTransformer $PropertyTransformer
     * @param UserTransformer $UserTransformer
     */
    public function __construct(PropertyTransformer $PropertyTransformer, UserTransformer $UserTransformer)
    {
        $this->PropertyTransformer = $PropertyTransformer;
        $this->UserTransformer = $UserTransformer;
    }


    /**
     * Transforms the data to a representable way
     *
     * @param Model $mortgage_referral
     * @return array
     */
    public function transform(Model $mortgage_referral)
    {
        $mortgage_referralTransform = [
            'id'           => (int) $mortgage_referral->id,
            'firstname'    => $mortgage_referral->firstname,
            'lastname'     => $mortgage_referral->lastname,
            'email'        => $mortgage_referral->email,
            'phone_number' => $mortgage_referral->phone_number,
            'json_data'    => json_decode($mortgage_referral->json_data)
        ];

        if ($mortgage_referral->property_id) {
            $property = \App\Property::find($mortgage_referral->property_id);
            $mortgage_referralTransform['property'] = $this->PropertyTransformer->transform($property);
        }

        return $mortgage_referralTransform;
    }
}
