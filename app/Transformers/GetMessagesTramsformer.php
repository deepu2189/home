<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabJobs;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class GetMessagesTramsformer extends Transformer
{
    public function transform(Model $m_message = null,$message = null)
    {
        if($m_message == null && $message != null)
            $error = $message;
        else
            $error = null;

        $GetMessagesTransform = [
            "MessageId"                    => isset($m_message->MessageId)?$m_message->MessageId:0,
            "MessageText"                  => isset($m_message->MessageText)?$m_message->MessageText:null,
            "MessageTime"                  => isset($m_message->MessageTime)?(string)$m_message->MessageTime:null,
            "IsSender"                     => isset($m_message->IsSender)?($m_message->IsSender == 1?"True":"False"):null,
            "IsRead"                       => isset($m_message->IsRead)?($m_message->IsRead == 1?"True":"False"):null,
            "Error"                        => $error,
        ];
        return $GetMessagesTransform;
    }
    public function TransformWithMessage(Model $jobs=null,$message)
    {
        return $this->transform(null,$message);
    }
}
?>