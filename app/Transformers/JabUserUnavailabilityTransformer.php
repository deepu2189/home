<?php
namespace App\Transformers;

use App\JabUserUnavailability;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabUserUnavailabilityTransformer extends Transformer
{
    public function transform(Model $usersschedule)
    {
        $JabUserUnavailabilityList = [
            'StartDate'     => $usersschedule->StartDate,
            'StartTime'     => $usersschedule->StartTime,
            'EndDate'       => $usersschedule->EndDate,
            'EndTime'       => $usersschedule->EndTime,
            'Startdate'     => $usersschedule->Startdate,
            'Enddate'       => $usersschedule->Enddate,
            'Notes'         => $usersschedule->Notes
        ];
        return $JabUserUnavailabilityList;
    }
}