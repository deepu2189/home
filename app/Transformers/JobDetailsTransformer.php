<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabJobs;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JobDetailsTransformer extends Transformer
{

    public function transform(Model $jobs = null,$message = null)
    {
    	if($jobs == null && $message != null)
            $error = $message;
        else
            $error = null;
        $jobsTransform = [
            "Jobid"=> isset($jobs->Jobid)?$jobs->Jobid:null,
            "HJJobID"=> isset($jobs->HJJobID)?($jobs->HJJobID):null,
            "HJPackageName"=> isset($jobs->HJPackageName)?$jobs->HJPackageName:null,
            "ContactName"=> isset($jobs->ContactName)?$jobs->ContactName:null,
            "ContactEmail"=> isset($jobs->ContactEmail)?$jobs->ContactEmail:null,
            "ContactPhone"=> isset($jobs->ContactPhone)?$jobs->ContactPhone:null,
            "ScheduleDate"=> isset($jobs->ScheduleDate)?date('m/d/Y',strtotime($jobs->ScheduleDate))." 12:00:00 AM":null,
            "PropertyAddress"=> isset($jobs->PropertyAddress)?$jobs->PropertyAddress:null,
            "ScheduleTime"=> isset($jobs->ScheduleTime)?$jobs->ScheduleTime:null,
            "StatusID"=> isset($jobs->StatusID)?$jobs->StatusID:null,
            "ContactOwnerName"=> isset($jobs->ContactOwnerName)?$jobs->ContactOwnerName:null,
            "ContactOwnerPhone"=> isset($jobs->ContactOwnerPhone)?$jobs->ContactOwnerPhone:null,
            "ContactOwnerEmail"=> isset($jobs->ContactOwnerEmail)?$jobs->ContactOwnerEmail:null,
            "ScheduleSlot"=> isset($jobs->ScheduleSlot)?$jobs->ScheduleSlot:null,
            "Comments"=> isset($jobs->Comments)?$jobs->Comments:null,
            "MustHaveShots"=> isset($jobs->MustHaveShots)?$jobs->MustHaveShots:null,
            "AcceptTime"=> isset($jobs->AcceptTime)?$jobs->AcceptTime:null,
            "DeclineTime"=> isset($jobs->DeclineTime)?$jobs->DeclineTime:"",
            "StartTime"=>isset($jobs->StartTime)?$jobs->StartTime:null,
            "CloseTime"=> isset($jobs->CloseTime)?$jobs->CloseTime:null,
            "Error"=> $error,
            "JobLat"=> isset($jobs->JobLat)?$jobs->JobLat:null,
            "JobLong"=> isset($jobs->JobLong)?$jobs->JobLong:null,
            "PhotographerLat"=> isset($jobs->PhotographerLat)?$jobs->PhotographerLat:null,
            "PhotographerLong"=> isset($jobs->PhotographerLong)?$jobs->PhotographerLong:null,
            "UserAddress"=> isset($jobs->UserAddress)?$jobs->UserAddress:null,
            "PackagePay"=> isset($jobs->packagepay)?$jobs->packagepay:null,
            "PackageInstructions"=> isset($jobs->packageinstructions)?$jobs->packageinstructions:null
        ];


        return $jobsTransform;
    }
    public function TransformWithMessage(Model $jobs=null,$message)
    {
        return $this->transform(null,$message);
    }
}
?>