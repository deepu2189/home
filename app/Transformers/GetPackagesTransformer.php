<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\Custom;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class GetPackagesTransformer extends Transformer
{

    public function transform(Model $custom = null,$message = null)
    {
        if($custom == null && $message != null)
            $error = $message;
        else
            $error = null;

        $GetPackagesTransform = [
            "id"                    => isset($custom->id)?$custom->id:0,
            "name"                  => isset($custom->name)?$custom->name:null,
            "short_description"     => isset($custom->short_description)?$custom->short_description:null,
            "package_type"          => isset($custom->package_type)?$custom->package_type:null,
            "price"                 => isset($custom->price)?$custom->price:0,
            "payment_to_photographer"=> isset($custom->payment_to_photographer)?$custom->payment_to_photographer:0,
            "package_instructions"  => isset($custom->package_instructions)?$custom->package_instructions:null,
            "Error"                 => $error,
        ];
        return $GetPackagesTransform;
    }
    public function TransformWithMessage(Model $jobs=null,$message)
    {
        return $this->transform(null,$message);
    }
}
?>