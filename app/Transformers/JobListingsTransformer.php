<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabJobs;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JobListingsTransformer extends Transformer
{

    public function transform(Model $jobs = null,$message = null)
    {
		if($jobs == null && $message != null)
            $error = $message;
        else
            $error = null;

        $jobsTransform = [
            "Jobid"=> isset($jobs->Jobid)?$jobs->Jobid:null,
            "HJJobID"=> isset($jobs->HJJobID)?($jobs->HJJobID):null,
            "HJPackageName"=> isset($jobs->HJPackageName)?$jobs->HJPackageName:null,
            "PackagePay"=> isset($jobs->packagepay)?$jobs->packagepay:null,
            "ContactName"=> isset($jobs->ContactName)?$jobs->ContactName:null,
            "ContactEmail"=> isset($jobs->ContactEmail)?$jobs->ContactEmail:null,
            "ContactPhone"=> isset($jobs->ContactPhone)?$jobs->ContactPhone:null,
            "ScheduleDate"=> isset($jobs->ScheduleDate)?date('m/d/Y',strtotime($jobs->ScheduleDate))." 12:00:00 AM":null,
            "PropertyAddress"=> isset($jobs->PropertyAddress)?$jobs->PropertyAddress:null,
            "ScheduleTime"=> isset($jobs->ScheduleTime)?$jobs->ScheduleTime:null,
            "StatusID"=> isset($jobs->StatusID)?$jobs->StatusID:null,
            "Error"=> $error,
        ];
        return $jobsTransform;
    }
    public function TransformWithMessage(Model $jobs=null,$message)
    {
        return $this->transform(null,$message);
    }
}
?>