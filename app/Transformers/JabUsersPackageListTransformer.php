<?php
namespace App\Transformers;

use App\JabUsersPackage;
use App\Package;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabUsersPackageListTransformer extends Transformer
{
    public function transform(Model $package)
    {
        $usersPackageListTransform = [
            'id'                        => (int)$package->id,
            'name'                      => $package->name,
            'short_description'         => $package->short_description,
            'long_description'          => $package->long_description,
            'price'                     => $package->price,
            'ask_price'                 => $package->ask_price,
            'package_type'              => $package->package_type,
            'payment_to_photographer'   => $package->payment_to_photographer,
            'package_instructions'      => $package->package_instructions,
        ];
        return $usersPackageListTransform;
    }

    public function UserPackageListTransform(Collection $items,$id){

        $this->JabUsersPackage = new JabUsersPackage();

        foreach($items as $key=>$val)
        {
            $usersPackageListTransform[$key] = $this->transform($val);

            if($id >= 0){
                $Userpackagelist = $this->JabUsersPackage->newQuery()->where('UserId',$id)->where('HJPackageId',$val->id)->first();

                if($Userpackagelist){
                    $usersPackageListTransform[$key]['IsChecked'] = True;
                }
            }
        }
        return $usersPackageListTransform;
    }

}
?>