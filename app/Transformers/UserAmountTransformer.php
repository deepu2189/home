<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the data from the user to an array interface
 *
 * @author STLP
 * @since 1.1.5
 * @package HomeJab
 * @subpackage Transformers
 */
class UserAmountTransformer extends Transformer
{

    /**
     *
     * @param HomeJab\Models\User $user
     * @return array
     */
    public function transform(Model $user)
    {

        $user = [
            'id'                => (int)   $user->id,
            //'user_id'           => (int)   $user->user_id,
            //'referral_id'       => (int)   $user->referral_id,
            'amount'            => (float) $user->total,
            //'is_transfer'       => (int)   $user->is_transfer,
            //'referalfname'     =>          $user->firstname,
            //'referallname'     =>          $user->lastname,
            'firstname'        =>          $user->firstname,
            'lastname'          =>         $user->lastname,
            'referral_code'     =>         $user->referral_code,
            //'created_at'        =>         $user->created_at,
            //'updated_at'        =>         $user->updated_at,
        ];
        return $user;
    }
}
