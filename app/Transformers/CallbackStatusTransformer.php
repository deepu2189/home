<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the Callback Status
 *
 * @author Mariana Perez Elena <mariana@serfe.com>
 * @since 1.3.0
 * @package HomeJab
 * @subpackage Transformers
 */
class CallbackStatusTransformer extends Transformer
{

    /**
     * Transforms Callback status
     *
     * @param Model $callbackStatus
     * @return array
     */
    public function transform(Model $callbackStatus)
    {

        $callbackStatusTransform = [
            'id'                    => (int) $callbackStatus->id,
            'order_id'              => (int) $callbackStatus->order_id,
            'type'                  => $callbackStatus->type,
            'type_label'            => \App\CallbackStatus::$typeList[$callbackStatus->type],
            'callback_sent'         => $callbackStatus->callback_sent,
            'callback_success'      => $callbackStatus->callback_success,
            'callback_failed_at'    => $callbackStatus->callback_failed_at,
            'created_at'            => $callbackStatus->created_at,
            'updated_at'            => $callbackStatus->updated_at
        ];

        return $callbackStatusTransform;
    }
}
