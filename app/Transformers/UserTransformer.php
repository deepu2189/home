<?php
 
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * Transforms the data from the user to an array interface*
 * 
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class UserTransformer extends Transformer
{

    
    public function transform(Model $user)
    {
       
        
        $userTransform = [
            'id'                    => (int) $user->id,
            'firstname'             => $user->firstname,
            'lastname'              => $user->lastname,
            'slug'                  => $user->slug,
            'email'                 => $user->email,
            'company_name'          => $user->company_name,
            'contact_phone'         => $user->contact_phone,
            'assistants_emails'     => json_decode($user->assistants_emails, false),
            'bio'                   => $user->bio,
            'account_type'          => $user->account_type,
            'account_verified'      => (bool) $user->account_verified,
            'is_admin'              => false,
            'is_rea'                => false,
            'is_photographer'       => false,
            'stripe_id'             => $user->stripe_id,
            'referral_code'         => $user->referral_code,
            'has_referral_amount'   => (bool) $user->has_referral_amount,
            'referral_amount'       => (float) $user->referral_amount,
            'commission_percent'    => (float) $user->commission_percent,
            'current_wallet_amount' => (float) $user->current_wallet_amount,
            'created_at'            => $user->created_at,
            'updated_at'            => $user->updated_at,
            'mortgage'              => $user->mortgage,
            'fullname'              => $user->fullname,
        ];     

        if ($user->isAdmin()) {

            if ($user->admin) {
                $userTransform['admin'] = [
                    'id' => $user->admin->id
                ];
                $userTransform['is_admin'] = true;
            }
        } else if ($user->isAgent()) {

            if (!$user->agent) {
                $user->agent();
            }
            //pr($user,1);
            $userTransform['agent'] = [
                'id' => $user->agent->id
            ];
            $userTransform['is_rea'] = true;
            $userTransform['agent']['fullname'] = $user->getFullNameAttribute();
        } else if ($user->isPhotographer()) {

            if ($user->photographer) {
                $userTransform['photographer'] = [
                    'id'            => $user->photographer->id
                ];
            }
            $userTransform['is_photographer'] = true;
        } else if ($user->isSeller()) {

            if (!$user->seller) {
                $user->seller();
            }

            if ($user->seller) {

                if(!empty($user->seller->id)) {
                    $userTransform['seller'] = [
                        'id' => $user->seller->id
                    ];
                }
            }
            $userTransform['is_seller'] = true;

            $userTransform['seller']['fullname'] = $user->getFullNameAttribute();
        }
        
        $image = $user->profileImage;
        if ($image) {
            $userTransform['profile_image'] = [
                'id'                => (int) $image->id,
                'type'              => $image->type,
                'original_name'     => $image->original_name,
                'app_name'          => $image->app_name,
                'extension'         => $image->extension,
                'external_storage'  => (bool) $image->external_storage,
                'external_url'      => $image->external_url,
                'sizes'             => [
                    'original'      =>  $image->getFileUrl('original'),
                    'medium'        =>  $image->getFileUrl('medium'),
                    'small'         =>  $image->getFileUrl('small'),
                    'tiny'          =>  $image->getFileUrl('tiny'),
                ],
                'created_at'        => $image->created_at,
                'updated_at'        => $image->updated_at
            ];
        }
        $image = $user->logoImage;
        if ($image) {
            $userTransform['logo_image'] = [
                'id'                => (int) $image->id,
                'type'              => $image->type,
                'original_name'     => $image->original_name,
                'app_name'          => $image->app_name,
                'extension'         => $image->extension,
                'external_storage'  => (bool) $image->external_storage,
                'external_url'      => $image->external_url,
                'sizes'             => [
                    'original'      =>  $image->getFileUrl('original'),
                    'medium'        =>  $image->getFileUrl('medium'),
                    'small'         =>  $image->getFileUrl('small'),
                    'tiny'          =>  $image->getFileUrl('tiny'),
                ],
                'created_at'        => $image->created_at,
                'updated_at'        => $image->updated_at
            ];
        }

        $userTransform['emptyProfile'] = \App\User::$emptyProfileImageUrl;

        $videoProfile = $user->profileVideo;

        if ($videoProfile) {
            $userTransform['videoProfile'] = $videoProfile;
            $userTransform['videoProfile']['metadata'] = json_decode($videoProfile['metadata'], true);
        }

        if (empty($user->referral_amount) && !empty($user->referral_code)) {
            $userTransform['referral_amount'] = \App\User::getReferralAmountSettings();
        }


        return $userTransform;
    }
}
