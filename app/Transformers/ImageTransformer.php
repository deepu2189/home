<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transformer for image model
 *
 * @author Luciano Masuero <luciano@serfe.com>
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class ImageTransformer extends Transformer
{

    public function transform(Model $image)
    {

        $imageTransform = [
            'id'                => (int) $image->id,
            'type'              => $image->type,
            'original_name'     => $image->original_name,
            'app_name'          => $image->app_name,
            'extension'         => $image->extension,
            'external_storage'  => (bool) $image->external_storage,
            'external_url'      => $image->external_url,
            'sizes'             => [
                'original'      =>  $image->getFileUrl('original'),
                'medium'        =>  $image->getFileUrl('medium'),
                'small'         =>  $image->getFileUrl('small'),
                'tiny'          =>  $image->getFileUrl('tiny'),
            ],
            'created_at'        => $image->created_at,
            'updated_at'        => $image->updated_at
        ];

        $user = $image->user;
        if ($user) {
            $imageTransform['user'] = [
                'id'                => (int) $user->id,
                'firstname'         => $user->firstname,
                'lastname'          => $user->lastname,
                'email'             => $user->email,
                'logo_image_id'     => (int) $user->logo_image_id,
                'contact_phone'     => (int) $user->contact_phone,
                'assistants_emails' => json_decode($user->assistants_emails, false),
                'bio'               => $user->bio,
                'account_type'      => $user->account_type,
                'account_verified'  => (bool) $user->account_verified,
                'is_admin'          => false,
                'created_at'        => $user->created_at,
                'updated_at'        => $user->updated_at
            ];
            
               
            
            $image = $user->profileImage;

            if ($image) {
               
                $imageTransform['user']['profile_image_id'] =$image->id;
            }
            
            $logo = $user->logoImage;
            if ($logo) {
                $imageTransform['user']['logo_image_id'] =$logo->id;
            }
        }
        return $imageTransform;
    }
}
