<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the page metadata for the frontend
 *
 * @author Guillermo Mione <guillermo@serfe.com>
 * @since 0.8
 * @package HomeJab
 * @subpackage Transformers
 */
class PageMetadataTransformer extends Transformer
{

    /**
     * Transforms the data to a representable way
     *
     * @param Model $pageMetadata
     * @return array
     */
    public function transform(Model $pageMetadata)
    {

        $pageMetadataTransform = [
            'id'                => (int) $pageMetadata->id,
            'url'               => $pageMetadata->url,
            'title'             => $pageMetadata->title,
            'description'       => $pageMetadata->description,
            'keywords'          => $pageMetadata->keywords,
            'created_at'        => $pageMetadata->created_at,
            'updated_at'        => $pageMetadata->updated_at
        ];

        return $pageMetadataTransform;
    }
}
