<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

use App\Transformers\AssetTypeTransformer;
use App\Transformers\PackageHasAssetTransformer;

use Log;

/**
 *
 * @author Esteban Zeller <esteban@serfe.com>
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 * @property \HomeJab\Transformers\AssetTypeTransformer $assetTypeTransformer
 */
class PackageTransformer extends Transformer
{

    /**
     *
     * @param AssetTypeTransformer $assetTypeTransformer
     */
    public function __construct(AssetTypeTransformer $assetTypeTransformer, PackageHasAssetTransformer $packageHasAsset)
    {
        $this->assetTypeTransformer = $assetTypeTransformer;
        $this->packageHasAssetsTransformer = $packageHasAsset;
    }

    /**
     *
     * @param HomeJab\Models\Package $package
     * @return type
     */
    public function transform(Model $package)
    {

        $packageTransform = [
            'id'                        => (int)   $package->id,
            'name'                      =>         $package->name,
            'short_description'         =>         $package->short_description,
            'long_description'          =>         $package->long_description,
            'template_description'      =>         $package->template_description,
            'delivered_text'            =>         $package->delivered_text,
            'package_type'              =>         $package->package_type,
            'price'                     => (float) $package->price,
            'position'                  => (int)   $package->position,
            'ask_price'                 => (bool)  $package->ask_price,
            'example_slug'              =>         $package->example_slug,
            'edit_assets'               => (bool)  $package->edit_assets,
            'enabled'                   => (bool)  $package->enabled,
            'for_imported'              => (bool)  $package->for_imported,
            'shooting_time'             => (int)   $package->shooting_time,
            'payment_to_photographer'   => (float) $package->payment_to_photographer,
            'package_instructions'      =>         $package->package_instructions,
            'created_at'                =>         $package->created_at,
            'updated_at'                =>         $package->updated_at,
            
            
        ];

        $assets = $package->AssetTypes;
        if ($assets) {
            $packageTransform['assetsTypes'] = $this->assetTypeTransformer->transformCollection($assets);
        }

        $packageHasAssets = $package->PackageHasAssetTypes;
        if ($packageHasAssets) {
            $packageTransform['packageHasAssets'] = $this->packageHasAssetsTransformer->transformCollection($packageHasAssets);
        }
        
        $image = $package->previewImage;
        if ($image) {
            $packageTransform['preview_image'] = [
                'id'                => (int) $image->id,
                'type'              => $image->type,
                'original_name'     => $image->original_name,
                'app_name'          => $image->app_name,
                'extension'         => $image->extension,
                'external_storage'  => (bool) $image->external_storage,
                'external_url'      => $image->external_url,
                'sizes'             => [
                    'original'      =>  $image->getFileUrl('original'),
                    'medium'        =>  $image->getFileUrl('medium'),
                    'small'         =>  $image->getFileUrl('small'),
                    'tiny'          =>  $image->getFileUrl('tiny'),
                ],
                'created_at'        => $image->created_at,
                'updated_at'        => $image->updated_at
            ];
        }

        return $packageTransform;
    }

    /**
     * Only shows public information
     *
     * @param \HomeJab\Models\Package $package
     * @return array
     */
    public function publicTransform(Model $package)
    {
        $packageTransform = [
            'id'                => (int) $package->id,
            'name'              => $package->name,
            'short_description' => $package->short_description,
            'long_description'  => $package->long_description,
            'delivered_text'    => $package->delivered_text,
            'package_type'      => $package->package_type,
            'price'             => (float) $package->price,
            'position'          => (int) $package->position,
            'ask_price'         => (bool) $package->ask_price,
            'example_slug'      => $package->example_slug,
            'template_description' => $package->template_description
        ];
        
        
        $image = $package->previewImage;
        if ($image) {
            $packageTransform['preview_image'] = [
                'id'                => (int) $image->id,
                'type'              => $image->type,
                'original_name'     => $image->original_name,
                'app_name'          => $image->app_name,
                'extension'         => $image->extension,
                'external_storage'  => (bool) $image->external_storage,
                'external_url'      => $image->external_url,
                'sizes'             => [
                    'original'      =>  $image->getFileUrl('original'),
                    'medium'        =>  $image->getFileUrl('medium'),
                    'small'         =>  $image->getFileUrl('small'),
                    'tiny'          =>  $image->getFileUrl('tiny'),
                ],
                'created_at'        => $image->created_at,
                'updated_at'        => $image->updated_at
            ];
        }
        
        return $packageTransform;
    }
}
