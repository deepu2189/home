<?php
namespace App\Transformers;

use App\JabMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class GetAllNotificationListTransformer extends Transformer
{
    public function transform(Model $notification)
    {
        $JabNotificationOnCountTransform = [
            'JobID'      => $notification->JobID,
            'UserId'      => $notification->UserId,
            'HJJobID'    => $notification->HJJobID,
            'Name'       => $notification->Name,
            'MessageText'=> $notification->MessageText,
            'Messagetime'=> $notification->Messagetime,
            'IsRead'     => $notification->IsRead,
            'IsClosed'     => $notification->isClosed,
        ];
        return $JabNotificationOnCountTransform;
    }
}
?>