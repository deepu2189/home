<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Transforms the data from the zip
 *
 * @author Andres Estepa <andres@serfe.com>
 * @since 1.2.1
 * @package HomeJab
 * @subpackage Transformers
 */
class ZipTransformer extends Transformer
{

    public function transform(Model $zip)
    {

        $zipTransform = [
            'id'              => $zip->id,
            'zip_code'        => $zip->zip_code,
            'coords'        => array(
                    'latitude'        => (float) $zip->lat,
                    'longitude'       => (float) $zip->lng,
            ),
            'city_name'       => $zip->city_name,
            //This data is set here because angular-google-maps
           'icon'            => Config::get('app.marker_icon')
            
        ];

        return $zipTransform;
    }


     /**
     *
     * @param type $items
     * @param array $expand
     * @return type
     */
    public function transformCollectionAndExpand($items, $expand = [])
    {
        return $items->map(function ($item) use ($expand) {
            return $this->transform($item, $expand);
        });
    }
}
