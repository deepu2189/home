<?php
namespace App\Transformers;

use App\JabMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabNotificationListTransformer extends Transformer
{
    public function transform(Model $notification)
    {
        $JabNotificationListTransform = [
            'JobID'      => $notification->JobID,
            'HJJobID'    => $notification->HJJobID,
            'Name'       => $notification->Name,
            'MessageText'=> $notification->MessageText,
            'Messagetime'=> $notification->Messagetime,
            'isClosed'   => $notification->isClosed,
            'Notification' => $notification->Name.' has sent you a message against order# '.$notification->HJJobID,
        ];

        return $JabNotificationListTransform;
    }

    public function publictransform(Model $notification,$id){
        $JabNotificationListTransform = [
            'JobID'      => $id,
            'isClosed'   => $notification->isClosed,
        ];
        return $JabNotificationListTransform;
    }
}
?>