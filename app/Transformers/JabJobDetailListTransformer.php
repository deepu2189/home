<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabJobs;
use App\Transformers\JabJobUserTransformer;
/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabJobDetailListTransformer extends Transformer
{
    public function __construct(JabJobUserTransformer $jabJobUserTransformer)
    {
        $this->JabJobUserTransformer = $jabJobUserTransformer;

    }
    public function transform(Model $orderdetail)
    {
        $JabJobDetailListTransform = [
            'JobId'         => $orderdetail->JobId,
            'HJJobID'       => $orderdetail->HJJobID,
            'HJPackageName'     => $orderdetail->HJPackageName,
            'PropertyAddress'   => $orderdetail->PropertyAddress,
            'ContactName'       => $orderdetail->ContactName,
            'ContactOwnerEmail' => $orderdetail->ContactOwnerEmail,
            'ScheduleDate'  => $orderdetail->ScheduleDate,
            'ScheduleTime'  => $orderdetail->ScheduleTime,
            'StatusID'      =>  $orderdetail->StatusID,
            'StatusName'    => $orderdetail->Status
        ];

		$jabjobuser = $orderdetail->JabJobUser;

		if ($jabjobuser) {
			$JabJobDetailListTransform['JobUser'] = $this->JabJobUserTransformer->transformCollection($jabjobuser);
		}
		
        return $JabJobDetailListTransform;


    }

}
?>