<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class MlsSmsTemplateListTransformer extends Transformer
{
	/**
	 *
	 * @param JabJobLogListTransformer $jobLogListTransformer
	 */
	public function __construct()
	{

	}
	public function transform(Model $smstemp)
	{

		$SmsTempListTransformer = [
			'mst_id'                 => $smstemp->mst_id,
			'mst_message'            => $smstemp->mst_message,
			'mst_is_visible'         => $smstemp->mst_is_visible,


		];
		return $SmsTempListTransformer;
	}
}
?>