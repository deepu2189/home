<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class AutoMlsAdminLogTransformer extends Transformer
{
	/**
	 *
	 * @param JabJobLogListTransformer $jobLogListTransformer
	 */
	public function __construct()
	{

	}
	public function transform(Model $log)
	{

		$logTransformer = [
			'total_sms'              => $log->total_sms,
			'total_infusionsoft'     => $log->total_infusionsoft,
			'SentDate'               => $log->SentDate,
			'dateobj'                => strtotime($log->SentDate)


		];
		return $logTransformer;
	}
}
?>