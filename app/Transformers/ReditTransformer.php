<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the data for redit propouse
 *
 * @author Andres Estepa <andres@serfe.com>
 * @package HomeJab
 * @subpackage Transformers
 */
class ReditTransformer extends Transformer
{

    /**
     * Transforms the data to a representable way
     *
     * @param Model $asset
     * @return array
     */
    public function transform(Model $asset)
    {

        $data = [];
        return $data;
    }


    public function transformAssets(Model $asset)
    {

        \Codeception\Util\Debug::debug($asset);
        die;
        $assetTransform = [
            'id'               => (int) $asset->id,
            'name'             => $asset->name,
            'description'      => $asset->description,
            'instructions'     => $asset->instructions,
            'enabled_3d'       => (bool) $asset->enabled_3d,
            'minimal_amount'   => (int) $asset->minimal_amount,
            'ewarp_product_id' => $asset->ewarp_product_id,
            'variant_1'        => $asset->variant_1,
            'unit'             => $asset->unit,
            'enabled'          => (bool) $asset->enabled,
            'created_at'       => $asset->created_at,
            'updated_at'       => $asset->updated_at
        ];
        return $assetTransform;
    }
}
