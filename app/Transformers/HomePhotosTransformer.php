<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

use HomeJab\Models\HomePhoto;

/**
 * Transforms the object form a homePhoto into a representable object
 *
 * @author Nicolas Taglienti <nicolas@serfe.com>
 * @since 0.8
 */
class HomePhotosTransformer extends Transformer
{

    /**
     * Tramsforms an Job Object into a representation
     *
     * @param Model $photo
     * @return mixed
     * @since 0.2
     */
    public function transform(model $photo)
    {

        $response = $photo;
        if ($photo->mediafile) {
            $response['mediafile'] = $photo->mediafile;
            $response['mediafile']['metadata'] = json_decode($photo->mediafile->metadata, true);
        }
        return $response;
    }
}
