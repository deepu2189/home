<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class AutoMlsAdminCsvLogTransformer extends Transformer
{
	/**
	 *
	 * @param JabJobLogListTransformer $jobLogListTransformer
	 */
	public function __construct()
	{

	}
	public function transform(Model $log)
	{

		$logTransformer = [
			'ListingId'              => $log->ListingId,
			'StreetNumber'           => $log->StreetNumber,
			'StreetName'             => $log->StreetName,
			'StateOrProvince'        => $log->StateOrProvince,
			'ListAgentMlsId'         => $log->ListAgentMlsId,
			'AgentFullName'          => $log->AgentFullName,
			'AgentEmail'             => $log->AgentEmail,
			'AgentPhone'             => $log->AgentPhone,
			'Medium'                 => $log->Medium,
			'InfusionSoftId'         => $log->InfusionSoftId,
			'Message'                => $log->Message,
			'SentDate'               => $log->SentDateTime


		];
		return $logTransformer;
	}
}
?>