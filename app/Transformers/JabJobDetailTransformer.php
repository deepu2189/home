<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Transformers\JabJobLogListTransformer;
use App\Transformers\JabJobScheduleListTransformer;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabJobDetailTransformer extends Transformer
{
    /**
     *
     * @param JabJobLogListTransformer $jobLogListTransformer
     */
    public function __construct(JabJobLogListTransformer $jabJobLogListTransformer, JabJobScheduleListTransformer $jabJobScheduleListTransformer)
    {
        $this->JabJobLogListTransformer = new $jabJobLogListTransformer;
        $this->JabJobScheduleListTransformer = new $jabJobScheduleListTransformer;
    }
    public function transform(Model $orderdetail)
    {

        $JabJobDetailTransform = [
            'JobId'              => $orderdetail->JobId,
            'HJJobID'            => $orderdetail->HJJobID,
            'HJPackageId'        => $orderdetail->HJPackageId,
            'HJPackageName'      => $orderdetail->HJPackageName,
            'ContactName'        => $orderdetail->ContactName,
            'ContactPhone'       => $orderdetail->ContactPhone,
            'ContactEmail'       => $orderdetail->ContactEmail,
            'ContactOwnerName'   => $orderdetail->ContactOwnerName,
            'ContactOwnerEmail'  => $orderdetail->ContactOwnerEmail,
            'ContactOwnerPhone'  => $orderdetail->ContactOwnerPhone,
            'Comments'           => $orderdetail->Comments,
            'PropertyZip'        => $orderdetail->PropertyZip,
            'PropertyState'      => $orderdetail->PropertyState,
            'PropertyCity'       => $orderdetail->PropertyCity,
            'PropertyAddress'    => $orderdetail->PropertyAddress,
            'MustHaveShots'      => $orderdetail->MustHaveShots,
            'ScheduleSlot'       => $orderdetail->ScheduleSlot,
            'ScheduleDate'       => $orderdetail->ScheduleDate,
            'ScheduleForHours'   => $orderdetail->ScheduleForHours,
            'ScheduleStartTime'  => $orderdetail->ScheduleStartTime,

        ];

        #Job Schedule Details
        $jobschedulelist = $orderdetail->JobsHasSchedule;
        if ($jobschedulelist) {
            $JabJobDetailTransform['JobSchedule'] = $this->JabJobScheduleListTransformer->transform($jobschedulelist);
        }

        #job Log List
        $jobloglist = $orderdetail->JobsHasLog;
        if ($jobloglist) {
            $JabJobDetailTransform['JobLog'] = $this->JabJobLogListTransformer->transformCollection($jobloglist);
        }

        return $JabJobDetailTransform;
    }
}
?>