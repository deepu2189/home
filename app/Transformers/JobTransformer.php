<?php

namespace App\Transformers;

use App\EwarpLog;
use Illuminate\Database\Eloquent\Model;

use App\AllJob as Job;

/**
 * Transforms the object form a job into a representable object
 *
 * @author Esteban Zeller <esteban@serfe.com>
 * @since 0.2
 */
class JobTransformer extends Transformer
{

    /**
     *
     * @param EwarpLogTransformer $EwarpLogTransformer
     */
    public function __construct(EwarpLogTransformer $EwarpLogTransformer)
    {
        $this->EwarpLogTransformer = $EwarpLogTransformer;
    }

    /**
     * Tramsforms an Job Object into a representation
     *
     * @param Model $job
     * @return mixed
     * @since 0.2
     */
    public function transform(Model $job)
    {

        $jobTransform = [
            'id'                    => (int) $job->id,
            'order_id'              => (int) $job->order_id,
            'photographer_id'       => (int) $job->photographer_id,
            'contact_email'         =>       $job->contact_email,
            'contact_phone'         =>       $job->contact_phone,
            'contact_owner_email'   =>       $job->contact_owner_email,
            'contact_owner_phone'   =>       $job->contact_owner_phone,
            'property_address'      =>       $job->property_address,
            'property_details'      =>       $job->property_details,
            'scheduling_date'       =>       $job->scheduling_date,
            'scheduling_time'       =>       $job->scheduling_time,
            'status'                =>       $job->status,
            'status_label'          =>       Job::$status[$job->status]['label'],
            'finished_by_su'        => (bool)$job->finished_by_su,
            'quantity'              => (int) $job->quantity,
            'batch_id'              =>       $job->batch_id,
            'max_files_amount'      => (int) $job->max_files_amount,
            'comments'              =>       $job->comments,
            'created_at'            =>       $job->created_at,
            'updated_at'            =>       $job->updated_at,
            'send_to_ewarp_by_su'   => (bool)$job->send_to_ewarp_by_su,
            'already_queued'        => (bool)$job->already_queued,
        ];

        $keys = array_keys(\App\AllJob::$schedulingTime);
        if (in_array($job->scheduling_time, $keys)) {
            $jobTransform['scheduling_time_label'] = Job::$schedulingTime[$job->scheduling_time]['label'];
        } else {
            $jobTransform['scheduling_time_label'] = $job->scheduling_time;
        }

        $photographer = $job->photographer;
        if ($photographer) {
            $jobTransform['photographer_fullname'] = $photographer->User->fullname;
        }

        $order = $job->order;
        if ($order) {
            $package = $order->package;
            if ($package) {
                $jobTransform['package_name'] = $package->name;
            }
        }

        $assetType = $job->assetType;
        if ($assetType) {
            $jobTransform['asset_type_name'] = $assetType->name;
        }

        $mediafiles = $job->mediafiles;
       
        if ($mediafiles) {
            $jobTransform['mediafiles'] = $mediafiles;
        }

        $must_have_shots = $job->order->must_have_shots;
        if ($must_have_shots) {
            $jobTransform['must_have_shots'] = $must_have_shots;
        }
     //   dd($job->ewarpLogs);
        $ewarp_logs = $job->ewarpLogs;

        if ($ewarp_logs) {
            //$jobTransform['ewarp_logs'] = $this->EwarpLogTransformer->transformCollection($ewarp_logs);
            $ewarplogNew = EwarpLog::where('job_id', $job->id)->get();
            $logTransform = [];
            foreach ($ewarplogNew as $log){

                $logTransform[] = [
                    'id'                    => (int) $log->id,
                    'job_id'                => (int) $log->job_id,
                    'log'                   => json_decode($log->log),
                    'description'           => $log->description,
                    'created_at'            => $log->created_at,
                    'updated_at'            => $log->updated_at
                ];
            }

            $jobTransform['ewarp_logs'] = $logTransform;
        }

        $jobTransform['uploaded_quantity']  = $job->getFilesAmount();

        if (!$job->redit) {
            $jobTransform['amount_originals_files']  = \App\PackageHasAssetType::getOriginalFilesAmount($order['Package']['id'], intval($job['assetType'] ['id']));
        } else {
            $jobTransform['amount_originals_files']  = 0;
        }

        return $jobTransform;
    }
}
