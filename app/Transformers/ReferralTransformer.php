<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * @author Hemant Upadhyay      <hemant.upadhyay@srmtechsol.com>
 * @since 1.1.5
 * @package HomeJab
 * @subpackage Transformers
 */
class ReferralTransformer extends Transformer
{
    /**
     *
     * @param HomeJab\Models\UserReferral $userReferral
     * @return type
     */
    public function transform(Model $userReferral)
    {

        $userReferralTransform = [
            'id'                => (int)   $userReferral->id,
            'user_id'           => (int)   $userReferral->user_id,
            'referral_id'       => (int)   $userReferral->referral_id,
            'amount'            => (float) $userReferral->amount,
            'type'              =>         $userReferral->type,
            'referalfname'      =>         $userReferral->firstname,
            'referallname'      =>         $userReferral->lastname,
            'firstname'         =>         $userReferral->user->firstname,
            'lastname'          =>         $userReferral->user->lastname,
            'created_at'        =>         $userReferral->created_at,
            'updated_at'        =>         $userReferral->updated_at,
            
        ];
        return $userReferralTransform;
    }
}
