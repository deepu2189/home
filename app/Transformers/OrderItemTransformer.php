<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the Order Item for the frontend
 *
 * @author Guillermo Mione <guillermo@serfe.com>
 * @since 0.9
 * @package HomeJab
 * @subpackage Transformers
 */
class OrderItemTransformer extends Transformer
{

    /**
     * Transforms a Order Item
     *
     * @param Model $item
     * @return array
     */
    public function transform(Model $item)
    {

        $itemTransform = [
            'id'                    => (int) $item->id,
            'order_id'              => (int) $item->order_id,
            'description'           => $item->description,
            'amount'                => $item->amount,
            'type'                  => $item->type,
            'placed_on_isoft_order' => (bool) $item->placed_on_isoft_order,
            'created_at'            => $item->created_at,
            'updated_at'            => $item->updated_at
        ];

        return $itemTransform;
    }
}
