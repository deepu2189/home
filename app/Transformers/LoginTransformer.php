<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabJobs;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class LoginTransformer extends Transformer
{

    public function transform(Model $user = null,$message = null)
    {
        if($user == null && $message != null)
            $error = $message;
        else
            $error = null;

        $loginTransform = [
            'UserID' => isset($user->UserID)?$user->UserID:null,
            'HomeJabId' => isset($user->HomeJabId)?$user->HomeJabId:null,
            'Name' => isset($user->Name)?$user->Name:null,
            'RoleId' => isset($user->RoleId)?$user->RoleId:null,
            'Error' => $error,
            'ProfilePic' => isset($user->ProfilePic)?Config('constants.ProfilePicUrl').$user->UserID.'/'.$user->ProfilePic:null,
        ];
        return $loginTransform;
    }
    public function TransformWithMessage(Model $jobs=null,$message)
    {
        return $this->transform(null,$message);
    }
}
?>