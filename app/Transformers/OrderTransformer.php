<?php

namespace App\Transformers;

use App\Transformers\MediaFileTransformer;
use App\Transformers\OrderItemTransformer;
use App\Transformers\CallbackStatusTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\AllJob;

use App\InfusionSoftIntegration\Models\InfusionSoftTokenModel;
use Infusionsoft\Token;

use Log;

/**
 *
 * @author Deepak Thakur
 * @since 0.2
 * @package HomeJab
 * @subpackage Transformers
 */
class OrderTransformer extends Transformer
{

    /**
     *
     * @param MediaFileTransformer $MediaFileTransformer
     */
    public function __construct(MediaFileTransformer $MediaFileTransformer, OrderItemTransformer $OrderItemTransformer, CallbackStatusTransformer $CallbackStatusTransformer)
    {
        $this->MediaFileTransformer = $MediaFileTransformer;
        $this->OrderItemTransformer = $OrderItemTransformer;
        $this->CallbackStatusTransformer = $CallbackStatusTransformer;
    }

    /**
     * Transforms the order data to a representable way
     *
     * @param Model $order
     * @return type
     */
    public function transform(Model $order, $expand = [])
    {

        $orderTransform = [
            'id'                 => (int)   $order->id,
            'amount'             => (float) $order->amount,
            'status'             =>         $order->status,
            'status_label'       => \App\Order::$statusList[$order->status],
            'manually'           => (bool)  $order->manually,
            'must_have_shots'    =>         $order->must_have_shots,
            'infusionsoft_id'    =>         $order->infusionsoft_id,
            'created_at'         =>         $order->created_at,
            'updated_at'         =>         $order->updated_at,
            'rescheduled_confirmed' =>      $order->rescheduled_confirmed
        ];

        if (in_array('*', $expand) || in_array('agent', $expand)) {
            $agent = $order->Agent;
            if ($agent) {
                $orderTransform['Agent'] = $agent;
                $user = $agent->User;
                $orderTransform['Agent']['user']['fullname']          = $user->fullname;
            }
        }

        if (in_array('*', $expand) || in_array('seller', $expand)) {
            $seller = $order->Seller;
            if ($seller) {
                $orderTransform['Seller'] = $seller;
                $user = $seller->User;
                $orderTransform['Seller']['user']['fullname']          = $user->fullname;
            }
        }

        if (!empty(array_intersect(['*', 'package'], $expand))) {
            $package = $order->package;
            if ($package) {
                $orderTransform['Package'] = $package;
                $orderTransform['Package']['ask_price'] = (boolean) $package['ask_price'];
            }
        }

        if (!empty(array_intersect(['*', 'jobs'], $expand))) {
            $jobs = $order->jobs;
            if ($jobs) {
                foreach ($jobs as $job) {
                    if (!$job->redit) {
                        $job['status_label']            = \App\AllJob::$status[$job->status]['label'];
                        $keys = array_keys(\App\AllJob::$schedulingTime);
                        if (in_array($job->scheduling_time, $keys)) {
                            $job['scheduling_time_label'] = \App\AllJob::$schedulingTime[$job->scheduling_time]['label'];
                        } else {
                            $job['scheduling_time_label'] = $job->scheduling_time;
                        }
                        $job['asset_type']               = ['name' => $job->AssetType->name ];
                        $job['needsEwarpProccess']      = \App\AssetType::needsEWarpProcess(intval($job['asset_type_id']));

                        $orderTransform['Jobs'][] = $job;
                        $job['amount_originals_files']  = \App\PackageHasAssetType::getOriginalFilesAmount($orderTransform['Package']['id'], intval($job['asset_type_id']));
                    }

                    $job['uploaded_quantity']  = $job->getFilesAmount();
                    $job['quantity']  = $job->quantity;
                    $job['already_queued'] = (bool)$job['already_queued'];
                }
            }
        }

        if (!empty(array_intersect(['*', 'Redit'], $expand))) {
            $jobs = $order->jobs;
            $redits = $order->redits;
            if ($redits) {
                $reditNumber = 0;
                foreach ($redits as $redit) {
                    $jobs = $redit->jobs;
                    foreach ($jobs as $job) {
                        if ($job->redit) {
                            $job['status_label']            = \App\AllJob::$status[$job->status]['label'];
                            $job['asset_type']               = ['name' => $job->AssetType->name ];
                            $job['needsEwarpProccess']      = \App\AssetType::needsEWarpProcess(intval($job['asset_type_id']));

                            $orderTransform['Redit'][$reditNumber][] = $job;
                        }
                    }
                    $reditNumber++;
                }
            }
        }

        if (!empty(array_intersect(['*', 'property'], $expand))) {
            $property = $order->property;

            if ($property) {
                $orderTransform['Property'] = $property;
                if ($property->published) {
                    $orderTransform['Property']['publish_status'] = trans('property.published');
                } else {
                    $orderTransform['Property']['publish_status'] = trans('property.unpublished');
                }

                if ($property->imported) {
                    $orderTransform['Property']['imported'] = true;
                } else {
                    $orderTransform['Property']['imported'] = false;
                }
                if (!empty(array_intersect(['*', 'mediafile'], $expand))) {

                    $mediafiles = $property->mediafileswithTrashed;

                    unset($orderTransform['Property']['mediafiles']);
                    if ($mediafiles) {
                        $orderTransform['Mediafiles'] = $this->MediaFileTransformer->transformCollection($mediafiles);

                    }
                }
            }
        }

        if (!empty(array_intersect(['*', 'transaction'], $expand))) {
            $transaction = $order->transaction;
            if ($transaction) {
                $orderTransform['Transaction'] = $transaction;
            }
        }

        if (!empty(array_intersect(['*', 'orderItem'], $expand))) {
            $items = $order->orderItem;
            if (count($items) > 0) {
                $orderTransform['OrderItems'] = $this->OrderItemTransformer->transformCollection($items);
            }
        }

        if (!empty(array_intersect(['*', 'callback_status'], $expand))) {
            $callback_status = $order->callback_status;
            if (count($callback_status)>0) {
                $orderTransform['CallbackStatus'] = $this->CallbackStatusTransformer->transformCollection($callback_status);
            }
        }
        if (!empty(array_intersect(['*', 'extraFiles'], $expand))) {
            $extraFiles = $order->extraFiles;
            if ($extraFiles) {
                $orderTransform['ExtraFiles'] = $this->MediaFileTransformer->transformCollection($extraFiles);
            }
        }

        $token = InfusionSoftTokenModel::getToken();
        $scope = null;
        if ($token) {
            // Review where!
            if (isset($token->getExtraInfo()['scope']) && $token->getExtraInfo()['scope']) {
                $scope_full = explode("|", $token->getExtraInfo()['scope']);
                $scope_aux = array_pop($scope_full);
                $scope = explode(".", $scope_aux);
                $orderTransform['token_scope'] = $scope[0];
            }
        }
        return $orderTransform;
    }

    /**
     *
     * @param type $items
     * @param array $expand
     * @return type
     */
    public function transformCollectionAndExpand($items, $expand = [])
    {
        return $items->map(function ($item) use ($expand) {
            return $this->transform($item, $expand);
        });
    }


    /**
     *  @apiDefine OrdersResponse
     *  @apiSuccess {int}         id                  Order id
     *  @apiSuccess {double}      amount              Order amount
     *  @apiSuccess {string}      status              Order status
     *  @apiSuccess {string}      status_label        Order status naming
     *  @apiSuccess {bool}        manually            Order created by admin or not
     *  @apiSuccess {int}         infusionsoft_id     Order infusionsoft id
     *  @apiSuccess {Date}        created_at          Order created date
     *  @apiSuccess {Date}        updated_at          Order updated date
     *  @apiSuccess {Object[]}    Agent/Seller        Order Agent/Seller Object
     *  @apiSuccess {int}             Agent/Seller.id           Agent/Seller id
     *  @apiSuccess {Date}            Agent/Seller.created_at   Agent/Seller created date
     *  @apiSuccess {Date}            Agent/Seller.updated_at   Agent/Seller updated date
     *  @apiSuccess {int}             Agent/Seller.user_id      User id
     *  @apiSuccess {Object[]}        Agent/Seller.User           Object
     *  @apiSuccess {string}              Agent/Seller.User.account_type         User account type
     *  @apiSuccess {int}                 Agent/Seller.User.account_verified     User account verified
     *  @apiSuccess {string}              Agent/Seller.User.assistants_emails    User assistants emails
     *  @apiSuccess {text}                Agent/Seller.User.bio                  User bio
     *  @apiSuccess {string}              Agent/Seller.User.company_name         User company name
     *  @apiSuccess {string}              Agent/Seller.User.confirmation_code    User confirmation code
     *  @apiSuccess {string}              Agent/Seller.User.contact_phone        User contact phone
     *  @apiSuccess {text}                Agent/Seller.User.contact_text         User contact text
     *  @apiSuccess {string}              Agent/Seller.User.email                User email
     *  @apiSuccess {string}              Agent/Seller.User.firstname            User firstname
     *  @apiSuccess {string}              Agent/Seller.User.fullname             User fullname
     *  @apiSuccess {int}                 Agent/Seller.User.id                   User id
     *  @apiSuccess {int}                 Agent/Seller.User.imported             User imported
     *  @apiSuccess {int}                 Agent/Seller.User.imported_id          User imported id
     *  @apiSuccess {int}                 Agent/Seller.User.infusionsoft_id      User infusionsoft id
     *  @apiSuccess {string}              Agent/Seller.User.lastname             User lastname
     *  @apiSuccess {int}                 Agent/Seller.User.logo_image_id        User logo image id
     *  @apiSuccess {int}                 Agent/Seller.User.profile_image_id     User profile image id
     *  @apiSuccess {int}                 Agent/Seller.User.profile_video_id     User profile video id
     *  @apiSuccess {string}              Agent/Seller.User.slug                 User slug
     *  @apiSuccess {int}                 Agent/Seller.User.stripe_id            User stripe id
     *  @apiSuccess {Date}                Agent/Seller.User.created_at           User created date
     *  @apiSuccess {Date}                Agent/Seller.User.updated_at           User updated date
     *
     *  @apiSuccess {Object[]}    Package                         Package object
     *  @apiSuccess {bool}            Package.ask_price           Package ask price
     *  @apiSuccess {string}          Package.delivered_text      Package delivered text
     *  @apiSuccess {bool}            Package.enabled             Package enabled
     *  @apiSuccess {string}          Package.example_slug        Package example slug
     *  @apiSuccess {bool}            Package.for_imported        Package for imported
     *  @apiSuccess {int}             Package.id                  Package id
     *  @apiSuccess {int}             Package.infusionsoft_id     Package infusionsoft id
     *  @apiSuccess {text}            Package.long_description    Package long description
     *  @apiSuccess {string}          Package.name                Package name
     *  @apiSuccess {string}          Package.package_type        Package type
     *  @apiSuccess {int}             Package.position            Package position
     *  @apiSuccess {int}             Package.preview_image_id    Package preview image id
     *  @apiSuccess {double}          Package.price               Package price
     *  @apiSuccess {string}          Package.short_description   Package short description
     *  @apiSuccess {Date}            Package.created_at          Package created date
     *  @apiSuccess {Date}            Package.updated_at          Package updated date
     *
     *  @apiSuccess {Object[]}    Property                        Property object
     *  @apiSuccess {string}          Property.address                        Property address
     *  @apiSuccess {int}             Property.agent_id                       Property id
     *  @apiSuccess {bool}            Property.agent_notified                 Property agent notified
     *  @apiSuccess {int}             Property.area                           Property area
     *  @apiSuccess {int}             Property.baths                          Property baths
     *  @apiSuccess {int}             Property.beds                           Property beds
     *  @apiSuccess {string}          Property.city                           Property city
     *  @apiSuccess {Date}            Property.created_at                     Property created date
     *  @apiSuccess {text}            Property.description                    Property description
     *  @apiSuccess {int}             Property.half_baths                     Property half baths
     *  @apiSuccess {int}             Property.id                             Property id
     *  @apiSuccess {bool}            Property.imported                       Property imported
     *  @apiSuccess {int}             Property.imported_id                    Property imported id
     *  @apiSuccess {Date}            Property.last_zip_online                Property last zip online
     *  @apiSuccess {Date}            Property.last_zip_print                 Property last zip print
     *  @apiSuccess {string}          Property.map_latitude                   Property map latitude
     *  @apiSuccess {string}          Property.map_longitude                  Property map longitude
     *  @apiSuccess {bool}            Property.media_created_notification     Property media created notification
     *  @apiSuccess {int}             Property.order_id                       Property order id
     *  @apiSuccess {string}          Property.owner_email                    Property owner email
     *  @apiSuccess {string}          Property.owner_phone                    Property owner phone
     *  @apiSuccess {int}             Property.preview_id                     Property preview id
     *  @apiSuccess {double}          Property.price                          Property price
     *  @apiSuccess {string}          Property.publish_status                 Property publish status
     *  @apiSuccess {bool}            Property.published                      Property published
     *  @apiSuccess {string}          Property.redirect_url_mls               Property redirect url mls
     *  @apiSuccess {string}          Property.redirect_url_normal            Property redirect url normal
     *  @apiSuccess {int}             Property.seller_id/agent_id             Property seller id or agent id
     *  @apiSuccess {string}          Property.slug                           Property slug
     *  @apiSuccess {string}          Property.title                          Property title
     *  @apiSuccess {string}          Property.type                           Property type
     *  @apiSuccess {Date}            Property.updated_at                     Property updated date
     *  @apiSuccess {int}             Property.views_amount                   Property views amount
     *  @apiSuccess {string}          Property.zip                            Property zip
     *  @apiSuccess {Object[]}        Property.Mediafiles                     Mediafiles object
     *  @apiSuccess {Object[]}    Mediafiles          Mediafiles object
     *  @apiSuccess {int}             Mediafiles.batch_id                    Mediafiles batch id
     *  @apiSuccess {bool}            Mediafiles.copied_to_amazon            Mediafiles copied to amazon
     *  @apiSuccess {bool}            Mediafiles.copied_to_ewarp             Mediafiles copied to ewarp
     *  @apiSuccess {Date}            Mediafiles.created_at                  Mediafiles created date
     *  @apiSuccess {Date}            Mediafiles.deleted_at                  Mediafiles deleted date
     *  @apiSuccess {string}          Mediafiles.extension                   Mediafiles extension
     *  @apiSuccess {bool}            Mediafiles.external_storage            Mediafiles external storage
     *  @apiSuccess {string}          Mediafiles.external_url                Mediafiles external url
     *  @apiSuccess {Object[]}        Mediafiles.homePhoto                   Mediafiles homePhoto
     *  @apiSuccess {int}             Mediafiles.id                          Mediafiles id
     *  @apiSuccess {int}             Mediafiles.job_id                      Mediafiles job id
     *  @apiSuccess {bool}            Mediafiles.imported                    Mediafiles imported
     *  @apiSuccess {int}             Mediafiles.imported_id                 Mediafiles imported id
     *  @apiSuccess {string}          Mediafiles.key                         Mediafiles key
     *  @apiSuccess {Object[]}        Mediafiles.metadata                    Mediafiles metadata object
     *  @apiSuccess {string}          Mediafiles.metadata.preview            Metadata preview
     *  @apiSuccess {Object[]}        Mediafiles.metadata.resolutions        Metadata resolutions object
     *  @apiSuccess {string}          Mediafiles.metadata.thumbnail          Metadata thumbnail
     *  @apiSuccess {string}          Mediafiles.original_name               Mediafiles original name
     *  @apiSuccess {string}          Mediafiles.origin                      Mediafiles origin
     *  @apiSuccess {string}          Mediafiles.md5                         Mediafiles md5
     *  @apiSuccess {string}          Mediafiles.photo_type                  Mediafiles photo type
     *  @apiSuccess {int}             Mediafiles.photographer_id             Mediafiles photographer id
     *  @apiSuccess {int}             Mediafiles.position                    Mediafiles position
     *  @apiSuccess {bool}            Mediafiles.property_has_mediafile_id   Mediafiles property has mediafile id
     *  @apiSuccess {bool}            Mediafiles.published                   Mediafiles published
     *  @apiSuccess {string}          Mediafiles.type                        Mediafiles type
     *  @apiSuccess {Date}            Mediafiles.updated_at                  Mediafiles updated date
     *  @apiSuccess {Object[]}    Jobs                              Jobs object
     *  @apiSuccess {int}             Jobs.asset_type_id            Jobs asset type id
     *  @apiSuccess {int}             Jobs.batch_id                 Jobs batch id
     *  @apiSuccess {string}          Jobs.comments                 Jobs comments
     *  @apiSuccess {string}          Jobs.contact_email            Jobs contact email
     *  @apiSuccess {string}          Jobs.contact_owner_email      Jobs contact owner email
     *  @apiSuccess {string}          Jobs.contact_owner_phone      Jobs contact owner phone
     *  @apiSuccess {string}          Jobs.contact_phone            Jobs contact phone
     *  @apiSuccess {Date}            Jobs.created_at               Jobs created date
     *  @apiSuccess {Date}            Jobs.deleted_at               Jobs deleted date
     *  @apiSuccess {int}             Jobs.id                       Jobs id
     *  @apiSuccess {int}             Jobs.max_files_amount         Jobs max files amount
     *  @apiSuccess {bool}            Jobs.needsEwarpProccess       Jobs needsEwarpProccess
     *  @apiSuccess {int}             Jobs.order_id                 Jobs order id
     *  @apiSuccess {int}             Jobs.photographer_id          Jobs photographer id
     *  @apiSuccess {string}          Jobs.property_address         Jobs property address
     *  @apiSuccess {string}          Jobs.property_details         Jobs property details
     *  @apiSuccess {int}             Jobs.quantity                 Jobs quantity
     *  @apiSuccess {Date}            Jobs.scheduling_date          Jobs scheduling date  - Format: <code>YYYY-mm-dd HH:ii:ss</code>
     *  @apiSuccess {string = 'anytime' , '8amto11am',  '9amto12pm',  '10amto1pm', '11amto2pm', '12pmto3pm', '1pmto4pm', '2pmto5pm', '3pmto6pm'}          Jobs.scheduling_time          Jobs scheduling time
     *  @apiSuccess {string}          Jobs.scheduling_time_label    Jobs scheduling time naming
     *  @apiSuccess {string}          Jobs.status                   Jobs status
     *  @apiSuccess {string}          Jobs.status_label             Jobs status naming
     *  @apiSuccess {Date}            Jobs.updated_at               Jobs updated date
     *  @apiSuccess {Object[]}        Jobs.asset_type                       Asset type object
     *  @apiSuccess {Date}              Jobs.asset_type.created_at          Asset type created date
     *  @apiSuccess {text}              Jobs.asset_type.description         Asset type description
     *  @apiSuccess {bool}              Jobs.asset_type.enabled             Asset type enabled
     *  @apiSuccess {int}               Jobs.asset_type.ewarp_product_id    Asset type ewarp product id
     *  @apiSuccess {int}               Jobs.asset_type.id                  Asset type id
     *  @apiSuccess {text}              Jobs.asset_type.instructions        Asset type instructions
     *  @apiSuccess {int}               Jobs.asset_type.minimal_amount      Asset type minimal amount
     *  @apiSuccess {string}            Jobs.asset_type.name                Asset type name
     *  @apiSuccess {string}            Jobs.asset_type.unit                Asset type unit
     *  @apiSuccess {Date}              Jobs.asset_type.updated_at          Asset type updated date
     *  @apiSuccess {string}            Jobs.asset_type.variant_1           Asset type variant_1
     *  @apiSuccess {Object[]}    Transaction                   Transaction object
     *  @apiSuccess {int}           Transaction.amount          Transaction amount
     *  @apiSuccess {Date}          Transaction.created_at      Transaction created date
     *  @apiSuccess {int}           Transaction.id              Transaction id
     *  @apiSuccess {int}           Transaction.order_id        Transaction order id
     *  @apiSuccess {int}           Transaction.stripe_id       Transaction stripe id
     *  @apiSuccess {string}        Transaction.type            Transaction type
     *  @apiSuccess {Date}          Transaction.updated_at      Transaction updated date

     *  @apiSuccess {Object[]}    OrderItems          OrderItems object
     *  @apiSuccess {int}           OrderItems.amount                   OrderItems amount
     *  @apiSuccess {Date}          OrderItems.created_at               OrderItems created date
     *  @apiSuccess {string}        OrderItems.description              OrderItems description
     *  @apiSuccess {int}           OrderItems.id                       OrderItems id
     *  @apiSuccess {int}           OrderItems.order_id                 OrderItems order id
     *  @apiSuccess {bool}          OrderItems.placed_on_isoft_order    OrderItems placed on isoft order
     *  @apiSuccess {string}        OrderItems.type                     OrderItems type
     *  @apiSuccess {Date}          OrderItems.updated_at               OrderItems updated date
     */
}
