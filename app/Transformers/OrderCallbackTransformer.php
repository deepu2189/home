<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\CallbackStatus;
use App\AllJob;
use Log;

/**
 * Transforms the transactions for the frontend
 *
 * @author Esteban Zeller <esteban@serfe.com>
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class OrderCallbackTransformer extends Transformer
{

    /**
     * Transforms a order for data translation
     *
     * @param Model $order
     * @return array
     */
    public function transform(Model $order)
    {

        $basic_data = [
            'HJJobID' => (int) $order->id,
            'HJPackageId' => (int) $order->package_id,
            'HJPackageName' => $order->Package->name,
            'PaymentToPhotographer' => (float) $order->Package->payment_to_photographer,
            'PackageInstructions' => $order->Package->package_instructions,
            
            'ContactOwnerName' => 'Not available',
            'ContactOwnerPhone' => $order->Property->owner_phone,
            'ContactOwnerEmail' => $order->Property->owner_email,
            
            'PropertyZip' => $order->Property->zip,
            'PropertyCity' => $order->Property->city,
            'PropertyState' => $order->Property->state,
            'PropertyAddress' => $order->Property->address,
        ];

        if ($order->agent_id) {
            $basic_data['ContactName'] = $order->Agent->User->fullname;
            $basic_data['ContactPhone'] = $order->Agent->User->contact_phone;
            $basic_data['ContactEmail'] = $order->Agent->User->email;
            $assistants = json_decode($order->Agent->User->assistants_emails, true);
            $response = [];
            if (is_array($assistants)) {
                foreach ($assistants as $email) {
                    $response[] = $email['email'];
                }
            }
            $basic_data['AssistantsEmails'] = $response;
        } else if ($order->seller_id) {
            $basic_data['ContactName'] = $order->Seller->User->fullname;
            $basic_data['ContactPhone'] = $order->Seller->User->contact_phone;
            $basic_data['ContactEmail'] = $order->Seller->User->email;
        }

        $job = $order->Jobs->first();

        $schedule_date = new \DateTime($job->scheduling_date);

        if (array_key_exists($job->scheduling_time, AllJob::$scheduling_time_mapping)) {
            $basic_data['ScheduleStartTime'] = AllJob::$scheduling_time_mapping[$job->scheduling_time];
        } else {
            $basic_data['ScheduleStartTime'] = "anytime";
        }
        //$basic_data['ScheduleDate'] = $schedule_date->format('m-d-y');
        $basic_data['ScheduleDate'] = $schedule_date->format('Y-m-d');
        if (array_key_exists($job->scheduling_time, AllJob::$scheduling_slot_mapping)) {
            $basic_data['ScheduleSlot'] = AllJob::$scheduling_slot_mapping[$job->scheduling_time];
        } else {
            $basic_data['ScheduleSlot'] = "anytime";
        }

        $basic_data['ScheduleForHours'] = (int) $order->Package->shooting_time;
        $basic_data['Comments'] = $job->property_details;

        $must_have_shots = $order->must_have_shots;
        if ($must_have_shots) {
            $basic_data['MustHaveShots'] = $must_have_shots;
        }

        return $basic_data;
    }

    /**
     * Transforms a order for data translation
     *
     * @param Model $order
     * @param boolean package_3d
     * @param string task Asset name
     * @param CallbackStatusType $type Callback Type status
     * @param array $rescheduledData [preferred_date, preferred_time]
     * @param User $user User data
     * @return array
     */
    public function transformData(Model $order, $package_3d, $task, $type, $rescheduledData = null, $user = null)
    {
        if ($type === CallbackStatus::$typeOrderPlaced) {
            $basic_data = [
                'HJJobID' => (int) $order->id,
                'HJPackageId' => (int) $order->package_id,
                'HJPackageName' => $order->Package->name,
                'HJPackage3D' => (boolean) $package_3d,
                'HJTask' => $task,
                'PaymentToPhotographer' => (float) $order->Package->payment_to_photographer,
                'PackageInstructions' => $order->Package->package_instructions,
                'ContactOwnerName' => 'Not available',
                'ContactOwnerPhone' => $order->Property->owner_phone,
                'ContactOwnerEmail' => $order->Property->owner_email,
                'PropertyZip' => $order->Property->zip,
                'PropertyCity' => $order->Property->city,
                'PropertyState' => $order->Property->state,
                'PropertyAddress' => $order->Property->address,
            ];

            if ($order->agent_id) {
                $basic_data['ContactName'] = $order->Agent->User->fullname;
                $basic_data['ContactPhone'] = $order->Agent->User->contact_phone;
                $basic_data['ContactEmail'] = $order->Agent->User->email;
                $assistants = json_decode($order->Agent->User->assistants_emails, true);
                $response = [];
                if (is_array($assistants)) {
                    foreach ($assistants as $email) {
                        $response[] = $email['email'];
                    }
                }
                $basic_data['AssistantsEmails'] = $response;
            } else if ($order->seller_id) {
                $basic_data['ContactName'] = $order->Seller->User->fullname;
                $basic_data['ContactPhone'] = $order->Seller->User->contact_phone;
                $basic_data['ContactEmail'] = $order->Seller->User->email;
            }
            $job = $order->Jobs->first();

            $schedule_date = new \DateTime($job->scheduling_date);

            if (array_key_exists($job->scheduling_time, AllJob::$scheduling_time_mapping)) {
                $basic_data['ScheduleStartTime'] = AllJob::$scheduling_time_mapping[$job->scheduling_time];
            } else {
                $basic_data['ScheduleStartTime'] = "anytime";
            }
            //$basic_data['ScheduleDate'] = $schedule_date->format('m-d-y');
            $basic_data['ScheduleDate'] = $schedule_date->format('Y-m-d');
            if (array_key_exists($job->scheduling_time, AllJob::$scheduling_slot_mapping)) {
                $basic_data['ScheduleSlot'] = AllJob::$scheduling_slot_mapping[$job->scheduling_time];
            } else {
                $basic_data['ScheduleSlot'] = "anytime";
            }

            $basic_data['ScheduleForHours'] = (int) $order->Package->shooting_time;
            $basic_data['Comments'] = $job->property_details;

            $must_have_shots = $order->must_have_shots;
            if ($must_have_shots) {
                $basic_data['MustHaveShots'] = $must_have_shots;
            }
        } else if ($type === CallbackStatus::$typeOrderRescheduled) {
            $reschedule_date = new \DateTime($rescheduledData ['preferred_date']);
            $basic_data = [
                'HJJobID' => (int) $order->id,
                'RescheduledTimeRequested' => $rescheduledData['preferred_time'],
                'RescheduledDateRequested' => $reschedule_date->format('Y-m-d'),
                'Comments' => 'Rescheduled requested'
            ];
        } else if ($type === CallbackStatus::$typeOrderOnHold) {
            $basic_data = [
                'HJJobID' => (int) $order->id,
                'RequestedByName' => $user->firstname . ' ' . $user->lastname,
                'RequestedByEmail' => $user->email,
            ];
        }

        return $basic_data;
    }
}
