<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

use Log;

/**
 * Transforms the search category data
 *
 * @author Deepak Thakur
 * @since 1.3.0
 * @package HomeJab
 * @subpackage Transformers
 */
class SearchCategoryHasPackgeTransformer extends Transformer
{

    public function transform(Model $categoryHasPackage)
    {

        $dataTransform = [
            'id'              => $categoryHasPackage->package_id,
        ];

        
        $package = $categoryHasPackage->package;

        if ($package) {
            $dataTransform['name'] = $package->name;
        }

        return $dataTransform;
    }


     /**
     *
     * @param type $items
     * @param array $expand
     * @return type
     */
    public function transformCollectionAndExpand($items, $expand = [])
    {
        return $items->map(function ($item) use ($expand) {
            return $this->transform($item, $expand);
        });
    }
}
