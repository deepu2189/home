<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabUser;
use App\JabUsersInGroups;
use Illuminate\Support\Collection;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class GroupListTransformer extends Transformer
{
    public function transform(Model $group)
    {

        $groupListTransform = [
            'GroupId'       => $group->GroupId,
            'GroupName'     => $group->GroupName,
            'GroupDetails'  => $group->GroupDetails,
            'CreatedBy'     => $group->CreatedBy,
            'CreatedOn'     => $group->CreatedOn,
            'UpdatedBy'     => $group->UpdatedBy,
            'UpdatedOn'     => $group->UpdatedOn,
            'IsActive'      => $group->IsActive
        ];

        return $groupListTransform;
    }

    public function UserGroupListTransform(Collection $items,$id){

        $this->JabUsersInGroups = new JabUsersInGroups();

        foreach($items as $key=>$val)
        {
            $groupListTransform[$key] = $this->transform($val);

            if($id >= 0){
                $UserInGroup = $this->JabUsersInGroups->newQuery()->where('UserId',$id)->where('GroupId',$val->GroupId)->get()->first();

                if($UserInGroup){
                    $groupListTransform[$key]['IsChecked'] = True;
                }
            }
         }
        return $groupListTransform;

    }

}
?>