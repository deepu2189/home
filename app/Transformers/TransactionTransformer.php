<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the transactions for the frontend
 *
 * @author Agustina Carignan <agustina@serfe.com>
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class TransactionTransformer extends Transformer
{

    /**
     * Transforms a transaction
     *
     * @param Model $transaction
     * @return array
     */
    public function transform(Model $transaction)
    {

        $transactionTransform = [
            'id'                => (int) $transaction->id,
            'order_id'          => (int) $transaction->order_id,
            'amount'            => $transaction->amount,
            'type'              => $transaction->type,
            'created_at'        => $transaction->created_at,
            'updated_at'        => $transaction->updated_at
        ];

        $agent_name = $transaction->order->agent->user;

        if ($agent_name) {
            $transactionTransform['agent_fullname'] =  $agent_name->fullname;
        } else {
            $transactionTransform['agent_fullname'] = null;
        }

        return $transactionTransform;
    }
}
