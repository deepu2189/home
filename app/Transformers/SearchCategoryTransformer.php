<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use App\Transformers\SearchCategoryHasPackgeTransformer;
use App\searchCategoryHasPackages;

use Log;

/**
 * Transforms the search category data
 *
 * @author Deepak Thakur
 * @since 1.3.0
 * @package HomeJab
 * @subpackage Transformers
 */
class SearchCategoryTransformer extends Transformer
{

    /**
    *
    * @param SearchCategoryHasPackages $searchCategoryHasPackages
    */
    public function __construct(SearchCategoryHasPackgeTransformer $categoryHasPackageTranformer)
    {
        $this->categoryHasPackageTranformer = $categoryHasPackageTranformer;
    }

    public function transform(Model $category)
    {
       
        $categoryTransform = [
            'id'              => $category->id,
            'title'           => $category->title,
            'subtitle'        => $category->subtitle,
        ];
      
        $category_has_package = $category->searchCategoryHasPackages;
       
        if ($category_has_package) {
            $categoryTransform['packages'] = $this->categoryHasPackageTranformer->transformCollection($category_has_package);
        }        
      
        return $categoryTransform;
    }


     /**
     *
     * @param type $items
     * @param array $expand
     * @return type
     */
    public function transformCollectionAndExpand($items, $expand = [])
    {
        return $items->map(function ($item) use ($expand) {
            return $this->transform($item, $expand);
        });
    }
}
