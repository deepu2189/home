<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the data from the user to an array interface
 *
 * @author Deepak Thakur
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class ClientRecommendationTransformer extends Transformer
{

    public function transform(Model $client_recommendation)
    {

        $clientRecommendationTransform = [
            'id'                 => (int) $client_recommendation->id,
            'client_name'        => $client_recommendation->client_name,
            'description'        => $client_recommendation->description,
            'agency'             => $client_recommendation->agency,
            'location'           => $client_recommendation->location,
            'show'               => (bool) $client_recommendation->show,
            'created_at'         => $client_recommendation->created_at,
            'updated_at'         => $client_recommendation->updated_at
         
        ];

        $image = $client_recommendation->clientImage;
        if ($image) {
            $clientRecommendationTransform['client_image'] = [
                'id'                => (int) $image->id,
                'type'              => $image->type,
                'original_name'     => $image->original_name,
                'app_name'          => $image->app_name,
                'extension'         => $image->extension,
                'external_storage'  => (bool) $image->external_storage,
                'external_url'      => $image->external_url,
                'sizes'             => [
                    'original'      =>  $image->getFileUrl('original'),
                    'medium'        =>  $image->getFileUrl('medium'),
                    'small'         =>  $image->getFileUrl('small'),
                    'tiny'          =>  $image->getFileUrl('tiny'),
                ],
                'created_at'        => $image->created_at,
                'updated_at'        => $image->updated_at
            ];
        }


        $clientRecommendationTransform['emptyProfile'] = \App\ClientRecommendation::$emptyProfileImageUrl;


        return $clientRecommendationTransform;
    }
}
