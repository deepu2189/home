<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the data for the assets
 *
 * @author @dpk
 * @since 0.3
 * @package HomeJab
 * @subpackage Transformers
 */
class AssetTypeTransformer extends Transformer
{

    /**
     * Transforms the data to a representable way
     *
     * @param Model $asset
     * @return array
     */
    public function transform(Model $asset)
    {

        $assetTransform = [
            'id'               => (int) $asset->id,
            'name'             => $asset->name,
            'description'      => $asset->description,
            'instructions'     => $asset->instructions,
            'enabled_3d'       => (bool) $asset->enabled_3d,
            'rework'           => (bool) $asset->rework,
            'no_material'      => (bool) $asset->no_material,
            'ewarp_product_id' => $asset->ewarp_product_id,
            'variant_1'        => $asset->variant_1,
            'unit'             => $asset->unit,
            'enabled'          => (bool) $asset->enabled,
            'created_at'       => $asset->created_at,
            'updated_at'       => $asset->updated_at
        ];
        return $assetTransform;
    }
}
