<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the object form a media file into a representable object
 *
 * @author Deepak Thakur
 * @since 0.2
 */
class MediaFileTransformer extends Transformer
{
/**
     * Tramsforms a media file Object into a representation
     *
     * @param Model $mediaFile
     * @return mixed
     * @since 0.2
     */
    
    public function transform(Model $mediaFile)
    {


        $mediaFileTransform = [
            'id'                    => (int) $mediaFile->id,
            'photographer_id'       => (int) $mediaFile->photographer_id,
            'job_id'                => $mediaFile->jobs->pluck('id'),
            'type'                  => $mediaFile->type,
            'original_name'         => $mediaFile->original_name,
            'key'                   => $mediaFile->key,
            'extension'             => $mediaFile->extension,
            'external_storage'      => $mediaFile->external_storage,
            'external_url'          => $mediaFile->external_url,
            'metadata'              => json_decode($mediaFile->metadata),
            'published'             => (bool) $mediaFile->published,
            'photo_type'            => $mediaFile->photo_type,
            'copied_to_amazon'      => (bool) $mediaFile->copied_to_amazon,
            'created_at'            => $mediaFile->created_at,
            'updated_at'            => $mediaFile->updated_at,
            'deleted_at'            => $mediaFile->deleted_at,
            'storage'               => $mediaFile->storage,
            'batch_id'              => $mediaFile->batch_id
            ];



        $property_has_mediafiles = $mediaFile->propertyHasMediaFile;
        
        if ($property_has_mediafiles) {
            $mediaFileTransform['position'] = (int) $property_has_mediafiles->position;
            $mediaFileTransform['property_has_mediafile_id'] = (int) $property_has_mediafiles->id;
        }
        
        $homePhoto = $mediaFile->homePhoto;
        
        if ($homePhoto) {
            $mediaFileTransform['homePhoto'] = $homePhoto;
        }
       
        return $mediaFileTransform;
    }
}
