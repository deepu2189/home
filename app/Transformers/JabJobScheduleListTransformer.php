<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\JabJobSchedule;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabJobScheduleListTransformer extends Transformer
{
    public function transform(Model $jobschedule)
    {

        $JabJobScheduleListTransform = [
            'JobScheduleId'       => $jobschedule->JobScheduleId,
            'JobId'               => $jobschedule->JobId,
            'HJJobID'             => $jobschedule->HJJobID,
            'ScheduleDate'        => $jobschedule->ScheduleDate,
            'ScheduleTime'        => $jobschedule->ScheduleTime,
            'StatusID'            => $jobschedule->StatusID,
            'IsActive'            => $jobschedule->IsActive,
            'PackagePay'          => $jobschedule->PackagePay,
            'PackageInstructions' => $jobschedule->PackageInstructions,
            'isMailSent'          => $jobschedule->isMailSent,

        ];
        return $JabJobScheduleListTransform;

    }
}
?>