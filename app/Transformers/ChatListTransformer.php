<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

use App\Transformers\AssetTypeTransformer;
use App\Transformers\PackageHasAssetTransformer;

use Log;


class ChatListTransformer extends Transformer
{

    public function transform(Model $message)
    {

        $chatTransform = [
            'MessageId' => (int)$message->MessageId,
            'JobId' => $message->JobId,
            'MessageText' => $message->MessageText,
            'Messagetime' => $message->Messagetime,
            'IsRead' => $message->IsRead,
            'isClosed' => $message->isClosed,

        ];

        $sender_info = $message->SenderJabuser;
        $chatTransform['SenderName'] = isset($sender_info->Name)?$sender_info->Name:'';

        $receiver_info = $message->ReceiverJabuser;
        $chatTransform['ReciverName'] = isset($receiver_info->Name)?$receiver_info->Name:'';


        return $chatTransform;
    }
}

?>