<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\MediaFileTransformer;

/**
 * Transforms the object form a homeOurWork into a representable object
 *
 * @author Mariana Perez Elena
 */
class HomeOurWorkTransformer extends Transformer
{

    /**
     *
     * @param MediaFileTransformer $MediaFileTransformer
     */
    public function __construct(MediaFileTransformer $MediaFileTransformer)
    {
        $this->MediaFileTransformer = $MediaFileTransformer;
    }

    /**
     *
     * @param \HomeJab\Transformers\model $ourWork
     * @return type
     */
    public function transform(Model $ourWork)
    {
        $ourWorkTransform = [
            'id' => (int) $ourWork->id,
            'position' => (int) $ourWork->position
        ];


        $property = $ourWork->Property;
        if ($property) {
            $ourWorkTransform['property_id'] = (int) $property->id;
            $ourWorkTransform['title'] = $property->title;
            $ourWorkTransform['slug'] = $property->slug;

            if ($property->preview_id) {
                $mediaPreview = \App\Mediafile::find(intval($property->preview_id));
                if ($mediaPreview) {
                    $property->previewImage = $this->MediaFileTransformer->transform($mediaPreview);
                } else {
                    $property->previewImage = false;
                }

                $ourWorkTransform['preview_Image'] = $property->previewImage['metadata'];
            } else {
                $ourWorkTransform['preview_Image'] = null;
            }
        }
        return $ourWorkTransform;
    }
}
