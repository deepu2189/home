<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class AWSPathTransformer extends Transformer
{


    /**
     * original files folder into amazon
     *
     * @var string
     */
    public static $originalAWSFolder = 'ORIGINALS';

    /**
     * Job files forder into amazon s3 bucket
     *
     * @var string
     */
    public static $JobAWSFolder = 'JOBS';

    /**
     * Property files forder into amazon s3 bucket
     *
     * @var string
     */
    public static $propertyAWSFolder = 'PROPERTY';
    /**
     * Property files forder into amazon s3 bucket
     *
     * @var string
     */
    public static $userProfileAWSFolder = 'UserProfileVideo';
    /**
     * Property files forder into amazon s3 bucket
     *
     * @var string
     */
    public static $homePhotosAWSFolder = 'HomePhotos';

    /**
     * Folder name inside property for the compressed files
     *
     * @var string
     */
    public static $compressedPropertyAWSFolder = 'COMPRESSED';
    
    /**
     * Order extra files folder into amazon s3 bucket
     *
     * @var string
     */
    public static $extraFilesAWSFolder = 'ExtraFiles';

    /**
     * Name for the bucket
     *
     * @var string
     */
    private $bucket_name = null;

    /**
     *
     */
    public function __construct()
    {
        $this->bucket_name = Config::get('aws.credentials.bucket');
    }

    /**
     *
     * @param integer $job_id
     * @return string
     */
    public function getJobOriginalsUrl($job_id)
    {
        return self::$JobAWSFolder . '/' . $job_id . '/' . self::$originalAWSFolder . '/';
    }

    /**
     *
     * @param integer $property_id
     * @param string $photo_type
     * @return string
     */
    public function getPropertyUrl($property_id, $assetType, $photo_type = null)
    {
        if ($assetType === \App\Mediafile::$imageType) {
            return self::$propertyAWSFolder . '/' . $property_id . '/' . $assetType . '/' . $photo_type . '/';
        }

        return self::$propertyAWSFolder . '/' . $property_id . '/' . $assetType . '/';
    }
    /**
     *
     * @param integer $user_id
     * @return string
     */
    public function getUserUrl($user_id)
    {
        return self::$userProfileAWSFolder . '/' . $user_id . '/';
    }
    /**
     *
     * @return string
     */
    public function getHomePhotosUrl()
    {
        return self::$homePhotosAWSFolder  . '/';
    }

        /**
     *
     * @param integer $order_id
     * @return string
     */
    public function getExtraFilesUrl($order_id)
    {
        return self::$extraFilesAWSFolder. '/'. $order_id . '/';
    }
    
    private $url_to_bucket_map = [
        'cdn.dev.m2.esoftsystems.com' => '?'
    ];

    /**
     * Tries to interpretate the bucket name from the url passed and it's key
     *
     * @param string $url
     * @return array
     */
    public function getValuesFromUrl($url)
    {
        $url = str_replace('http://', '', $url);
        $url = str_replace('https://', '', $url);
        $parts = explode('/', $url);

        $bucket_name = array_shift($parts);
        if ($bucket_name == 's3.amazonaws.com') {
            $bucket_name = array_shift($parts);
        } else if (in_array($bucket_name, array_keys($this->url_to_bucket_map))) {
            $bucket_name = $this->url_to_bucket_map[$bucket_name];
        }
        $key = implode('/', $parts);
        return array(
            'Bucket' => $bucket_name,
            'Key' => $key
        );
    }

    /**
     *
     * @param Model $url
     * @return type
     */
    public function transform(Model $url)
    {
        return $url;
    }

    /**
     * Returns the key for the compressed files requested for a property
     *
     * @param string $filename
     * @return string
     */
    public static function getCompressedPropertyPath($filename)
    {
        return self::$propertyAWSFolder . '/'  . self::$compressedPropertyAWSFolder . '/' . $filename;
    }
    /**
     * Returns the key for the compressed files requested for a property
     *
     * @param string $filename
     * @return string
     */
    public static function getCompressedExtraFilesPath($filename)
    {
        return self::$extraFilesAWSFolder . '/'  . self::$compressedPropertyAWSFolder . '/' . $filename;
    }
}
