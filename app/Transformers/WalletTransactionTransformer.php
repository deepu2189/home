<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * @author harihar.kushwaha     <harihar.kushwaha@srmtechsol.com>
 * @author Dario Grau <dario@serfe.com>
 * @since 1.1.5
 * @package HomeJab
 * @subpackage Transformers
 */
class WalletTransactionTransformer extends Transformer
{


    /**
     *
     * @param HomeJab\Models\WalletTransaction $walletTransaction
     * @return type
     */
    public function transform(Model $walletTransaction)
    {

        $walletTransactionTransform = [
            'id'                => (int)   $walletTransaction->id,
            'user_id'           => (int)   $walletTransaction->user_id,
            'description'       =>         $walletTransaction->description,
            'type'              =>         $walletTransaction->type,
            'amount'            => (float) $walletTransaction->amount,
            'date'              =>         $walletTransaction->date,
            'created_at'        =>         $walletTransaction->created_at,
            'updated_at'        =>         $walletTransaction->updated_at,
            
        ];
        return $walletTransactionTransform;
    }
    
    /**
     *
     * @param type $items
     * @param array $expand
     * @return type
     */
    public function transformCollectionAndExpand($items, $expand = [])
    {
        return $items->map(function ($item) use ($expand) {
            return $this->transform($item, $expand);
        });
    }
}
