<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

use App\Transformers\AssetTypeTransformer;
use App\Transformers\PackageHasAssetTransformer;
use Illuminate\Support\Facades\Config;

use Log;

/**
 *
 * @author Nicolas Taglienti <nicolas@serfe.com>
 * @since 0.4
 * @package HomeJab
 * @subpackage Transformers
 */
class PackageListAvaliablesTransformer extends Transformer
{


    /**
     *
     * @param AssetTypeTransformer $assetTypeTransformer
     */
    public function __construct(AssetTypeTransformer $assetTypeTransformer, PackageHasAssetTransformer $packageHasAsset)
    {
        $this->assetTypeTransformer = $assetTypeTransformer;
        $this->packageHasAssetsTransformer = $packageHasAsset;
    }


    /**
     *  @apiDefine PackagesResponse
     *  @apiSuccess {Object[]}    standard_packages
     *  @apiSuccess {Object[]}    standard_packages.package
     *  @apiSuccess {int}     standard_packages.package.id                  Package id
     *  @apiSuccess {string}  standard_packages.package.name                Package name
     *  @apiSuccess {string}  standard_packages.package.short_description   Package short description
     *  @apiSuccess {string}  standard_packages.package.delivered_text      Package delivered text
     *  @apiSuccess {string="Standard", "Luxury"} standard_packages.package.package_type        Package package type
     *  @apiSuccess {int}     standard_packages.package.position            Package position
     *  @apiSuccess {float}   standard_packages.package.price               Package price
     *  @apiSuccess {bool}    standard_packages.package.ask_price           If is true, ask contact for price.
     *  @apiSuccess {string}  standard_packages.package.example_slug        Property slug example package.
     *  @apiSuccess {Object[]}    standard_packages.package.preview_image
     *  @apiSuccess {string}      standard_packages.package.preview_image.app_name   name
     *  @apiSuccess {Object[]}    standard_packages.package.preview_image.sizes
     *  @apiSuccess {string}          standard_packages.package.preview_image.sizes.original    URL for original size of package image
     *  @apiSuccess {string}          standard_packages.package.preview_image.sizes.medium      URL for medium size of package image
     *  @apiSuccess {string}          standard_packages.package.preview_image.sizes.small       URL for small size of package image
     *  @apiSuccess {string}          standard_packages.package.preview_image.sizes.tiny        URL for tiny size of package image
     *  @apiSuccess {Object[]}    luxury_packages
     *  @apiSuccess {Object[]}    luxury_packages.package
     *  @apiSuccess {int}     luxury_packages.package.id                  Package id
     *  @apiSuccess {string}  luxury_packages.package.name                Package name
     *  @apiSuccess {string}  luxury_packages.package.short_description   Package short description
     *  @apiSuccess {string}  luxury_packages.package.delivered_text      Package delivered text
     *  @apiSuccess {string="Standard", "Luxury"} luxury_packages.package.package_type        Package package type
     *  @apiSuccess {int}     luxury_packages.package.position            Package position
     *  @apiSuccess {float}   luxury_packages.package.price               Package price
     *  @apiSuccess {bool}    luxury_packages.package.ask_price           If is true, ask contact for price.
     *  @apiSuccess {string}  luxury_packages.package.example_slug        Property slug example package.
     *  @apiSuccess {Object[]}    luxury_packages.package.preview_image
     *  @apiSuccess {string}      luxury_packages.package.preview_image.app_name   name
     *  @apiSuccess {Object[]}    luxury_packages.package.preview_image.sizes
     *  @apiSuccess {string}          luxury_packages.package.preview_image.sizes.original    URL for original size of package image
     *  @apiSuccess {string}          luxury_packages.package.preview_image.sizes.medium      URL for medium size of package image
     *  @apiSuccess {string}          luxury_packages.package.preview_image.sizes.small       URL for small size of package image
     *  @apiSuccess {string}          luxury_packages.package.preview_image.sizes.tiny        URL for tiny size of package image
     *  @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     {
        "data":{
           "standard_packages":{
              "0":{
                 "id":1,
                 "name":"PHOTOGRAPHY",
                 "short_description":"30 professional photos plus free property website.",
                 "delivered_text":"Delivered the next day in high resolution & online versions.",
                 "package_type":"Standard",
                 "position":1,
                 "price":150,
                 "ask_price":false,
                 "example_slug":null,
                 "preview_image":{
                    "app_name":"is1nexp7s3ko4h1000000000jpg",
                    "sizes":{
                       "original":"http:\/\/api.beta.homejab.com\/assets\/images\/package\/3\/2\/is1nexp7s3ko4h1000000000jpg_original.jpeg",
                       "medium":"http:\/\/api.beta.homejab.com\/assets\/images\/package\/3\/2\/is1nexp7s3ko4h1000000000jpg_medium.jpeg",
                       "small":"http:\/\/api.beta.homejab.com\/assets\/images\/package\/3\/2\/is1nexp7s3ko4h1000000000jpg_small.jpeg",
                       "tiny":"http:\/\/api.beta.homejab.com\/assets\/images\/package\/3\/2\/is1nexp7s3ko4h1000000000jpg_tiny.jpeg"
                    }
                 }
              }
           },
           "luxury_packages":{
              "1":{
                 "id":8,
                 "name":"HDR PHOTOS",
                 "short_description":"Highest quality HDR photos.   30 hi-res shots plus virtual dusk.",
                 "delivered_text":"Delivered the next day with free property website included.",
                 "package_type":"Luxury",
                 "position":8,
                 "price":225,
                 "ask_price":false,
                 "example_slug":null
              }
           }
        }
     }


     *
    */

    /**
     * This is the transformer used on the HOME pege.
     * @param HomeJab\Models\Package $package
     * @return type
     */
    public function transform(Model $package)
    {

        $packageTransform = [
            'id'                => (int) $package->id,
            'name'              => $package->name,
            'short_description' => $package->short_description,
            'template_description' => $package->template_description,
            'delivered_text'    => $package->delivered_text,
            'package_type'      => $package->package_type,
            'position'          => (int) $package->position,
            'price'             => (float) $package->price,
            'ask_price'         => (bool) $package->ask_price,
            'example_slug'      => $package->example_slug,
            
        ];


        $image = $package->previewImage;
        if ($image) {
            $packageTransform['preview_image'] = [
                'app_name'          => $image->app_name,
                'sizes'             => [
                    'original'      =>  $image->getFileUrl('original'),
                    'medium'        =>  $image->getFileUrl('medium'),
                    'small'         =>  $image->getFileUrl('small'),
                    'tiny'          =>  $image->getFileUrl('tiny'),
                ],
            ];

          
        }

        if ($package->package_type !== "3D") {
            $packageTransform['ask_price']    = (bool) $package->ask_price;
            $packageTransform['example_slug'] = $package->example_slug;
            $packageTransform['short_description'] = $package->short_description;
            $packageTransform['template_description'] = $package->template_description;
        }

        if ($package->package_type === "3D") {
            //Get Asset Types
            $assets = $package->AssetTypes;
            
            $addons = array('video' => false, 'aerials' => false, 'photos' => false);

           
            if ($package->hasAddonPhotos()) {
                $addons['photos'] = true;
            }
            if ($package->hasAddonAerials()) {
                $addons['aerials'] = true;
            }
            if ($package->hasAddonWalkthroug()) {
                $addons['video'] = true;
            }
            


            if ($assets) {
                $packageTransform['assetsTypes'] = $this->assetTypeTransformer->transformCollection($assets);
                //Get addons of package asset types
                foreach ($assets as $asset) {
                    if ($asset->id == Config::get('app.3d_asset_1500_less_id')) {
                        $addons['sqft'] = "3D SQFT 1500-Less";
                    }
                    if ($asset->id == Config::get('app.3d_asset_1500_3000_id')) {
                        $addons['sqft'] = "3D SQFT 1500-3000";
                    }
                    if ($asset->id == Config::get('app.3d_asset_3000_5000_id')) {
                        $addons['sqft'] = "3D SQFT 3000-5000";
                    }
                    if ($asset->id == Config::get('app.3d_asset_5000_7000_id')) {
                        $addons['sqft'] = "3D SQFT 5000-7000";
                    }
                    if ($asset->id == Config::get('app.3d_asset_7000_10000_id')) {
                        $addons['sqft'] = "3D SQFT 7000-10000";
                    }
                }

                $packageTransform['addons'] = $addons;
            }
        }

        return $packageTransform;
    }
}
