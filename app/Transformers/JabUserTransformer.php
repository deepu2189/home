<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabUser;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabUserTransformer extends Transformer
{
    public function transform(Model $user)
    {
        $JabUserTransform = [
            'UserId'    =>  $user->UserId,
            'Name'      => $user->Name,
            'Email'     => $user->Email,
            'PhoneNo'   => $user->PhoneNo,
            'HomeJabId' => $user->HomeJabId,
            'Address'   => $user->Address,
            'RoleId'    =>  $user->RoleId,
            'RankId'    =>  $user->RankId,
            'IsActive'  => $user->IsActive

        ];
        return $JabUserTransform;
    }

}
?>