<?php

namespace App\Transformers\Webhooks;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\Transformer;

/**
 * Transforms the transactions for the frontend
 *
 * @author Esteban Zeller <esteban@serfe.com>
 * @since 1.7.0
 * @package HomeJab
 * @subpackage Webhooks\Transformers
 */
class AssistantUpdateEmailTransformer extends Transformer
{

    /**
     * Transforms a order for data translation
     *
     * @param Model $user
     * @return array
     */
    public function transform(Model $user)
    {
        $response = [
            'ContactEmail' => null,
            'AssistantEmails' => null
        ];
        $response['ContactEmail'] = $user->email;
        // From the original filtering we already know is an agent or seller
        $assistants = json_decode($user->assistants_emails, true);
        $emails = [];
        if (is_array($assistants)) {
            foreach ($assistants as $email) {
               // $emails[] = $email['email'];
            }
        }
        $response['AssistantEmails'] = join(', ', $emails);
        return $response;
    }
}
