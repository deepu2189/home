<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabJobUser;
use App\Transformers\JabUserTransformer;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabJobUserTransformer extends Transformer
{
    public function __construct(JabUserTransformer $jabUserTransformer)
    {
        $this->JabUserTransformer = $jabUserTransformer;
    }
    public function transform(Model $jabjobuser)
    {
        $JabJobUserTransform = [
            'JobUserId'     => $jabjobuser->JobUserId,
            'JobId'         => $jabjobuser->JobId,
            'JobScheduleId' => $jabjobuser->JobScheduleId,
            'UserId'        => $jabjobuser->UserId,
            'IsActive'      => $jabjobuser->IsActive,
            'Name'          => $jabjobuser->JabUser->Name,
        ];
        return $JabJobUserTransform;
    }

}
?>