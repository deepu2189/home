<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * Transformer for the Property
 *
 * @author Deepak Thakur
 * @since 0.3
 * @package HomeJab
 * @subpackage Transformers
 */
class PropertyTransformer extends Transformer
{

    /**
     * Transforms a property
     *
     * @param Model $property
     * @return array
     */
    public function transform(Model $property)
    {
        
        $propertyTransform = [
            'id'                    => (int) $property->id,
            'order_id'              => (int) $property->order_id,
            'title'                 => $property->title,
            'type'                  => $property->type,
            'address'               => $property->address,
            'slug'                  => $property->slug,
            'zip'                   => $property->zip,
            'city'                  => $property->city,
            'beds'                  => (int)$property->beds,
            'baths'                 => (int)$property->baths,
            'half_baths'                 => (int)$property->half_baths,
            'area'                  => (int)$property->area,
            'price'                 => (float)$property->price,
            'owner_phone'           => $property->owner_phone,
            'owner_email'           => $property->owner_email,
            'views_amount'          => (int)$property->views_amount,
            'published'             => (bool)$property->published,
            'map_longitude'         => $property->map_longitude,
            'map_latitude'          => $property->map_latitude,
            'description'           => $property->description,
            'preview_id'            => (int)$property->preview_id,
            'previewImage'          => $property->previewImage,
            'imported'              => (bool) $property->imported,
            'redirect_url_mls'      => $property->redirect_url_mls,
            'redirect_url_normal'   => $property->redirect_url_normal,
            'created_at'            => $property->created_at,
            'updated_at'            => $property->updated_at
        ];


        
        if (intval($propertyTransform['preview_id']) <= 0) {
            $propertyTransform['preview_id'] = null;
        }
        $agent = $property->Agent;
        if ($agent) {
            $propertyTransform['agent_id']              = (int) $property->agent_id;
            $propertyTransform['agent_name']            = $agent->User->fullname;
            $propertyTransform['agent_fisrtname']       = $agent->User->firstname;
            $propertyTransform['agent_lastname']        = $agent->User->lastname;
            $propertyTransform['agent_user_id']         = (int) $agent->User->id;
            $propertyTransform['agent_email']           = $agent->User->email;
            $propertyTransform['agent_slug']            = $agent->User->slug;
            if ($agent->User->profileVideo) {
                $propertyTransform['agent_profile_video']               = $agent->User->profileVideo;
                $propertyTransform['agent_profile_video']['metadata']   = json_decode($agent->User->profileVideo['metadata'], true);
            }
        }

        $seller = $property->Seller;
        if ($seller) {
            $propertyTransform['seller_id']             = (int) $property->seller_id;
            $propertyTransform['seller_name']           = $seller->User->fullname;
            $propertyTransform['seller_fisrtname']      = $seller->User->firstname;
            $propertyTransform['seller_lastname']       = $seller->User->lastname;
            $propertyTransform['seller_user_id']        = (int) $seller->User->id;
            $propertyTransform['seller_email']          = $seller->User->email;
            $propertyTransform['seller_slug']           = $seller->User->slug;
            if ($seller->User->profileVideo) {
                $propertyTransform['seller_profile_video']               = $seller->User->profileVideo;
                $propertyTransform['seller_profile_video']['metadata']   = json_decode($seller->User->profileVideo['metadata'], true);
            }
        }

        if ($property->Order) {
            $propertyTransform['order'] = $property->Job;
            $propertyTransform['order_status'] = $property->Order->status;
            if ($property->Order->Package) {
                $propertyTransform['order']['package'] = $property->Order->Package;
            }
        }

        if ($property->imported) {
            $propertyTransform['redirect_url_mls']         = Config::get('app.new_domain_mls_seo') . $property->redirect_url_mls;
            $propertyTransform['redirect_url_normal']      = Config::get('app.new_domain_seo') . $property->redirect_url_normal;
        }

        return $propertyTransform;
    }
}
