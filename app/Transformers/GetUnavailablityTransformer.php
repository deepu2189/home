<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabUserUnavailability;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class GetUnavailablityTransformer extends Transformer
{
    public function transform(Model $userUnavailabilty = null,$message = null)
    {
        if($userUnavailabilty == null && $message != null)
            $error = $message;
        else
            $error = null;

        $GetUnavailablityTransform = [
            "UserUnavailabilityID"=> isset($userUnavailabilty->UserUnavailabilityID)?$userUnavailabilty->UserUnavailabilityID:'0' ,
            "UserId"=> isset($userUnavailabilty->UserId)?$userUnavailabilty->UserId:null,
            "StartDate"=> isset($userUnavailabilty->StartDate)?date('m/d/Y',strtotime($userUnavailabilty->StartDate)):null,
            "StartTime"=> isset($userUnavailabilty->StartTime)?$userUnavailabilty->StartTime:null,
            "EndDate"=> isset($userUnavailabilty->EndDate)?date('m/d/Y',strtotime($userUnavailabilty->EndDate)):null,
            "EndTime"=> isset($userUnavailabilty->EndTime)?$userUnavailabilty->EndTime:null,
            "Notes"=> isset($userUnavailabilty->Notes)?$userUnavailabilty->Notes:null,
            "IsDeletePossible"=> isset($userUnavailabilty->IsDeletePossible)?(($userUnavailabilty->IsDeletePossible == 1)?"True":"False"):null,
            "Error"=> $error,
            "JobID"=> isset($userUnavailabilty->JobId)?$userUnavailabilty->JobId:null,
            "Address"=> isset($userUnavailabilty->propertyaddress)?$userUnavailabilty->propertyaddress:null,
        ];
        return $GetUnavailablityTransform;
    }
    public function TransformWithMessage(Model $jobs=null,$message)
    {
        return $this->transform(null,$message);
    }
}
?>