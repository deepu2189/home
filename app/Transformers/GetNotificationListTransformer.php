<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class GetNotificationListTransformer extends Transformer
{
    public function transform(Model $m_message = null,$message = null)
    {
        if($m_message == null && $message != null)
            $error = $message;
        else
            $error = null;

        $GetNotificationListTransform = [
            "MessageId"                                       => isset($m_message->MessageId)?$m_message->MessageId:0,
            "NotificationMessage"                   => isset($m_message->NotificationMessage)?($m_message->NotificationMessage == null?"":$m_message->NotificationMessage):null,
            "JobId"                                               => isset($m_message->JobId)?(string)$m_message->JobId:null,
            "HomeJabID"                                       => isset($m_message->HJJobID)?$m_message->HJJobID:null,
            "IsRead"                                             => isset($m_message->IsRead)?($m_message->IsRead == 1?"True":"False"):null,
            "Error"                                               => $error,
        ];
        return $GetNotificationListTransform;
    }
    public function TransformWithMessage(Model $jobs=null,$message)
    {
        return $this->transform(null,$message);
    }
}
?>
