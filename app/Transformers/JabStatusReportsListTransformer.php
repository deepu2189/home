<?php
namespace App\Transformers;

use App\JabJobs;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabStatusReportsListTransformer extends Transformer
{
    public function transform(Model $reportdata)
    {

        $jabStatusReportsListTransform = [
            'JobId' => $reportdata->JobId,
            'HJJobId' => $reportdata->HJJobId,
            'ContactName' => $reportdata->ContactName,
            'ContactPhone' => $reportdata->ContactPhone,
            'ContactEmail' => $reportdata->ContactEmail,
            'MustHaveShots' => $reportdata->MustHaveShots,
            'ContactOwnerPhone' => $reportdata->ContactOwnerPhone,
            'ContactOwnerEmail' => $reportdata->ContactOwnerEmail,
            'ScheduleSlot' => $reportdata->ScheduleSlot,
            'ScheduleTime' => $reportdata->ScheduleTime,
            'ScheduleForHours' => $reportdata->ScheduleForHours,
            'PropertyAddress' => $reportdata->PropertyAddress,
            'Comments' => $reportdata->Comments,
            'NAME' => $reportdata->NAME,
            'PackagePay' => $reportdata->PackagePay,
            'PackageInstructions' => $reportdata->PackageInstructions,
            'HJPackageName' => $reportdata->HJPackageName,
            'status' => $reportdata->Status,
            'ScheduleDate' => $reportdata->ScheduleDate,
            'UpdatedOn'     =>$reportdata->UpdatedOn
        ];

        return $jabStatusReportsListTransform;
    }
}
?>