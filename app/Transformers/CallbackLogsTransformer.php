<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the Callback Log for the frontend
 *
 * @author Deepak Thakur
 * @since 1.0
 * @package HomeJab
 * @subpackage Transformers
 */
class CallbackLogsTransformer extends Transformer
{

    /**
     * Transforms Ewarp log
     *
     * @param Model $log
     * @return array
     */
    public function transform(Model $log)
    {

        $logTransform = [
            'id'                    => (int) $log->id,
            'order_id'              => (int) $log->order_id,
            'callback_status_id'    => (int) $log->callback_status_id,
            'payload'               => json_decode($log->payload),
            'attempt'               => $log->attempt,
            'type'                  => $log->type,
            'created_at'            => $log->created_at,
            'updated_at'            => $log->updated_at
        ];

        return $logTransform;
    }
}
