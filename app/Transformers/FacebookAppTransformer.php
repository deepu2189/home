<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

/**
 * Transforms the data for the assets
 *
 * @author Esteban Zeller <esteban@serfe.com>
 * @since 0.3
 * @package HomeJab
 * @subpackage Transformers
 */
class FacebookAppTransformer extends Transformer
{

    /**
     * Transforms the data to a representable way
     *
     * @param Model $asset
     * @return array
     */
    public function transform(Model $facebookApp)
    {

        $facebookAppTransform = [
            'id'               => (int) $facebookApp->id,
            'app_id'             => $facebookApp->app_id,
            'app_secret'        => $facebookApp->app_secret,
        ];
        return $facebookAppTransform;
    }
}
