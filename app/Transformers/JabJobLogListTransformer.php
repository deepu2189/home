<?php
namespace App\Transformers;
use App\JobLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class JabJobLogListTransformer extends Transformer
{
    public function transform(Model $joblog)
    {
        $JabJobLogListTransform = [
            'LogId'          => $joblog->LogId,
            'JobId'          => $joblog->JobId,
            'JobScheduleId'  => $joblog->JobScheduleId,
            'Description'    => $joblog->Description,
            'CreatedBy'      => $joblog->CreatedBy,
            'LogId'          => $joblog->LogId,

        ];

        return $JabJobLogListTransform;
    }
}
?>