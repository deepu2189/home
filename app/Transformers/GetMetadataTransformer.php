<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabMetadata;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class GetMetadataTransformer extends Transformer
{
    public function transform(Model $metadata)
    {
        $MetadataListTransform = [
            'metaID'            => $metadata->metaID,
            'metaTitle'         => $metadata->metaTitle,
            'metaKeywords'      => $metadata->metaKeywords,
            'metaDescription'   => $metadata->metaDescription,

        ];
        return $MetadataListTransform;
    }

}
?>