<?php
namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use App\JabUser;

/**
 * Transforms the data from the user to an array interface*
 *
 * @since 0.1
 * @package HomeJab
 * @subpackage Transformers
 */
class PhotograherListTransformer extends Transformer
{
    public function transform(Model $user)
    {

        $photographerListTransform = [
            'UserId'    => $user->UserId,
            'Name'      => $user->Name,
            'Email'     => $user->Email,
            'Password'  => $user->Password,
            'HomeJabId' => $user->HomeJabId,
            'PhoneNo'   => $user->PhoneNo,
            'Address'   => $user->Address,
            'State'     => $user->state,
            'City'      => $user->city,
            'Zip'       => $user->Zip,
            'IsActive'  => $user->IsActive,
            'RankId'    => $user->RankId,
            'Rank'      => $user->Rank,
            'ProfilePic'=> ($user->ProfilePic != '' && file_exists(Config('constants.ProfilePicUrl').$user->UserId."/".$user->ProfilePic))?Config('constants.ProfilePicUrl').$user->UserId."/".$user->ProfilePic:Config('constants.ProfilePicUrl')."/default.png",
			'Profile'=> file_exists(Config('constants.ProfilePicUrl').$user->UserId."/".$user->ProfilePic)?'true':false
        ];
	    return $photographerListTransform;
    }

}
?>