<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

use App\AllJob;
use App\Order;
use App\PackageHasAssetType;

use Log;

/**
 * Transforms the object form a job into a representable object
 *
 * @author Deepak Thakur
 * @since 0.2
 */
class JobUploadMediaTransformer extends Transformer
{

    /**
     * Tramsforms an Job Object into a representation
     *
     * @param Model $job
     * @return mixed
     * @since 0.2
     */
    public function transform(Model $job)
    {
        
        $filesAmount = \App\JobHasMediaFile::where('job_id', '=', $job->id)->count();

        $jobTransform = [
            'id'                    => (int) $job->id,
            'order_id'              => (int) $job->order_id,
            'photographer_id'       => (int) $job->photographer_id,
            'comments'              =>       $job->comments,
            'contact_email'         =>       $job->contact_email,
            'contact_phone'         =>       $job->contact_phone,
            'contact_owner_email'   =>       $job->contact_owner_email,
            'contact_owner_phone'   =>       $job->contact_owner_phone,
            'property_address'      =>       $job->property_address,
            'property_details'      =>       $job->property_details,
            'quantity'              => (int) $job->quantity,
            'max_files_amount'      => (int) $job->max_files_amount,
            'already_uploaded'      =>       $filesAmount,
            'scheduling_date'       =>       $job->scheduling_date,
            'scheduling_time'       =>       $job->scheduling_time,
            'status'                =>       $job->status,
            'status_label'          =>       AllJob::$status[$job->status]['label'],
            'callback_sent'         => (bool)$job->callback_sent,
            'created_at'            =>       $job->created_at,
            'updated_at'            =>       $job->updated_at,
        ];
        
        // Very important
        if (in_array(
            $job->status,
            array(
                AllJob::$status['new']['key'],
                AllJob::$status['assigned']['key'],
                AllJob::$status['scheduled']['key'],
                AllJob::$status['in_progress']['key']
            )
        )) {
            $jobTransform['workable'] = true;
        } else {
            $jobTransform['workable'] = false;
        }
        $keys = array_keys(\App\AllJob::$schedulingTime);
        if (in_array($job->scheduling_time, $keys)) {
            $jobTransform['scheduling_time_label'] = \App\AllJob::$schedulingTime[$job->scheduling_time]['label'];
        } else {
            $jobTransform['scheduling_time_label'] = $job->scheduling_time;
        }
        
                                                                   
        $asset=$job->assetType;
        $jobTransform['assetType']=[];
        if ($asset) {
            $jobTransform['assetType']['name'] = $asset->name;
            $jobTransform['assetType']['description'] = $asset->description;
            $jobTransform['assetType']['instructions'] = $asset->instructions;
            $jobTransform['assetType']['ewarp_product_id'] = $asset->ewarp_product_id;
            if (is_null($asset->ewarp_product_id)) {
                $jobTransform['workable'] = false;
            }

            $order = Order::find($job->order_id);
            if ($order) {
                $packageId = $order->package_id;
                $packageHasAsset = new PackageHasAssetType();
                $amount_originals = $packageHasAsset->getOriginalFilesAmount($packageId, $asset->id);
                 
                $jobTransform['assetType']['minimal_amount'] = ((intval($amount_originals) - intval($filesAmount)) <= 0) ? 0 : intval($amount_originals) - intval($filesAmount);
                $jobTransform['assetType']['minimal_amount_for_production'] = $amount_originals;
            }
        }

        $agent = $job->Order->Agent;
        if ($agent) {
            $user = $job->Order->Agent->User;
        } else {
            $user = $job->Order->Seller->User;
        }
        
        if ($user) {
            $jobTransform['agent_user']['fullname']          = $user->fullname;
            $jobTransform['agent_user']['contact_phone']     = $user->contact_phone;
            $jobTransform['agent_user']['contact_email']     = $user->email;
        }

        
        $must_have_shots = $job->Order->must_have_shots;
        if ($must_have_shots) {
            $jobTransform['must_have_shots'] = $must_have_shots;
        }

        $media = $job->mediafiles;
        $jobTransform['mediafiles']=[];
        if ($media) {
            foreach ($media as $file) {
                $jobTransform['mediafiles'][]['original_name']        = $file['original_name'];
            }
        }
       
        return $jobTransform;
    }
}
