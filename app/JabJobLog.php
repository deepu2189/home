<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use App\Observers\CustomeObservers;

class JabJobLog extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';


    protected $table = "tbl_joblog";

    protected $primaryKey = 'LogId';

    //public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'JobId', 'JobScheduleId', 'Description', 'CreatedBy', 'CreatedOn'
    ];
    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new CustomeObservers());

        parent::boot();
    }
    public static function Insert($POST)
    {
	    # add job log
	    $_post                  = array();
	    $_post['JobId']         = $POST["JobId"];
	    $_post['JobScheduleId'] = $POST["JobScheduleId"];
	    $_post['Description']   = $POST["Description"];
	    //$_post['LogId']         = $POST["LogId"];

	    $objJoblog = new JabJobLog();
	    $objJoblog->fill($_post);


	   $objJoblog->save();
	   return true;

    }
}
?>