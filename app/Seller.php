<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;



class Seller extends Model
{
    
    protected $fillables =[
        'user_id'
    ];

    protected $table = 'sellers';
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    
    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
    
    /*
     * Save buyer that has contacted the seller 
     * 
     */
    public static function registerBuyerOnInfusionSoft($data)
    {
        Event::fire(new ReaOrSellerContactFormSubmit($data));
    }
    
    /**
     * Get seller data by agent_id
     * @param integer $id
     * @param \HomeJab\Model\Seller
     */
    public static function getSeller($id)
    {
        return self::find($id);
    }
    
    /**
     * Get user_id by seller_id
     * @param integer $id
     * @return integer
     */
    public static function getUserId($id)
    {
        return self::query()->where('id', $id)->value('user_id');
    }

}
