<?php

namespace App;

use App\Notifications\CustomResetPasswordNotification;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Config;
use Laravel\Passport\HasApiTokens;
use Carbon\Carbon;
use Event;
use Log;
use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Setting;

use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use Mockery\Exception;


/**
 * Model for the users
 *
 * @author Luciano Masuero <luciano@serfe.com>
 * @since 0.1
 * @pacakge App
 * @subpackage Models
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{

    use HasApiTokens,
        Notifiable,
        Authenticatable,
        Authorizable,
        CanResetPassword;

    public static $rulesOnRegister = [
        'firstname' => 'required',
        'lastname' => 'required',
        'contact_phone' => 'required|min:10|integer',
        'email' => 'required|email|unique:users',
        'password' => 'required|confirmed|min:6',
        'account_type' => 'required|in:rea,hjph,su,seller',
    ];
    public static $rulesOnArtistRegister = [
        'firstname' => 'required',
        'lastname' => 'required',
        'contact_phone' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        'account_type' => 'required|in:hjph',
        'city' => 'required'
    ];
    public static $rulesOnSellerRegister = [
        'firstname' => 'required',
        'lastname' => 'required',
        'contact_phone' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        'account_type' => 'required|in:rea,seller',
    ];
    public static $rulesOnPasswordUpdate = [
        'password' => 'required',
        'new_password' => 'required|confirmed|min:6',
    ];
    public static $rulesOnPasswordRecovery = [
        'email' => 'required|email',
    ];
    public static $rulesOnPasswordReset = [
        'password' => 'required|confirmed|min:6',
    ];
    public static $rulesOnPasswordResetBySU = [
        'new_password' => 'required|confirmed|min:6',
    ];
    public static $rulesOnSubmitEmail = [
        'email' => 'required|email',
    ];

    /**
     * @apiDefine SU SuperUser profile
     * This profile have access to all the functions in the system. It's the administrator of the system
     */
    /**
     * @apiDefine REA Real State Agent profile
     *
     */
    /**
     * @apiDefine Seller Seller profile
     *
     */
    /**
     * @apiDefine HJPH HomeJab Photographer profile
     *
     */

    /**
     * @apiDefine Visitor Normal visitor of the website
     * This profile have access to all the public sections of the page
     */
    public static $account_type = [
        'rea' => [
            'key' => 'rea',
            'label' => 'Real Estate Agent'
        ],
        'hjph' => [
            'key' => 'hjph',
            'label' => 'Photographer'
        ],
        'su' => [
            'key' => 'su',
            'label' => 'Administator'
        ],
        'seller' => [
            'key' => 'seller',
            'label' => 'Seller'
        ],
    ];

    /**
     * Empty picture for profile image
     * @var string
     */
    public static $emptyProfileImageUrl = '/assets/images/empty_profile.svg';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'slug',
        'email',
        'contact_phone',
        'contact_text',
        'bio',
        'account_type',
        'password',
        'confirmation_code',
        'profile_image_id',
        'logo_image_id',
        'assistants_emails',
        'company_name',
        'stripe_id',
        'referral_code',
        'has_referral_amount',
        'referral_amount',
        'commission_percent',
        'current_wallet_amount',
        'city',
        'mortgage',
        'referral_code_referenced'
    ];

    /**
     * Fields that area allowed to be filled at update
     * @var array
     */
    public static $fillableOnUpdateAdmin = [
        'firstname',
        'lastname',
        'slug',
        'email',
        'contact_phone',
        'contact_text',
        'bio',
        'profile_image_id',
        'profile_video_id',
        'logo_image_id',
        'assistants_emails',
        'company_name',
        'referral_code',
        'has_referral_amount',
        'referral_amount',
        'commission_percent',
        'mortgage',
        'referral_code_referenced'
    ];

    /**
     *
     * @var array
     */
    public static $fillableOnUpdate = [
        'firstname',
        'lastname',
        'slug',
        'email',
        'contact_phone',
        'contact_text',
        'bio',
        'profile_image_id',
        'profile_video_id',
        'logo_image_id',
        'assistants_emails',
        'company_name',
        'city',
        'referral_code',
        'stripe_id',
        'mortgage'
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /*
     * Validation rules for update
     *
     * @return array
     */


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token));
    }


    public function getRulesForUpdate()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'contact_phone' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->id,
            'referral_amount' => 'numeric',
            'comm_percent' => 'numeric',
        ];
    }

    /**
     * The User may have one Agent
     *
     * @return type
     */
    public function agent()
    {
        return $this->hasOne('App\Agent');
    }

    /**
     * The User may have one Photographer
     *
     * @return type
     */
    public function photographer()
    {
        return $this->hasOne('App\Photographer');
    }

    /**
     * The User may have one Admin
     *
     * @return type
     */
    public function admin()
    {
        return $this->hasOne('App\Admin');
    }

    /**
     * The User may have one Seller
     *
     * @return type
     */
    public function seller()
    {
        return $this->hasOne('App\Seller');
    }

    /**
     * The User may have one Referral
     *
     * @return type
     */
    public function userReferral()
    {
        return $this->hasOne('App\UserReferral', 'user_id');
    }

    /**
     * The User may have a image profile
     *
     * @return type
     */
    public function profileImage()
    {
        return $this->belongsTo('App\ImageFile', 'profile_image_id');
    }

    /**
     * The User may have a video profile
     *
     * @return type
     */
    public function profileVideo()
    {
        return $this->belongsTo('App\Mediafile', 'profile_video_id');
    }

    /**
     * The User may have logo image
     *
     * @return type
     */
    public function logoImage()
    {
        return $this->belongsTo('App\ImageFile', 'logo_image_id');
    }

    /**
     * Check if the user is an admin
     *
     * @return type
     */
    public function isAdmin()
    {
        return $this->account_type === self::$account_type['su']['key'];
    }

    /**
     * Check if the user is an agent
     *
     * @return type
     */
    public function isAgent()
    {
        return $this->account_type === self::$account_type['rea']['key'];
    }

    /**
     * Check if the user is an Photographer
     *
     * @return type
     */
    public function isPhotographer()
    {
        return $this->account_type === self::$account_type['hjph']['key'];
    }

    /**
     * Check if the user is a Seller
     *
     * @return type
     */
    public function isSeller()
    {
        return $this->account_type === self::$account_type['seller']['key'];
    }

    /**
     * Register user and create necessary account type
     *
     * @param type $request
     */
    public static function register($data, $auto_activate = false)
    {
        $account_type = $data['account_type'];
       \DB::beginTransaction();

        $fullname = $data['firstname'] . " " . $data['lastname'];
        $data['slug'] = str_slug($fullname, "-");

        if (array_key_exists('assistants_emails', $data)) {
            $data['assistants_emails'] = json_encode($data['assistants_emails']);
        }

        $user = new User();
        $user->fill($data);

        if ($auto_activate) {
            // No confirmation will be needed in this case.
            $user->account_verified = true;
        } else {
            //Set confirmation code hash
            $user->confirmation_code = self::generateRandomHash();
        }

        if ($user->save()) {
            $result = false;
            switch ($account_type) {
                case self::$account_type['rea']['key']:
                    $agent = new Agent();
                    $result = $user->agent()->save($agent);
                    break;

                case self::$account_type['hjph']['key']:
                    $photographer = new Photographer();
                    $result = $user->photographer()->save($photographer);
                    break;

                case self::$account_type['su']['key']:
                    $admin = new Admin();
                    $result = $user->admin()->save($admin);
                    break;

                case self::$account_type['seller']['key']:
                    $seller = new Seller();
                    $result = $user->seller()->save($seller);
                    break;

                default:
                    //Treat error
                    break;
            }

            // Update referral code
            // Referral code
            $fname = trim($user->firstname);
            if (!empty($fname)) {
                $namecode = strtoupper(substr($fname, 0, 2));
            }
            $referralCode = User::referalCode($user->id, $namecode);
            $referralAmount = User::getReferralAmountSettings();
            $user->referral_code = $referralCode;
            $user->has_referral_amount = '1';
            $user->referral_amount = $referralAmount;
            $user->commission_percent = 10;
            $user->save();

            if ($result) {
                \DB::commit();
                return $user;
            }
        }

        \DB::rollback();

        return false;
    }

    /**
     * It generates a random alphanumeric hash , if the length is not specified is 30
     *
     * @param type $length
     * @return string
     */
    public static function generateRandomHash($length = 30)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Update user based on $data received as parametter
     *
     * @param type $data
     * @return boolean
     */
    public function profileUpdate($data)
    {
        DB::beginTransaction();

        if ($data['firstname'] && $data['lastname']) {
            $fullname = $data['firstname'] . " " . $data['lastname'];
            $data['slug'] = str_slug($fullname, "-");
        }
        $this->fill($data);

        if (array_key_exists('assistants_emails', $data)) {
            $this->assistants_emails = json_encode($data['assistants_emails']);
        }

        if ($this->save()) {
            DB::commit();
            return true;
        }

        DB::rollback();
        return false;
    }

        /**
     * Update user based on $data received as parametter
     *
     * @param type $data
     * @return boolean
     */
    public function mortgageUpdate()
    {
        DB::beginTransaction();
        $mortgage = $this->mortgage;
        $this->mortgage= !$mortgage;

        if ($this->save()) {
            DB::commit();
            return true;
        }

        DB::rollback();
        return false;
    }

    /**
     * Update user password based on $data received as parametter
     *
     * @param type $data
     * @return boolean
     */
    public function passwordUpdate($data)
    {
        DB::beginTransaction();

        $this->password = $data['new_password'];

        if ($this->save()) {
            DB::commit();
            return true;
        }

        DB::rollback();
        return false;
    }

    /**
     * Update user password and delete reset password token
     *
     * @param array $data
     * @param string $token
     * @return boolean
     */
    public function passwordReset($data, $email)
    {
        DB::beginTransaction();

        $this->password = $data['password'];

        if ($this->save()) {
            if (DB::table('password_resets')->where('email', $email)->delete()) {
                DB::commit();
                return true;
            }
        }

        DB::rollback();
        return false;
    }

    /**
     * Password mutator for hashing
     *
     * @param type $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }


    

    /**
     * Firstname mutator for hashing
     *
     * @param type $firstname
     */
    public function setFirstnameAttribute($firstname)
    {
        $this->attributes['firstname'] = $firstname;
    }

    /**
     * Lastname mutator for hashing
     *
     * @param type $lastname
     */
    public function setLastnameAttribute($lastname)
    {
        $this->attributes['lastname'] = $lastname;
    }

    /**
     * Mutator for create slug url
     *
     * @param string $title
     * @return string
     */
    public function setSlugAttribute()
    {
        if ($this->attributes['firstname'] && $this->attributes['lastname']) {
            $fullname = $this->attributes['firstname'] . " " . $this->attributes['lastname'];
            $slug = str_slug($fullname, "-");

            // check if user exists
            if (array_key_exists('id', $this->attributes)) {
                $user = User::where('id', '=', $this->attributes['id'])->first(['firstname', 'lastname']);


                if ($user) {
                    if ($this->attributes['firstname'] === $user->firstname &&
                            $this->attributes['lastname'] === $user->lastname) {
                        return true;
                    }
                }
            }

            // slug has changed or is new user
            // incremental number for equal slugs
            $inc = 1;
            while (User::where('slug', $slug)->count() > 0) {
                $slug = str_slug($fullname, "-") . "-" . $inc;
                $inc++;
            }
            $this->attributes['slug'] = $slug;
        }
    }

    /**
     * Accesor for created at date
     *
     * @param type $date
     * @return type
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param type $date
     * @return type
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Return complete name of the user concatenation fistname and lastname
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->attributes['firstname'] . ' ' . $this->attributes['lastname'], ', ');
    }

    /**
     * Returns a user by the confirmation code
     *
     * @param type $confirmationCode
     * @return \App\User
     */
    public static function getByConfirmationCode($confirmationCode)
    {
        return self::where('confirmation_code', $confirmationCode)->first();
    }

    /**
     * Returns a user that match the given email
     *
     * @param type $email
     * @return \App\User
     */
    public static function getByEmail($email)
    {
        return self::where('email', $email)->first();
    }

    /**
     * Get user by password reset token
     *
     * @param type $token
     * @return type
     */
    public static function getByPasswordResetToken($token)
    {
            
        $email = DB::table('password_resets')->where('token', $token)->value('email');
        return self::getByEmail($email);
    }

    /**
     * Activate a user account current user account
     *
     * @return type
     */
    public function activateUserAccount()
    {
        DB::beginTransaction();
        //change data to activate the user account
        $this->confirmation_code = null;
        $this->account_verified = 1;
        //save data and return the result
        if ($this->save()) {
            DB::commit();
            return true;
        } else {
            DB::rollback();
            return false;
        }
    }

    /**
     * Get all user by his account type
     *
     * @param string $account_type  {rea|su|hjph}
     * @return Array
     */
    public static function getAllByAccountType($account_type)
    {
        return self::query()->where('account_type', $account_type)->get();
    }

    /**
     * Returns the emails of the assitants for the emails jobs.
     *
     * @return type
     */
    public function getAssistantsEmails()
    {
        $assistants = json_decode($this->assistants_emails, true);
        $response = [];
        if (is_array($assistants)) {
            foreach ($assistants as $email) {
                $response[] = [$email['email'] => $email['email']];
            }
        }
        return $response;
    }

    /**
     * Create a customer on Stripe
     *
     * @param $data = order data
     */
    public static function createStripeCustomer($data, $user_id)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $response = null;
        $stripe_data = null;
        $user = null;

        if (array_key_exists('agent_id', $data) && $data['agent_id'] !== null) {
            $stripe_data = array(
                "description" => "Customer for " . $data['agent_email'],
                "source" => $data['payment']['id'],
                "email" => $data['agent_email'],
                'metadata' => [
                    'agent_id' => $data['agent_id'],
                ]
            );

            $agent = Agent::find($data['agent_id']);
            $user = User::find($agent->user_id);
        } else if (array_key_exists('seller_id', $data) && $data['seller_id'] !== null) {
            $stripe_data = array(
                "description" => "Customer for " . $data['seller_email'],
                "source" => $data['payment']['id'],
                "email" => $data['seller_email'],
                'metadata' => [
                    'seller_id' => $data['seller_id'],
                ]
            );

            $seller = Seller::find($data['seller_id']);
            $user = User::find($seller->user_id);
        }

        try {

            $customer = \Stripe\Customer::create($stripe_data);

        } catch (\Stripe\Error\Card $e) {
            Log::error("Customer cannot be created");
            $body = $e->getJsonBody();
            $err = $body['error'];

            $response['success'] = false;
            $response['error_code'] = $err['code'];
            $response['message'] = $err['message'];

            return $response;
        }

        if ($customer->id) {
            DB::beginTransaction();

            $user->stripe_id = $customer->id;

            if ($user->save()) {
                DB::commit();
                $response['success'] = true;
                $response['user'] = $user;
                return $response;
            } else {
                DB::rollback();
                $customer->delete();
                $response['success'] = false;
                return $response;
            }
        }

        $response['success'] = false;
        return $response;
    }

    /**
     * Create or retrieve the card passed on stripe token
     *
     * @param $token_id = stripe token id
     * @param $customer_id = stripe customer id
     * @return /Stripe/Card
     */
    public static function createOrRetrieveStripeCard($token_id, $customer_id)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        // generate a token with private key, so i can see card fingerprint
        $new_token = \Stripe\Token::retrieve($token_id);

        $customer = \Stripe\Customer::retrieve($customer_id);

        $cards = $customer->sources->data;

        // search if card of frontend order exists on customer's cards
        foreach ($cards as $card) {
            if ($card->fingerprint === $new_token['card']['fingerprint']) {
                return $customer->sources->retrieve($card->id);
            }
        }

        return $customer->sources->create(["source" => $new_token['id']]);
    }

    /*
     * Register User on InfusionSoft
     *
     */

    public static function registerUserOnInfusionSoft($data)
    {
        Event::fire(new UserCreated($data));
    }

    /*
     * Update Rea on InfusionSoft
     *
     */

    public static function updateUserOnInfusionSoft($data)
    {
        Event::fire(new UserUpdated($data));
    }

    /**
     * Get user data
     *
     * @param integer $id
     * @returns \App\User
     */
    public static function getUser($id)
    {
        return self::find($id);
    }

    /**
     * Get email by user_id
     *
     * @param integer $id
     * @returns string
     */
    public static function getEmail($id)
    {
        return self::query()->where('id', $id)->value('email');
    }

    /*
     * get contact phone by user_id
     *
     */

    public static function getContactPhone($id)
    {
        return self::query()->where('id', $id)->value('contact_phone');
    }

    /**
     * Returns a Referral Amount that match the given id
     *
     * @param type $id
     * @return \App\Setting
     */
    public static function getReferralAmountSettings()
    {
        $referralAmountSetting = Setting::select("value")->where('text', 'referral_amount')->first();
        if ($referralAmountSetting) {
            return floatval($referralAmountSetting->value);
        } else {
            return 0.0;
        }
    }


    /**
     *
     * @param type $user_id
     * @return type
     */
    public static function getReferralStripeData($user_id)
    {
        return User::where('id', '=', $user_id)->select('stripe_id')->first()->stripe_id;
    }

    /**
     *
     * @param type $user_id
     * @return type
     */
    public static function getCurrentWalletStatus($user_id)
    {
        $query = User::where('id', '=', $user_id)->select('current_wallet_amount')->first();
        return $query->current_wallet_amount;
    }

    /**
     * Adds to the current user wallet
     *
     * @param integer $user_id
     * @param float $amount
     * @return boolean
     */
    public static function addToWalletAmount($user_id, $amount)
    {
        return self::where('id', '=', $user_id)->first()->increment('current_wallet_amount', $amount);
    }


    /**
     * removes to the current user wallet
     *
     * @param integer $user_id
     * @param float $amount
     * @return boolean
     */
    public static function removeToWalletAmount($user_id, $amount)
    {
        return self::where('id', '=', $user_id)->first()->decrement('current_wallet_amount', $amount);
    }

    /**
     * A function to calculate referal code
     *
     * @param $id
     * @param $namecode
     * @returns
     * @author STPL
     */
    public static function referalCode($id = null, $namecode = null)
    {
        if (empty($namecode)) {
            $namecode = 'HJ';
        }
        if (strlen($id) == 1) {
            $generateEventIDString = '0000' . $id;
        } else if (strlen($id) == 2) {
            $generateEventIDString = '000' . $id;
        } else if (strlen($id) == 3) {
            $generateEventIDString = '00' . $id;
        } else if (strlen($id) == 4) {
            $generateEventIDString = '0' . $id;
        } else {
            $generateEventIDString = $id;
        }
        $string = $namecode . $generateEventIDString;
        return $string;
    }
}
