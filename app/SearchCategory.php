<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class SearchCategory extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'search_categories';

    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'title',
        'subtitle'
    ];

    public static $validator = [
        'title'     => 'required|string',
        'subtitle'  => 'required|string'
    ];
    
    /**
     * Relation with packages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function searchCategoryHasPackages()
    {
        return $this->hasMany('App\SearchCategoryHasPackages', 'search_category_id');
    }

    public function packages()
    {
        return $this->belongsToMany('App\Package', 'search_category_has_packages');
    }

    /**
     * Add category to database
     *
     * @param array $data
     * @return boolean
     */
    public static function addCategory($data)
    {
        DB::beginTransaction();

        $category = new SearchCategory();
        $category->fill($data);

        if ($category->save()) {
            DB::commit();
            return $category;
        } else {
            DB::rollback();
            return false;
        }
    }

    /**
    *   Return categories list
    *
    *   @return {array}
    */
    public static function getAllCategories()
    {
        return SearchCategory::all();
    }

    /**
    *   Update details (title and subtitle)
    *
    *   @return {array}
    */
    public static function updateDetails($data)
    {
        DB::beginTransaction();

        $category = SearchCategory::find($data['id']);

        if ($category) {
            $category->title = $data['title'];
            $category->subtitle = $data['subtitle'];

            if ($category->save()) {
                DB::commit();
                return $category;
            }
        }
        DB::rollback();
        return false;
    }
}
