<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
/**
 *
 * @author Deepak Thakur
 * @since 0.9
 * @package HomeJab
 * @subpackage Models
 */
class OrderItem extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_items';
        
    /**
     * Relation with Order
     *
     * @return type
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    
    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
    
    /**
     * Validation rules when adding entries
     * @var array
     */
    public static $rulesOnPlaceOrder = [
        'description'   => 'required|string',
        'amount'        => 'required|numeric|min:0',
        'type'          => 'required|string'
    ];
    
        
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'description',
        'amount',
        'type'
    ];
    
    /**
     * Defines the charge type
     * @var string
     */
    public static $chargeType = 'charge';
    
    /**
     * Defines the discount type
     * @var string
     */
    public static $discountType = 'discount';
    
    /**
     * Defines the discount done over the costumers wallet
     * @var string
     */
    public static $walletDiscountType = 'wall_disc';
    
    
    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
    
    /**
     * Insert order items on given order as parametter
     *
     * @param array $items_data
     * @param int $order_id
     * @return boolean
     */
    public static function insertOrderItems($items_data, $order_id)
    {
        foreach ($items_data as $item_data) {
            $item_data['order_id'] = $order_id;

            $item = new OrderItem();
            $item->fill($item_data);

            if (!$item->save()) {
                return false;
            }
        }
        return true;
    }
}
