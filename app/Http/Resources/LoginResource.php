<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    /*public function toArray($request)
    {
        echo"<pre>";print_r($this->resource);die;
        parent::wrap('');
        parent::with(array('error'=>''));
        return parent::toArray($request);
    }*/
    public function toArray($request)
    {

       parent::wrap('');
       return [
           'UserID' => $this->UserID,
           'HomeJabId' => $this->HomeJabId,
           'Name' => $this->Name,
           'RoleId' => $this->RoleId,
           'Error' => '',
           'ProfilePic' => $this->ProfilePic,
       ];
    }

}
