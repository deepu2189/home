<?php

namespace App\Http\Controllers\Api;

use App\PageMetadata;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Transformers\PageMetadataTransformer;
use Illuminate\Support\Facades\Config;

use Log;

/**
 * Page metadata controller class
 *
 * @author Deepak Thakur
 * @since 0.8
 * @package HomeJab
 * @subpackage Controllers
 */
class PageMetadataController extends RestfulController
{
   /**
     *
     * @param \HomeJab\Http\Controllers\PageMetadataTransformer $pageMetadataTransformer
     */
    public function __construct(PageMetadataTransformer $pageMetadataTransformer)
    {
        parent::__construct();
        $this->pageMetadataTransformer = $pageMetadataTransformer;
    }

    /**
     * Get the url metadata
     *
     * @param Request $request
     * @return type
     */
    public function getUrlMetadata(Request $request)
    {
        $pageMetadata = null;
        
        if ($request->has('url') && $request->get('url') !== null) {
            $pageMetadata = PageMetadata::where('url', '=', $request->get('url'))->first();
            
            if (!$pageMetadata) {
                // check if is a url without '/property/view/' and the slug only
                $explode = explode("/", $request->get('url'));
                $slug = array_pop($explode);
                
                $pageMetadata = PageMetadata::where('url', '=', '/property/view/'.$slug)->first();
                
                if (!$pageMetadata) {
                    return $this->respond(['data' => []]);
                }
            }
                        
            return $this->respond(['data' => $this->pageMetadataTransformer->transform($pageMetadata)]);
        } else {
            return $this->respondValidationFailed(trans('page_metadata.validations_fail'));
        }
    }
    
    /**
     * Create or Update the url metadata
     *
     * @param Request $request
     * @return type
     */
    public function createOrUpdate(Request $request)
    {
                                
        $user_id            = $request->user_info->id;
        $user               = User::find($user_id);
        
        if ($user->isAdmin() || $user->isAgent() || $user->isSeller()) {
            $page_metadata = null;
            if ($request->has('url') && $request->get('url') !== null) {
                $page_metadata = PageMetadata::where('url', '=', $request->get('url'))->first();
            }
            
            //Create validator based on rules
            $validator = \Validator::make($request->all(), PageMetadata::$rules);

            if ($validator->fails()) {
                return $this->respondValidationFailed(trans('page_metadata.validations_fail'), $validator->errors());
            }
            
            $page_metadata = PageMetadata::addOrUpdatePageMetadata($request->all());
            if ($page_metadata) {
                // Emit the event to remove the cached version of the page
                event(new \App\Events\PageMetadataChanged($request->url));
                
                return $this->respondCreated([
                    'message' => trans('page_metadata.meta_updated'),
                    'data' => $this->pageMetadataTransformer->transform($page_metadata)
                ]);
            }
        } else {
            return $this->respondForbidden(trans('users.invalid_user'));
        }
    }
}
