<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ClientRecommendation;
use App\Transformers\ClientRecommendationTransformer;
use Illuminate\Support\Facades\Config;
use Cache;


/**
 * Client Reommendations Controller class
 *
 * @author Deepak Thakur
 * @since 1.3.0
 * @property \App\Transformers\ClientRecommendationTransformer $ClientRecommendationTransformer
 */
class ClientRecommendationsController extends RestfulController
{
   /**
     *
     * @param \App\Http\Controllers\ClientRecommendationTransformer $ClientRecommendationTransformer
      */
      public function __construct(ClientRecommendationTransformer $ClientRecommendationTransformer)
      {
          parent::__construct();
          $this->ClientRecommendationTransformer     = $ClientRecommendationTransformer;
      }
  
      /**
       * @api {get} /client_recommendation/:clientRecommendationId Get an client recommendation
       * @apiGroup Client Recommendations
       * @apiName GetClientRecommendation
       * @apiParam {Number} clientRecommendationId Client Recommendation identifier
       * @apiSuccess {Object} data Data
       * @apiSuccess {Number} data.id ClientRecommendation ID
       * @apiSuccess {String} data.client_name name
       * @apiSuccess {String} data.description Description
       * @apiSuccess {String} data.location Location
       * @apiSuccess {Number} data.agency Agency
       * @apiSuccess {Boolean} data.show Published or not
       * @apiSuccess {message} message Message
       * @apiError (404) NotFound The id for the ClientRecommendation was not found
       * @apiPermission SU
       * @apiRequestUrClientRecommendation off
       */
      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
          $this->ClientRecommendation = new ClientRecommendation();
          $query = $this->ClientRecommendation->where('id', '=', $id);
          if ($query->count() <= 0) {
              return $this->respondNotFound(trans('clientRecommendation.not_exists'));
          } else {
              return $this->respond(['data' => $this->ClientRecommendationTransformer->transform($query->first())]);
          }
      }
  
      /**
       * @api {get} /client_recommendation Get the client recommendation list
       * @apiGroup Client Recommendations
       * @apiParam {Number} limit amount of Client Recommendations per page
       * @apiParam {Number} page page to look for
       * @apiSuccess {Object[]} data ClientRecommendation list
       * @apiPermission SU
       * @apiSampleRequest off
       */
      /**
       * Retrive list of client recommendations paginated
       *
       * @param Request $request
       */
      public function index(Request $request)
      {
          $this->ClientRecommendation = new ClientRecommendation();
         
          $query = $this->ClientRecommendation->newQuery()->orderBy('id');
  
          if ($request->has('id') && $request->get('id') !== null) {
              $query->where('id', $request->get('id'));
          }
  
          if ($request->has('user_name') && $request->get('user_name') !== null) {
              $query->whereRaw('LCASE(`user_name`) LIKE ?', ["%" . trim(strtolower($request->get('user_name'))) . "%"]);
          }
  
          if ($request->has('agency') && $request->get('agency') !== null) {
              $query->WhereRaw('LCASE(`agency`) LIKE ?', ["%" . trim(strtolower($request->get('agency'))) . "%"]);
          }
  
          if ($request->has('location') && $request->get('location') !== null) {
              $query->WhereRaw('LCASE(`location`) LIKE ?', ["%" . trim(strtolower($request->get('location'))) . "%"]);
          }
          
          if ($request->get('pagination', "true") == "true") {
              $paginator = $query->paginate(
                  $request->get('limit', Config::get('app.pagination_limit')),
                  ['*'],
                  'page',
                  $request->get('page', 1)
              );
              return $this->respondWithPagination(
                  $paginator,
                  [
                      'data' => $this->ClientRecommendationTransformer->transformCollection($paginator->getCollection()),
                      'message' => ''
                  ]
              );
          }
          return $this->respond(['data' => $this->ClientRecommendationTransformer->transformCollection($query->get())]);
      }
       
      /**
       * List of published client recommendations for front end
       */
      /**
       * @api {get} /client_recommendation/public Gets the list of client recommendations published for frontend
       * @apiGroup Client Recommendations
       * @apiDescription Request a list of all avaliable Client Recommendations.
       * @apiPermission Visitor
       * @apiSuccess {Object} Data Client Recommendations list
       * @apiVersion 1.3.0
       */
      /**
       * List of published client recommendations for front end
       */
      public function listAvaliables()
      {         
        // $result = Cache::get(Config::get('app.cache_recomendations')); 

        // if (!$result) {
            $clientsRecommendations = ClientRecommendation::listAvaliables();
            $result = $this->ClientRecommendationTransformer->transformCollection($clientsRecommendations);
        //     Cache::forever(Config::get('app.cache_recomendations'), $result);
        // }
        return $this->respond(['data' => $result]);
      }
         
         
      /**
       * Update user profile information based on the given request
       *
       * @param Request $request
       * @return json
       */
      public function update(Request $request, $id)
      {
          $user_id = $request->user_info->id;
          $user = User::find($user_id);
  
          if (!$user->isAdmin()) {
              return $this->respondUnauthorized();
          }
          $clientRecommendation = ClientRecommendation::findOrFail($id);
          
     
  
          //Create validator based on rules for update
          $validator = \Validator::make($request->all(), $clientRecommendation::$rulesOnUpdate);
  
          if ($validator->fails()) {
              return $this->respondValidationFailed(trans('client_recommendation.update_fail'), $validator->errors());
          }
          if ($clientRecommendation->update($request->only(ClientRecommendation::$fillableOnUpdate))) {
              return $this->respondCreated([
              'message'   => trans('client_recommendation.update_success'),
              'data'      => $this->ClientRecommendationTransformer->transform($clientRecommendation)
              ]);
          } else {
              return $this->respondValidationFailed(trans('client_recommendation.update_fail'), ['general' => trans('client_recommendation.update_fail')]);
          }
      }
      
      /**
       * Store a newly created resource in storage.
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $user_id = $request->user_info->id;
          $user = User::find($user_id);
  
          if (!$user->isAdmin()) {
              return $this->respondUnauthorized();
          }
          
          $validator = \Validator::make($request->all(), ClientRecommendation::$rulesOnStore);
          
          if ($validator->fails()) {
              return $this->respondValidationFailed(trans('client_recommendation.creation_fails_validation'), $validator->errors());
          }
  
          $clientRecommendation = ClientRecommendation::addClientRecommendation($request->all());
          if ($clientRecommendation) {
              return $this->respondCreated([
                  'message' => trans('client_recommendation.client_created'),
                  'data' => $this->ClientRecommendationTransformer->transform($clientRecommendation)
              ]);
          }
          return $this->respondValidationFailed(trans('client_recommendation.creation_failed'), ['general' => trans('client_recommendation.creation_failed')]);
      }
      
      /**
       * Changes the activation status
       *
       * @param Request $request
       * @param mixed $id
       * @api {get} /client_recommendation/changeActivationStatus Changes the active status
       * @apiGroup Client Recommendation
       * @apiParam {Number} id ClientRecommendation identifier
       * @apiParam {Boolean} active Activate or deactivate the ClientRecommendation
       * @apiSuccess {Object[]} data ClientRecommendation list
       * @apiSuccess {string} message Message to show to the user
       * @apiPermission SU
       * @apiSampleRequest off
       */
      public function changeActivationStatus(Request $request, $id)
      {
          $user_id = $request->user_info->id;
          $user = User::find($user_id);
  
          if (!$user->isAdmin()) {
              return $this->respondUnauthorized();
          }
          
          $clientRecommendation = ClientRecommendation::findOrFail($id);
  
          $activate = (bool) $request->input('active');
  
          if ($activate) {
              if ($clientRecommendation->activate($id)) {
                  return $this->respond([
                     'message' => trans('client_recommendation.activated'),
                     'data' => $this->ClientRecommendationTransformer->transform($clientRecommendation)
                  ]);
              } else {
                  return $this->respond([
                      'message' => trans('client_recommendation.activate_failed'),
                      'data' => []
                  ]);
              }
          } else {
              if ($clientRecommendation->deactivate($id)) {
                  return $this->respond([
                     'message' => trans('client_recommendation.deactivated'),
                     'data' => $this->ClientRecommendationTransformer->transform($clientRecommendation)
                  ]);
              } else {
                  return $this->respond([
                      'message' => trans('client_recommendation.deactivate_failed'),
                      'data' => []
                  ]);
              }
          }
      }
}
