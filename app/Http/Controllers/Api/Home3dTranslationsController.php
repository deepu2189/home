<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Storage;


class Home3dTranslationsController extends RestfulController
{
     /**
     * update 3d translations lang file
     * @param Request $request
     */
    public function update(Request $request)
    {
        // Validate role
        $user_id            = $request->user_info->id;
        $user               = User::find($user_id);
        if (!($user->isAdmin())) {
            return $this->respondForbidden(trans('home3dTranslations.role_not_allowed'));
        }
  
        $data = $request->getContent();
        $ob = json_decode($data);
        
        if ($ob === null) {
            return $this->respondValidationFailed(trans('home3dTranslations.json_fail'));
        }

        $result = Storage::disk('public')->put('home3d-en.json', $data);
        if ($result !== false) {
            event(new \App\Events\Home3DTranslationsEdited('/3D'));
            return $this->respond([
                'message' => trans('home3dTranslations.update_success'),
            ]);
        } else {
            return $this->respondError(trans('home3dTranslations.update_fail'), ['general' => trans('search_page.update_fail')]);
        }
    }
}
