<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Stripe;

// Chandan

class AddMoneyController extends RestfulController
{

    /**
     * @param Request $request
     * @return null
     * Author Deepak Thakur
     */

    public function getEphemeralKey(Request $request){

        $response = null;
        $api_version = $request->get('api_version');

        $user_id    = $request->user_info->id;
        $user       = User::find($user_id);
        $user_email = $user->email;
       
        $stripe_id = trim($user->stripe_id);

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
		if($user->stripe_id == ''){

           $customer =  \Stripe\Customer::create(array(
              "description" => "New Customer",
              "email" => $user_email
            ));
           
           if($customer->id){ 
            
                $user->stripe_id = $customer->id;
                if ($user->save()) {
                    
                    try {
                        $key = \Stripe\EphemeralKey::create(
                          array("customer" => $user->stripe_id),
                          array("stripe_version" => $api_version)
                        );
                        header('Content-Type: application/json');
                        $response['success'] = true;
                        $response['key'] = json_encode($key);
                        return $response;
                    } catch (Exception $e) {
                        
                        $response['success'] = false;
                        $response['message'] = "EphemeralKey cannot be generated.";
                        return $response;
                   }

                } else {
                    $customer->delete();
                    $response['success'] = false;
                    return $response;
                }
                
            }

        }
        else{

            try {
                $key = \Stripe\EphemeralKey::create(
                  array("customer" => $stripe_id),
                  array("stripe_version" => $api_version)
                );
                header('Content-Type: application/json');
                $response['success'] = true;
                $response['data'] = ($key);
                return $response;
            } catch (Exception $e) {
                $response['success'] = false;
                $response['message'] = "EphemeralKey cannot be generated.";
                return $response;

		    }
    	
        }

   
    }
}

