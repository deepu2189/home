<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AllJob;
use App\EwarpLog;
use App\Order;
use App\Photographer;
use App\User;
use App\Transformers\JobTransformer;
use App\Transformers\JobUploadMediaTransformer;
use App\Jobs\SendJobFinishedEmail;
use App\Jobs\SendUploadFailedNotification;
use App\Jobs\SendPropertyCreated;

use App\eWarpIntegration\Jobs\CreateEWarpOrderFromJob;
use App\eWarpIntegration\Jobs\CreateEWarpOrderReEdit;

use Illuminate\Support\Facades\Config;
use Log;
use Carbon;

/**
 * Controller for the Jobs Model
 *
 * @author Deepak Thakur
 * @property \App\Transformers\JobTransformer $jobTransformer
 * @property \App\Transformers\JobUploadMediaTransformer $JobUploadMediaTransformer
 */

class JobsController extends RestfulController
{

    /**
     *
     * @param \App\Transformers\JobTransformer $jobTransformer
     * @param \App\Transformers\JobUploadMediaTransformer $JobUploadMediaTransformer
     */
    public function __construct(JobTransformer $jobTransformer, JobUploadMediaTransformer $JobUploadMediaTransformer)
    {
        parent::__construct();
        $this->jobTransformer               = $jobTransformer;
        $this->JobUploadMediaTransformer    = $JobUploadMediaTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @param {Request} $request
     * @return \Illuminate\Http\Response
     *
     * @api {get} /jobs Retrieve job list
     * @apiPermission SU
     * @apiGroup Jobs
     * @apiDescription List the current jobs
     * @apiParam {integer} page Page number
     * @apiParam {integer} limit Limit the amount of numbers
     * @apiParam {integer} id
     * @apiParam {integer} order_id
     * @apiParam {integer} package
     * @apiParam {integer} asset
     * @apiParam {string} status
     * @apiParam {integer} photographer
     * @apiParam {integer} redit Filter jobs for re-edit (Independent of job's status)
     * @apiParam {integer} created_from
     * @apiParam {integer} created_to
     * @apiSuccess (200 OK) {Object[]} data Jobs list
     * @apiVersion 1.5.0
     * @apiSampleRequest off
     */
    public function index(Request $request)
    {

        $this->Job          = new AllJob();
        $this->User         = new User();
        $this->Photographer = new Photographer();
        $this->Order        = new Order();

        $query = $this->Job->newQuery()->orderBy('id', 'Desc');
       
        if ($request->has('id') && $request->get('id') !== null) {
            $query->where('id', $request->get('id'));
        }

        if ($request->has('order_id') && $request->get('order_id') !== null) {
            $query->where('order_id', $request->get('order_id'));
        }

        if ($request->has('package') && $request->get('package') !== null) {
            $orders_id = $this->Order->newQuery()->where('package_id', '=', $request->get('package'))->get(['id']);

            $query->whereIn('order_id', $orders_id);
        }

        if ($request->has('asset') && $request->get('asset') !== null) {
            $query->where('asset_type_id', '=', $request->get('asset'));
        }

        if ($request->has('status') && $request->get('status') !== null) {
            $text = $request->get('status');
            $text = strtolower($text);
            $text = trim($text);
            $query->where(function ($query) use ($text) {
                $query->whereRaw('LCASE(`status`) LIKE ?', ["%$text%"]);
            });
        }

        if ($request->has('photographer') && $request->get('photographer') !== null) {
            $users = $this->User->newQuery()->where('account_type', 'hjph');

            $users = $users->whereRaw('LCASE(`firstname`) LIKE ?', ["%".trim(strtolower($request->get('photographer')))."%"])
                        ->orWhereRaw('LCASE(`lastname`) LIKE ?', ["%".trim(strtolower($request->get('photographer')))."%"])
                        ->orWhereRaw('LCASE(`email`) LIKE ?', ["%".trim(strtolower($request->get('photographer')))."%"])
                        ->get(['id']);

            $photographer_ids = $this->Photographer->newQuery()->whereIn('user_id', $users)->get(['id']);

            $query->whereIn('photographer_id', $photographer_ids);
        }

        if ($request->has('created_from') && $request->get('created_from') !== null) {
            $query->whereDate('created_at', ">=", date($request->get('created_from')));
        }

        if ($request->has('created_to') && $request->get('created_to') !== null) {
            $query->whereDate('created_at', "<=", date($request->get('created_to')));
        }

        if ($request->has('redit')) {

            $redit =  $request->input('redit') == "true" ? 1 : 0;
          
            $query->where('redit', $redit);
        }
        if ($request->get('pagination', "true") == "true") {
            $paginator = $query->paginate(
                $request->get('limit', Config::get('app.pagination_limit')),
                ['*'],
                'page',
                $request->get('page', 1)
            );
            return $this->respondWithPagination(
                $paginator,
                ['data' => $this->jobTransformer->transformCollection($paginator->getCollection())]
            );
        }
        return $this->respond(['data' => $this->jobTransformer->transformCollection($query->get())]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), AllJob::$rulesOnStore);

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('job.creation_fails_validation'), $validator->errors());
        }

        $job = AllJob::addJob($request->all());
        if ($job) {
            return $this->respondCreated([
                'message' => trans('job.job_created'),
                'data' => $this->jobTransformer->transform($job)
            ]);
        }
        return $this->respondValidationFailed(trans('job.creation_failed'), ['general' => trans('job.creation_failed')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // Check if the job exist
        $this->Job = new AllJob();
        $query = $this->Job->where('id', $id);

       

        if ($query->count() <= 0) {
            return $this->respondNotFound(trans('job.job_not_exists'));
        } else {
            return $this->respond(['data' => $this->jobTransformer->transform($query->first())]);
        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user_id = $request->user_info->id;
        $user = \App\User::find($user_id);

        if ($user->isAgent()) {
             return $this->respondUnauthorized();
        }
        
        $job = AllJob::find($id);
        if (!$job) {
            return $this->respondNotFound();
        }

        //Create validator based on rules for update
        $validator = \Validator::make($request->all(), AllJob::$rulesOnUpdate);

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('jobs.update_fail'), $validator->errors());
        }

        // if job's asset needs ewarp process, do not send property ready email
        $exists_finished = true;
        $asset = $job->assetType;
        if ($asset && is_null($asset->ewarp_product_id)) {
            $exists_finished = AllJob::areFinishedJobsByOrderId($job->order_id);
        }

        $update = $request->only(AllJob::$fillableOnUpdate);

        if (array_key_exists('max_files_amount', $update) && intval($update['max_files_amount'])==0) {
            unset($update['max_files_amount']);
        }

        if ($job->update($update)) {
            if (!$exists_finished) {
                $order = Order::find($job->order_id);
                if ($order) {
                    $property = $order->property;
                    $email = (new SendPropertyCreated($property))->onQueue(Config::get('aws.sqs.email'));
                    $this->dispatch($email);
                }
            } else {
                Order::markAsFinished($job->order_id);
            }
            return $this->respondCreated([
                'message'   => trans('jobs.update_success'),
                'data'      => $this->jobTransformer->transform($job)
            ]);
        } else {
            return $this->respondValidationFailed(trans('jobs.update_fail'), ['general' => trans('jobs.update_fail')]);
        }
    }


    /**
     * Reviewes if exists a relation between a photographer and a order
     *
     * @param Request $request
     */
    public function getJobByPhotographer(Request $request)
    {
        

        if (!$request->has('photographer_id')) {
            return $this->respondNotFound(trans('jobs.missing_photographer_id_parameter'));
        }
        $photographer_id = $request->get('photographer_id');

        if (!$request->has('order_number')) {
            return $this->respondNotFound(trans('jobs.missing_order_number_parameter'));
        }

        $order_number = $request->get('order_number');

        if (Photographer::where('id', '=', $photographer_id)->count() <= 0) {
            return $this->respondNotFound(trans('jobs.photographer_not_found'));
        }

        $order = Order::find($order_number);

       // pr($order,1);

        if (!$order) {
             return $this->respond([
                'message' => trans('jobs.no_order_found'),
                'data' => [
                    'found' => false,
                    'job' => []
                ]
             ]);
        }

        $jobsCollection = $order->jobs()
                                ->where('redit', '=', 0)
                                ->orderBy('asset_type_id')
                                ->get(['*'])
                                ->filter(function ($job, $user_id) {   
                                    $user_info = \Request::all();
                                    $user_id = $user_info['user_info']['id'];                                                         
                                    $photographer = Photographer::where('user_id', $user_id)->first();
                                    return (in_array($job['photographer_id'], [null, $photographer->id]));
                                });
                               

        if (count($jobsCollection) <= 0) {
            return $this->respond([
                'message' => trans('jobs.no_order_found'),
                'data' => [
                    'found' => false,
                    'job' => []
                ]
            ]);
        } else {
            return $this->respond([
                'message' => trans('jobs.order_found'),
                'data' => [
                    'found' => true,
                    'job' => $this->JobUploadMediaTransformer->transformCollection($jobsCollection)
                ]
            ]);
        }
    }

    /**
     * Reserves a job vo a specific photographer
     *
     * @param Request $request
     * @return type
     *
     * @api {post} /jobs/reserve Reserves a job
     * @apiGroup Jobs
     * @apiDescription reserves a job for an specific photographer
     * @apiVersion 0.5.0
     * @apiParam {integer} job_id Job Identifier
     * @apiPermission HJPH
     * @apiSampleRequest off
     */
    public function reserve(Request $request)
    {
        if (!$request->has('job_id')) {
            return $this->respondNotFound(trans('jobs.missing_job_id_parameter'));
        }

        if (!$request->has('photographer_id')) {
            return $this->respondNotFound(trans('jobs.missing_photograper_id_parameter'));
        }

        $job_id = intval($request->get('job_id'));
        $photographer_id = intval($request->get('photographer_id'));



        // Check that both exists
        if (AllJob::where('id', '=', $job_id)->count() <= 0) {
            return $this->respondNotFound(trans('jobs.job_not_found'));
        }
        if (Photographer::where('id', '=', $photographer_id)->count() <= 0) {
            return $this->respondNotFound(trans('jobs.photographer_not_found'));
        }


        $job = AllJob::find($job_id);


        if (intval($job->photographer_id) === intval($photographer_id)) {
            return $this->respond([
                'message' => trans('jobs.job_reserved'),
                'data' => [
                    'success' => true,
                    'job' => $this->jobTransformer->transform(AllJob::find($job_id))
                ]
            ]);
        }

        if ($job->isReserved($job_id)) {
            return $this->respondNotFound(trans('jobs.job_already_reserved'));
        } else {
            if ($job->reserve($job_id, $photographer_id)) {
                return $this->respond([
                    'message' => trans('jobs.job_reserved'),
                    'data' => [
                        'success' => true,
                        'job' => $this->jobTransformer->transform(AllJob::find($job_id))
                    ]
                ]);
            } else {
                return $this->respond([
                    'message' => trans('jobs.job_reserve_failed'),
                    'data' => ['success' => false]
                ]);
            }
        }
    }

    /**
     * Mark a job as Uploaded it's upload
     *
     * @param Request $request
     * @return type
     * @api {put} /jobs/finishUpload Finish the upload of a job
     * @apiGroup Jobs
     * @apiDescription Marks a job as completed it's uploads
     * @apiVersion 1.7.0
     * @apiParam {integer} job_id Job Identifier
     * @apiPermission HJPH
     * @apiSampleRequest off
     */
    public function finishUpload(Request $request)
    {
       
        if (!$request->has('job_id')) {
            return $this->respondNotFound(trans('jobs.missing_job_id_parameter'));
        }
        $job_id = $request->get('job_id');

        if (AllJob::where('id', '=', $job_id)->count() <= 0) {
            return $this->respondNotFound(trans('jobs.job_not_found'));
        }
        $job = AllJob::find($job_id);
        //dd($job);
        if ($job->already_queued) {
            return $this->respondCreated([
                'message' => trans('jobs.job_finished'),
                'data' => $this->jobTransformer->transform($job),
                'remaining_amount' => 0
            ]);
        }

        $filesAmount = \App\JobHasMediaFile::where('job_id', '=', $job_id)->count();
        if ($filesAmount < $job->max_files_amount) {
            return $this->respond([
                'message' => trans('jobs.job_finished_files_still'),
                'remaining_amount'  => intval($job->max_files_amount) - $filesAmount,
                'already_uploaded'  => $filesAmount,
            ]);
        } 

        try {
            $job = $job->sendJobToEwarp();
            if ($job) {
                return $this->respond([
                    'message' => trans('jobs.job_finished'),
                    'data' => $this->jobTransformer->transform($job),
                    'remaining_amount'  => 0
                ]);
            } else {
                return $this->respondBadRequest(trans('jobs.job_could_not_be_finished'));
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->respondBadRequest(
                trans('jobs.job_could_not_be_finished'),
                [ 'error' => $e->getMessage()]
            );
        }
    }


    /**
     * Send and email notification when an upload action fail
     *
     * @param Request $request
     * @return type
     */
    public function sendUploadFailedNotification(Request $request)
    {

        if (!$request->has('job_id')) {
            return $this->respondNotFound(trans('jobs.missing_job_id_parameter'));
        }

        $data = $request->get('data');

        $job_id = $request->get('job_id');

        // #50295 - Check size of the data to be sent
        // Amazon SQS requires Message must be shorter than 262144 bytes
        $data = json_encode($data);
        if (mb_strlen($data) > 252144) { // using 10000 bytes less to handle the email
            $data = mb_strcut($data, 0, 252144);
        }

        $email = (new SendUploadFailedNotification(intval($job_id), $data))->onQueue(Config::get('aws.sqs.email'));
        $this->dispatch($email);

        return $this->respond([]);
    }

    /**
     * Cancel the reserve for the photographer
     * @param type $job_id
     */
    public function cancelReserve($job_id)
    {

        $user_info = request()->all();
        $user_id = $user_info['user_info']['id'];

        $user = \App\User::find($user_id);

        if (!$user->isAdmin()) {
             return $this->respondUnauthorized();
        }

        if (!$job_id) {
            return $this->respondValidationFailed(trans('jobs.missing_job_id_parameter'));
        }

        $job = AllJob::find($job_id);

        if (!$job) {
            return $this->respondNotFound(trans('jobs.job_not_found'));
        }

        $job_material = \App\JobHasMediaFile::where('job_id', $job_id)->count();
        if ($job_material > 0) {
            return $this->respondValidationFailed(trans('jobs.cancel_reserve_failed'));
        }

        if (in_array($job->status, [AllJob::$status['new']['key'], AllJob::$status['uploaded']['key'],
            AllJob::$status['processing']['key'], AllJob::$status['finished']['key']])) {
            return $this->respondValidationFailed(trans('jobs.cancel_reserve_failed'));
        }

        $job->status = AllJob::$status['new']['key'];
        $job->photographer_id = null;

        if ($job->save()) {
            return $this->respond([
                'message' => trans('jobs.reserve_canceled'),
                'data' => $this->jobTransformer->transform($job)
            ]);
        } else {
            return $this->respondValidationFailed(trans('jobs.update_fail'), ['general' => trans('jobs.update_fail')]);
        }
    }

     /**
     * Send job to ewarp, require SU permissions.
     *
     * @param  int $job_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @api {post} /jobs/:job_id Finish upload manually by SU
     * @apiDescription Send job to ewarp
     * @apiGroup Jobs
     * @apiPermission SU
     * @apiSuccess (200 OK) {integer} code=200 If the request was processed correctly
     * @apiSuccess (200 OK) {string} message Message to show to the user
     * @apiSuccess (200 OK) {data[]} data Job data
     * @apiError (422 - Unprocesable entry) {integer} code=422 If there where an issue with the job
     * @apiError (422 - Unprocesable entry) {string} message Message to show to the user
     * @apiError (403 - Not Authorized) {number} code Code 403
     * @apiError (403 - Not Authorized) {string}  message Message to show to the user
     * @apiSampleRequest off
     */
    public function sendToEwarp($job_id)
    {

        //Verify if the user is SU
        $user_info = \Request::all();
        $user_id = $user_info['user_info']['id'];
        $user = \App\User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondUnauthorized();
        }

        //Find Job
        $job = AllJob::find($job_id);

        if (!$job) {
            return $this->respondNotFound(trans('jobs.job_not_found'));
        }

        if ($job->already_queued) {
            return $this->respondUnprocesableEntry([
                'message' => trans('jobs.already_finish')
            ]);
        }

        //Amount of files can't be zero
        $filesAmount = \App\JobHasMediaFile::where('job_id', '=', $job_id)->count();


        if ($filesAmount == 0) {
            return $this->respondUnprocesableEntry([
                'message' => trans('jobs.job_no_files'),
            ]);
        }


        //Send to ewarp
        try {
            $job = $job->sendJobToEwarp(true);
            if ($job) {
                return $this->respond([
                    'message' => trans('jobs.job_finished'),
                    'data' => $this->jobTransformer->transform($job)
                ]);
            } else {
                return $this->respondBadRequest(trans('jobs.job_could_not_be_finished'));
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return $this->respondBadRequest(
                trans('jobs.job_could_not_be_finished'),
                [ 'error' => $e->getMessage()]
            );
        }
    }


    public function cronMaterialsReady(){


        //$mytime = Carbon\Carbon::now();
       // echo $mytime->toDateTimeString();

        $date = date("H:i:s");
        $time = strtotime($date);
        $time = $time - (5 * 60);
        $timeBeforeFive = date("H:i:s", $time);
      // echo $timeBeforeFive;
     //  echo '<pre>';
      // echo $date;
        $allOrders = (new Order())
            ->join('all_jobs', 'all_jobs.order_id', '=', 'orders.id')
            ->whereTime('orders.created_at', '>=', $timeBeforeFive)
            ->whereTime('orders.created_at', '<=', $date)
            ->select('orders.id', 'all_jobs.id as job_id', 'all_jobs.batch_id')
            ->get()->toArray();


        
        foreach ($allOrders as $orderForCron){
            $url = route('ewarp.callback.materialsReady', ['batchid' => $orderForCron['batch_id']]);
        }



    }
}
