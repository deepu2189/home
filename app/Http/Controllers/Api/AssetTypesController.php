<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AssetType;
use App\Transformers\AssetTypeTransformer;
use Illuminate\Support\Facades\Config;
use Log;


/**
 * Class that manages the asset types calls
 *
 * @author Deepak Thakur
 * @since 0.3
 * @package HomeJab
 * @subpackage Controllers
 * @property AssetTypeTransformer $assetTypeTransformer
 */
class AssetTypesController extends RestfulController
{
    /**
     * Controller constructor
     *
     * @param \App\Transformers\AssetTypeTransformer $assetTransformer
     */
    public function __construct(AssetTypeTransformer $assetTransformer)
    {
        parent::__construct();
        $this->assetTypeTransformer = $assetTransformer;
    }

    /**
     * Retrive list of asset types paginated
     *
     * @param Request $request
     *
     * @api {get} /asset_types Get the asset types list
     * @apiGroup Asset Types
     * @apiName GetAssetTypes
     * @apiParam {Number} limit amount of packages per page
     * @apiParam {Number} page page to look for
     * @apiParam {bool} show only enabled assets
     * @apiSuccess {Object[]} data AssetType list
     * @apiPermission SU
     * @apiSampleRequest off
     */
    public function index(Request $request)
    {
        $this->AssetType = new AssetType();
        if ($request->exists('filterEnabled') && $request->filterEnabled) {
            $query = $this->AssetType->newQuery()->where('enabled', '=', 1)->orderBy('name', 'DESC');
        } else {
            $query = $this->AssetType->newQuery()->orderBy('name', 'DESC');
        }

        $query->orderBy('id', 'asc');

        if ($request->exists('assetsType') && $request->get('assetsType', "assets3D") === "assets3D") {
            $query = $query->where('enabled_3d', '=', 1);
        } else if ($request->exists('assetsType') && $request->get('assetsType', "assetsCommon") === "assetsCommon") {
            $query = $query->where('enabled_3d', '=', 0)->where('rework', '=', false);
        } else if ($request->exists('assetsType') && $request->get('assetsType', "assetsRedit") === "assetsRedit") {
            $query = $query->whereNotNull('ewarp_product_id')->where('rework', '=', true);
        }

        if ($request->get('pagination', "true") == "true") {
            $paginator = $query->paginate(
                $request->get('limit', Config::get('app.pagination_limit')),
                ['*'],
                'page',
                $request->get('page', 1)
            );
            return $this->respondWithPagination(
                $paginator,
                [
                    'data' => $this->assetTypeTransformer->transformCollection($paginator->getCollection()),
                    'message' => ''
                ]
            );
        }
        return $this->respond(['data' => $this->assetTypeTransformer->transformCollection($query->get())]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * @api {post} /asset_types Saves an asset type
     * @apiGroup Asset Types
     * @apiName storeAssetType
     * @apiParam {Number} id identifier for the asset type
     * @apiParam {String} name Name for the asset
     * @apiParam {String} description Description
     * @apiParam {String} instructions Instructions to display to the phorographer
     * @apiParam {String} ewarp_product_id eWarp product ID
     * @apiParam {Boolean} enabled Enabled or not
     * @apiParam {Boolean} enabled_3d Enabled 3D or not
     * @apiSuccess (200: OK) {Object} data Empty object
     * @apiSuccess (200: OK) {String} message Message to show to the user
     *
     * @apiError (422: Validation Failed) {String} message Message to show to the user
     * @apiError (422: Validation Failed) {Number} status_code Http error code string
     * @apiError (422: Validation Failed) {Number} code Code
     * @apiPermission SU
     * @apiSampleRequest off
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), AssetType::$rulesOnStore);

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('asset_type.creation_fails_validation'), $validator->errors());
        }

        $asset = AssetType::add($request->all());
        if ($asset) {
            return $this->respondCreated([
                'message' => trans('asset_type.asset_created'),
                'data' => $this->assetTypeTransformer->transform($asset)
            ]);
        }
        return $this->respondValidationFailed(
            trans('asset_type.creation_failed'),
            ['general' => trans('asset_type.creation_failed')]
        );
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
      *
     * @api {get} /asset_types/:assetTypeId Get an asset type
     * @apiGroup Asset Types
     * @apiName GetAssetType
     * @apiParam {Number} assetTypeId Asset Type identifier
     * @apiSuccess {Object} data Data
     * @apiSuccess {Number} data.id Asset ID
     * @apiSuccess {String} data.name name
     * @apiSuccess {String} data.description Description
     * @apiSuccess {String} data.description Description
     * @apiSuccess {String} data.instructions Instructions to display to the phorographer
     * @apiSuccess {String} data.ewarp_product_id eWarp product ID
     * @apiSuccess {Boolean} data.enabled Enabled or not
     * @apiSuccess {Date} data.created_at Creation date
     * @apiSuccess {Date} data.updated_at Last modification date
     * @apiSuccess {message} message Message
     * @apiError (404) NotFound The id for the package was not found
     * @apiPermission none
     * @apiRequestUrAssetTypel off
     */
    public function show($id)
    {
        $this->AssetType = new AssetType();
        $query = $this->AssetType->where('id', '=', $id);

        if ($query->count() <= 0) {
            return $this->respondNotFound(trans('asset_type.not_found'));
        } else {
            return $this->respond(['data' => $this->assetTypeTransformer->transform($query->first()), 'message' => '']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->AssetType = new AssetType();
        $query = $this->AssetType->where('id', '=', $id);

        if ($query->count() <= 0) {
            return $this->respondNotFound(trans('asset_type.not_found'));
        }

        $asset = $query->first();

        //Create validator based on rules for update
        $validator = \Validator::make($request->all(), AssetType::$rulesOnUpdate);

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('asset_type.update_fail'), $validator->errors());
        }

        if ($asset->update($request->only(AssetType::$fillableOnUpdate))) {
            return $this->respondCreated([
                'message'   => trans('asset_type.update_success'),
                'data'      => $this->assetTypeTransformer->transform($asset)
            ]);
        } else {
            return $this->respondValidationFailed(trans('asset_type.update_fail'), ['general' => trans('asset_type.update_fail')]);
        }
    }

    /**
     * Changes the activation status
     *
     * @param Request $request
     * @param mixed $id
     * @api {get} /asset_types/changeActivationStatus Changes the active status
     * @apiGroup Asset Types
     * @apiName changeActivationStatus
     * @apiParam {Number} id Asset Type identifier
     * @apiParam {Boolean} active Activate or deactivate the asset type
     * @apiSuccess {Object[]} data Package list
     * @apiSuccess {string} message Message to show to the user
     * @apiPermission SU
     * @apiSampleRequest off
     */
    public function changeActivationStatus(Request $request, $id)
    {
         $this->AssetType = new AssetType();
        $query = $this->AssetType->where('id', '=', $id);

        if ($query->count() <= 0) {
            return $this->respondNotFound(trans('asset_type.not_found'));
        }

        $asset = $query->first();

        $activate = (bool) $request->input('enabled');
     //   pr($activate,1);
        if ($activate) {
            if ($asset->activate($id)) {
                return $this->respond([
                   'message' => trans('asset_type.activated'),
                   'data' => $this->assetTypeTransformer->transform($asset)
                ]);
            } else {
                return $this->respond([
                    'message' => trans('asset_type.activate_failed'),
                    'data' => []
                ]);
            }
        } else {
            if ($asset->deactivate($id)) {
                return $this->respond([
                   'message' => trans('asset_type.deactivated'),
                   'data' => $this->assetTypeTransformer->transform($asset)
                ]);
            } else {
                return $this->respond([
                    'message' => trans('asset_type.deactivate_failed'),
                    'data' => []
                ]);
            }
        }
    }




    /**
     * Request assets types with ewarp product id
     *
     * @api {get} /asset_types/re-edit Get assets list belong to eWarp products
     * @apiDescription Return list of assets types with the product_id assigned (assets that can be processed by eWarp)
     */
    public function getEwarpProducts()
    {

        try {

            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];

            $user = User::find($user_id);


            if (!$user->isAdmin()) {
                return $this->respondForbidden(['message' => trans('redit.action_not_allowed')]);
            }

            $assetTypes = AssetType::whereNotNull('ewarp_product_id')->where('rework', true)->get();


            if ($assetTypes) {
                return $this->respond(['data' => $this->assetTypeTransformer->transformCollection($assetTypes)]);
            }

            return $this->respond(['data' => '']);
        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' ' . $e->getFile() . ':' . $e->getLine());
            return $this->respondError();
        }

    }
}
