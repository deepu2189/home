<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Property;
use App\Jobs\SendPropertyStatistics;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Log;

/**
 * Analytics Controller class
 *
 * @author Deepak Thakur
 * @since 0.9
 */
class AnalyticsController extends RestfulController
{
    protected $client;
    
    /**
     * Get token for Analytics access
     *
     * @return \Illuminate\Http\Response
     */
    public function getToken()
    {
        
        $user_info          = \Request::all();
        //$user_id            = $user_info['user_info']['id'];
        $user_id            = 48;
        $user               = User::find($user_id);
       
              //  pr($user,1);
        if (!$user->isAgent() && !$user->isSeller()) {
            return $this->respondForbidden(trans('agents.not_allowed'));
        }
                
        $json_key_file = Config::get('app.json_config_file');
        
        if (!file_exists($json_key_file)) {
            throw new \Exception("The file 'google_client_secret.json' was not found. Check JSON_CONFIG_FILE on environment");
        }
        
        $client = new \Google_Client;
        
        // this function does: $this->setClientId, $this->setClientSecret, $this->setRedirectUri
        $client->setAuthConfig($json_key_file);
        $scopes = [\Google_Service_Analytics::ANALYTICS_READONLY];
        $client->setScopes($scopes);
        
        $token = null;
        if ($client->isAccessTokenExpired()) {
            $token = $client->refreshTokenWithAssertion();
        }
        
        return $this->respondCreated([
                    'data' => ['token' => $token['access_token']]
        ]);
    }
    
    /**
     * Get query results for given parametters
     *
     * @return \Illuminate\Http\Response
     */
    private function getAnalyticsResults($view_id, $agentId, $propertyId, $startDate, $endDate, $metrics, $dimensions = null, $filters = null)
    {
        $json_key_file = Config::get('app.json_config_file');
        
        if (!file_exists($json_key_file)) {
            throw new \Exception("The file 'google_client_secret.json' was not found. Check JSON_CONFIG_FILE on environment");
        }
        
        $client = new \Google_Client;
        
        // this function does: $this->setClientId, $this->setClientSecret, $this->setRedirectUri
        $client->setAuthConfig($json_key_file);
        $scopes = [\Google_Service_Analytics::ANALYTICS_READONLY];
        $client->setScopes($scopes);
        
        $token = null;
        if ($client->isAccessTokenExpired()) {
            $token = $client->refreshTokenWithAssertion();
        }
        
        $analytics = new \Google_Service_Analytics($client);
        
        try {
            $results = $analytics
                            ->data_ga
                            ->get(
                                $view_id,
                                $startDate,
                                $endDate,
                                $metrics,
                                [
                                    'dimensions' => $dimensions ,
                                    'filters'    => $filters
                                ]
                            );
        } catch (\Google_Service_Exception $e) {
            $errors = $e->getErrors();
            
            $response['error']      = true;
            $response['data']       = [];
            $response['message']    = 'Google API said: '.array_shift($errors)['message'];
            
            return $response;
        }
        
        $response['error']  = false;
        $response['data']   = $results;
            
        return $response;
    }
    
    /**
     * Send property statistics by email
     *
     * @param Request $request
     * @return type
     */
    public function sendEmailPropertyStatistics(Request $req)
    {
        
       // $user_id            = $req->user_info->id;
        $user_id            = 48;
        $user               = User::find($user_id);
           
        if (!$user->isAgent() && !$user->isSeller()) {
            return $this->respondForbidden(trans('agents.not_allowed'));
        }
        
        // Get query
        $query = $req->all();
        
        // Validate request
        $validator = \Validator::make(
            $query,
            [
                'agent_id'      => 'required',
                'property_id'   => 'required',
                'period'        => 'required|in:7daysAgo,30daysAgo,90daysAgo',
                'dateTo'        => 'required|in:yesterday',
                'queries'       => 'required',
                'email'         => 'required|email',
                'ids'           => 'required'
            ]
        );
                    
        // Check if there are errors
        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('agents.send_contact_email_validation_fail'), $validator->errors());
        }
        
        // Get parameters
        $agentId       = $query['agent_id'];
        $propertyId    = $query['property_id'];
        $currentPeriod = $query['period'];
        $currentDateTo = $query['dateTo'];
        $data          = $query['queries'];
        $email         = $query['email'];
        $view_id       = $query['ids'];
        

        
        // validates the user
        $property = Property::find($propertyId);

        

        if ($user->isAgent()) {
            if ($property->agent_id != $user->agent->id) {
                return $this->respondNotFound(trans('property.forbidden'));
            }
        } else if ($user->isSeller()) {
            if ($property->seller_id != $user->seller->id) {
                return $this->respondNotFound(trans('property.forbidden'));
            }
        } else {
            return $this->respondNotFound(trans('property.property_not_found'));
        }
        
        // Compute pastDateTo and pastPeriod
        switch ($currentPeriod) {
            case '7daysAgo':
                // Set title
                $currentTitle = "This week";
                $pastTitle    = "Last week";
                
                // Current start and end date
                $currentDateTo = Carbon::yesterday()->toDateString();
                $currentPeriod = Carbon::now()->subDay(7)
                                              ->toDateString();
                // Past start and end date
                $pastDateTo = Carbon::yesterday()->subDay(7)
                                                 ->toDateString();
                $pastPeriod = Carbon::yesterday()->subDay(14)
                                                 ->toDateString();
                break;
            case '30daysAgo':
                // Set titles
                $currentTitle = "This month";
                $pastTitle    = "Last month";
                
                // Current start and end date
                $currentDateTo = Carbon::yesterday()->toDateString();
                $currentPeriod = Carbon::now()->subDay(30)
                                              ->toDateString();
                // Past start and end date
                $pastDateTo = Carbon::yesterday()->subDay(30)
                                                 ->toDateString();
                $pastPeriod = Carbon::yesterday()->subDay(60)
                                                 ->toDateString();
                break;
            case '90daysAgo':
                // Set titles
                $currentTitle = "This three months";
                $pastTitle    = "Last three months";
                
                // Current start and end date
                $currentDateTo = Carbon::yesterday()->toDateString();
                $currentPeriod = Carbon::now()->subDay(90)
                                              ->toDateString();
                // Past start and end date
                $pastDateTo = Carbon::yesterday()->subDay(90)
                                                 ->toDateString();
                $pastPeriod = Carbon::yesterday()->subDay(180)
                                                 ->toDateString();
                break;
        }
        
        // Initialize arrays to store the results
        $current = [
            'visits'        => '0',
            'avgTime'       => [
                'tendency'      => 0,
                'time'          => '00:00:00'
            ],
            'country'       => 0,
            'city'          => 0,
            'state'         => 0
        ];
        
        $past    = [
            'visits'        => '0',
            'avgTime'       => [
                'tendency'      => 0,
                'time'          => '00:00:00'
            ],
            'country'       => 0,
            'city'          => 0,
            'state'         => 0
        ];
        
        foreach ($data as $key => $params) {
            // check that metrics exists inside queries
            $metrics = null;
            if (array_key_exists('metrics', $params)) {
                $metrics = $params['metrics'];
            } else {
                return $this->respondValidationFailed(trans('agents.metrics_required'));
            }
            
            // check that dimensions exists inside queries
            $dimensions = null;
            if (array_key_exists('dimensions', $params)) {
                $dimensions = $params['dimensions'];
            }
            
            // check that dimensions exists inside queries
            $filters = null;
            if (array_key_exists('filters', $params)) {
                $filters = $params['filters'];
            }
                        
            // Get analytics for current perriod
            $currentResult = $this->getAnalyticsResults(
                $view_id,
                $agentId,
                $propertyId,
                $currentPeriod,
                $currentDateTo,
                $metrics,
                $dimensions,
                $filters
            );
            
            if ($currentResult['error'] === true) {
                return $this->respondValidationFailed($currentResult['message']);
            }
            
            
            // Get analytics for past period
            $pastResult    = $this->getAnalyticsResults(
                $view_id,
                $agentId,
                $propertyId,
                $pastPeriod,
                $pastDateTo,
                $metrics,
                $dimensions,
                $filters
            );
            
            if ($pastResult['error'] === true) {
                return $this->respondValidationFailed($currentResult['message']);
            }
            
            $currentData = $currentResult['data']['rows'];
            $pastData    = $pastResult['data']['rows'];
           
            switch ($key) {
                case "country":
                    $currentCounter = count($currentData);
                    $pastCounter    = count($pastData);
                    break;
                
                case "city":
                    $currentCounter = count($currentData);
                    $pastCounter    = count($pastData);
                    break;
                
                case "state":
                    // Count number of rows
                    $currentCounter = count($currentData);
                    $pastCounter    = count($pastData);
                    break;
                
                case "visits":
                    // current period visits
                    if (is_null($currentData[0][0])) {
                        $currentCounter = '0';
                    } else {
                        $currentCounter = $currentData[0][0];
                    }
                    
                    // past periodvisits
                    if (is_null($pastData[0][0])) {
                        $pastCounter = '0';
                    } else {
                        $pastCounter = $pastData[0][0];
                    }
                    break;
                    
                case "avgTime":
                    $currentCounter = $currentData[0][0];
                    $pastCounter    = $pastData[0][0];
                    break;
            }
            
            if ($key == 'avgTime') {
                // Convert seconds to hours
                $currentHs = $this->convertSecondsToHours(floor(floatval($currentCounter)));
                $pastHs    = $this->convertSecondsToHours(floor(floatval($pastCounter)));
                
                if ($pastCounter != 0) {
                    // Compute tendency percent
                    $tendency = round(($currentCounter - $pastCounter) / $pastCounter * 100, 2);
                } else {
                    $tendency = 0;
                }

                // Store results
                $current[$key]['tendency'] = $tendency;
                $current[$key]['time']     = $currentHs;
                $past[$key]['time']        = $pastHs;
            } else {
                // Store results
                $current[$key] = $currentCounter;
                $past[$key]    = $pastCounter;
            }
        }
             
        // Create send property statistics job object
        $sendPropertyStatisticsJob = new SendPropertyStatistics(
            $agentId,
            $propertyId,
            $current,
            $past,
            $currentTitle,
            $pastTitle,
            $email
        );
        
        // Enqueue job
        $job = $sendPropertyStatisticsJob->onQueue(Config::get('aws.sqs.email'));
        
        // Dispatch job
        $this->dispatch($job);

        return $this->respondCreated([
            'message' => trans('agents.send_contact_email_sent') ,
            'data'    => true
        ]);
    }
    
    /**
     * Convert seconds to hours (hh:mm:ss)
     *
     * @param  int    $seconds
     * @return string
     */
    private function convertSecondsToHours($seconds)
    {
        // Compute hours
        $hours   = floor($seconds / 3600);
        
        // Compute minutes
        $minutes = floor(($seconds - ($hours * 3600)) / 60);
        
        // Compute seconds
        $seconds = $seconds - ($hours * 3600) - ($minutes * 60);
 
        // Format
        if (preg_match("/^\d$/", $hours)) {
            $hours   = '0' . $hours;
        }
        if (preg_match("/^\d$/", $minutes)) {
            $minutes = '0' . $minutes;
        }
        if (preg_match("/^\d$/", $seconds)) {
            $seconds = '0' . $seconds;
        }
        
        return $hours . ':' . $minutes . ":" . $seconds;
    }
}
