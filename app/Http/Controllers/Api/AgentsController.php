<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Agent;
use App\Transformers\UserTransformer;
use App\Transformers\PublicUserDataTransformer;
use App\Mail\sendEmailMailable;
use App\Jobs\SendContactEmail;
use Illuminate\Support\Facades\Config;

//use Carbon\Carbon;


class AgentsController extends RestfulController
{
   
    /**
     *
     * @param UserTransformer $PublicUserDataTransformer
     */
    public function __construct(PublicUserDataTransformer $PublicUserDataTransformer, UserTransformer $UserTransformer)
    {
        parent::__construct();
        $this->PublicUserDataTransformer = $PublicUserDataTransformer;
        $this->UserTransformer = $UserTransformer;
    }

    /**
     * returns the public information about a certain agent
     *
     * @param type $agentId
     * @return type
     */
    public function getPublicInfo($agentId = null)
    {
        $agent= Agent::find($agentId);

        if (!$agent) {
            return $this->respondNotFound(trans('agents.not_found'));
        }

        return $this->respondCreated([
           'data' =>   [
           'emptyProfile' => User::$emptyProfileImageUrl,
           'agent'=> $this->PublicUserDataTransformer->transform($agent->User)
           ]]);
    }

    /**
     * Display the specified resource serching by slug.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function getBySlug($slug)
    {

        $user = User::where('slug', $slug)->first();       

        if ($user) {
            return $this->respond(['data' => $this->PublicUserDataTransformer->transform($user)]);
        } else {
            return $this->respondNotFound(trans('agents.not_found'));
        }
    }

    /**
     * Send a contact email to a certain agent
     *
     * @param Request $request
     * @return type
     */
    public function sendContactEmail(Request $request)
    {
        $validator = \Validator::make($request->all(), Agent::$rulesOnSendContactEmail);

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('agents.send_contact_email_validation_fail'), $validator->errors());
        }      


        $userId = $request->only(['userId'])['userId'];
       
        $user = User::find($userId);       

        if ($user) {
            // add a job to send the contact email
            $job = (new SendContactEmail($user, $request->all()))->onQueue(Config::get('aws.sqs.email'));
            $this->dispatch($job);

            Agent::registerBuyerOnInfusionSoft($request->all());

            return $this->respondCreated([
                        'message' => trans('agents.send_contact_email_sent'),
                        'data' => true
            ]);
        } else {
            return $this->respondNotFound(trans('users.not_found'), []);
        }
    }

    /**
     * @api {get} /agent/getAllAgents Get the full list of agents
     * @apiGroup Agent
     * @apiPermission Visitor
     * @apiVersion 1.2.0
     * @apiSuccess (200 OK) {Object} data
     */
    /**
     * Get all Agents
     *
     * @return type
     */
    public function getAllAgents()
    {
        $agents_id = Agent::all('user_id');
        $agents = User::whereIn('id', $agents_id)->get();

        return $this->respond(['data' => $this->UserTransformer->transformCollection($agents)]);
    }


    public function searchAgent(Request $request)
    {
        $searchKeys = $request->all();
        $value = $searchKeys['value'];
        $this->User = new User();
        $userIds = $this->User->whereRaw("LCASE(`firstname`) LIKE ?", ["%$value%"])
                                        ->orWhereRaw("LCASE(`lastname`) LIKE ?", ["%$value%"])->get(['id']);
      //  dd($userIds->id);
        $agentUserIds = Agent::whereIn('user_id', $userIds)->get(['id']);

        //dd($agentUserIds);
        $query = $this->User->newQuery()->whereIn('id', $agentUserIds);

             if ($request->get('pagination', "true") == "true") {
                 $paginator = $query->paginate(
                     $request->get('limit', Config::get('app.pagination_limit')),
                     ['*'],
                     'page',
                     $request->get('page', 1)
                 );
                 return $this->respondWithPagination(
                     $paginator,
                     ['data' => $paginator->getCollection()]
                 );
             }

        return $this->respond(['data' => 'no data found']);
    }

}
