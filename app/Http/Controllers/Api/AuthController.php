<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;
use Guzzle\Http\Client;
use Mockery\Exception;
use App\Jobs\SendPasswordRecoveryEmail;
use Illuminate\Support\Facades\Config;

use Mail;


class AuthController extends RestfulController
{

    

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(UserTransformer $userTransformer)
    {
        parent::__construct();
        $this->userTransformer = $userTransformer;
    }



    // public function store(Request $request)
    // {
    //     try {
    //         $grantType = $request->get('grant_type', false);
    //         //We need to validate username and password only for password grand type
    //         if ($grantType === 'password') {
    //             $rulesOnLogin = [
    //                 'username' => 'required|email',
    //                 'password' => 'required',
    //             ];

    //             $validator = \Validator::make($request->all(), $rulesOnLogin);
    //             if ($validator->fails()) {
    //                 return $this->respondValidationFailed(trans('users.login_fail'), $validator->errors());
    //             }
    //         }               

    //         $credentials = $request->only('username', 'password');                    

    //         if (Auth::attempt(['email' => $credentials['username'], 'password' => $credentials['password']])){                
    //            echo 'no';
    //            die();
    //         }           
           
    //         //$response = Authorizer::issueAccessToken();           
    //     } catch (InvalidRequestException $e) {
    //         return $this->respondValidationFailed($e->getMessage(), [$e->errorType => $e->getMessage()]);
    //     } catch (FailedSignRequestException $e) {
    //         return $this->respondValidationFailed($e->getMessage(), $e->errors());
    //     } catch (InvalidCredentialsException $e) {
    //         return $this->respondValidationFailed($e->getMessage());
    //     } catch (OAuthException $e) {
    //         return $this->respondValidationFailed($e->getMessage());
    //     }
    // }


    /**
     * Verify if the reset password token is valid
     *
     * @param type $token
     * @return type
     *
     * @api {PATCH} /password/verify_password_token/:token Validate token
     * @apiGroup User
     * @apiDescription Validates if the token recieved is a valid token
     * @apiVersion 0.1.0
     * @apiPermision Visitor
     */
    public function verifyPasswordToken($token)
    {
        $user = User::getByPasswordResetToken($token);       
        if (!$user) {
            return $this->respondNotFound(trans('auth.invalid_password_token'));
        } else {
            return $this->respond([
                'message' => trans('auth.valid_password_token'),
                'data' => $this->userTransformer->transform($user)            
            ]);
        }
    }

    /**
     * Send the email to allow the a user reset his password
     *
     * @param Request $request
     * @return json
     *
     * @api {POST} /password/recovery Recover Password
     * @apiDescription Sends a request for password recovery
     * @apiGroup User
     * @apiVersion 0.2.0
     */
    public function passwordRecovery(Request $request)
    {
        $validator = \Validator::make($request->all(), User::$rulesOnPasswordRecovery);

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('auth.password_recovery_fails'), $validator->errors());
        }


        $user = User::getByEmail($request->only(['email']));
      
        if ($user) {       

            //Add email sent into job que for user account verification
            $job = (new SendPasswordRecoveryEmail($user))->onQueue(Config::get('aws.sqs.email'));
            $this->dispatch($job);
            
             return $this->respondCreated([
                 'message' => trans('auth.password_recovery_success'),
                 'data' => $user
             ]);
        } else {
            return $this->respondValidationFailed(trans('users.password_recovery_invalid'), ['email' => [trans('users.password_recovery_invalid')]]);
        }
    }

    /**
     * Reset password based on token
     *
     * @param Request $request
     * @return json
     */
    public function passwordReset($token, Request $request)
    {
        $email = $request->only('email');
        
        // $user = User::getByPasswordResetToken($email);
        $user = User::getByEmail($email['email']);       
       
        if (!$user) {
            return $this->respondNotFound(trans('tokens.not_found'));
        }

        $validator = \Validator::make($request->all(), User::$rulesOnPasswordReset);
        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('users.password_reset_fails'), $validator->errors());
        }
      
        if ($user->passwordReset($request->all(), $email['email'])) {            
            return $this->respond([
                'message' => trans('users.password_reset_success'),
                'data' => $this->userTransformer->transform($user)
            ]);
        } else {
            return $this->respondValidationFailed(trans('users.password_reset_fails'));
        }
    }

    /**
     * Get authenticated user data
     */
    public function user(Request $request)
    {
        $inputs = $request->all();
        $user = $inputs['user_info'];             
        if ($user) {
            return $this->respondCreated([
                'message' => trans('auth.user_get'),
                'data' => $this->userTransformer->transform($user)
            ]);
        } else {
            return $this->respondValidationFailed(trans('auth.no_user'), ['general' => trans('auth.no_user')]);
        }
    }
     
}
