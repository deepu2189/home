<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Config;
use App\Events\AddonImageChanged;
use Storage;
use Log;
/**
 * Properties examples controller
 * @author Deepak Thakur
 */
class AddonsImagesController extends RestfulController
{
     /**
     * Save the image for the 3d addon, replacing the existing one
     *
     * @param Request $request
     * @param type $type
     * @return type
     * @api {POST} /addonsImages
     * @apiGroup AddonsImages
     * @apiVersion 1.3.0
     * @apiDescription Save the image for specific 3d addon
     * @apiPermission SU
     */
    public function saveImage(Request $request)
    {
        $user_id            = $request->user_info->id;
        $user               = User::find($user_id);
        
        if (!$user->isAdmin()) {
            return $this->respondForbidden([
                    'message' => trans('addonsImages.role_not_allowed'),
                    'data' => []
                ]);
        }
        
        if (!$request->get('base64')) {
             return $this->respondUnprocesableEntry([
                    'message' => trans('addonsImages.image_not_exists'),
                    'data' => []
                ]);
        }
        
        if (!$request->has('base64') && $request->get('base64')) {
             return $this->respondValidationFailed([
                    'message' => trans('addonsImages.image_not_exists'),
                    'data' => []
                ]);
        }
       

        $addons_folder='assets/images';
        $type = $request->get('addon');
        if ($type === 'aerial') {
            $filename = 'addon_aerial.jpg';
        } else if ($type === 'hdr') {
             $filename = 'addon_hdr.jpg';
        } else if ($type === 'walkhtrough') {
             $filename = 'addon_walkhtrough.jpg';
        } else {
            return $this->respondUnprocesableEntry([
                    'message' => trans('addonsImages.type_not_exists'),
                    'data' => []
                ]);
        }
        list($imageType, $encodeImageCoded) = explode(';', $request->get('base64'));
        list(, $encodedImage) = explode(',', $encodeImageCoded);

        try {
            Storage::disk('angular')->put($addons_folder.DIRECTORY_SEPARATOR.$filename, base64_decode($encodedImage));
            event(new \App\Events\AddonImageChanged());
        } catch (\Exception $ex) {

            return $this->respondError([
                'message' => trans('addonsImages.image_error'),
                'data' => [
                    'error' => $ex->getMessage()
                ]
            ]);
        }
        
       
        return $this->respond([
            'message' => trans('addonsImages.image_changed'),
            'data' => []
        ]);
    }
}
