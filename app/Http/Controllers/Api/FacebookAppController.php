<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\FacebookApp;
use App\Transformers\FacebookAppTransformer;
use Log;

/**
 * Facebook App Controller class
 *
 * @author Deepak Thakur
 * @since 1.5.0
 */

class FacebookAppController extends RestfulController
{
     /**
     *
     * @param \App\Http\Transformers\FacebookAppTransformer $FacebookAppTransformer
     */
    public function __construct(FacebookAppTransformer $FacebookAppTransformer)
    {
        parent::__construct();
        $this->FacebookAppTransformer = $FacebookAppTransformer;
    }

    /**
     * @param Request $request
     * @return {json}
     * @api {get} /facebook/getApp  Return facebook app
     * @apiGroup FacebookApp
     * @apiPermission Admin
     * @apiSuccessParam (200 OK) {object}
     */
    public function getApp()
    {
        $user_info = \Request::all();
        $user_id = $user_info['user_info']['id'];
        $user = User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondForbidden(trans('facebookApp.not_allowed'));
        }

        $app = FacebookApp::getApp();
        if ($app) {
            return $this->respond(['data' => $this->FacebookAppTransformer->transform($app), 'message' => '']);
        } else {
            //Not found
            return $this->respondNotFound(trans('facebookApp.not_found'));
        }
    }

    /**
     *
     * @param Request $request
     * @return {json}
     * @api {post} /facebook/createNewApp Create new Facebook developer app.
     * @apiGroup FacebookApp
     * @apiPermission Admin
     * @apiSuccessParam (200 OK) {object}
     * @apiParam {string} app_id: Identifier facebook app. Unique.
     * @apiParam {string} app_secret: facebook secret app.
     */
    public function createNewFacebookApp(Request $request)
    {
        $user_id = $request->user_info->id;
        $user = User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondForbidden(trans('facebook_app.not_allowed'));
        }

        if (!($request['app_id'] && $request['app_secret'])) {
            $arr = array('message' => trans('facebookApp.missing_parameters'), 'success' => 0);
            return $this->respondUnprocesableEntry($arr);
        }

        $newApp = FacebookApp::addOrUpdate($request->all());
        if ($newApp) {
            return $this->respondCreated([
                        'message' => trans('facebookApp.app_added'),
                        'data' => $this->FacebookAppTransformer->transform($newApp)
            ]);
        } else {
            $arr = array('message' => trans('facebookApp.fail_to_save'), 'success' => 0);
            return $this->respondUnprocesableEntry($arr);
        }
    }
}
