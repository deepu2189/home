<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\CallbackLog;
use Log;

/**
 * Controller to manage the callbacks logs
 *
 * @author Deepak Thakur
 * @since 1.0
 * @package HomeJab
 * @subpackage Controllers
 */

class CallbackLogsController extends RestfulController
{
    /**
     *
     * @var \HomeJab\Transformers\CallbackLogsTransformer
     */
    private $CallbackLogTransformer = null;
    
    /**
     *
     * @param \HomeJab\Transformers\CallbackLogsTransformer $t
     */
    public function __construct(\App\Transformers\CallbackLogsTransformer $t)
    {
        parent::__construct();
        $this->CallbackLogTransformer = $t;
    }
    

    /**
     * @api {get} /callback_logs get the list of callback logs for an specific order
     * @apiGroup CallbackLogs
     * @apiDescription Get the list of callbacks for an specific Job
     * @param Request $request
     * @apiParam {integer} order_id Order identifier
     * @apiSuccess (200 OK) {integer} code=200
     * @apiSuccess (200 OK) {Object[]} data
     * @apiPermission SU
     * @apiSampleRequest off
     */
    /**
     * Display a listing of the resource.
     * @param integer $order_id
     * @return \Illuminate\Http\Response
     */
    public function index($order_id = null)
    {


        $user_info = \Request::all();
        $user_id = $user_info['user_info']['id'];
        $user = User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondForbidden();
        }

        if (is_null($order_id)) {
            return $this->respondNotFound();
        }

        if (Order::where('id', '=', $order_id)->count() <= 0) {
            return $this->respondNotFound();
        }

        $data = $this->CallbackLogTransformer->transformCollection(
            CallbackLog::where('order_id', '=', $order_id)->get(['*'])
        );

        return $this->respond([
                    'message' => trans(''),
                    'data' => $this->CallbackLogTransformer->transformCollection(
                        CallbackLog::where('order_id', '=', $order_id)->get(['*'])
                    )
        ]);
    }
}
