<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Stripe;

// Chandan

class AddApiController extends RestfulController
{

    public function get_location(Request $request){

        $response = null;

        $ip = $request->get('ip_address');
        if(!empty($ip)){
            $url = "https://api.ipinfodb.com/v3/ip-city/?key=c7d2a17b2b784f4ba922fa193c20981bf660fba360a7f514a17cc59b967dd4ae&ip=".$ip;
            
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);

                $data = curl_exec($ch);
                $result = explode(';', $data);
                
                curl_close($ch);
                $response['success'] = true;
                $response['data'] = $result;
                return $response;
        }else{
            $response['success'] = false;
            return $response;
        }
        
    }
}

