<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailToSales;
use App\User;
use App\Events\ContactMsgSent;
use Event;
use Illuminate\Support\Facades\Config;


class ContactUsController extends RestfulController
{
    
    /**
     * @api {post} /frontend/sendEmail Sends an email to Sales
     * @apiGroup Frontend
     * @apiVersion 1.6.0
     * @apiDescription Endpoint to request something to sales - Temporal
     * @apiPermission REA
     * @apiPermission Seller
     * @apiParam {string} message Message to send to sales
     * @apiSuccess (200 OK) {string} message
     * @apiSuccess (200 OK) {boolean} data
     * @apiSampleRequest off
     * @apiError (422 Validation failed) {string} message
     * @apiError (401 Not allowed) {string} message
     */
    /**
     * Send a contact email to Sales
     *
     * @param Request $request
     * @author @dpk
     * @since 1.6.0
     * @return type
     */
    public function sendEmail(Request $request)
    {
        $inputs = $request->all();       
        $user = $inputs['user_info'];      

        if (!$user->isAgent() && !$user->isSeller()) {
            return $this->respondForbidden();
        }

        $validator = \Validator::make(
            $request->all(),
            [
                'message' => 'required'
            ]
        );

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('contactUs.send_email_validation_fail'), $validator->errors());
        }

        $data = [
            'email' => $user->email,
            'message' => $request->get('message'),
            'name' => $user->fullname,
            'subject' => $user->fullname.' request'
        ];

       
        // add a job to send the contact email
        $job = (new SendEmailToSales($data))->onQueue(Config::get('aws.sqs.email'));
        $this->dispatch($job);
        return $this->respondCreated(['message' => trans('contactUs.send_contact_email_sent'), 'data' => true]);
    }

    /**
     * @api {post} /frontend/sendEmailAndAddContact Sends contact email
     * @apiGroup Frontend
     * @apiVersion 0.7.0
     * @apiDescription Sends a contact email and adds the user to infusion soft
     * @apiPermission Visitor
     * @apiParam {string} message Message to send to sales
     * @apiParam {string} email
     * @apiParam {string} firstname
     * @apiSuccess (201 Created) {string} message
     * @apiSuccess (201 Created) {boolean} data
     * @apiError (422 Validation failed) {string} message
     * @apiSampleRequest off
     */
    /**
     * Send a contact email to Sales and add contact to infusionsoft
     *
     * @param Request $request
     * @return type
     */
    public function sendEmailAndAddContact(Request $request)
    {
        $validator = \Validator::make($request->all(), ['email' => 'required|email', 'message' => 'required', 'name' => 'required']);

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('contactUs.send_email_validation_fail'), $validator->errors());
        }
        //add contact to infusionsoft
        $data = ["email" => $request->get('email'), "firstname" => $request->get('name'), "tag" => "blog"];
        Event::fire(new ContactMsgSent($data));

        // add a job to send the contact email
        $job = (new SendEmailToSales($request->all()))->onQueue(Config::get('aws.sqs.email'));
        $this->dispatch($job);
        return $this->respondCreated(['message' => trans('contactUs.send_contact_email_sent'), 'data' => true]);
    }

}
