<?php

namespace App\Http\Controllers\Api;

use App\Jobs\updateSiteMapNew;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Order;
use App\OrderItem;
use App\Property;
use App\AllJob;
use App\Agent;
use App\Seller;
use App\User;
use App\Package;
use App\Transaction;
use App\UserReferral;
use App\OrderHasExtraFile;
use App\CallbackStatus;
use App\CallbackLog;
use App\Transformers\OrderTransformer;
use App\Transformers\PropertyTransformer;
use App\Transformers\JobTransformer;
use App\Transformers\MediaFileTransformer;
use App\Jobs\SendOrderCreated;
use App\Jobs\SendStripeError;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Mediafile;
use App\Jobs\DeleteMediafilesById;
use App\Jobs\SendOrderPlacedCallback;
use App\Jobs\SendOrderRescheduledCallback;
use App\Jobs\SendOrderOnHoldCallback;

use Event;
use App\Events\OrderDeleted;
use App\Events\SiteMapChanged;
use App\Events\OrderCompleted;
use Log;
use Illuminate\Support\Facades\Config;

/**
 * Orders Controller class
 *
 * @author Deepak Thakur
 * @property App\Transformers\JobTransformer jobTransformer
 * @property App\Transformers\PropertyTransformer propertyTransformer
 * @property App\Transformers\OrderTransformer orderTransformer
 * @property App\Transformers\MediaFileTransformer mediafileTransformer
 */
class OrdersController extends RestfulController
{
    public function __construct(OrderTransformer $orderTransformer, PropertyTransformer $propertyTransformer, JobTransformer $jobTransformer, MediaFileTransformer $mediafileTransformer)
    {
        parent::__construct();
        $this->orderTransformer = $orderTransformer;
        $this->propertyTransformer = $propertyTransformer;
        $this->jobTransformer = $jobTransformer;
        $this->mediafileTransformer = $mediafileTransformer;
    }


    /**
     * Places an order
     * @param Request $request
     */

    /**
     * @api {post} /order/place Places a new order
     * @apiDescription Places a new order from the front-end interface
     * @apiGroup Orders
     * @apiPermission REA
     * @apiPermission Seller
     * @apiPermission SU
     * @apiSampleRequest off
     * @apiParam {integer} agent_id Agent indentifier
     * @apiParam {integer} seller_id Seller identifier
     * @apiParam {integer} package_id Package identifier
     * @apiParam {double} amount Total amount of the order
     * @apiParam {string} package_name Package name
     * @apiParam {string} agent_email Agent or Seller email address
     * @apiParam {Object} job
     * @apiParam {string} job.contact_email Contact email for the contractor to use
     * @apiParam {string} job.contact_phone Contact phone for the contractor to use
     * @apiParam {string} job.contact_owner_email HomeOwner email address
     * @apiParam {string} job.contact_owner_phone HomeOwner phone number
     * @apiParam {string} job.property_address Property address
     * @apiParam {string} job.property_details Property details
     * @apiParam {string} job.scheduling_date Date scheduled
     * @apiParam {string} job.scheduling_time Selected scheduling time
     * @apiParam {string} address Entered property address
     * @apiParam {Object} property
     * @apiParam {string} property.administrative_area_level_1
     * @apiParam {string} property.country
     * @apiParam {string} property.locality
     * @apiParam {double} property.map_latitude
     * @apiParam {double} property.map_longitude
     * @apiParam {string} property.postal_code
     * @apiParam {string} property.route
     * @apiParam {string} property.street_number
     * @apiParam {boolean} manually=false
     * @apiParam {boolean} doManualPayment=false
     * @apiParam {string} must_have_shots
     * @apiParam {string} payment Stripe token
     * @apiParam {Array} additionals
     * @apiParam {double} additionals.amount amount of the item
     * @apiParam {string} additionals.description Order item line description
     * @apiParam {string=charge,discount,wallet_discount} additionals.type Order line item type
     */
    public function place(Request $request)
    {
        $user_id = $request->user_info->id;
        $user = User::find($user_id);


        if ($user->isPhotographer()) {
            return $this->respondForbidden(trans('order.place_not_allowed'));
        }

        // Validate order rules
        $validator = \Validator::make($request->all(), Order::$rulesOnPlaceOrder);

        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('order.place_order_fail'), $validator->errors());
        }

        if (!$request->has('job')) {
            return $this->respondValidationFailed(trans('order.place_order_fail_job'), []);
        }

        $validator_job = \Validator::make($request->get('job'), AllJob::$rulesOnPlaceOrder);
        if ($validator_job->fails()) {
            return $this->respondValidationFailed(trans('order.place_order_fail_job'), $validator_job->errors());
        }

        //check if scheduling_date is before than tomorrow
        $tomorrow = Carbon::now()->addDay();
        $job = $request->get('job');
        if ($job['scheduling_date'] < ($tomorrow->toDateTimeString())) {
            return $this->respondValidationFailed(trans('order.scheduling_date_fail'), $validator_job->errors());
        }

        // check order items

        $order_additionals = null;
        if ($request->has('additionals') && count($request->get('additionals')) > 0) {

            $order_additionals = $request->get('additionals');
            foreach ($request->get('additionals') as $additional) {
                $validator_order_item = \Validator::make($additional, OrderItem::$rulesOnPlaceOrder);


                if ($validator_order_item->fails()) {
                    return $this->respondValidationFailed(trans('order.place_order_fail'), $validator_order_item->errors());
                }

                if ($additional['type'] === OrderItem::$walletDiscountType) {
                    // Validate that the user has the desired amount
                    $validate_user_wallet = null;

                    if ($user->isAdmin()) {
                        // search for the user on the order
                        if ($request->get('agent_id') !== null) {
                            $validate_user_wallet = Agent::getUserId($request->get('agent_id'));
                        } else if ($request->get('seller_id')) {
                            $validate_user_wallet = Seller::getUserId($request->get('seller_id'));
                        }
                    } else {
                        $validate_user_wallet = $user_id;
                    }

                    if (\App\WalletTransaction::canPlaceOrderWithWalletCredit(
                        $additional['amount'],
                        $validate_user_wallet
                    )) {
                        return $this->respondValidationFailed(trans('order.wallet_amount_not_enough'), []);
                    }
                }
            }
        }

        $check_total = Order::checkOrderTotal($request->get('package_id'), $order_additionals, $request->get('amount'));
        if (!$check_total) {
            return $this->respondValidationFailed(trans('order.invalid_total'));
        }

        // Check if the package and the agent or seller are valid
        if ($request->has('agent_id') && $request->get('agent_id') !== null) {

            if (Agent::where('id', '=', $request->get('agent_id'))->count() <= 0) {
                return $this->respondValidationFailed(trans('order.agent_not_exists'));
            }
        } else if ($request->has('seller_id') && $request->get('seller_id') !== null) {
            if (Seller::where('id', '=', $request->get('seller_id'))->count() <= 0) {
                return $this->respondValidationFailed(trans('order.seller_not_exists'));
            }
        }

        if (Package::where('id', '=', $request->get('package_id'))->count() <= 0) {
            return $this->respondValidationFailed(trans('order.package_not_exists'));
        }

        $order = Order::place($request->all());

        if ($order['success'] === true) {
            Order::createOrderOnIS($order['order_data']['id']);
            if ($job['contact_owner_email'] && $job['contact_owner_phone']) {
                Order::createOwnerContactOnIS($order['order_data']['id']);
            }
            $callback_job = (new SendOrderPlacedCallback($order['order_data']['id']));
            $callback_job->onQueue(Config::get('aws.sqs.callbacks'));
            $this->dispatch($callback_job);

            $job = (new SendOrderCreated($order['order_data']))->onQueue(Config::get('aws.sqs.email'));

           $this->dispatch($job);

            return $this->respondCreated([
                'message' => trans('order.place_success'),
                'data' => $this->orderTransformer->transform($order['order_data'], ['package'])
            ]);
        } else {


            if (array_key_exists('error_code', $order)
                && array_key_exists('order_data', $order)
                && array_key_exists('message', $order)) {
                $job = (new SendStripeError($order['error_code'], $order['message'], $order['order_data']))
                    ->onQueue(Config::get('aws.sqs.email'));
                $this->dispatch($job);
            }

            $message = null;
            if (array_key_exists('message', $order)) {
                $message = $order['message'];
            } else {
                $message = trans('order.place_fail');
            }

            return $this->respondValidationFailed($message, ['general' => trans('order.place_fail')]);
        }
    }

    /**
     * Retrive list of Orders paginated
     *
     * @param Request $request
     * @return type
     */
    public function index(Request $request)
    {

        $this->Order = new Order();
        $this->User = new User();
        $this->Agent = new Agent();
        $this->Seller = new Seller();

        $user_id = $request->user_info->id;
        $user = User::find($user_id);

        if ($user->isAgent()) {
            $query = $this->Order->newQuery()->where('agent_id', $user->agent->id)->orderBy('id', 'Desc');
        } else if ($user->isAdmin()) {
            $query = $this->Order->newQuery()->orderBy('id', 'Desc');
        } else if ($user->isSeller()) {
            $query = $this->Order->newQuery()->where('seller_id', '=', $user->seller->id)->orderBy('id', 'Desc');
        } else if ($user->isPhotographer()) {
            return $this->respondForbidden(trans('orders.profile_not_allowed'));
        }

        if ($user->isAgent() || $user->isSeller()) {


            if ($request->has('id') && $request->get('id') !== null) {
                $query->where('id', $request->get('id'));
            }

            if ($request->has('status') && $request->get('status') !== null) {
                $text = $request->get('status');
                $text = strtolower($text);
                $text = trim($text);
                $query->where(function ($query) use ($text) {
                    $query->whereRaw('LCASE(`status`) LIKE ?', ["%$text%"]);
                });
            }

            if ($request->has('package') && $request->get('package') !== null) {
                $query->where('package_id', '=', $request->get('package'));
            }

            if ($request->has('created_from') && $request->get('created_from') !== null) {
                $query->whereDate('created_at', ">=", date($request->get('created_from')));
            }

            if ($request->has('created_to') && $request->get('created_to') !== null) {
                $query->whereDate('created_at', "<=", date($request->get('created_to')));
            }


        }


        $query->where('imported', '!=', 1);

        if ($user->isAdmin()) {

            if ($request->has('agent') && $request->get('agent') !== null) {

                $users = $this->User->newQuery()->where('account_type', 'rea');

                $users = $users->whereRaw('LCASE(`firstname`) LIKE ?', ["%" . trim(strtolower($request->get('agent'))) . "%"])
                    ->orWhereRaw('LCASE(`lastname`) LIKE ?', ["%" . trim(strtolower($request->get('agent'))) . "%"])
                    ->orWhereRaw('LCASE(`email`) LIKE ?', ["%" . trim(strtolower($request->get('agent'))) . "%"])
                    ->orWhereRaw('LCASE(`company_name`) LIKE ?', ["%" . trim(strtolower($request->get('agent'))) . "%"])
                    ->get(['id']);

                $agent_ids = $this->Agent->newQuery()->whereIn('user_id', $users)->get(['id']);

                $query->whereIn('agent_id', $agent_ids);
            }


            if ($request->has('id') && $request->get('id') !== null) {
                $query->where('id', $request->get('id'));
            }

            if ($request->has('status') && $request->get('status') !== null) {
                $text = $request->get('status');
                $text = strtolower($text);
                $text = trim($text);
                $query->where(function ($query) use ($text) {
                    $query->whereRaw('LCASE(`status`) LIKE ?', ["%$text%"]);
                });
            }

            if ($request->has('package') && $request->get('package') !== null) {
                $query->where('package_id', '=', $request->get('package'));
            }

            if ($request->has('created_from') && $request->get('created_from') !== null) {
                $query->whereDate('created_at', ">=", date($request->get('created_from')));
            }

            if ($request->has('created_to') && $request->get('created_to') !== null) {
                $query->whereDate('created_at', "<=", date($request->get('created_to')));
            }
        }

        if ($request->get('pagination', "true") == "true") {
            $paginator = $query->paginate(
                $request->get('limit', Config::get('app.pagination_limit')),
                ['*'],
                'page',
                $request->get('page', 1)
            );

            return $this->respondWithPagination(
                $paginator,
                ['data' => $this->orderTransformer->transformCollectionAndExpand($paginator->getCollection(), ['package', 'agent', 'property'])]
            );
        }
        return $this->respond(['data' => $this->orderTransformer->transformCollectionAndExpand($query->get(), ['package', 'agent', 'property'])]);
    }

    /**
     * Request for a specified order.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @api {get} /orders/:id Request order id
     * @apiGroup Orders
     * @apiDescription Request a order information for a order_id. If its exist, show information about Agent or Seller, Packages, Jobs, Property, Mediafiles, Transaction and OrderItems associated to its.
     * @apiPermission SU
     * @apiPermission REA
     * @apiPermission Seller
     * @apiError (403 Not Authorized) {integer} code=403 code
     * @apiError (403 Not Authorized) {string} message This code will be thrown if the user does not have access to the order
     * @apiError (404 Not Found) {string} message This code is thrown if the selected order does not exists
     * @apiUse OrdersResponse
     * @apiSampleRequest off
     * @apiVersion 0.9.0
     */
    public function show($id)
    {

        $order = Order::find($id);

        if (!$order) {
            return $this->respondNotFound(trans('order.order_not_exists'));
        } else {
            $user_info = \Request::all();
            $user_id = $user_info['user_info']['id'];
            $user = User::find($user_id);
            if ($user->isAgent()) {

                if ($user->agent->id != $order->agent_id) {
                    return $this->respondUnauthorized([
                        'message' => trans('order.respondUnauthorized'),
                    ]);
                }
            }
            return $this->respond([
                'message' => trans(''),
                'data' => $this->orderTransformer->transform($order, ['*'])
            ]);
        }
    }

    /**
     * Get Stripe information from order_id if the order is not created by SU
     *
     * @param  int $order_id
     * @return
     */
    public function getStripeInfo($order_id)
    {
        try {
            Order::find($order_id);

            $data_stripe = Order::getStripeInfo($order_id);

        } catch (\Exception $e) {
            $data_stripe = array(
                'error' => true,
                'message' => $e->getMessage()
            );
        } finally {
            return $this->respond(['data_stripe' => $data_stripe]);
        }
    }

    /**
     * Sets an order as completed
     *
     * @param  int $order_id
     * @return
     */

    /**
     * @api {patch} /orders/markAsFinished/:id Mark as finished
     * @apiDescription Marks as finished one order
     * @apiGroup Orders
     * @apiVersion 0.9.0
     * @apiPermission SU
     * @apiSuccess (201 Created) {integer} code=201 If the request was processed correctly
     * @apiSuccess (201 Created) {string} message Message to show to the user
     * @apiSuccess (201 Created) {Object[]} data Order data
     * @apiError (404 Not Found) {integer} code=404 If the order does not exists
     * @apiError (404 Not Found) {string} message Message to show to the user
     * @apiError (422 Unprocesable entry) {integer} code=422 If there where an issue with the order processing
     * @apiError (422 Unprocesable entry) {string} message Message to show to the user
     * @apiSampleRequest off
     */
    public function markAsFinished($order_id)
    {
        $order = Order::find($order_id);

        if ($order) {
            if (Order::markAsFinished($order_id)) {

                Event::fire(new OrderCompleted($order_id));
               
                return $this->respondCreated([
                    'message' => trans('order.order_finished'),
                    'data' => $this->orderTransformer->transform($order, ['*'])
                ]);
            } else {
                return $this->respondValidationFailed([
                    'message' => trans('order.order_not_finished'),
                    'data' => []
                ]);
            }
        } else {
            return $this->respondNotFound([
                'message' => trans('order.order_not_found'),
                'data' => []
            ]);
        }
    }

    /**
     * Update must have shots
     *
     * @param  int $order_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @api {put} /orders/:id/updateMustHaveShots Update must have shots
     * @apiDescription update field must have shots
     * @apiGroup Orders
     * @apiVersion 1.1.7
     * @apiPermission SU
     * @apiSuccess (200 - OK) {integer} code=200 If the request was processed correctly
     * @apiSuccess (200 - OK) {string} message Message to show to the user
     * @apiError (404 - Not Found) {integer} code=404 If the order does not exists
     * @apiError (404 - Not Found) {string} message Message to show to the user
     * @apiError (422 - Unprocesable entry) {integer} code=422 If there where an issue with the order processing
     * @apiError (422 - Unprocesable entry) {string} message Message to show to the user
     * @apiError (403 - Not Authorized) {number} code Code 403
     * @apiError (403 - Not Authorized) {string}  message Message to show to the user
     * @apiParam {string} must_have_shots New text to be included
     * @apiSampleRequest off
     */
    public function updateMustHaveShots(Request $request, $order_id)
    {

        $user_id = $request->user_info->id;
        $user = User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondUnauthorized(['message' => trans('order.action_not_allowed')]);
        }

        $order = Order::find($order_id);
        if (!is_null($order)) {
            if ($request->exists('must_have_shots')) {
                $order->must_have_shots = $request->get('must_have_shots');
                if ($order->save()) {
                    return $this->respond([
                        'message' => trans('order.update_must_have_shots'),
                        'data' => []
                    ]);
                } else {
                    return $this->respondValidationFailed([
                        'message' => trans('order.order_not_update_must_have_shots'),
                        'data' => []
                    ]);
                }
            } else {
                return $this->respondValidationFailed([
                    'message' => trans('order.order_not_update_must_have_shots'),
                    'data' => []
                ]);
            }
        } else {
            return $this->respondNotFound([
                'message' => trans('order.order_not_found'),
                'data' => []
            ]);
        }
    }

    /**
     * Delete an order by id
     *
     * @param int $order_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function destroy(Request $request, $order_id)
    {
        $user_id = $request->user_info->id;
        $user = User::find($user_id);

        if ($user->isAdmin()) {

            $order = Order::find($order_id);

            if ($order) {
                $property_id = $order->property->id;
                $infusionSoft_invoice_id = $order->infusionsoft_invoice_id;
                $jobs = $order->jobs;
                $job_mediafiles = [];

                $property_mediafiles = DB::table('property_has_mediafiles')
                    ->where('property_id', $property_id)
                    ->get(['id', 'mediafile_id']);

                foreach ($jobs as $job) {
                    $job_mediafiles = DB::table('jobs_has_mediafiles')
                        ->where('job_id', $job->id)
                        ->get(['id', 'mediafile_id']);
                }

                if (Order::deleteOrderById($order_id)) {
                    /*
                     * background job 
                     * after deleting averything we dispatch the background jobs
                     * 
                     */

                    foreach ($property_mediafiles as $mediafile) {
                        $background_job = new DeleteMediafilesById(intval($mediafile->mediafile_id));
                        $background_job->onQueue(Config::get('aws.sqs.amazon'));
                        $this->dispatch($background_job);
                    }

                    // Delete InfusionSoft
                    Event::fire(new orderDeleted(['invoice_id' => $infusionSoft_invoice_id]));
                    // Regenerate siteMap
                    Event::fire(new SiteMapChanged());

                    return $this->respond([
                        'message' => trans('order.deleted'),
                    ]);
                } else {
                    return $this->respondError([
                        'message' => trans('order.delete_order_finished'),
                    ]);
                }
            } else {
                return $this->respondForbidden([
                    'message' => trans('order.not_found'),
                ]);
            }
        } else {
            return $this->respondForbidden(trans('order.delete_not_allowed'));
        }
    }

    /**
     * Update agent Id, in order, jobs and property
     *
     * @param request
     * @param order_id Order identifier
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @api {post} /orders/migrateAgent/:id Migrate
     * @apiGroup Orders
     * @apiDescription Allows to migrate an order to a new agent
     * @apiVersion 0.8.0
     * @apiPermission SU
     * @apiParam {integer} [agent_id] Target Agent identifier
     * @apiParam {integer} [seller_id] Target Seller identifier
     * @apiParam {integer} property_id Property identifier
     * @apiSuccess (200 - OK) {number} code Code 200
     * @apiSuccess (200 - OK) {string} message
     * @apiError (404 - Not Found) {number} code Code 404 if the property, agent or seller identifier were not found
     * @apiError (403 - Not Authorized) {number} code Code 403
     */
    public function migrateAgentId(Request $request, $order_id = null)
    {
        //validate role
        $user_id = $request->user_info->id;
        $user = User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondForbidden(trans('order.migrate_not_allowed'));
        }
        $order = Order::getOrder($order_id);



        if ($order) {


            //validations fields required
            if (empty($request->all()) ||
                (is_null($request->get('seller_id')) && is_null($request->get('agent_id'))) ||
                ($request->get('agent_id') != null && $request->get('seller_id')!= null)) {
                return $this->respondValidationFailed(trans('order.validations_fail'), ['general' => trans('order.validations_fail')]);
            }

           // pr($request->all(),1);
            $this->Property = new Property();
            //if this start in property edit, set order_id from property model
            if ($request->get('property_id') != null) {
                $from = 'property';
                $property_id = $request->get('property_id');
            } else {
                $from = 'order';
                $property_id = Property::getPropertyByOrderId($order_id);
            }
            //validate property exist
            $property = Property::getProperty($property_id);
            if (!$property) {
                return $this->respondNotFound(trans('property.property_not_exists'));
            }

            //validate that defined seller_id or agent_id exist
            if ($request->get('seller_id') != null) {
                $seller = Seller::getSeller($request->get('seller_id'));

                if (!$seller) {
                    return $this->respondNotFound(trans('seller_not_exists'));
                }
                $user_id = Seller::getUserId($request->get('seller_id'));

            } else {
                $agent = Agent::getAgent($request->get('agent_id'));
                if (!$agent) {
                    return $this->respondNotFound(trans('agent_not_exists'));
                }
                $user_id = Agent::getUserId($request->get('agent_id'));
            }


            DB::beginTransaction();
            //And process to update fields:
            // (1) update order: agent_id and seller_id
            if (Order::migrateAgentSeller($order_id, $request->only('seller_id', 'agent_id'))) {
                // (2) update property: agent_id and seller_id
                if (Property::migrateAgentSeller($property_id, $request->only('seller_id', 'agent_id'))) {


                    // (3) update all jobs that have order_id like order updated
                    $jobs = AllJob::getJobByOrderId($order_id);
                    //pr($jobs, 1);
                    $contact_email = User::getEmail($user_id);
                    $contact_phone = User::getContactPhone($user_id);
                    foreach ($jobs as $job) {
                        //update job: contact_email and contact_phone
                        if (!AllJob::migrateAgentSeller($job['id'], array('contact_email' => $contact_email, 'contact_phone' => $contact_phone))) {
                            DB::rollback();
                            return $this->respondNotFound(trans('job.update_fail'));
                        }
                    }
                    //emit event to update order in InfusionSoft

                    Order::updateOrderInfusionSoft($order_id);
                    DB::commit();
                    // and return correspond data and message
                    if ($from == 'order') {
                        return $this->respond([
                            'message' => trans('order.order_update_success'),
                            'data' => $this->orderTransformer->transform($order)
                        ]);
                    } else {
                        return $this->respond([
                            'message' => trans('property.update_success'),
                            'data' => $this->propertyTransformer->transform($property)
                        ]);
                    }
                } else {
                    DB::rollback();
                    return $this->respondValidationFailed(trans('property.update_fail'), ['general' => trans('property.update_fail')]);
                }
            } else {
                DB::rollback();
                return $this->respondValidationFailed(trans('order.order_update_fail'), ['general' => trans('order.order_update_fail')]);
            }
        } else {
            return $this->respondNotFound(trans('order_not_exists'));
        }
    }

    /**
     * @api {put} /orders/{order_id}/changeSchedule Change schedule
     * @apiGroup Orders
     * @apiDescription Change scheduling time and status (to assigned) of all the jobs related to an order
     * @apiPermission SU
     * @apiVersion 1.2.3
     * @apiParam {integer} order_id Order id
     * @apiParam {date} scheduling_date New scheduling date (YYYY-MM-DD)
     * @apiParam {string = 'anytime' , '8amto11am',  '9amto12pm',  '10amto1pm', '11amto2pm', '12pmto3pm', '1pmto4pm', '2pmto5pm', '3pmto6pm'} scheduling_time New scheduling time
     * @apiParam {string="new", "scheduled", "in_progress"} status New status to set. The uploaded and finished status will be handled by the application.
     * @apiSuccess (200 - OK) {number} code Code 200
     * @apiSuccess (200 - OK) {string} message
     * @apiSuccess (200 - OK) {Object[]} updated_jobs List of updated jobs
     * @apiError (404 - Not Found) {number} code Code 404 if the order was not found
     * @apiError (403 - Not Allowed) {number} code Code 403 if your profile is not allowed to updathe the scheduling
     * @apiError (422 - Unprocesable entry) {number} code Code 422 if there when an error validating the data sent
     * @apiError (422 - Unprocesable entry) {string} message Message
     * @apiError (422 - Unprocesable entry) {Object[]} errors List of errors and explanations
     * @apiSampleRequest off
     */
    /**
     * Change scheduling time and status of all the jobs related to an order
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeSchedule(Request $request, $order_id)
    {
        // Check that request has order_id
        if (is_null($order_id) || intval($order_id) <= 0) {
            return $this->respondNotFound(trans('order.missing_order_id_parameter'));
        }

        // Validate role
        $user_id = $request->user_info->id;
        $user = User::find($user_id);
        if (!($user->isAdmin() || $user->isPhotographer())) {
            return $this->respondForbidden(trans('order.schedule_not_allowed'));
        }

        if (empty($request->all())) {
            return $this->respondValidationFailed(trans('order.validations_fail'), ['general' => trans('order.validations_fail')]);
        }
        // Check that order exists
        if (Order::where('id', '=', $order_id)->count() <= 0) {
            return $this->respondNotFound(trans('order.order_not_exists'));
        }

        if ($request->has('scheduling_date') && $request->has('scheduling_time') && $request->has('status')) {
            // Change schedule and status of all the jobs related to the order
            $order = Order::find($order_id);
            $jobs = $order->jobs;
            $job_req = [];
            $jobs_data = [];

            // Add new scheduling date to job upload request
            $job_req['scheduling_date'] = $request->get('scheduling_date');
            // Add new scheduling time to job upload request
            $job_req['scheduling_time'] = $request->get('scheduling_time');
            // Add new status to job upload request
            $job_req['status'] = $request->get('status');

            // Validate the inputed data
            $validator = \Validator::make($job_req, AllJob::$rulesOnScheduleUpdate);

            if ($validator->fails()) {
                return $this->respondValidationFailed(trans('order.error_validation_job'), $validator->errors());
            }

            if (AllJob::allJobsFinished($order->id)) {
                return $this->respondValidationFailed(trans('order.error_all_jobs_completed'), []);
            }

            try {
                $changeCounter = 0;
                foreach ($jobs as $job) {
                    //Check jobs status
                    if (!$job->isFinished() &&
                        !$job->isProcessing() &&
                        !$job->isUploaded() &&
                        !$job->isRedit()) {
                        // Update job with $job_req
                        $job->update($job_req);
                        array_push($jobs_data, $this->jobTransformer->transform($job));
                        $changeCounter += 1;
                    }
                }

                if ($changeCounter == 0) {
                    return $this->respondValidationFailed(trans('order.error_jobs_already_uploaded'), []);
                }

                $order->status = 'scheduled';
                $order->rescheduled_confirmed = true;
                $order->save();

                return $this->respond([
                    'message' => trans('order.order_update_success'),
                    'data' => ['updated_jobs' => $jobs_data]
                ]);
            } catch (\Exception $ex) {
                return $this->respondError([
                    'message' => trans('order.error_jobs'),
                    'error' => $ex->getMessage()
                ]);
            }
        } else {
            return $this->respondValidationFailed(trans('order.error_validation_job'), ['errors' => trans('order.missing_parameters')]);
        }
    }

    /**
     * @api {patch} /orders/:job_id/retryCallback Retry Callback for an order
     * @apiGroup Orders
     * @apiDescription Allows to send the callback for an specific order
     * @apiPermission SU
     * @apiParam {integer} order_id Order identifier
     * @apiParam {string} type Callback type
     * @apiSuccess (200 OK) {string} message If the callback was queued again
     * @apiSuccess (200 OK) {integer} code=200
     * @apiError (403 Not Authorized) {string} message If you dont have permissions
     * @apiError (403 Not Authorized) {integer} code=403
     * @apiError (422 Bad Request) {string} message If the <i>order_id</i> parameter was not present
     * @apiError (422 Bad Request) {integer} code=422
     * @apiError (404 Not Found) {string} message If the order does not exists or the callback was send correctly already
     * @apiError (404 Not Found) {integer} code=404
     */
    /**
     * Send back to the Queue of retry the callback for an specific job
     *
     * @param Request $request
     * @param integer $order_id
     * @return type
     */
    public function retryCallback(Request $request, $order_id = null)
    {
        $user_id = $request->user_info->id;
        $user = \App\User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondUnauthorized();
        }

        if (is_null($order_id)) {
            return $this->respondBadRequest();
        }

        if (Order::where('id', '=', $order_id)->count() <= 0) {
            return $this->respondNotFound();
        }

        $type = $request->get('type');
        $callback_status = CallbackStatus::where('order_id', '=', $order_id)->where('type', '=', $type)->orderBy('id', 'DESC')->first();


        if ($callback_status) {
            if ($callback_status->callback_sent === true) {
                return $this->respondValidationFailed(trans('order.callback_was_unsucessfull'));
            }

            // Send the job back to the queue again generating a new Job
            if ($type === CallbackStatus::$typeOrderPlaced) {
                $callback_job = (new SendOrderPlacedCallback($order_id));
                $callback_job->onQueue(Config::get('aws.sqs.callbacks'));
                $this->dispatch($callback_job);

                $callback_status->callback_sent = false;
                $callback_status->callback_failed_at = null;
                $callback_status->callback_success = null;

                if ($callback_status->save()) {
                    return $this->respond([
                        'message' => trans('order.callback_sent_to_retry'),
                        'data' => []
                    ]);
                } else {
                    return $this->respondError(trans('order.callback_job_status_failed'));
                }
            } else if ($type === CallbackStatus::$typeOrderRescheduled) {
                $callback_log = CallbackLog::where('callback_status_id', '=', $callback_status->id)->first();
                $payload = json_decode($callback_log->payload, true);
                $preferred_time = $payload['data']['RecheduledTimeRequested'];
                $preferred_date = $payload['data']['RecheduledDateRequested'];
                $callback_job = (new SendOrderRescheduledCallback($order_id, $preferred_time, $preferred_date));
                $callback_job->onQueue(Config::get('aws.sqs.callbacks'));
                $this->dispatch($callback_job);
            } else if ($type === CallbackStatus::$typeOrderOnHold) {
                $callback_job = (new SendOrderOnHoldCallback($order_id, $user));
                $callback_job->onQueue(Config::get('aws.sqs.callbacks'));
                $this->dispatch($callback_job);
            }

            $callback_status->callback_sent = false;
            $callback_status->callback_failed_at = null;
            $callback_status->callback_success = null;
            if ($callback_status->save()) {
                return $this->respond([
                    'message' => trans('order.callback_sent_to_retry'),
                    'data' => []
                ]);
            }
        } else {
            return $this->respondValidationFailed(trans('order.callback_not_found'));
        }
    }

    /**
     * @api {put} /orders/{order_id}/changeRescheduledStatus Change reschedule status
     * @apiGroup Orders
     * @apiDescription Change reschedule status
     * @apiPermission REA
     * @apiPermission Seller
     * @apiVersion 1.3.0
     * @apiParam {integer} order_id Order id
     * @apiParam {boolean} rescheduled_confirmed Rescheduled status confirmed (1) o requested (0)
     * @apiParam {date} preferred_date Rescheduled date - Format: <code>YYYY-mm-dd</code>
     * @apiParam {string = 'anytime' , '8amto11am',  '9amto12pm',  '10amto1pm', '11amto2pm', '12pmto3pm', '1pmto4pm', '2pmto5pm', '3pmto6pm'} preferred_time Rescheduled slot time
     * @apiSuccess (200 - OK) {number} code Code 200
     * @apiSuccess (200 - OK) {string} message
     * @apiSuccess (200 - OK) Send RescheduledCallback
     * @apiError (404 - Not Found) {number} code Code 404 if the order was not found
     * @apiError (403 - Not Allowed) {number} code Code 403 if your profile is not allowed to change rescheduled status
     * @apiError (422 - Unprocesable entry) {number} code Code 422 if there when an error validating the data sent
     * @apiError (422 - Unprocesable entry) {string} message Message
     * @apiError (422 - Unprocesable entry) {Object[]} errors List of errors and explanations
     * @apiSampleRequest off
     */

    /**
     * Change reschedule status
     * @param Request $request
     * @param type $order_id
     * @return type
     */
    public function changeRescheduledStatus(Request $request, $order_id)
    {
        $user_id = $request->user_info->id;
        $user = User::find($user_id);

        if ($user->isPhotographer()) {
            return $this->respondUnauthorized(['message' => trans('order.action_not_allowed')]);
        }


        $order = Order::find($order_id);
        if (!is_null($order)) {
            $jobs = AllJob::where('order_id', '=', $order_id)->get();
            $count = 0;
            foreach ($jobs as $job) {
                if (!($job->status === 'new' || $job->status === 'assigned' || $job->status === 'scheduled')) {
                    $count++;
                }
            }
            if ($count === $jobs->count()) {
                return $this->respondValidationFailed([
                    'message' => trans('order.request_rescheduled_failed'),
                    'data' => []
                ]);
            }
            if ($user->isAgent()) {
                $agent = Agent::where('user_id', '=', $user_id)->first();
                if ($agent->id !== $order->agent_id) {
                    return $this->respondUnauthorized(['message' => trans('order.action_not_allowed')]);
                }
            }
            if ($user->isSeller()) {
                $seller = Seller::where('user_id', '=', $user_id)->first();
                if ($seller->id !== $order->seller_id) {
                    return $this->respondUnauthorized(['message' => trans('order.action_not_allowed')]);
                }
            }

            if ($request->get('rescheduled_confirmed') === false) {
                if ($order->rescheduled_confirmed !== null && intval($order->rescheduled_confirmed) !== 1) {
                    return $this->respondValidationFailed([
                        'message' => trans('order.previous_rescheduled_not_confirmed'),
                        'data' => []
                    ]);
                }
            }

            if ($request->exists('rescheduled_confirmed')) {

                $order->rescheduled_confirmed = $request->get('rescheduled_confirmed');

                if ($order->save()) {

                    if ($order->rescheduled_confirmed == false) {
                        Order::changeStatus($order_id, 'new');
                        $callback_job = (new SendOrderRescheduledCallback($order_id, $request->get('preferred_time'), $request->get('preferred_date')));
                        $callback_job->onQueue(Config::get('aws.sqs.callbacks'));
                        $this->dispatch($callback_job);
                        return $this->respond([
                            'message' => trans('order.request_rescheduled_success'),
                            'data' => $this->orderTransformer->transform($order, ['*'])
                        ]);
                    } else if ($order->rescheduled_confirmed == true) {
                        Order::changeStatus($order_id, 'scheduled');
                        return $this->respond([
                            'message' => trans('order.confirm_rescheduled_success'),
                            'data' => []
                        ]);
                    }
                } else {
                    if ($order->rescheduled_confirmed === false) {
                        return $this->respondValidationFailed([
                            'message' => trans('order.request_rescheduled_failed'),
                            'data' => []
                        ]);
                    } else if ($order->rescheduled_confirmed === true) {
                        return $this->respond([
                            'message' => trans('order.confirm_rescheduled_failed'),
                            'data' => []
                        ]);
                    }
                }
            } else {
                return $this->respondValidationFailed([
                    'message' => trans('order.order_not_update_rescheduled_confirmed'),
                    'data' => []
                ]);
            }
        } else {
            return $this->respondNotFound([
                'message' => trans('order.order_not_found'),
                'data' => []
            ]);
        }
    }

    /**
     * @api {patch} /orders/setStatusOnHold/:id Set status on hold
     * @apiDescription Set order status 'on_hold'
     * @apiGroup Orders
     * @apiVersion 1.3.0
     * @apiPermission SU
     * @apiPermission REA
     * @apiPermission Seller
     * @apiParam {integer} order_id Order id
     * @apiSuccess (200 OK)  {integer} code=200
     * @apiSuccess (200 OK) {string} message Message to show to the user
     * @apiSuccess (200 OK) {Object[]} data Order data
     * @apiError (403 Not Authorized) {string} message If you dont have permissions
     * @apiError (403 Not Authorized) {integer} code=403
     * @apiError (404 Not Found) {integer} code=404 If the order does not exists
     * @apiError (404 Not Found) {string} message Message to show to the user
     * @apiError (422 Unprocesable entry) {integer} code=422 If there where an issue with the order processing
     * @apiError (422 Unprocesable entry) {string} message Message to show to the user
     * @apiSampleRequest off
     */

    /**
     * Set order on hold
     * @param integer $order_id
     * @return type
     */
    public function setStatusOnHold($order_id)
    {
        $user_info = \Request::all();
        $user_id = $user_info['user_info']['id'];
        $user = User::find($user_id);

        if (!($user->isSeller() || $user->isAgent())) {
            return $this->respondUnauthorized(['message' => trans('order.action_not_allowed')]);
        }

        $order = Order::find($order_id);
        if ($order) {
            if ($order->agent_id) {
                $owner = $order->Agent->User;
            } else if ($order->seller_id) {
                $owner = $order->Seller->User;
            }

            if (($user->isAgent() || $user->isSeller()) && $owner->id !== $user_id) {
                return $this->respondUnauthorized(['message' => trans('order.action_not_allowed')]);
            }

            if ($order->status === 'on_hold') {
                return $this->respondValidationFailed(trans('order.put_on_hold_failed'), ['errors' => trans('order.already_on_hold')]);
            }

            $jobs = $order->jobs;
            foreach ($jobs as $job) {
                if (!($job->status === 'new' || $job->status === 'assigned' || $job->status === 'scheduled')) {
                    return $this->respondValidationFailed(trans('order.put_on_hold_failed'), ['errors' => trans('order.already_uploaded')]);
                }
            }

            if (Order::changeStatus($order_id, 'on_hold')) {
                $callback_job = (new SendOrderOnHoldCallback($order_id, $user_id));
                $callback_job->onQueue(Config::get('aws.sqs.callbacks'));
                $this->dispatch($callback_job);
                return $this->respond([
                    'message' => trans('order.put_on_hold_success'),
                    'data' => $this->orderTransformer->transform($order, ['*'])
                ]);
            } else {
                return $this->respondValidationFailed(trans('order.put_on_hold_failed'), ['errors' => trans('order.status_not_updated')]);
            }
        } else {
            return $this->respondNotFound([
                'message' => trans('order.order_not_found'),
                'data' => []
            ]);
        }
    }

    public function amountExtraFiles($order_id)
    {
        $user_info = \Request::all();
        $user_id = $user_info['user_info']['id'];
        $user = \App\User::find($user_id);

        if (!($user->isPhotographer() || $user->isAdmin())) {
            return $this->respondUnauthorized();
        }

        if (is_null($order_id)) {
            return $this->respondBadRequest();
        }

        $filesAmount = OrderHasExtraFile::where('order_id', '=', $order_id)->count();
        return $this->respond(['message' => trans('order.extra_files_amount'),
            'data' => ['amount' => $filesAmount]
        ]);
    }

    /**
     * Get extra files of order
     * @param type $order_id
     * @return type
     */
    public function getExtraFiles($order_id)
    {
        $order = Order::find($order_id);
        $user_info = \Request::all();
        $user_id = $user_info['user_info']['id'];
        $user = User::find($user_id);

        if (!($user->isPhotographer() || $user->isAdmin())) {
            return $this->respondUnauthorized(['message' => trans('order.action_not_allowed')]);
        }

        if (!$order) {
            return $this->respondNotFound(trans('order.order_not_exists'));
        } else {
            $extraFiles = $order->extraFiles;
            return $this->respond(['data' => $this->mediafileTransformer->transformCollection($extraFiles)]);
        }
    }
}
