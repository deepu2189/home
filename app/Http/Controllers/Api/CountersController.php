<?php

namespace App\Http\Controllers\Api;


use App\HomeCounter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Property;
use App\User;
use App\Transformers\HomeCountersTransformer;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Log;
use Cache;

/**
 * Counters Controller class
 *
 * @author Deepak Thakur
 * @since
 * @package HomeJab
 * @subpackage Controllers
 * @property
 */

class CountersController extends RestfulController
{
    /**
     *
     * @param \HomeJab\Http\Transformers\HomeCountersTransformer $HomeCountersTransformer
     */
    public function __construct(HomeCountersTransformer $HomeCountersTransformer)
    {
   
       parent::__construct();
       $this->HomeCountersTransformer = $HomeCountersTransformer;
    }
    
    /**
     * Gets the actual home counters that are saved in the database
     * @return type
     */
    public function getHomeCounters()
    {

        // $counters = Cache::get(Config::get('app.cache_counters'));
        
        // if (!$counters) {
            $data = HomeCounter::findOrFail(1);
            $counters = $this->HomeCountersTransformer->transform($data);
            // Cache::forever(Config::get('app.cache_counters'), $counters);
        // }
        
        return $this->respond([
            'data' => $counters,
            'message' => ''
        ]);
        
    }
    
    /**
     * Gets the recommended home counters
     * @return type
     */
    public function getHomeCountersRecommended()
    {
        
        $user_info   = \Request::all();
        $user_id    = $user_info['user_info']['id'];
        $user       = User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondUnauthorized(trans('homeCounters.role_not_allowed'));
        }

        $counters = HomeCounter::getRecommendedCounters();
        
        return $this->respond([
            'data' => $this->HomeCountersTransformer->transform($counters),
            'message' => ''
        ]);
    }
    
    /**
     * Updates the new home counters with the new counters values into the database
     *
     * @param Request $request
     */
    public function updateHomeCounters(Request $request)
    {

        $user_id    = $request->user_info->id;
        $user       = User::find($user_id);

        if (!$user->isAdmin()) {
            return $this->respondUnauthorized(trans('homeCounters.role_not_allowed'));
        }

        $validator = \Validator::make($request->all(), HomeCounter::$rulesOnUpdate);

        // Check if there are errors
        if ($validator->fails()) {
            return $this->respondValidationFailed(trans('homeCounters.invalid_home_counters'));
        }

        $data = $request->all();

        $homeCounters = HomeCounter::updateHomeCounters($data);

        if ($homeCounters) {
            return $this->respond([
                'data' => [],
                'message' => trans('homeCounters.home_counters_updated')
            ]);
        } else {
            return $this->respondError([
                'message' => trans('homeCounters.home_counters_error'),
                'data' => []
            ]);
        }
    }
}
