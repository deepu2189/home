<?php
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

/**
 * View composer for loading the metadata associated with a page
 *
 * @author Esteban Zeller <esteban@serfe.com>
 * @author Deepak thakur
 * @package HomeJab
 * @subpackage Metadata
 * @since 0.8
 */
class MetadataComposer
{

    /**
     *
     * @var \HomeJab\Models\PageMetadata
     */
    private $MetadataModel = null;

    /**
     *
     * @var ChrisKonnertz\OpenGraph\OpenGraph
     */
    private $OpenGraph = null;

    /**
     *
     * @var Illuminate\Http\Request
     */
    private $request = null;

    /**
     * Create a new profile composer.
     *
     * @return void
     */
    public function __construct(OpenGraph $open_graph, Request $request, \App\PageMetadata $metadataModel)
    {
        // Dependencies automatically resolved by service container...
        $this->OpenGraph = $open_graph;
        $this->request = $request;
        $this->MetadataModel = $metadataModel;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        // Will return the current url or default metadata
        $metadata = $this->MetadataModel->getByUrl($this->request->path());

        if ($metadata) {  // Can be null if rendering an email or no element on the table
            $view->with('title', $metadata->title);
            $view->with('keywords', $metadata->keywords);
            $view->with('description', $metadata->description);
        }

        // Static url from the application
        $view->with('static_url', Config::get('static_url'));

        // API url from the application
        $view->with('api_url', Config::get('app_url'));

        // Open Graph title and metadata object
        $view->with('og', $this->OpenGraph);
    }
}
