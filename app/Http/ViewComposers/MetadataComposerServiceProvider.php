<?php
namespace App\Http\ViewComposers;

use Illuminate\Support\ServiceProvider;

/**
 * Base class for the provider for the Metadata
 *
 * @author Esteban Zeller <esteban@serfe.com>
 * @author Deepak Thakur
 * @since 0.8
 * @package HomeJab
 * @subpackage Metadata
 */
class MetadataComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        view()->composer(
            '*',
            '\App\Http\ViewComposers\MetadataComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind('metadata', function()
//        {
//            return new \HomeJab\Http\ViewComposers\MetadataComposer;
//        });
    }
}
