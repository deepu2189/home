<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;



use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Get user information based on the user token
        $user = Auth::guard('admin')->user();
        //Check user exists
        if (!$user) {
            // Catch an OAuth exception
            return response()->json(['error' => trans('jabuser.no_user')]);
        }

        if($user->RoleId == 3)
            return response()->json(['error' => trans('jabuser.no_permission')]);


        if($user->IsActive == 0)
            return response()->json(['error' => trans('jabuser.inactive_user')]);

        return $next($request);

    }
}
