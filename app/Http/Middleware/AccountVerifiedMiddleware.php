<?php

namespace App\Http\Middleware;

use Closure;

class AccountVerifiedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /**
         * This is to avoid problems at OPTION requests execcuted
         * at CORS by the browsers
         */
        // if ($request->getMethod() == Request::METHOD_OPTIONS) {
        //     return $next($request);
        // }

        

        $user = $request->user();


        $userDetails = $request->merge([ 'user_info' => $user ]);

       
        if (!$user) {
            // Catch an OAuth exception
            return response(trans('auth.no_user'), 403);
        }
        //Check user account is verified
        if (!$user->account_verified) {
            // Catch an OAuth exception
            return response(trans('users.account_no_verified'), 403);
        }
        
        return $next($request);
    }
}
