<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class registerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if($request->isMethod('post')){   

        return [
        'firstname' => 'required',
        'lastname' => 'required',
        'contact_phone' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|confirmed|min:6',
        'account_type' => 'required|in:rea,hjph,su,seller',
        ];
    }   
    }


    public function messages()
    {
        return [
            'firstname.required' => 'Firstname is required',
            'lastname.required'  => 'Lastname is required',
        ];
    }
}
