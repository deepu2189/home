<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

/**
 * Model to manage the Redit Orders
 *
 * @author Deepak Thakur
 * @since
 * @package HomeJab
 * @subpackage Models
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $Order
 * @property \Illuminate\Database\Eloquent\Relations\BelongsTo $User
 */
class Redit extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'redit';
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Fields that can be filled in a mass assignation
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'order_id',
    ];

    public static $rulesToReEdit = [
       // 'order_id'              => 'required|exists:orders,id|orderIsComplete',
        'order_id'              => 'required|exists:orders,id',
        //'reedit'                => 'array|mediafilesExists:order_id|extraFilesExists:order_id|assetTypeExists|quantityFiles',
        'reedit'                => 'array',
    ];

    /**
     * Relation with the order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * Relation with the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
    * Relation with Job
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function jobs()
    {
        return $this->belongsToMany('App\AllJob', 'redit_has_jobs', 'redit_id');
    }

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
}
