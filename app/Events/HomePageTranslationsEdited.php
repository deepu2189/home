<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class HomePageTranslationsEdited
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

   /**
     * Url that did changed
     * @var string
     */
    private $url = null;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($url = null)
    {
        $this->url = $url;
    }
    
    /**
     * Returns the url changed
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
