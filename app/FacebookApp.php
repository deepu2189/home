<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FacebookApp extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facebook_application';

    /**
     * Validation rules for update
     * @var array
     */
    public static $rulesOnUpdate = [
        'app_id' => 'string',
        'app_secret' => 'secret',
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     *
     * @var array
     */
    protected $fillable = [
        'app_id',
        'app_secret',
    ];

    /**
     * Fields that can be updated through and edit
     * @var array
     */
    public static $fillableOnUpdate = [
        'app_id',
        'app_secret',
    ];

    /**
     *
     * @return boolean
     */
    public static function getApp()
    {
        $facebook_app = self::first();
        if ($facebook_app) {
             return $facebook_app;
        } else {
            return false;
        }
    }

    /**
     * Creates a FacebookApp
     *
     * @param array $data
     * @returns boolean
     */
    public static function addOrUpdate($data)
    {
        
        $facebook_app = FacebookApp::first();

        if (!$facebook_app) {
            $facebook_app = new FacebookApp();
        }

        $facebook_app->fill($data);
        
        DB::beginTransaction();
        if ($facebook_app->save()) {
            DB::commit();
            return $facebook_app;
        } else {
            DB::rollback();
            return false;
        }
    }
}
