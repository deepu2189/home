<?php

namespace App;

use App\Observers\CustomeObservers;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Mail;


class JabUsersPackage extends Model
{
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'Createdon';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'Updatedon';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'HJPackageId', 'HJPackageName', 'HJPackageType', 'UserId', 'CreatedBy', 'CreatedOn', 'UpdatedBy', 'UpdatedOn', 'HJPackageShortDescription', 'HJPackagePrice'
    ];

    public static $fillableOnUpdate = [
        'HJPackageId', 'HJPackageName', 'HJPackageType', 'UserId', 'UpdatedBy', 'UpdatedOn', 'HJPackageShortDescription', 'HJPackagePrice'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $table = "tbl_userspackage";

    protected $primaryKey = 'PackageId';

    //public $timestamps = false;

    public static $rulesOnAssignPackages = [
        'package_list'      => 'required|regex:/^[\d\s,]*$/',
        'photographer_list' => 'required|regex:/^[\d\s,]*$/'
    ];

    public static  $rulesOnAssignPackage = [
        'package_list'  => 'required'
    ];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new CustomeObservers(1));

        parent::boot();
    }

    public function JabUser(){
        return $this->belongsTo('App\JabUser', 'UserId',UserId);
    }

}