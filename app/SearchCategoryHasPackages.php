<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

/**
 * Class to handle the packages associated to search category
 *
 * @author Deepak Thakur
 */

class SearchCategoryHasPackages extends Model
{
      /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'search_category_has_packages';

    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Fields that are mass assignable
     *
     * @var type
     */
    protected $fillable = [
        'search_category_id',
        'package_id'
    ];
    
    public static $validatorToAddPackage = [
        'search_category_id' => 'required|integer',
        'packages' => 'required|array',
    ];

    public static $validatorToDelete = [
        'search_category_id' => 'required|integer',
        'package_id' => 'required|integer',
    ];

    /**
     * Relation with category
     *
     * @return type
     */
    public function category()
    {
        return $this->belongsTo('App\SearchCategory', 'search_category_id');
    }


    /**
     * Relation with package
     *
     * @return type
     */
    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }

    public static function addPackage($package_id, $category_id)
    {
        DB::beginTransaction();

        $entry = new SearchCategoryHasPackages();
        $entry->search_category_id = $category_id;
        $entry->package_id = $package_id;

        if ($entry->save()) {
            DB::commit();
            return true;
        } else {
            DB::rollback();
            return false;
        }
    }

    /**
    * Delete package from category
    *
    * @return {boolean}
    */
    public static function deletePackage($data)
    {
        DB::beginTransaction();

        $entry =  SearchCategoryHasPackages::where('search_category_id', '=', $data['search_category_id'])
                                            ->where('package_id', '=', $data['package_id']);

        if ($entry && $entry->delete()) {
            DB::commit();
            return true;
        } else {
            DB::rollback();
            return false;
        }
    }


   /**
    * Function to know if a package already exist in the category
    *
    * @return {boolean}
    */
    public static function packageAlreadyExist($package_id, $category_id)
    {
        
        $entity = SearchCategoryHasPackages::where('search_category_id', '=', $category_id)
                                            ->where('package_id', '=', $package_id)
                                            ->first();
        if ($entity) {
            return true;
        } else {
            return false;
        }
    }
}
