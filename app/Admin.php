<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class Admin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admins';

    protected $fillables =[
        'user_id'
    ];
    protected $primaryKey = 'id';

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates    =   ['created_at', 'updated_at'];

    /**
     * The Admin belongs to a User
     *
     * @return type
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    
    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }
}
