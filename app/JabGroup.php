<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Observers\CustomeObservers;

class JabGroup extends Model
{
    use Notifiable;

    const CREATED_AT = 'CreatedOn';
    const UPDATED_AT = 'UpdatedOn';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'GroupName', 'GroupDetails', 'CreatedBy', 'CreatedOn', 'UpdatedBy', 'UpdatedOn', 'IsActive'
    ];

    //public $timestamps = false;

    protected $table = "tbl_group";

    protected $primaryKey = 'GroupId';


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public static $rulesOnAddGroup = [
        'GroupName' => 'required',
        //'GroupDetails' => 'required',
    ];
    public static function boot()
    {
        $class = get_called_class();
        $class::observe(new CustomeObservers());

        parent::boot();
    }
    public function chechIfgroupExists($groupname,$id=false)
    {
        $query = $this->newQuery()->where('GroupName',$groupname);

        if($id > 0)
            $query->where('GroupName','!=',$groupname);

        $info = $query->get();

        if(count($info) > 0)
            return true;
        else
            return false;
    }

    public function InsertGroups($POST)
    {

        DB::beginTransaction();
        # Insert record in tbl_group
        $post['GroupName']    = $POST['GroupName'];
        $post['GroupDetails'] = isset($POST['GroupDetails'])?$POST['GroupDetails']:'';
        $post['IsActive']     = 1;

        $objjabgroup = new JabGroup();
        $objjabgroup->fill($post);

        if ($objjabgroup->save()) {
            DB::commit();
            return $objjabgroup;
        }
        DB::rollback();
        return false;

    }

    public function UpdateGroups($POST,$groupid){

        DB::beginTransaction();
        # update record in tbl_group
        $post['GroupName']    = $POST['GroupName'];
        $post['GroupDetails'] = isset($POST['GroupDetails'])?$POST['GroupDetails']:'';
        $post['IsActive']     = 1;

        $objjabgroup = JabGroup::find($groupid);
        $objjabgroup->fill($post);

        if ($objjabgroup->save()) {
            DB::commit();
            return $objjabgroup;
        }
        DB::rollback();
        return false;
    }

    public function ActiveInactive($POST,$id){

        DB::beginTransaction();

        $objjabgroup = JabGroup::find($id);
        $objjabgroup->IsActive = $POST;

        if ($objjabgroup->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();

        return false;
    }

}