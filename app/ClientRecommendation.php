<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Rutorika\Sortable\BelongsToSortedManyTrait;
use Event;
use App\Events\clientRecommendationCreated;

/**
 * Model to manage the clientRecommendation
 *
 * @author Deepak Thakur
 * @since 1.3.0
 * @package HomeJab
 * @subpackage Models
 */
class ClientRecommendation extends Model
{
    use BelongsToSortedManyTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Empty picture for profile image
     * @var string
     */
    public static $emptyProfileImageUrl = '/assets/images/empty_profile.svg';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_recommendations';

    
    /**
     * Validation rules for creation
     * @var array
     */
    public static $rulesOnStore = [
        'client_name' => 'required|string',
        'description' => 'required|string',
        'agency' => 'required|string',
        'location' => 'required|string',
        'show' => 'required|boolean'
    ];

    /**
     * Validation rules for update
     * @var array
     */
    public static $rulesOnUpdate = [
        'client_name' => 'required|string',
        'description' => 'required|string',
        'agency' => 'required|string',
        'location' => 'required|string',
        'show' => 'required|boolean'
    ];

    /**
     * Fields that can be filled in a mass assignation
     *
     * @var array
     */
    protected $fillable = [
        'client_image_id',
        'client_name',
        'description',
        'agency',
        'location',
        'show',
    ];

    /**
     * fields for update
     *
     * @var type
     */
    public static $fillableOnUpdate = [
        'client_image_id',
        'client_name',
        'description',
        'agency',
        'location',
        'show'
    ];

    /**
     * The ClientRecommendation may have a image client
     *
     * @return type
     */
    public function clientImage()
    {
        return $this->belongsTo('App\ImageFile', 'client_image_id');
    }

    /**
     * Add client recommendation and save them to DB
     *
     * @param array $data
     * @return boolean
     */
    public static function addClientRecommendation($data)
    {
        DB::beginTransaction();

        $clientRecommendation = new ClientRecommendation();
        $clientRecommendation->fill($data);

        if (!$clientRecommendation->save()) {
            DB::rollback();
            return false;
        } else {
            DB::commit();
            return $clientRecommendation;
        }
    }

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Update clientRecommendation based on $data received as parametter
     *
     * @param type $data
     * @return boolean
     */
    public function updateClientRecommendation($data)
    {
        DB::beginTransaction();

        $this->fill($data);

        if ($this->save()) {
            Event::fire(new clientRecommendationCreated($this->id));
            DB::commit();
            return true;
        }

        DB::rollback();
        return false;
    }

    /*
     * get clientRecommendation data
     *
     */

    public static function getClientRecommendation($id)
    {
        return self::find($id);
    }

    /**
     * Get list of published recommendations
     *
     * @return array
     */
    public static function listAvaliables()
    {
        return self::where('show', 1)->get();
    }
    
       /**
     * Activate a recommendation
     *
     * @param type $clientRecommendation_id
     * @return boolean
     */
    public function activate($clientRecommendation_id)
    {
        DB::beginTransaction();

        $this->find($clientRecommendation_id);
        $this->show = true;

        if ($this->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();

        return false;
    }

    /**
     * Deactivate a recommendation
     *
     * @param type $clientRecommendation_id
     * @return boolean
     */
    public function deactivate($clientRecommendation_id)
    {
        DB::beginTransaction();

        $this->find($clientRecommendation_id);
        $this->show = false;

        if ($this->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();

        return false;
    }
}
