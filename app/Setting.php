<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class Setting extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * [protected description]
     * @var [type]
     */
    protected $fillable = [
        'text',
        'value'
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates =  [
      'created_at',
      'updated_at'
    ];

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /*
     * Validation rules for update
     *
     * @return array
     */
    public function getRulesForUpdate()
    {
        return [
            'value'   => 'numeric',
        ];
    }

    /*
     * Get Referral Amount
     *
     * @return value
     */
    public static function getReferralAmount()
    {
        $referralAmount = Setting::where('text', '=', 'referral_amount')->first();
        if (!empty($referralAmount->value)) {
            return ['referralAmount' => floatval($referralAmount->value)];
        } else {
            return ['referralAmount' => 0];
        }
    }

    /*
     * Get Double callback enabled
     *
     * @return value
     */
    public static function getDoubleCallbackEnabled()
    {
        $doubleCallbackEnabled = Setting::where('text', '=', 'double_callback_enabled')->first();
        if (!empty($doubleCallbackEnabled->value)) {
            return ['doubleCallbackEnabled' => (boolean)($doubleCallbackEnabled->value)];
        } else {
            return ['doubleCallbackEnabled' => false];
        }
    }

    /*
     * Put Double callback enabled
     *
     * @return value
     */
    public static function putDoubleCallbackEnabled($doubleCallbackEnabled)
    {
        $doubleCallbackEnabled = Setting::where('text', 'double_callback_enabled')->update(['value' => boolval($doubleCallbackEnabled)]);
        //if (!empty($doubleCallbackEnabled)) {
          $value = Setting::select('value')->where('text', 'double_callback_enabled')->first();         
          return ['doubleCallbackEnabled' => ($value['value'] == 1) ? 'True' : 'False' ];
       // } else {
          //  return ['doubleCallbackEnabled' => false];
      //  }
    }
}
