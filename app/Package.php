<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Event;
use App\Events\ProductAdded;
use App\Events\ProductUpdated;
use Log;

/**
 *
 * @author Deepak Thakur
 * @since 0.1
 * @package App
 * @subpackage Models
 * @property \Illuminate\Database\Eloquent\Relations\HasMany $Orders
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany $AssetTypes
 */
class Package extends Model
{
    use \Rutorika\Sortable\SortableTrait;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'packages';
    
    /**
     * Validation rules for creation
     * @var array
     */
    public static $rulesOnStore = [
        'name'                      => 'required|unique:packages|string',
        'short_description'         => 'required|string',
        'long_description'          => 'required|string',
        'delivered_text'            => 'required|string',
        'enabled'                   => 'required|boolean',
        'package_type'              => 'required|string',
        'shooting_time'             => 'integer',
        'payment_to_photographer'   => 'numeric|min:0',
        'package_instructions'      => 'string',
        'template_description'      => 'required|string',
    ];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'short_description',
        'long_description',
        'template_description',
        'delivered_text',
        'price',
        'ask_price',
        'enabled',
        'package_type',
        'preview_image_id',
        'position',
        'example_slug',
        'shooting_time',
        'payment_to_photographer',
        'package_instructions'
    ];

    /**
     *
     * @var array
     */
    public static $fillableOnUpdate = [
        'name',
        'short_description',
        'long_description',
        'template_description',
        'delivered_text',
        'price',
        'ask_price',
        'enabled',
        'package_type',
        'preview_image_id',
        'example_slug',
        'shooting_time',
        'payment_to_photographer',
        'package_instructions'
    ];
    
    /*
     * Validation rules for update
     *  
     * @return array
     */
    public function getRulesForUpdate()
    {
        return [
                'name'                      => 'required|string|unique:packages,name,'.$this->id,
                'short_description'         => 'required|string',
                'long_description'          => 'required|string',
                'template_description'      => 'required|string',
                'delivered_text'            => 'required|string',
                'enabled'                   => 'required|boolean',
                'package_type'              => 'required|string',
                'shooting_time'             => 'integer',
                'payment_to_photographer'   => 'numeric|min:0',
                'package_instructions'      => 'string'
            ];
    }
    
    /**
     * The package may have a image preview
     *
     * @return type
     */
    public function previewImage()
    {
        return $this->belongsTo('App\ImageFile', 'preview_image_id');
    }

    /**
     * Order relation. A package can have many orders related to it
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    
    /**
     * Relation with asset Types
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function assetTypes()
    {
        return $this->belongsToMany('App\AssetType', 'package_has_asset_types');
    }

    /**
     * Relation with asset Types
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function packageHasAssetTypes()
    {
        return $this->hasMany('App\PackageHasAssetType', 'package_id');
    }

    public function searchCategory()
    {
        return $this->belongsToMany('App\SearchCategory', 'search_category_has_packages');
    }

    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        if (!is_null($date)) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
        }
        return null;
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        if (!is_null($date)) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
        }
        return null;
    }

    /**
     * Add packages and save them to DB
     *
     * @param array $data
     * @return boolean
     */
    public static function addPackage($data)
    {
        DB::beginTransaction();

        $package = new Package();
        $package->fill($data);

        if (!$package->save()) {
            DB::rollback();
            return false;
        } else {
            $package->position = $package->id;
            if ($package->save()) {
                DB::commit();
                return $package;
            } else {
                DB::rollback();
                return false;
            }
        }
    }

    /**
     * Activate a package
     *
     * @param type $package_id
     * @return boolean
     */
    public function activate($package_id)
    {
        DB::beginTransaction();

        $this->find($package_id);
        $this->enabled = true;

        if ($this->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();

        return false;
    }

    /**
     * Deactivate a package
     *
     * @param type $package_id
     * @return boolean
     */
    public function deactivate($package_id)
    {
        DB::beginTransaction();

        $this->find($package_id);
        $this->enabled = false;

        if ($this->save()) {
            DB::commit();
            return true;
        }
        DB::rollback();

        return false;
    }

    /**
     * Get list and availabled of packages
     *
     * @return type
     */
    public static function listAvaliables()
    {
        return self::where('enabled', 1)->get();
    }
    
    /**
     * Get list and availabled 3d packages
     *
     * @return type
     */
    public static function listAvaliables3D()
    {
        return self::where('enabled', 1)->where('package_type', '=', '3D')->get();
    }


    /**
     * Get related assets of given package
     *
     * @return type
     */
    public static function getPackageAssets($id)
    {
        return self::find($id)->assetTypes()->allRelatedIds();
    }
    
    /**
     * Associates a package with an asset type
     *
     * @param array $asset_type
     * @return boolean
     */
    public function associateAssetType($asset_type_id)
    {
        
      //  $asset_type_id = $asset_type['asset_id'];
       // $asset_type_pos = $asset_type['asset_pos'];
    
        if ($asset_type_id=== Config::get('app.3d_asset_hdr_photos')) {
             $amount_originals=3;
        } else {
            $amount_originals=1;
        }
        // Check if the relation already exists
        if ($this->assetTypes()->getQuery()->where([
            'package_id' => $this->id,
            'asset_type_id' => $asset_type_id
        ])->count() > 0) {
            return false;
        }
        
        DB::beginTransaction();
        $this->assetTypes()->attach($asset_type_id, [ 'amount_originals_files' => $amount_originals]);
        DB::commit();
        return true;
    }

    /**
     * Removes the relation between a package and a asset type
     *
     * @param integer $asset_type_id
     * @return boolean
     */
    public function removeAssociatedAssetType($asset_type_id)
    {

        // Check if the relation already exists
        if ($this->assetTypes()->getQuery()->where([
            'package_id' => $this->id,
            'asset_type_id' => $asset_type_id
        ])->count() <= 0) {
            return false;
        }
        
        DB::beginTransaction();
        $this->assetTypes()->detach($asset_type_id);
        DB::commit();
        return true;
    }

    /**
     * Removes all relation between a package and a asset type
     *
     *
     * @return boolean
     */
    public function removeAllAssociatedAssetType()
    {
        
        DB::beginTransaction();
        $this->assetTypes()->detach();
        DB::commit();
        return true;
    }
    
    /**
     * Save package on InfusionSoft
     *
     * @param array $data
     * @return type
     */
    public static function addPackageToInfusionSoft($data)
    {
        Event::fire(new ProductAdded($data));
    }
    
    /**
     * Save InfusionSoft product_id on packages table
     *
     * @param integer $id
     * @return type
     */
    public static function savePackageInfusionSoftId($package_id, $product_id)
    {
        $package = self::find($package_id);
        
        DB::beginTransaction();
        
        if ($package && $product_id) {
            $package->infusionsoft_id = $product_id;
            if ($package->save()) {
                DB::commit();
                return true;
            }
        }
        
        DB::rollback();
        return false;
    }
    
    /**
     * Save package on InfusionSoft
     *
     * @param array $data
     * @return type
     */
    public static function updatePackageOnInfusionSoft($data)
    {
        Event::fire(new ProductUpdated($data));
    }


    public function hasAddonPhotos()
    {
        $hdr_photos = array(
            Config::get('app.3d_asset_virtual_dusk'),
            Config::get('app.3d_asset_hdr_photos'),
            Config::get('app.3d_asset_standard_photos')
        );

        $count = DB::table('package_has_asset_types')
            ->where('package_has_asset_types.package_id', '=', $this->id)
            ->whereIn('package_has_asset_types.asset_type_id', $hdr_photos)
            ->count('package_has_asset_types.asset_type_id');
         
        if ($count == count($hdr_photos)) {
            return true;
        }

        return false;
    }

    public function hasAddonAerials()
    {
        $aerials = array(
            Config::get('app.3d_asset_video_standard'),
            Config::get('app.3d_asset_standard_photos')
        );

        $count = DB::table('package_has_asset_types')
            ->where('package_has_asset_types.package_id', '=', $this->id)
            ->whereIn('package_has_asset_types.asset_type_id', $aerials)
            ->count('package_has_asset_types.asset_type_id');

        if ($count == count($aerials)) {
            return true;
        }

        return false;
    }

    public function hasAddonWalkthroug()
    {
         $walkthroug = array(
            Config::get('app.3d_asset_video_intermediate')
        );

        $count = DB::table('package_has_asset_types')
            ->where('package_has_asset_types.package_id', '=', $this->id)
            ->whereIn('package_has_asset_types.asset_type_id', $walkthroug)
            ->count('package_has_asset_types.asset_type_id');

        if ($count == count($walkthroug)) {
            return true;
        }

        return false;
    }
}
