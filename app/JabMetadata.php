<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use Edujugon\PushNotification\PushNotification;
use League\Flysystem\Config;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class JabMetadata extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'metaTitle', 'metaKeywords', 'metaDescription'
    ];

    public $timestamps = false;

    protected $table = "tbl_metadata";

    protected $primaryKey = 'metaID';
    public static $rulesOnupdateMetadata = [
        'metaTitle' => 'required',
        'metaKeywords' => 'required',
        'metaDescription' => 'required',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }


    public function updateMetadata($POST,$id){

        DB::beginTransaction();
        # update metadata in tbl_metadata
        $post['metaTitle']    = $POST['metaTitle'];
        $post['metaKeywords'] = $POST['metaKeywords'];
        $post['metaDescription'] = $POST['metaDescription'];

        $objjabMetadata = JabMetadata::find($id);
        $objjabMetadata->fill($post);


        if ($objjabMetadata->save()) {
            DB::commit();
            return $objjabMetadata;
        }
        DB::rollback();
        return false;
    }
}