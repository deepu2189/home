<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

use Laravel\Cashier\Billable;
use App\User;
use Log;

class Transaction extends Model 
{
    use Billable;

    /**
     * Data conversion date
     *
     * @var type
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'stripe_id',
        'amount',
        'type'
    ];

    /** Payment type constant **/
    public static $paymentType = 'payment';

    /**
     * Relation with Order
     *
     * @return type
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }


    /**
     * Accesor for created at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Accesor for updated at date
     *
     * @param string $date
     * @return \Carbon\Carbon
     */
    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(Config::get('app.datetime_format'));
    }

    /**
     * Creates a transaction from an order
     *
     * @param array $data
     * @param integer $order_id
     * @return boolean
     */
    public static function addChargeFromOrder($data, $order_id)
    {

        $transaction = new Transaction();

        $user               = null;
        $stripe_metadata    = null;

        // generate the stripe payment metadata
        if (array_key_exists('agent_id', $data) && $data['agent_id'] !== null) {
            $agent      = Agent::find($data['agent_id']);
            $user       = User::find($agent->user_id);
            $stripe_metadata = [
                'agent_id'      => $data['agent_id'],
                'agent_email'   => $data['agent_email'],
            ];
        } else if (array_key_exists('seller_id', $data) && $data['seller_id'] !== null) {
            $seller = Seller::find($data['seller_id']);
            $user   = User::find($seller->user_id);
            $stripe_metadata = [
                'seller_id'     => $data['seller_id'],
                'seller_email'  => $data['seller_email'],
            ];
        }
        
        $stripe_metadata['order_id']       = $order_id;
        $stripe_metadata['package_id']     = $data['package_id'];
        $stripe_metadata['package_name']   = $data['package_name'];

        $customer = null;

        // create customer if not exists for the user
        if ($user && $user->stripe_id === null) {
            $customer_response = User::createStripeCustomer($data, $user->id);
           
            if ($customer_response['success'] === true) {
                $customer = $customer_response['user'];
            } else {
                return $customer_response;
            }
        } else {
            $customer = $user;
        }

        $charge = null;

        if ($customer && $customer->stripe_id !== null) {
            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

            // make Charge data
            $chargeData = [
                'amount' => $data['amount'] * 100.00, // stripe amount is in cents
                'customer' => $customer->stripe_id,
                'description' => $data['package_name'],
                'receipt_email' => $data['payment']['card']['name'],
                "currency" => "usd",
                'metadata' => $stripe_metadata
            ];

            // on frontend order, do the charge on the given card
            if (array_key_exists('manually', $data) && !$data['manually']) {
                $card = null;
                $card = User::createOrRetrieveStripeCard($data['payment']['id'], $customer->stripe_id);

                if ($card) {
                    $chargeData['card'] = $card->id;
                }
            }
            try {

                $charge = \Stripe\Charge::create($chargeData);
            } catch (\Stripe\Error\Card $e) {

                Log::error("The payment could not be charged to customer");
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $response['success']    = false;
                $response['error_code'] = $err['code'];
                $response['message']    = $err['message'];

                return $response;
            } catch (\Stripe\Error\InvalidRequest $e) {
                $body = $e->getJsonBody();
                $err  = $body['error'];

                $response['success']    = false;
                $response['error_code'] = null;
                if (isset($err['code'])) {
                    $response['error_code'] = $err['code'];
                }
                $response['message']    = $err['message'];

                return $response;
            }

            if ($charge['status'] === 'succeeded') {
                // Create the entry for the database
                $transaction_data = [
                    'order_id'  => $order_id,
                    'amount'    => $data['amount'],
                    'type'      => self::$paymentType,
                    'stripe_id' => $charge['id']
                ];
                $transaction->fill($transaction_data);
                if ($transaction->save()) {
                    $response['success']    = true;
                    return $response;
                } else {
                    $response['success']    = false;
                    Log::error("Transaction could not be saved on database");
                    return $response;
                }
            }
        } else {
            // the customer was not created on stripe or not saved on local database
            $response['success'] = false;
            Log::error("The customer was not created on stripe or not saved on local database");
            return $response;
        }

        $response['success'] = false;
        return $response;
    }
}
