<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class UserUnavailability extends Authenticatable
{
    use Notifiable;

    protected $table = "tbl_usersunavailability";

    public $timestamps = false;

    public static $rulesOnGetUnavailablity = [
        'UserId'          =>  'required',
        'StartDate'       =>  'required',
        'EndDate'         =>  'required',
    ];
    public static $rulesOnDeleteUnavailablity = [
        'UserUnavailabilityID'  =>  'required',
        'UserId'                =>  'required',
    ];
    public static function GetUnavailablityResponseJson($ret,$error=''){
        $retval = '';
        foreach($ret as $key => $val)
        {
            $retval .= response()->json([
                "UserUnavailabilityID"=> is_object($val)?$val->UserUnavailabilityID:'0' ,
                "UserId"=> is_object($val)?$val->UserId:null,
                "StartDate"=> is_object($val)?$val->StartDate:null,
                "StartTime"=> is_object($val)?$val->StartTime:null,
                "EndDate"=> is_object($val)?$val->EndDate:null,
                "EndTime"=> is_object($val)?$val->EndTime:null,
                "Notes"=> is_object($val)?$val->Notes:null,
                "IsDeletePossible"=> is_object($val)?$val->IsDeletePossible:null,
                "Error"=> $error,
                "JobID"=> is_object($val)?$val->JobId:null,
                "Address"=> is_object($val)?$val->propertyaddress:null,
            ]);
            echo"<pre>";print_r(123131);die;
        }
        return $retval;
    }
    public static function GetUnavailability($POST)
    {
        if(!is_array($POST))
            return false;

		$startdate  =  $POST['StartDate'];
		$enddate    =  $POST['EndDate'];
        $post['UserId']             = $POST['UserId'];
        $post['StartDate']          = date("Y-m-d", strtotime($startdate));
        $post['EndDate']            = date("Y-m-d", strtotime($enddate));

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_HomeJab_API_Get_Unavailablity('.rtrim($params,','  ).')');

        return UserUnavailability::hydrate($ret);
    }
    public static function DeleteUnavailability($POST)
    {
        if(!is_array($POST))
            return false;

        $post['UserUnavailabilityID'] = $POST['UserUnavailabilityID'];
        $post['UserId']               = $POST['UserId'];

        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_HomeJab_API_Delete_Unavailablity('.rtrim($params,','  ).')');
        if(isset($ret[0]->Result))
            return $ret[0]->Result;
        else
            return false;
    }
    public static function AddEditUserUnavailability($POST)
    {
        if(!is_array($POST))
            return false;

        $post['UserUnavailabilityID'] = $POST['UserUnavailabilityID'];
        $post['UserId']               = $POST['UserId'];
        $post['StartDate']            = $POST['StartDate'];
        $post['StartTime']            = $POST['StartTime'];
        $post['EndDate']              = $POST['EndDate'];
        $post['EndTime']              = $POST['EndTime'];
        $post['Notes']                = $POST['Notes'];


        $params = '';
        foreach($post as $key=>$val)
        {
            $params .= "'".addslashes(stripslashes(trim($val)))."'".",";
        }

        $ret = DB::select('call SP_HomeJob_API_AddEdit_Unavailablity('.rtrim($params,','  ).')');
//		echo "<pre>"; print_r($ret); die;
        if(isset($ret[0]->Result))
            return $ret[0]->Result;
        else
            return false;
    }
	public static function updateActiveFieldByJobId($jobid)
	{
		if($jobid  == '')
			return false;

		return self::where('JobId',$jobid)->update(array('IsActive'=>0));
	}
}
?>