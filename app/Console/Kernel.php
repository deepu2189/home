<?php

namespace App\Console;
use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
	    //$schedule->command(DB::select('call SP_HomeJob_API_EndJob'))->dailyAt('10:00');

	    $schedule->command('CronJobsController:ChangeJobStatusToUrgent') ->dailyAt('9:01');
	    $schedule->command('CronJobsController:HomeJabNotification') ->dailyAt('8:00');
	    $schedule->command('GetNewJobetailsController:AssignJobsToTire2Users') ->dailyAt('10:00');
	    $schedule->command('CronJobsController:HomeJabSendWeeklyReport')->sundays('09:00');

        $schedule->command('inspire')->hourly();
        /**
         * Running email queue
         */
        $schedule->command('queue:listen --queue=emails --tries=3')->everyMinute();
        $schedule->command('queue:listen --queue=emails --tries=3')->everyFiveMinutes();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
