<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class MlsSmsTemplate extends Authenticatable
{
	use Notifiable;

	protected $table = "mls_sms_templates";

	public $timestamps = false;

	public $primaryKey = 'mst_id';
	protected $fillable = [
		'mst_message', 'mst_is_visible'
	];
	public static $rulesOnAddSmsTemplate = [
		'mst_message' => 'required',
		//'GroupDetails' => 'required',
	];
	public function InsertTemplate($POST)
	{

		DB::beginTransaction();

		$objmlssmstemp = new MlsSmsTemplate();
		$objmlssmstemp->fill($POST);

		if ($objmlssmstemp->save()) {
			DB::commit();
			return $objmlssmstemp;
		}
		DB::rollback();
		return false;

	}
	public function UpdateTemplate($POST,$smstempid){

		DB::beginTransaction();

		# update record
		$objmlssmstemp = MlsSmsTemplate::find($smstempid);
		$objmlssmstemp->fill($POST);

		if ($objmlssmstemp->save()) {
			DB::commit();
			return $objmlssmstemp;
		}
		DB::rollback();
		return false;
	}

}
?>